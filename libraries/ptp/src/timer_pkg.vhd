LIBRARY ieee;
USE ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

PACKAGE timer_pkg IS


TYPE timer_fields_in is RECORD
    timer_goal_in_ns            : std_logic_vector(31 downto 0);
    timer_on                    : std_logic;
END RECORD;

TYPE timer_fields_out is RECORD
    timer_in_reset              : std_logic;
    timing_pulse                : std_logic;
END RECORD;

TYPE timer_fields_in_arr IS ARRAY (INTEGER RANGE <>) OF timer_fields_in;
TYPE timer_fields_out_arr IS ARRAY (INTEGER RANGE <>) OF timer_fields_out;

end timer_pkg;