----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: May 2023
-- Design Name: 
-- Module Name: sub_second_timer
-- Dependencies: Timeslave 
-- 
-- Revision:
-- 0.01 - File Created
-- Additional Comments:
-- 
--  Take in 32 bit subseconds from PTP vector.
--  Resolution is nanoseconds, counts to 999,999,999 then wraps.
--    
-- PTP time, seconds update
--    
-- 
--    
----------------------------------------------------------------------------------

library IEEE, common_lib, signal_processing_common, Timeslave_CMAC_lib;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE Timeslave_CMAC_lib.timer_pkg.ALL;

entity sub_second_timer is
    Generic (
        DEBUG_ILA           : BOOLEAN := FALSE
    );
    Port ( 
        i_clk                       : in std_logic;
        i_reset                     : in std_logic;

        i_ptp_time                  : in std_logic_vector(79 downto 0);
        i_pps                       : in std_logic;
        
        i_timer_fields_in           : in timer_fields_in;
        o_timer_fields_out          : out timer_fields_out
    
    );
end sub_second_timer;

architecture Behavioral of sub_second_timer is

COMPONENT ila_0
PORT (
    clk : IN STD_LOGIC;
    probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
END COMPONENT;

signal clk              : std_logic;
signal reset            : std_logic;

signal pps_internal     : std_logic;
signal pulse            : std_logic;
signal timer_on         : std_logic;
signal timer_goal       : unsigned(31 downto 0);
signal timer_base       : unsigned(31 downto 0);
signal timer_sec        : unsigned(47 downto 0);
signal timer_inc        : unsigned(31 downto 0);
signal timer_interval   : unsigned(31 downto 0);
signal current_subsec   : unsigned(31 downto 0);
signal current_second   : unsigned(47 downto 0);
signal next_second      : unsigned(47 downto 0);
    
type timer_statemachine is (INIT, IDLE, CALC, TRIM, ACTION, PPS, ZERO_CROSSING, COMPLETE);
signal timer_sm : timer_statemachine;

signal timer_sm_debug   : std_logic_vector(3 downto 0);

signal pps_count        : unsigned(7 downto 0);

begin

------------------------------------------------------------------------------------------------------------------------------------------------------
clk                                 <= i_clk;
reset                               <= i_reset;

timer_on                            <= i_timer_fields_in.timer_on;

o_timer_fields_out.timing_pulse     <= pulse;

------------------------------------------------------------------------------------------------------------------------------------------------------    
timer_proc : process(clk)
begin
    if rising_edge(clk) then
        if (reset = '1') or (timer_on = '0') then
            timer_sm_debug                      <= x"F";
            timer_sm                            <= INIT;
            o_timer_fields_out.timer_in_reset   <= '1';
            pulse                               <= '0';
            timer_goal                          <= ( others => '0' );
            timer_base                          <= ( others => '0' );
            --timer_sec                           <= ( others => '0' );
            timer_inc                           <= ( others => '0' );
            current_subsec                      <= ( others => '0' );
            current_second                      <= ( others => '0' );
            pps_count                           <= x"00";
        else
            o_timer_fields_out.timer_in_reset   <= '0';
            timer_inc                           <= unsigned(i_timer_fields_in.timer_goal_in_ns);
            current_subsec                      <= unsigned(i_ptp_time(31 downto 0));
            current_second                      <= unsigned(i_ptp_time(79 downto 32));
            pps_internal                        <= i_pps;

            case timer_sm is
                when INIT =>
                    timer_sm_debug      <= x"E";
                    pulse               <= '1';
                    timer_sm            <= IDLE;
                    
                when IDLE =>
                    timer_sm_debug      <= x"0";
                    pulse               <= '0';
                    pps_count           <= x"00";
                    -- subtract 10 ns from target time as that is ~ latency in this circuit.
                    --timer_interval      <= timer_inc - 10;
                    if timer_on = '1' then
                        -- capture current time.
                        timer_base      <= unsigned(i_ptp_time(31 downto 0));
                        --timer_sec       <= unsigned(i_ptp_time(79 downto 32));
                        timer_sm        <= CALC;
                    end if;

                when CALC =>
                    timer_sm_debug      <= x"1";
                    timer_goal          <= timer_base + timer_inc;
                    timer_sm            <= TRIM;

                when TRIM =>
                    timer_sm_debug      <= x"2";
                    -- if goal time falls over to next second, trim.
                    if timer_goal > 999999999 then
                        timer_goal      <= timer_goal - 1000000000;
                        timer_sm        <= PPS;
                        -- this will occur next second, add 1 to current second and seek.
                        --next_second <= timer_sec;
                    else
                        timer_sm        <= ACTION;
                    end if;

                when PPS =>
                    timer_sm_debug      <= x"3";
                    if pps_internal = '1' then
                        timer_sm        <= ZERO_CROSSING;
                    end if;

                when ZERO_CROSSING =>
                    timer_sm_debug      <= x"4";
                    -- count pulses after the PPS, for NS read out to catch up.
                    if pps_count = 40 then
                        timer_sm        <= ACTION;
                        pps_count       <= x"00";
                    else
                        pps_count       <= pps_count + 1;
                    end if;

                when ACTION =>
                    timer_sm_debug      <= x"5";
                    if (current_subsec >= timer_goal) then
                        timer_sm        <= COMPLETE;
                        pulse           <= '1';
                    end if;

                when COMPLETE =>
                    timer_sm_debug      <= x"6";
                    pulse               <= '0';
                    timer_sm            <= IDLE;
                    timer_base          <= timer_goal;

                when others =>
                    timer_sm <= IDLE;
            end case;
        end if;
    end if;
end process;

-----------------------------------------------------------------
-- Debug
gen_debug : IF DEBUG_ILA GENERATE
    PTP_scheduled_ila : ila_0
        port map (
            clk                     => clk,

            probe0(31 downto 0)     => std_logic_vector(timer_goal),
            probe0(63 downto 32)    => std_logic_vector(timer_inc),

            probe0(95 downto 64)   => std_logic_vector(current_subsec),
            probe0(143 downto 96)  => std_logic_vector(current_second),

            probe0(175 downto 144)  => std_logic_vector(timer_base),

            probe0(183 downto 176)  => (others => '0'),
            probe0(184)             => timer_on,
            probe0(185)             => o_timer_fields_out.timer_in_reset,
            probe0(189 downto 186)  => timer_sm_debug,
            probe0(190)             => pulse,
            probe0(191)             => pps_internal
        );
END GENERATE;
    

end Behavioral;
