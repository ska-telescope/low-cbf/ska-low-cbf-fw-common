----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 10/16/2024 11:47:15 PM
-- Module Name: eth_disable_tb - Behavioral
-- Description: 
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity eth_disable_tb is
--  Port ( );
end eth_disable_tb;

architecture Behavioral of eth_disable_tb is

    signal i_ap_clk  : std_logic := '0';
    signal i_reset   : std_logic;  
    signal o_reset   : std_logic; -- Goes high following i_reset after the 100G ethernet has been blocked
    signal o_fsm_dbg : std_logic_vector(4 downto 0); -- fsm state 
        -----------------------------------------------------
        -- Everything else is on i_eth_clk
    signal i_eth_clk    : std_logic := '0';
        -----------------------------------------------------
        -- Received data from 100GE
    signal i_tdata : std_logic_vector(511 downto 0); -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
    signal i_tkeep : std_logic_vector(63 downto 0);  -- one bit per byte in i_axi_tdata
    signal i_tlast : std_logic;                      
    signal i_tuser : std_logic_vector(79 downto 0);  -- Timestamp for the packet.
    signal i_tvalid : std_logic;
        -- Data output - 1 clock latency from input
    signal o_tdata : std_logic_vector(511 downto 0); -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
    signal o_tkeep : std_logic_vector(63 downto 0);  -- one bit per byte in i_axi_tdata
    signal o_tlast : std_logic;                      
    signal o_tuser : std_logic_vector(79 downto 0);  -- Timestamp for the packet.
    signal o_tvalid : std_logic;

    signal eth_count : std_logic_vector(31 downto 0) := x"00000000";
    
begin

    i_ap_clk <= not i_ap_clk after 3.333 ns;
    i_eth_clk <= not i_eth_clk after 3.106 ns;
    
    process
    begin
       i_reset <= '0';
       wait for 690ns;
       i_reset <= '1';
       wait for 20us;
       i_reset <= '0';
       wait for 50us;
       i_reset <= '1';
       wait for 40us;
       i_reset <= '0';
       wait for 200us;
    end process;
    
    process(i_eth_clk)
    begin
        if rising_Edge(i_eth_clk) then
            eth_count <= std_logic_vector(unsigned(eth_count) + 1);
        end if;
    end process;
    
    i_tdata <= (others => '0');
    i_tkeep <= (others => '0');
    i_tuser <= (others => '0');
    
    i_tvalid <= eth_count(8) and (not eth_count(13));
    i_tlast <= '1' when eth_count(8 downto 0) = "111111111" else '0';
    
    dut : entity work.eth_disable
    generic map (
        -- Number of i_ap_clk clocks to wait after blocking ethernet traffic before driving o_reset
        -- This allows us to wait a while for e.g. SPS input writes to HBM to complete properly
        g_HOLDOFF => 1024, -- : integer := 1024;
        -- Number of i_eth_clk clocks to wait before unblocking ethernet traffic after de-asserting o_reset
        g_RESTART_HOLDOFF => 2048 -- : integer := 4096
    ) port map (
        -- Reset signal is on i_ap_clk
        i_ap_clk  => i_ap_clk,  --  in std_logic;
        i_reset   => i_reset, --  in std_logic;  
        o_reset   => o_reset, --  out std_logic; -- Goes high following i_reset after the 100G ethernet has been blocked
        o_fsm_dbg => o_fsm_dbg, --  out std_logic_vector(4 downto 0); -- fsm state 
        -----------------------------------------------------
        -- Everything else is on i_eth_clk
        i_eth_clk    => i_eth_clk, --  in std_logic;
        -----------------------------------------------------
        -- Received data from 100GE
        i_axis_tdata => i_tdata, --  in std_logic_vector(511 downto 0); -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
        i_axis_tkeep => i_tkeep, -- in std_logic_vector(63 downto 0);  -- one bit per byte in i_axi_tdata
        i_axis_tlast => i_tlast, -- in std_logic;                      
        i_axis_tuser => i_tuser, -- in std_logic_vector(79 downto 0);  -- Timestamp for the packet.
        i_axis_tvalid => i_tvalid, -- in std_logic;
        -- Data output - 1 clock latency from input
        o_axis_tdata => o_tdata, -- out std_logic_vector(511 downto 0); -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
        o_axis_tkeep => o_tkeep, -- out std_logic_vector(63 downto 0);  -- one bit per byte in i_axi_tdata
        o_axis_tlast => o_tlast, -- out std_logic;                      
        o_axis_tuser => o_tuser, -- out std_logic_vector(79 downto 0);  -- Timestamp for the packet.
        o_axis_tvalid => o_tvalid -- out std_logic
        -----------------------------------------------------
    );
    
    
    
end Behavioral;
