-------------------------------------------------------------------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: August 2023
-- Design Name: 
-- Module Name: hbm_axi_reset_handler
-- 
-- Additional Comments:
--      This has been written to deal with the lack of ability to reset the HBM subsystem in an ALVEO while the kernel is running.
--      
--      The component is to sit in the AXI stream between the HBM interface and general kernel logic in the RTL kernel of an ALVEO.
--      
--      Counts will be kept of WR and RD queues.
--      Initial Implementation - assert reset, disconnect busses from logic as appropriate.
--                             - wr - once addr and data queues equal zero, stop any more.
--                             - rd - stop transfers and allow inflight to complete.
--
--
--      Part B impl
--      When a reset is asserted, the module will disconnect the logic from the AXI interface and assert an "in_reset" signals
--      Balance out any pending transfers.
--      In the case of write, create dummy writes of data or address until the numbers pending are equal.
--      In the case of read, not pass data valid signal until pending retrievals have returned.
--    
--      Once both directions are dealt with, the component will de-assert "in_reset".
--
--
--      This module has been written with variable write sizes which are binary increments,(2,4,8,16 etc) and variable read sizes in mind.
--      Assume len vector is always set to a binary increment.
--
-------------------------------------------------------------------------------------------------------------------------------------------

library IEEE, axi4_lib, technology_lib, common_lib, signal_processing_common;
USE IEEE.STD_LOGIC_1164.ALL;
USE axi4_lib.axi4_stream_pkg.ALL;
USE common_lib.common_pkg.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE axi4_lib.axi4_lite_pkg.ALL;
USE axi4_lib.axi4_full_pkg.ALL;

entity hbm_axi_reset_handler is
    Generic (
        HBM_DATA_WIDTH          : INTEGER := 512;
        HBM_ADDR_BITS           : INTEGER := 40;
        DEBUG_ILA               : BOOLEAN := TRUE
    );
    Port ( 
        i_clk                   : in std_logic;
        i_reset                 : in std_logic;

        i_logic_reset           : in std_logic;
        o_in_reset              : out std_logic;
        o_reset_complete        : out std_logic_vector(7 downto 0);
        o_dbg                   : out std_logic_vector(31 downto 0);
        
        -----------------------------------------------------
        -- To HBM
        -- Data out to the HBM
        -- ADDR
        o_hbm_axi_aw_addr       : out std_logic_vector((HBM_ADDR_BITS-1) downto 0);
        o_hbm_axi_aw_len        : out std_logic_vector(7 downto 0);
        o_hbm_axi_aw_valid      : out std_logic;

        i_hbm_axi_awready       : in std_logic;

        -- DATA
        o_hbm_axi_w_data        : out std_logic_vector((HBM_DATA_WIDTH-1) downto 0);
        o_hbm_axi_w_resp        : out std_logic_vector(1 downto 0);
        o_hbm_axi_w_last        : out std_logic;
        o_hbm_axi_w_valid       : out std_logic;
        i_hbm_axi_wready        : in std_logic;

        i_hbm_axi_b_valid       : in std_logic;
        i_hbm_axi_b_resp        : in std_logic_vector(1 downto 0);
        
        -- reading from HBM
        -- ADDR
        o_hbm_axi_ar_addr       : out std_logic_vector((HBM_ADDR_BITS-1) downto 0);
        o_hbm_axi_ar_len        : out std_logic_vector(7 downto 0);
        o_hbm_axi_ar_valid      : out std_logic;
        i_hbm_axi_arready       : in std_logic;

        -- DATA
        i_hbm_axi_r_data        : in std_logic_vector((HBM_DATA_WIDTH-1) downto 0);
        i_hbm_axi_r_resp        : in std_logic_vector(1 downto 0);
        i_hbm_axi_r_last        : in std_logic;
        i_hbm_axi_r_valid       : in std_logic;
        o_hbm_axi_rready        : out std_logic;

        -----------------------------------------------------
        -- To Logic
        -- ADDR
        i_logic_axi_aw_addr     : in std_logic_vector((HBM_ADDR_BITS-1) downto 0);
        i_logic_axi_aw_len      : in std_logic_vector(7 downto 0);
        i_logic_axi_aw_valid    : in std_logic;

        o_logic_axi_awready     : out std_logic;

        -- DATA
        i_logic_axi_w_data      : in std_logic_vector((HBM_DATA_WIDTH-1) downto 0);
        i_logic_axi_w_resp      : in std_logic_vector(1 downto 0);
        i_logic_axi_w_last      : in std_logic;
        i_logic_axi_w_valid     : in std_logic;
        o_logic_axi_wready      : out std_logic;

        o_logic_axi_b_valid     : out std_logic;
        o_logic_axi_b_resp      : out std_logic_vector(1 downto 0);
        
        -- reading from logic
        -- ADDR
        i_logic_axi_ar_addr     : in std_logic_vector((HBM_ADDR_BITS-1) downto 0);
        i_logic_axi_ar_len      : in std_logic_vector(7 downto 0);
        i_logic_axi_ar_valid    : in std_logic;
        o_logic_axi_arready     : out std_logic;

        -- DATA
        o_logic_axi_r_data      : out std_logic_vector((HBM_DATA_WIDTH-1) downto 0);
        o_logic_axi_r_resp      : out std_logic_vector(1 downto 0);
        o_logic_axi_r_last      : out std_logic;
        o_logic_axi_r_valid     : out std_logic;
        i_logic_axi_rready      : in std_logic
    
    );

    -- prevent optimisation across module boundaries.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of hbm_axi_reset_handler : entity is "yes";

end hbm_axi_reset_handler;

architecture Behavioral of hbm_axi_reset_handler is

COMPONENT ila_0
PORT (
    clk : IN STD_LOGIC;
    probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
END COMPONENT;

signal clk                  : std_logic;
signal reset                : std_logic;
    
signal logic_reset_d        : std_logic;
signal n_logic_reset_d      : std_logic;

signal wr_gate_enable       : std_logic;

-- internal use signals of top ports
signal o_hbm_axi_ar_valid_int   : std_logic;

signal o_hbm_axi_w_valid_int    : std_logic;
signal o_hbm_axi_w_last_int     : std_logic;
signal o_hbm_axi_aw_valid_int   : std_logic;

-- RD data registers
signal rd_r_last            : std_logic;
signal rd_r_valid           : std_logic;
signal rd_r_ready           : std_logic;

signal rd_addr_valid_d      : std_logic;
signal rd_addr_ready        : std_logic;

-- WR data registers
signal wr_d_last            : std_logic;
signal wr_d_valid           : std_logic;
signal wr_d_ready           : std_logic;

signal wr_addr_valid_d      : std_logic;
signal wr_addr_ready        : std_logic;
signal wr_addr_len          : std_logic_vector(7 downto 0);
signal wr_addr_amount       : unsigned(7 downto 0);

-----------------------------
signal hbm_rd_tracker       : unsigned(11 downto 0) := x"000";
signal hbm_wr_tracker       : unsigned(11 downto 0) := x"000";

signal SM_finished          : std_logic;

type hbm_reset_statemachine is (IDLE, CHECK_WR, ALIGN_WR, CATCHUP_WR_D, CATCHUP_WR_A, CHECK_RD, TRANSACT_WR_A, TRANSACT_WR_D, CLEANUP);
signal hbm_reset_sm : hbm_reset_statemachine;

signal hbm_reset_sm_debug   : std_logic_vector(3 downto 0);

signal reset_wr_data_driver : std_logic;
signal reset_wr_last_driver : std_logic;
signal reset_wr_addr_driver : std_logic;

signal hold                 : std_logic_vector(3 downto 0);

signal reset_complete       : std_logic;

signal rd_tracker_time_since_good, wr_tracker_time_since_good : unsigned(15 downto 0) := x"0000";
signal rd_tracker_bad, wr_tracker_bad : std_logic := '0';
signal wr_tracker_changed, rd_tracker_changed : std_logic := '0';

begin

------------------------------------------------------------------
o_reset_complete    <= hbm_reset_sm_debug & "000" & reset_complete;

o_hbm_axi_ar_valid  <= o_hbm_axi_ar_valid_int;

o_hbm_axi_w_valid   <= o_hbm_axi_w_valid_int;
o_hbm_axi_w_last    <= o_hbm_axi_w_last_int;
o_hbm_axi_aw_valid  <= o_hbm_axi_aw_valid_int;

clk                 <= i_clk;
reset               <= i_reset;

reg_proc : process(clk)
begin
    if rising_edge(clk) then
    
    
        if hbm_rd_tracker = 0 or rd_tracker_changed = '1' then
            rd_tracker_time_since_good <= (others => '0');
            rd_tracker_bad <= '0';
        elsif rd_tracker_time_since_good < 8191 then
            rd_tracker_time_since_good <= rd_tracker_time_since_good + 1;
            rd_tracker_bad <= '0';
        else
            rd_tracker_bad <= '1';
        end if;
        
        if hbm_wr_tracker = 0 or wr_tracker_changed = '1' then
            wr_tracker_time_since_good <= (others => '0');
            wr_tracker_bad <= '0';
        elsif wr_tracker_time_since_good < 8191 then
            wr_tracker_time_since_good <= wr_tracker_time_since_good + 1;
            wr_tracker_bad <= '0';
        else
            wr_tracker_bad <= '1';
        end if;

        o_dbg(0) <= rd_tracker_bad;
        o_dbg(1) <= wr_tracker_bad;
        o_dbg(2) <= i_logic_reset;
        o_dbg(3) <= '0';
        o_dbg(15 downto 4) <= std_logic_vector(hbm_rd_tracker);
        o_dbg(27 downto 16) <= std_logic_vector(hbm_wr_tracker);
        o_dbg(31 downto 28) <= hbm_reset_sm_debug;
        
        
        if reset = '1' then
            hbm_reset_sm            <= IDLE;
            hbm_reset_sm_debug      <= x"F";
            reset_wr_data_driver    <= '0';
            reset_wr_addr_driver    <= '0';
            reset_wr_last_driver    <= '0';
            logic_reset_d           <= '0';
            n_logic_reset_d         <= '1';
            reset_complete          <= '0';
        else
            logic_reset_d   <= i_logic_reset;
            n_logic_reset_d <= NOT i_logic_reset;

            case hbm_reset_sm is
                when IDLE =>
                    hbm_reset_sm_debug      <= x"0";
                    reset_complete          <= '0';
                    reset_wr_data_driver    <= '0';
                    reset_wr_addr_driver    <= '0';
                    reset_wr_last_driver    <= '0';

                    SM_finished             <= '0';
                    if i_logic_reset = '1' then
                        hbm_reset_sm    <= CHECK_WR;

                        o_in_reset      <= '1';
                    end if;

                when CHECK_WR =>
                    hbm_reset_sm_debug      <= x"1";
                    if hbm_wr_tracker = x"000" then
                        hbm_reset_sm    <= ALIGN_WR;
                    end if;

                when ALIGN_WR =>
                    hbm_reset_sm_debug      <= x"2";
                    if wr_gate_enable = '0' then
                        hbm_reset_sm    <= CHECK_RD;
                    end if;

                ---------------------------------------------------------

                when CHECK_RD =>
                    hbm_reset_sm_debug      <= x"3";
                    if hbm_rd_tracker = x"000" then
                        hbm_reset_sm    <= CLEANUP;
                    end if;

                when CLEANUP =>
                    hbm_reset_sm_debug      <= x"4";
                    o_in_reset          <= '1';
                    SM_finished         <= '1';
                    reset_complete      <= '1';
                    if i_logic_reset = '0' then
                        hbm_reset_sm    <= IDLE;
                    end if;

                when others => hbm_reset_sm <= IDLE;

            end case;
        end if;
    end if;
end process;

------------------------------------------------------------------
------------------------------------------------------------------
-- HBM WR BUS
-- ADDR
o_hbm_axi_aw_addr       <= i_logic_axi_aw_addr;
o_hbm_axi_aw_len        <= i_logic_axi_aw_len;
o_hbm_axi_aw_valid_int  <= (i_logic_axi_aw_valid AND (wr_gate_enable));

o_logic_axi_awready     <= i_hbm_axi_awready;

-- DATA
o_hbm_axi_w_data        <= i_logic_axi_w_data;
o_hbm_axi_w_resp        <= i_logic_axi_w_resp;
o_hbm_axi_w_last_int    <= i_logic_axi_w_last OR reset_wr_last_driver;
o_hbm_axi_w_valid_int   <= (i_logic_axi_w_valid AND (wr_gate_enable));

o_logic_axi_wready      <= i_hbm_axi_wready;

o_logic_axi_b_valid     <= i_hbm_axi_b_valid;
o_logic_axi_b_resp      <= i_hbm_axi_b_resp;

-- gate enable
wr_gate_enable          <=  '0' when (hbm_wr_tracker = "000" AND logic_reset_d = '1') else
                            '1';


wr_addr_valid_d     <= o_hbm_axi_aw_valid_int;
wr_addr_ready       <= i_hbm_axi_awready;
wr_addr_len         <= i_logic_axi_aw_len;

wr_d_last           <= o_hbm_axi_w_last_int;
wr_d_valid          <= o_hbm_axi_w_valid_int;
wr_d_ready          <= i_hbm_axi_wready;

wr_addr_amount      <= unsigned(wr_addr_len) + 1;
WR_cnt_proc : process(clk)
begin
    if rising_edge(clk) then
        if reset = '1' then
            hbm_wr_tracker      <= x"000";
            wr_tracker_changed <= '0';
        else
            if (wr_d_valid = '1' AND wr_d_ready = '1') AND (wr_addr_valid_d = '1' AND wr_addr_ready = '1') then
                hbm_wr_tracker  <= hbm_wr_tracker - unsigned(wr_addr_len);
                wr_tracker_changed <= '1';
            elsif (wr_d_valid = '1' AND wr_d_ready = '1') then     -- WR DATA last
                hbm_wr_tracker  <= hbm_wr_tracker + 1;
                wr_tracker_changed <= '1';
            elsif (wr_addr_valid_d = '1' AND wr_addr_ready = '1') then   -- WR ADDR
                hbm_wr_tracker  <= hbm_wr_tracker - wr_addr_amount;
                wr_tracker_changed <= '1';
            else
                wr_tracker_changed <= '0';
            end if;

        end if;
    end if;
end process;

-- Registered and delayed                            
-- WR_cnt_proc : process(clk)
-- begin
--     if rising_edge(clk) then
--         wr_addr_valid_d     <= o_hbm_axi_aw_valid_int;
--         wr_addr_ready       <= i_hbm_axi_awready;
--         wr_addr_len         <= i_logic_axi_aw_len;

--         wr_d_last           <= o_hbm_axi_w_last_int;
--         wr_d_valid          <= o_hbm_axi_w_valid_int;
--         wr_d_ready          <= i_hbm_axi_wready;
        
        
--         -- wr_tracker takes care of WR loop completion
--         -- Need to also track how far thru a data wr sequence we are.

--         if reset = '1' OR SM_finished = '1' then
--             hbm_wr_tracker      <= x"000";
--         else
--             if (wr_d_valid = '1' AND wr_d_ready = '1') AND (wr_addr_valid_d = '1' AND wr_addr_ready = '1') then
--                 hbm_wr_tracker  <= hbm_wr_tracker;
--             elsif (wr_d_valid = '1' AND wr_d_ready = '1') then     -- WR DATA last
--                 hbm_wr_tracker  <= hbm_wr_tracker + 1;
--             elsif (wr_addr_valid_d = '1' AND wr_addr_ready = '1') then   -- WR ADDR
--                 hbm_wr_tracker  <= hbm_wr_tracker - unsigned(wr_addr_len);
--             end if;

--         end if;
--     end if;
-- end process;

------------------------------------------------------------------
------------------------------------------------------------------
-- HBM RD BUS
-- reading from HBM
-- ADDR
o_hbm_axi_ar_addr       <= i_logic_axi_ar_addr;
o_hbm_axi_ar_len        <= i_logic_axi_ar_len;
o_hbm_axi_ar_valid_int  <= i_logic_axi_ar_valid AND (n_logic_reset_d);

o_logic_axi_arready     <= i_hbm_axi_arready AND (n_logic_reset_d);

-- DATA
o_logic_axi_r_data      <= i_hbm_axi_r_data;
o_logic_axi_r_resp      <= i_hbm_axi_r_resp;
o_logic_axi_r_last      <= i_hbm_axi_r_last;
o_logic_axi_r_valid     <= i_hbm_axi_r_valid AND (n_logic_reset_d);

o_hbm_axi_rready        <= i_logic_axi_rready;

-- it is not important to record how many words are requested on the addr bus (len signal).
-- just need to keep tally of how many rd requests are made and then subtract how many r_lasts come in.
-- when that is zero, nothing in flight.
RD_cnt_proc : process(clk)
begin
    if rising_edge(clk) then
        rd_addr_valid_d     <= o_hbm_axi_ar_valid_int;
        rd_addr_ready       <= i_hbm_axi_arready;

        rd_r_last           <= i_hbm_axi_r_last;
        rd_r_valid          <= i_hbm_axi_r_valid;
        rd_r_ready          <= i_logic_axi_rready;

        if reset = '1' then
            hbm_rd_tracker  <= x"000";
            rd_tracker_changed <= '0';
        else
            if (rd_addr_valid_d = '1' AND rd_addr_ready = '1') AND (rd_r_valid = '1' AND rd_r_last = '1') then
                hbm_rd_tracker  <= hbm_rd_tracker;
                rd_tracker_changed <= '1';
            elsif (rd_addr_valid_d = '1' AND rd_addr_ready = '1') then
                hbm_rd_tracker  <= hbm_rd_tracker + 1;
                rd_tracker_changed <= '1';
            elsif (rd_r_valid = '1' AND rd_r_last = '1') then
                hbm_rd_tracker  <= hbm_rd_tracker - 1;
                rd_tracker_changed <= '1';
            else
                rd_tracker_changed <= '0';
            end if;
        end if;
    end if;
end process;

gen_debug_ila : IF DEBUG_ILA GENERATE
    hbm_axi_reset_ila : ila_0 PORT MAP (
        clk                     => clk,
        probe0(11 downto 0)     => std_logic_vector(hbm_rd_tracker),
        probe0(23 downto 12)    => std_logic_vector(hbm_wr_tracker),
        probe0(27 downto 24)    => hbm_reset_sm_debug,
        probe0(28)              => reset_complete,

        probe0(29)              => i_logic_reset,
        probe0(30)              => logic_reset_d,
        probe0(31)              => n_logic_reset_d,

        probe0(39 downto 32)    => std_logic_vector(wr_addr_amount),
        probe0(40)              => wr_addr_ready,
        probe0(41)              => wr_addr_valid_d,

        probe0(42)              => wr_d_last,
        probe0(43)              => wr_d_valid,
        probe0(44)              => wr_d_ready,
        
        probe0(45)              => rd_addr_valid_d,
        probe0(46)              => rd_addr_ready,

        probe0(47)              => rd_r_last,
        probe0(48)              => rd_r_valid,
        probe0(49)              => rd_r_ready,

        
        probe0(191 downto 50)  => (others => '0')
        );
        
END GENERATE;

end Behavioral;
