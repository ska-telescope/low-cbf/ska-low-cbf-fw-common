-------------------------------------------------------------------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: October 2024
-- Design Name: 
-- Module Name: eth_disable
-- 
-- Additional Comments:
--
--                               ___________________________________________________
-- i_reset             __________|                                                 |_______________________________________
--                               ----wait tlast---->                               --g_RESTART_HOLDOFF + wait tlast-->
--                                                  ___________________________________________________________________
-- ethernet blocked    _____________________________|                                                                 |____________
-- eth_stop_fsm =     --running-><---wait_to_stop---><-----------stopped-----------><-wait_to_start-><-wait_for_tlast-><--running---
--                                
--                                                  --wait g_HOLDOFF-->_____________
-- o_reset             ________________________________________________|           |______________________________
-- (to hbm_resetter)             
-- (i.e. block HBM transactions) 
-- rst_out_fsm =      --idle----><-wait_eth_stopped-><-wait_g_holdoff-><-set_o_rst-><-idle-------
--
--
-- Notes 
--   * "wait tlast" in the diagram above also includes a timeout that will trigger if nothing has happened on the streaming data bus for 1024 clocks.
--   * When i_reset goes high, the whole state machine has to run through. So o_reset will go high after a delay of (wait tlast) + g_HOLDOFF,
--     even if i_reset has already gone low.
--   * Once o_reset goes high, it will go low immediately after i_reset goes low.  
--
-------------------------------------------------------------------------------------------------------------------------------------------

library IEEE, axi4_lib, technology_lib, common_lib, signal_processing_common;
USE IEEE.STD_LOGIC_1164.ALL;
USE axi4_lib.axi4_stream_pkg.ALL;
USE common_lib.common_pkg.ALL;
USE IEEE.NUMERIC_STD.ALL;
Library xpm;
use xpm.vcomponents.all;

entity eth_disable is
    generic (
        -- Number of i_ap_clk clocks to wait after blocking ethernet traffic before driving o_reset
        -- This allows us to wait a while for e.g. SPS input writes to HBM to complete properly
        g_HOLDOFF : integer := 1024;
        -- Number of i_eth_clk clocks to wait before unblocking ethernet traffic after de-asserting o_reset
        g_RESTART_HOLDOFF : integer := 4096
    );
    port (
        -- Reset signal is on i_ap_clk
        i_ap_clk  : in std_logic;
        i_reset   : in std_logic;  
        o_reset   : out std_logic; -- Goes high following i_reset after the 100G ethernet has been blocked
        o_fsm_dbg : out std_logic_vector(4 downto 0); -- fsm state 
        -----------------------------------------------------
        -- Everything else is on i_eth_clk
        i_eth_clk    : in std_logic;
        -----------------------------------------------------
        -- Received data from 100GE
        i_axis_tdata : in std_logic_vector(511 downto 0); -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
        i_axis_tkeep : in std_logic_vector(63 downto 0);  -- one bit per byte in i_axi_tdata
        i_axis_tlast : in std_logic;                      
        i_axis_tuser : in std_logic_vector(79 downto 0);  -- Timestamp for the packet.
        i_axis_tvalid : in std_logic;
        -- Data output - 1 clock latency from input
        o_axis_tdata : out std_logic_vector(511 downto 0); -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
        o_axis_tkeep : out std_logic_vector(63 downto 0);  -- one bit per byte in i_axi_tdata
        o_axis_tlast : out std_logic;                      
        o_axis_tuser : out std_logic_vector(79 downto 0);  -- Timestamp for the packet.
        o_axis_tvalid : out std_logic
        -----------------------------------------------------
    );
    
    -- prevent optimisation across module boundaries.
    --attribute keep_hierarchy : string;
    --attribute keep_hierarchy of hbm_axi_reset_handler : entity is "yes";
    
end eth_disable;

architecture Behavioral of eth_disable is

    signal stop_timeout : std_logic_vector(11 downto 0);
    type eth_stop_fsm_t is (running, wait_to_stop, stopped, wait_to_start, wait_for_tlast);
    signal eth_stop_fsm : eth_stop_fsm_t := running;
    type rst_out_fsm_t is (idle, wait_eth_stopped, wait_g_holdoff, set_o_reset);
    signal rst_out_fsm : rst_out_fsm_t;
    signal reset_eth_clk : std_logic := '0';
    signal eth_stopped_eth_clk : std_logic := '0';
    signal start_timer, g_holdoff_count : std_logic_vector(31 downto 0);
    signal eth_stopped_ap_clk : std_logic;
    signal dbg_eth_stop_fsm, dbg_eth_stop_fsm_ap_clk : std_logic_vector(2 downto 0);
    signal dbg_rst_out_fsm : std_logic_vector(1 downto 0);
    signal reset_int : std_logic := '0';
    
begin
    
    o_reset <= reset_int;
    
    process(i_ap_clk)
    begin
        if rising_edge(i_ap_clk) then
            
            o_fsm_dbg <= dbg_rst_out_fsm & dbg_eth_stop_fsm_ap_clk;
            
            case rst_out_fsm is
                when idle =>
                    dbg_rst_out_fsm <= "00";
                    if i_reset = '1' then
                        rst_out_fsm <= wait_eth_stopped;
                    end if;
                    reset_int <= '0';
                    
                when wait_eth_stopped =>
                    dbg_rst_out_fsm <= "01";
                    if eth_stopped_ap_clk = '1' then
                        rst_out_fsm <= wait_g_holdoff;
                    end if;
                    g_holdoff_count <= std_logic_vector(to_unsigned(g_HOLDOFF,32));
                    reset_int <= '0';
                    
                when wait_g_holdoff =>
                    dbg_rst_out_fsm <= "10";
                    if (unsigned(g_holdoff_count) = 0) then
                        rst_out_fsm <= set_o_reset;
                    end if;
                    g_holdoff_count <= std_logic_vector(unsigned(g_holdoff_count) - 1);
                    reset_int <= '0';
                    
                when set_o_reset =>
                    dbg_rst_out_fsm <= "11";
                    if i_reset = '0' then
                        rst_out_fsm <= idle;
                    end if;
                    reset_int <= '1';
                    
                when others =>
                    rst_out_fsm <= idle;
                    
            end case;
            
        end if;
    end process;
    
    
    -- Clock crossings from ap_clk to i_eth100G_clk 
    xpm_cdc_single_ap2eth_inst : xpm_cdc_single
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1   -- DECIMAL; 0=do not register input, 1=register input
    ) port map (
        dest_out => reset_eth_clk, -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
        dest_clk => i_eth_clk,     -- 1-bit input: Clock signal for the destination clock domain.
        src_clk => i_ap_clk,       -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in => i_reset          -- 1-bit input: Input signal to be synchronized to dest_clk domain.
    );
    
    -- Clock crossings from i_eth100G_clk to ap_clk 
    xpm_cdc_single_eth2ap_inst : xpm_cdc_single
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1   -- DECIMAL; 0=do not register input, 1=register input
    ) port map (
        dest_out => eth_stopped_ap_clk, -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
        dest_clk => i_ap_clk,           -- 1-bit input: Clock signal for the destination clock domain.
        src_clk => i_eth_clk,           -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in => eth_stopped_eth_clk   -- 1-bit input: Input signal to be synchronized to dest_clk domain.
    );
    
    xpm_cdc_array_single_inst : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
        WIDTH => 3           -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => dbg_eth_stop_fsm_ap_clk, -- WIDTH-bit output: src_in synchronized to the destination clock domain. This output is registered.
        dest_clk => i_ap_clk, -- 1-bit input: Clock signal for the destination clock domain.
        src_clk => i_eth_clk, -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in => dbg_eth_stop_fsm -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain. 
    );
    
    
    -- When HBM reset is high, wait for end of packet, then mask off the 100GE input
    -- Or if there has been no i_axis_tvalid for more than 1024 clocks, timeout and finish
    -- Maximum length of a packet is about 240 clocks (9000 bytes x 8 bit / (100Gb/sec) / 3 ns/clock = 240 clocks)
    process(i_eth_clk)
    begin
        if rising_edge(i_eth_clk) then
            -- i_axis_tvalid, i_axis_tlast
            case eth_stop_fsm is
                when running =>
                    dbg_eth_stop_fsm <= "000";
                    if reset_eth_clk = '1' then
                        eth_stop_fsm <= wait_to_stop;
                    end if;
                    stop_timeout <= (others => '0');
                    eth_stopped_eth_clk <= '0';
                
                when wait_to_stop =>
                    dbg_eth_stop_fsm <= "001";
                    if ((i_axis_tvalid = '1' and i_axis_tlast = '1') or
                        (i_axis_tvalid = '0' and stop_timeout(10) = '1')) then
                        eth_stop_fsm <= stopped;
                    end if;
                    if i_axis_tvalid = '1' then
                        stop_timeout <= (others => '0');
                    else
                        stop_timeout <= std_logic_vector(unsigned(stop_timeout) + 1);
                    end if;
                    eth_stopped_eth_clk <= '0';
                
                when stopped =>
                    dbg_eth_stop_fsm <= "010";
                    if reset_eth_clk = '0' then
                        eth_stop_fsm <= wait_to_start;
                    end if;
                    stop_timeout <= (others => '0');
                    eth_stopped_eth_clk <= '1';
                    start_timer <= std_logic_vector(to_unsigned(g_RESTART_HOLDOFF,32));
                
                when wait_to_start =>
                    dbg_eth_stop_fsm <= "011";
                    if unsigned(start_timer) = 0 then
                        eth_stop_fsm <= wait_for_tlast;
                    end if;
                    start_timer <= std_logic_vector(unsigned(start_timer) - 1);
                    stop_timeout <= (others => '0');
                    eth_stopped_eth_clk <= '1';
                
                when wait_for_tlast =>
                    dbg_eth_stop_fsm <= "100";
                    -- Avoid generating corrupt packets, only start after the link has been idle for a while
                    -- or after a tlast
                    if ((i_axis_tvalid = '1' and i_axis_tlast = '1') or
                        (i_axis_tvalid = '0' and stop_timeout(10) = '1')) then
                        eth_stop_fsm <= running;
                    end if;
                    if i_axis_tvalid = '1' then
                        stop_timeout <= (others => '0');
                    else
                        stop_timeout <= std_logic_vector(unsigned(stop_timeout) + 1);
                    end if;
                    -- 
                    eth_stopped_eth_clk <= '1';
                
                when others =>
                    eth_stop_fsm <= running;
                
            end case;
            
            o_axis_tdata <= i_axis_tdata;
            o_axis_tkeep <= i_axis_tkeep;
            o_axis_tuser <= i_axis_tuser;
            o_axis_tlast <= i_axis_tlast;
            if (eth_stop_fsm = stopped or eth_stop_fsm = wait_to_start or eth_stop_fsm = wait_for_tlast) then 
                o_axis_tvalid <= '0';
            else
                o_axis_tvalid <= i_axis_tvalid;
            end if;
        end if;
    
    end process;
    
    ---------------------------------------------------------------------

end Behavioral;
