------------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 08.03.2019 15:11:37
-- Module Name: LFAADecodeTop - Behavioral
-- Description: 
--  Decode LFAA packets.
-- Assumptions:
--  - No Q-tags for ethernet frames; ethertype field will always be 2 bytes
--  - IPv4 will always be 20 bytes long
--  - 40GE MAC will always put at least 8 bytes of idle time on the data_rx_sosi bus
--
--  
--  The timestamp of the most recent packet for each virtual channel is recorded
--  here with a fractional part using 24 bits, counting from 0 to 15624999, i.e. in units of 64
--  ns, and a 32 bit integer part.
--
-- Structure :
--  - LFAAProcess : Takes in the data from the 40GE interface, validates it and outputs packets 
--                  for downstream processing, with a header that contains the virtual channel.
--  - dummyProcess : Ignores the 100GE interface, and instead outputs packets in accord with the
--                   virtual channel table, with data generated from an LFSR.
--  - muxBlock : Grants either LFAAProcess or dummyProcess access to the virtual channel table 
--               and virtual channel statistics memories, which reside in the registers.
--  - registers : register interface for control and statistics registers, and also for the 
--                virtual channel table and 
--  - ptpBlock : transfer ptp time to the data_clk domain.
------------------------------------------------------------------------------------

library IEEE, axi4_lib, xpm, LFAADecode100G_lib, dsp_top_lib, technology_lib, signal_processing_common;
--use ctc_lib.ctc_pkg.all;
use DSP_top_lib.DSP_top_pkg.all;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use axi4_lib.axi4_stream_pkg.ALL;
use axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.ALL;
use xpm.vcomponents.all;
use LFAADecode100G_lib.LFAADecode100G_lfaadecode100G_reg_pkg.ALL;
USE technology_lib.tech_mac_100g_pkg.ALL;

entity LFAADecodeTop100G is
    port(
        -- Data in from the 100GE MAC
        i_axis_tdata   : in std_logic_vector(511 downto 0); -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
        i_axis_tkeep   : in std_logic_vector(63 downto 0);  -- one bit per byte in i_axi_tdata
        i_axis_tlast   : in std_logic;                      
        i_axis_tuser   : in std_logic_vector(79 downto 0);  -- Timestamp for the packet.
        i_axis_tvalid  : in std_logic;
        i_100GE_clk    : in std_logic;     -- 322 MHz from the 100GE MAC; note 512 bits x 322 MHz = 165 Mbit/sec, so even full rate traffic will have .valid low 1/3rd of the time.
        i_100GE_rst    : in std_logic;
        -- Data out to corner turn module.
        -- This is just the header for each packet. The data part goes direct to the HBM via the wdata part of the AXI-full bus 
        --  
        o_virtualChannel : out std_logic_vector(15 downto 0); -- Single number which incorporates both the channel and station.
        o_packetCount    : out std_logic_vector(39 downto 0);
        -- Output from the registers that are used elsewhere (on i_s_axi_clk)
        -- When the virtual channel table is switched over, these values will change when o_valid goes high
        -- for the first packet delivered using the new table.
        o_totalChannels : out std_logic_vector(11 downto 0);  -- total channels for the table currently being used in this module
        o_totalChannelsTable0  : out std_logic_vector(11 downto 0); -- total channels for vct table 0
        o_totalChannelsTable1  : out std_logic_vector(11 downto 0); -- total channels for vct table 1
        o_totalStations  : out std_logic_vector(11 downto 0);
        o_totalCoarse    : out std_logic_vector(11 downto 0);
        o_tableSelect    : out std_logic;
        o_valid          : out std_logic;
        ---------------------------------------------------------------------        
        -- wdata portion of the AXI-full external interface (should go directly to the external memory)
        o_axi_w         : out t_axi4_full_data; -- w data bus : out t_axi4_full_data; (.valid, .data(511:0), .last, .resp(1:0)) => o_m01_axi_w,    -- w data bus (.wvalid, .wdata, .wlast)
        i_axi_wready    : in std_logic;
        -- Registers AXI Lite Interface
        i_s_axi_mosi     : in t_axi4_lite_mosi;
        o_s_axi_miso     : out t_axi4_lite_miso;
        i_s_axi_clk      : in std_logic;
        i_s_axi_rst      : in std_logic;
        -- registers AXI Full interface
        i_vcstats_MM_IN  : in  t_axi4_full_mosi;
        o_vcstats_MM_OUT : out t_axi4_full_miso;

        -- control signals in to select which virtual channel table to use
        i_vct_table_select : in std_logic;
        ---------------------------------------------------------------------
        
        -- hbm reset   
        o_hbm_reset        : out std_logic;
        i_hbm_status       : in std_logic_vector(7 downto 0);
        
        o_reset_to_ct      : out std_logic;

        -- debug ports
        o_dbg            : out std_logic_vector(13 downto 0)
    );
    
    -- prevent optimisation across module boundaries.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of LFAADecodeTop100G : entity is "yes";   
   
end LFAADecodeTop100G;

architecture Behavioral of LFAADecodeTop100G is

    signal reg_rw       : t_statctrl_rw;
    signal reg_ro       : t_statctrl_ro;

    -- One only of the "LFAAProcess" and the "TestProcess" modules controls the memories in the registers.
    signal LFAAVCTable_addr : std_logic_vector(11 downto 0);
    
    signal VCStats_ram_in_wr_dat : std_logic_vector(31 downto 0);
    signal VCStats_ram_in_wr_en : std_logic; 
    signal VCStats_ram_in_adr : std_logic_vector(12 downto 0);
    signal VCStats_ram_out_rd_dat : std_logic_vector(31 downto 0);
    
    signal cdcSendCount : std_logic_vector(3 downto 0) := "0000";
    signal cdcSend : std_logic := '0';
    signal cdcSrcIn, cdcDestOut : std_logic_vector(35 downto 0);
    signal cdcDestReq, cdcRcv : std_logic;
    signal AXI_totalChannels : std_logic_vector(11 downto 0);
    signal AXI_totalStations : std_logic_vector(11 downto 0);
    signal AXI_totalCoarse   : std_logic_vector(11 downto 0);
    
    signal hbm_reset_cmac   : std_logic;
    signal hbm_status_cmac  : std_logic;

    signal LFAA_axi_wrapper_bram_rst        : std_logic;
    signal LFAA_axi_wrapper_bram_clk        : std_logic;
    signal LFAA_axi_wrapper_bram_en         : std_logic;
    signal LFAA_axi_wrapper_bram_we_byte    : std_logic_vector(3 DOWNTO 0);
    signal LFAA_axi_wrapper_bram_addr       : std_logic_vector(15 DOWNTO 0);
    signal LFAA_axi_address                 : std_logic_vector(13 DOWNTO 0);
    signal bram_addr_d1                     : std_logic_vector(13 DOWNTO 0);
    signal bram_addr_d2                     : std_logic_vector(13 DOWNTO 0);
    signal LFAA_axi_wrapper_bram_wrdata     : std_logic_vector(31 DOWNTO 0);
    signal LFAA_axi_wrapper_bram_rddata     : std_logic_vector(31 DOWNTO 0);
    signal vc_stats_ram_return              : std_logic_vector(31 downto 0);
    signal vc_table_ram_return              : std_logic_vector(31 downto 0);
    signal vc_stats_ram_wren                : std_logic;
    signal vc_table_ram_wren                : std_logic;
    signal vc_table_data                    : std_logic_vector(31 downto 0);
    signal vc_table_address                 : std_logic_vector(11 downto 0);
    
begin
    
    
    -------------------------------------------------------------------------------------------------
    -- Process packets from the 100GE LFAA input 
    
    LFAAProcessInst : entity LFAADecode100G_lib.LFAAProcess100G
    port map(
        -- Data in from the 100GE MAC
        i_axis_tdata        => i_axis_tdata,
        i_axis_tkeep        => i_axis_tkeep,
        i_axis_tlast        => i_axis_tlast,
        i_axis_tuser        => i_axis_tuser,
        i_axis_tvalid       => i_axis_tvalid,
        i_data_clk          => i_100GE_clk,     -- in std_logic;     -- 312.5 MHz for 40GE MAC
        i_data_rst          => i_100GE_rst,     -- in std_logic;
        -- Data out to the memory interface; This is the wdata portion of the AXI full bus.
        i_ap_clk            => i_s_axi_clk,  -- in  std_logic;
        o_axi_w             => o_axi_w,      -- out t_axi4_full_data
        i_axi_wready        => i_axi_wready, -- in std_logic;
        -- Only the header data goes to the corner turn.
        o_virtualChannel    => o_virtualChannel, -- out std_logic_vector(15 downto 0); -- Single number which incorporates both the channel and station.
        o_packetCount       => o_packetCount,    -- out std_logic_vector(31 downto 0);
        o_valid             => o_valid,          -- out std_logic;
        o_total_channels    => o_totalChannels,  -- out (11:0)
        o_total_coarse      => o_totalCoarse,    -- out (11:0)
        o_total_stations    => o_totalStations,  -- out (11:0)
        o_table_select      => o_tableSelect,    -- out std_logic; -- Which vct table did this use.
        -- Interface to the registers
        i_lfaa_decode_reset => reg_rw.lfaa_decode_reset, -- in std_logic;
        i_HBM_reset         => reg_rw.HBM_reset,         -- in std_logic;
        i_table_select      => i_vct_table_select,       -- in std_logic;
        i_total_channels_table0 => reg_rw.total_channels_table0, --  in std_logic_vector(15 downto 0);
        i_total_channels_table1 => reg_rw.total_channels_table1, --  in (15:0)
        i_total_coarse_table0 => reg_rw.total_coarse_table0, --  in std_logic_vector(15 downto 0);
        i_total_coarse_table1 => reg_rw.total_coarse_table1, --  in (15:0)
        i_total_Stations_table0 => reg_rw.total_stations_table0, -- in (15:0)
        i_total_stations_table1 => reg_rw.total_stations_table1, -- in (15:0)
        o_reg_ro            => reg_ro,
        -- Virtual channel table memory in the registers
        o_searchAddr        => vc_table_address,       -- out(11:0); -- read address to the VCTable_ram in the registers.
        i_VCTable_rd_data   => vc_table_data, -- in(31:0);  -- read data from VCTable_ram in the registers; assumed valid 2 clocks after searchAddr.
        -- Virtual channel stats in the registers.
        o_statsWrData       => VCStats_ram_in_wr_dat,  -- out(31:0);
        o_statsWE           => VCStats_ram_in_wr_en,   -- out std_logic;
        o_statsAddr         => VCStats_ram_in_adr,     -- out(12:0);
        i_statsRdData       => VCStats_ram_out_rd_dat, -- in(31:0)

        -- hbm reset   
        o_hbm_reset         => o_hbm_reset,
        i_hbm_status        => i_hbm_status,

        -- debug ports
        o_dbg               => o_dbg   -- out(13:0)
    );
    
    o_totalChannelsTable0 <= reg_rw.total_channels_table0(11 downto 0);
    o_totalChannelsTable1 <= reg_rw.total_channels_table1(11 downto 0);
    ---------------------------------------------------------------------------------------------------
    -- Register interface
    regif : entity work.LFAADecode100G_lfaadecode100G_reg
    --   GENERIC (g_technology : t_technology := c_tech_select_default);
    port map (
        MM_CLK                  => i_s_axi_clk, --  IN    STD_LOGIC;
        MM_RST                  => i_s_axi_rst, --  IN    STD_LOGIC;

        SLA_IN                  => i_s_axi_mosi, --  IN    t_axi4_lite_mosi;
        SLA_OUT                 => o_s_axi_miso, --  OUT   t_axi4_lite_miso;
        statctrl_fields_rw      => reg_rw,       --  out t_statctrl_rw;
        STATCTRL_FIELDS_RO      => reg_ro
    );
    
    lfaa_full_axi_args: entity LFAADecode100G_lib.LFAA_decode_axi_bram_wrapper
    port map ( 
        i_clk                    => i_s_axi_clk,
        i_rst                    => i_s_axi_rst,

        bram_rst                 => LFAA_axi_wrapper_bram_rst,
        bram_clk                 => LFAA_axi_wrapper_bram_clk,
        bram_en                  => LFAA_axi_wrapper_bram_en,
        bram_we_byte             => LFAA_axi_wrapper_bram_we_byte,
        bram_addr                => LFAA_axi_wrapper_bram_addr,
        bram_wrdata              => LFAA_axi_wrapper_bram_wrdata,
        bram_rddata              => LFAA_axi_wrapper_bram_rddata,

        i_LFAA_full_axi_mosi     => i_vcstats_MM_IN,
        o_LFAA_full_axi_miso     => o_vcstats_MM_out

    );

    LFAA_axi_address            <= LFAA_axi_wrapper_bram_addr(15 downto 2);

    vc_stats_ram_wren           <= '1' when (LFAA_axi_address(13) = '0')               AND LFAA_axi_wrapper_bram_we_byte(0) = '1' AND LFAA_axi_wrapper_bram_en = '1' else
                                   '0';
    vc_table_ram_wren           <= '1' when (LFAA_axi_address(13 downto 12) = "10")    AND LFAA_axi_wrapper_bram_we_byte(0) = '1' AND LFAA_axi_wrapper_bram_en = '1' else
                                   '0';

    bram_return_data_proc : process(i_s_axi_clk)
    begin
        if rising_edge(i_s_axi_clk) then
            bram_addr_d1    <= LFAA_axi_address;
            bram_addr_d2    <= bram_addr_d1;
        end if;
    end process;

    LFAA_axi_wrapper_bram_rddata    <=  vc_stats_ram_return     when bram_addr_d2(13) = '0'             else
                                        vc_table_ram_return     when bram_addr_d2(13 downto 12) = "10"  else
                                        x"DEADBEEF";

    vc_stats_ram : entity signal_processing_common.memory_tdp_wrapper 
    generic map (
        MEMORY_INIT_FILE    => "none",
        g_READ_LATENCY_B    => 1,
        g_NO_OF_ADDR_BITS   => 13,
        g_D_Q_WIDTH         => 32 )
    port map ( 
        clk_a           => i_s_axi_clk,
        clk_b           => i_100GE_clk,
    
        data_a          => LFAA_axi_wrapper_bram_wrdata,
        addr_a          => LFAA_axi_address(12 downto 0),
        data_a_wr       => vc_stats_ram_wren,
        data_a_q        => vc_stats_ram_return,
        
        data_b          => VCStats_ram_in_wr_dat,
        addr_b          => VCStats_ram_in_adr,
        data_b_wr       => VCStats_ram_in_wr_en,
        data_b_q        => VCStats_ram_out_rd_dat
    );

    vc_table_ram : entity signal_processing_common.memory_tdp_wrapper 
    generic map (
        MEMORY_INIT_FILE    => "vc_table_tb.mem",
        g_READ_LATENCY_B    => 1,
        g_NO_OF_ADDR_BITS   => 12,
        g_D_Q_WIDTH         => 32 )
    port map ( 
        clk_a           => i_s_axi_clk,
        clk_b           => i_100GE_clk,
    
        data_a          => LFAA_axi_wrapper_bram_wrdata,
        addr_a          => LFAA_axi_address(11 downto 0),
        data_a_wr       => vc_table_ram_wren,
        data_a_q        => vc_table_ram_return,
        
        data_b          => (others => '0'),
        addr_b          => vc_table_address,
        data_b_wr       => '0',
        data_b_q        => vc_table_data
    );
    
    o_reset_to_ct       <= reg_rw.lfaa_decode_reset;

end Behavioral;
