----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: Nov 2020
-- Design Name: tb_packetisertop
-- 
-- Target Devices: +US
-- 
-- test bench written to be used in Vivado
-- project and waveview view also provided, tb_packetiser.xpr for Vivado 2020.1
--
library IEEE,technology_lib, PSR_Packetiser_lib, signal_processing_common, axi4_lib, ethernet_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ethernet_lib.ethernet_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;
use axi4_lib.axi4_stream_pkg.ALL;
use axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.ALL;

USE technology_lib.tech_mac_100g_pkg.ALL;

entity tb_packetisertop is
--  Port ( );
end tb_packetisertop;

architecture Behavioral of tb_packetisertop is


signal clock_300 : std_logic := '0';    -- 3.33ns
signal clock_400 : std_logic := '0';    -- 2.50ns
signal clock_322 : std_logic := '0';

signal testCount            : integer   := 0;
signal testCount_300        : integer   := 0;
signal testCount_322        : integer   := 0;

signal clock_400_rst        : std_logic := '1';
signal clock_300_rst        : std_logic := '1';
signal clock_322_rst        : std_logic := '1';

signal i_clk400_rst         : std_logic := '1';

signal power_up_rst_clock_400   : std_logic_vector(31 downto 0) := c_ones_dword;
signal power_up_rst_clock_300   : std_logic_vector(31 downto 0) := c_ones_dword;
signal power_up_rst_clock_322   : std_logic_vector(31 downto 0) := c_ones_dword;

signal i_clk400                 : std_logic;

signal test_count               : integer := 0;

signal bytes_to_transmit        : STD_LOGIC_VECTOR(13 downto 0);     -- 
signal data_to_player           : STD_LOGIC_VECTOR(511 downto 0);
signal data_to_player_wr        : STD_LOGIC;
signal data_to_player_rdy       : STD_LOGIC;

signal o_data_to_transmit      : t_lbus_sosi;
signal i_data_to_transmit_ctl  : t_lbus_siso;

-- AXI to CMAC interface to be implemented
signal o_tx_axis_tdata          : STD_LOGIC_VECTOR(511 downto 0);
signal o_tx_axis_tkeep          : STD_LOGIC_VECTOR(63 downto 0);
signal o_tx_axis_tvalid         : STD_LOGIC;
signal o_tx_axis_tlast          : STD_LOGIC;
signal o_tx_axis_tuser          : STD_LOGIC;
signal i_tx_axis_tready         : STD_LOGIC;

signal packetiser_ctrl          : packetiser_stream_ctrl;
signal packetiser_config        : packetiser_config_in;

signal beamformer_to_packetiser_data    :  packetiser_stream_in; 
signal beamformer_to_packetiser_data_2  :  packetiser_stream_in;
signal beamformer_to_packetiser_data_3  :  packetiser_stream_in;
 
signal packet_stream_stats              :  t_packetiser_stats(2 downto 0);

signal packetiser_stream_1_host_bus_in  : packetiser_config_in;
signal packetiser_stream_2_host_bus_in  : packetiser_config_in;
signal packetiser_stream_3_host_bus_in  : packetiser_config_in;
  
signal packetiser_host_bus_out          : packetiser_config_out;  
signal packetiser_host_bus_out_2        : packetiser_config_out;
signal packetiser_host_bus_out_3        : packetiser_config_out;  

signal packetiser_host_bus_ctrl         :  packetiser_stream_ctrl;
signal packetiser_host_bus_ctrl_2       :  packetiser_stream_ctrl;
signal packetiser_host_bus_ctrl_3       :  packetiser_stream_ctrl;

signal test_cycles_between      : std_logic_vector(31 downto 0);
signal test_byte                : std_logic_vector(7 downto 0);
signal test_dword_lower         : std_logic_vector(31 downto 0) := x"00000002";
signal test_dword_upper         : std_logic_vector(31 downto 0) := x"00000001";
signal test_word_loop           : std_logic_vector(15 downto 0);
signal test_runs                : std_logic_vector(31 downto 0);
signal test_beam_number         : std_logic_vector(3 downto 0);

signal test_scale               : std_logic_vector(63 downto 0);

signal test_data                : std_logic_vector(63 downto 0);
signal test_data_valid          : std_logic;

constant proper_packet          : integer := 785;
constant fake_packet            : integer := 780;
signal packet_length_test       : integer := 0;

signal current_dest_IP          : std_logic_vector(7 downto 0);

signal PST_virtual_channel      : std_logic_vector(10 downto 0);
signal PST_beam                 : std_logic_vector(7 downto 0);
signal PST_time_ref             : std_logic_vector(39 downto 0);

signal time_between             : std_logic_vector(31 downto 0) := x"00000080"; 
signal sim_pst_data             : std_logic;

type test_mode_statemachine is (IDLE, RUN);
signal test_mode_sm : test_mode_statemachine;

begin

clock_300 <= not clock_300 after 3.33 ns;
clock_322 <= not clock_322 after 3.11 ns;

clock_400 <= not clock_400 after 2.50 ns;


i_clk400        <= clock_400;

packetiser_config.config_data_clk <= clock_300;

-------------------------------------------------------------------------------------------------------------
-- powerup resets for SIM
test_runner_proc_clk300: process(clock_300)
begin
    if rising_edge(clock_300) then
        -- power up reset logic
        if power_up_rst_clock_300(31) = '1' then
            power_up_rst_clock_300(31 downto 0) <= power_up_rst_clock_300(30 downto 0) & '0';
            clock_300_rst   <= '1';
            testCount_300   <= 0;
        else
            clock_300_rst   <= '0';
            testCount_300   <= testCount_300 + 1;
            

        end if;

    end if;
end process;

test_runner_proc_clk322: process(clock_322)
begin
    if rising_edge(clock_322) then
        -- power up reset logic
        if power_up_rst_clock_322(31) = '1' then
            power_up_rst_clock_322(31 downto 0) <= power_up_rst_clock_322(30 downto 0) & '0';
            clock_322_rst   <= '1';
            testCount_322   <= 0;
        else
            clock_322_rst   <= '0';
            testCount_322   <= testCount_322 + 1;
            

        end if;

    end if;
end process;

test_runner_proc_clk400: process(clock_400)
begin
    if rising_edge(clock_400) then
        -- power up reset logic
        if power_up_rst_clock_400(31) = '1' then
            power_up_rst_clock_400(31 downto 0) <= power_up_rst_clock_400(30 downto 0) & '0';
            testCount           <= 0;
            clock_400_rst       <= '1';
        else
            testCount       <= testCount + 1;
            clock_400_rst   <= '0';
         
        end if;
    end if;
end process;

-------------------------------------------------------------------------------------------------------------

dut_stim_proc : process(clock_400)
begin
    if rising_edge(clock_400) then
        if clock_400_rst = '1' then
            sim_pst_data    <= '0';
            
        else
            if testCount = 1500 then
            
                sim_pst_data    <= '1';
            end if;
            
        end if;
    end if;
end process;

args_stim_proc : process(clock_300)
begin
    if rising_edge(clock_300) then
        -- 0 - enable packetiser
        -- 1 - enable test gen
        -- 2 - limited runs
        -- 3 - use defaults
        --  0x03 = Run Test gen
        --  0x09 = Run packetiser with defaults, assume BF sourced data.
        if (clock_300_rst = '1') then
            packetiser_host_bus_ctrl.instruct    <= x"00";
            packetiser_host_bus_ctrl_2.instruct  <= x"00";
            packetiser_host_bus_ctrl_3.instruct  <= x"00";
        else 
            if (testCount_300 = 1000) then
                packetiser_host_bus_ctrl.instruct    <= x"01";
                packetiser_host_bus_ctrl_2.instruct  <= x"01";
                packetiser_host_bus_ctrl_3.instruct  <= x"01";
            end if;
            if (testCount_300 = 20000) then
                packetiser_host_bus_ctrl.instruct    <= x"01";
                packetiser_host_bus_ctrl_2.instruct  <= x"01";
                packetiser_host_bus_ctrl_3.instruct  <= x"01";
            end if;
            if (testCount_300 = 200000) then
                packetiser_host_bus_ctrl.instruct    <= x"01";
                packetiser_host_bus_ctrl_2.instruct  <= x"00";
                packetiser_host_bus_ctrl_3.instruct  <= x"00";
            end if;
        end if;
    end if;
end process;
 
cmac_emulator : process(clock_322)
begin
    if rising_edge(clock_322) then
        if (testCount_322 > 15) then
            if testCount_322 = 200 or testCount_322 = 250 then
                i_data_to_transmit_ctl.ready    <= '0';
                i_tx_axis_tready                <= '0';
            else    
                i_data_to_transmit_ctl.ready    <= '1';
                i_tx_axis_tready                <= '1';
            end if;
        else
            i_data_to_transmit_ctl.ready        <= '0';   
            i_data_to_transmit_ctl.overflow     <= '0';
            i_data_to_transmit_ctl.underflow    <= '0';
            i_tx_axis_tready                    <= '0';     
        end if;
    end if;
end process;

packetiser_stream_1_host_bus_in.config_data_clk <= i_clk400;
packetiser_stream_2_host_bus_in.config_data_clk <= i_clk400;
packetiser_stream_3_host_bus_in.config_data_clk <= i_clk400;

--- DUT/UUT
DUT_1 : entity PSR_Packetiser_lib.psr_packetiser100G_Top 
Generic Map (
    g_DEBUG_ILA                 => TRUE,
    g_Number_of_streams         => 3,
    g_PST_config                => TRUE
)
Port Map ( 
    -- ~322 MHz
    i_cmac_clk                  => clock_322,
    i_cmac_rst                  => clock_322_rst,
    
    i_packetiser_clk            => i_clk400,
    i_packetiser_rst            => clock_400_rst,
    
    i_beam_output_stream_1      => null_t_bf_output_stream,
    i_beam_output_stream_2      => null_t_bf_output_stream,
    
    -- Lbus to MAC
    o_data_to_transmit          => o_data_to_transmit,
    i_data_to_transmit_ctl      => i_data_to_transmit_ctl,
    
    -- AXI to CMAC interface to be implemented
    o_tx_axis_tdata             => o_tx_axis_tdata,
    o_tx_axis_tkeep             => o_tx_axis_tkeep,
    o_tx_axis_tvalid            => o_tx_axis_tvalid,
    o_tx_axis_tlast             => o_tx_axis_tlast,
    o_tx_axis_tuser             => o_tx_axis_tuser,
    i_tx_axis_tready            => i_tx_axis_tready,
    
    -- signals from signal processing/HBM/the moon/etc
    i_packet_stream_ctrl(0)     => packetiser_host_bus_ctrl,
    i_packet_stream_ctrl(1)     => packetiser_host_bus_ctrl_2,
    i_packet_stream_ctrl(2)     => packetiser_host_bus_ctrl_3,
    
    o_packet_stream_stats       => packet_stream_stats,
            
    i_packet_stream(0)          => beamformer_to_packetiser_data,
    i_packet_stream(1)          => beamformer_to_packetiser_data_2,
    i_packet_stream(2)          => beamformer_to_packetiser_data_3,
    o_packet_stream_out         => open,
    
    -- AXI BRAM to packetiser
    i_packet_config_in_stream_1 => packetiser_stream_1_host_bus_in,
    i_packet_config_in_stream_2 => packetiser_stream_2_host_bus_in,
    i_packet_config_in_stream_3 => packetiser_stream_3_host_bus_in,
    
    -- AXI BRAM return path from packetiser 
    o_packet_config_stream_1    => packetiser_host_bus_out.config_data_out,
    o_packet_config_stream_2    => packetiser_host_bus_out_2.config_data_out,
    o_packet_config_stream_3    => packetiser_host_bus_out_3.config_data_out
    
);

--------------------------------------------------------------------------------------------------
beamformer_to_packetiser_data.data_clk                  <= i_clk400;
beamformer_to_packetiser_data.data_in_wr                <= test_data_valid; -- AND (NOT beamformer_to_packetiser_data.PST_beam(0));
beamformer_to_packetiser_data.data(63 downto 0)         <= test_data when test_data_valid = '1' else zero_qword;
beamformer_to_packetiser_data.data(511 downto 64)       <= zero_256 & zero_128 & zero_qword; 
beamformer_to_packetiser_data.bytes_to_transmit         <= "00" & x"000";

beamformer_to_packetiser_data.PSR_beam_freq_index       <= PST_virtual_channel;
beamformer_to_packetiser_data.PSR_beam                  <= '0' & PST_beam;
beamformer_to_packetiser_data.PSR_time_ref              <= x"00" & PST_time_ref;
beamformer_to_packetiser_data.PSR_jones                 <= "1110";
beamformer_to_packetiser_data.PSR_delay_poly            <= "01";

beamformer_to_packetiser_data_2.data_clk                <= i_clk400;
beamformer_to_packetiser_data_2.data_in_wr              <= test_data_valid;-- AND (beamformer_to_packetiser_data.PST_beam(0));
beamformer_to_packetiser_data_2.data(63 downto 0)       <= test_data;
beamformer_to_packetiser_data_2.data(511 downto 64)     <= zero_256 & zero_128 & zero_qword; 
beamformer_to_packetiser_data_2.bytes_to_transmit       <= "00" & x"000";    

beamformer_to_packetiser_data_2.PSR_beam_freq_index     <= PST_virtual_channel;
beamformer_to_packetiser_data_2.PSR_beam                <= '0' & PST_beam;
beamformer_to_packetiser_data_2.PSR_time_ref            <= x"00" & PST_time_ref;
beamformer_to_packetiser_data_2.PSR_jones               <= "1110";
beamformer_to_packetiser_data_2.PSR_delay_poly          <= "01";

------------------------------------------------------------------------------------------
-- simluate output from PST beamformer.
test_proc : process(i_clk400)
begin
    if rising_edge(i_clk400) then
        if sim_pst_data = '0' then
            test_count          <= 0;
            test_cycles_between <= zero_dword;
            test_data           <= zero_qword;
            test_data_valid     <= '0';
            test_byte           <= one_byte;
            test_dword_lower    <= x"00000002";
            test_dword_upper    <= x"00000001";
            test_word_loop      <= x"0001";
            test_runs           <= X"00000000";
            test_beam_number    <= x"7";        -- this will emulate 16 beams
            test_scale          <= x"FFFFFFFF0005C47E";
            PST_virtual_channel <= "000" & x"00";
        else
           
            case test_mode_sm is
                when IDLE =>
                    if test_cycles_between = time_between then
                        test_count          <= 0;
                        test_mode_sm        <= RUN;
                        test_dword_lower    <= x"00000002";
                        test_dword_upper    <= x"00000001";
                        
                        if test_beam_number(1 downto 0) = "11" then
                            --packet_length_test <= fake_packet;
                        else
                            packet_length_test <= proper_packet;
                        end if;

                        packet_length_test <= proper_packet;
                        
                        test_scale(31 downto 0) <= test_beam_number & test_scale(27 downto 0);
                    else
                        test_cycles_between <= std_logic_vector(unsigned(test_cycles_between) + 1);
                    end if;
                
                
                when RUN =>
                    test_cycles_between     <= zero_dword;
                    
                    test_count              <= test_count + 1;

                    -- test pattern generate
                    if test_data_valid = '1' then
                        test_byte           <= std_logic_vector(unsigned(test_byte) + 1);
                        test_dword_lower    <= std_logic_vector(unsigned(test_dword_lower) + 2);
                        test_dword_upper    <= std_logic_vector(unsigned(test_dword_upper) + 2);
                        
                        test_data           <= test_beam_number & test_word_loop(11 downto 0) & test_dword_upper(15 downto 0) & test_beam_number & test_word_loop(11 downto 0) & test_dword_lower(15 downto 0);
                    else
                        test_data           <= test_scale; -- test leading meta data.
                    end if;
                    
                    -- 6192 bytes = 774 writes
                    -- With a leading write for scale data so 775.
                    if (test_count = 10) then
                        test_data_valid         <= '1';
                        --PST_virtual_channel     <= "000" & x"00"; 
                        PST_beam                <= x"0" & test_beam_number;
                        PST_time_ref            <= x"0000000000";
                    elsif (test_count = packet_length_test) then --785) then
                        test_data_valid         <= '0';
                        PST_virtual_channel     <= std_logic_vector(unsigned(PST_virtual_channel) + 1); 
                        PST_beam                <= x"00";
                        PST_time_ref            <= x"0000000000";
                    end if;
                    
                    if (test_count = 789) then
                        if unsigned(PST_virtual_channel) = 20 then
                            PST_virtual_channel <= (others => '0');
                            if test_beam_number = x"7" then
                                test_beam_number    <= zero_nibble;
                            else
                                test_beam_number    <= std_logic_vector(unsigned(test_beam_number) + 1);
                            end if;
                        end if;
                    end if;
                    
                    if (test_count = 790) then
                        test_mode_sm        <= IDLE;
                        test_count          <= 0;
                        test_runs           <= std_logic_vector(unsigned(test_runs) + 1);
                        
                        if test_beam_number = x"0" then
                            test_word_loop  <= std_logic_vector(unsigned(test_word_loop) + 1);
                        end if;
                    end if;
                
                when OTHERS =>
                    test_mode_sm <= IDLE;
            
            end case;
        end if;
    end if;
end process;

end Behavioral;
