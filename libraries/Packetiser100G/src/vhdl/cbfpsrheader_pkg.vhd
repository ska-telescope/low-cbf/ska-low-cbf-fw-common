LIBRARY ieee;
USE ieee.std_logic_1164.all;

PACKAGE CbfPsrHeader_pkg IS

-- PST packet type
-- LOW PST = 6334 bytes.
-- subtract 4 bytes which are CRC done automatically by 100G core.
-- 6330 - 14 Ethernet - 20 IPv4 - 8 UDP - 96 UDP header = 6192
-- 6192 bytes on 64 bit data interface = 774 writes
-- 48 bytes(6 writes) are realtive weight, rest is data.

-- PSS packet type
-- LOW PSS = 3706
-- 14 Ethernet + 20 IPv4 + 8 UDP    = 42 bytes
-- PSR Header                       = 96 bytes
-- Relative weights(54 x 2 bytes)   = 108 bytes
-- Pad Weights to modulo 16 with 0  = 4 bytes
-- Sample Data (4b * 16 time samples * 54 fine channels = 3456 bytes.

constant c_pst_packet_size      : integer   := 6330;
constant c_pst_packet_ipv4      : integer   := c_pst_packet_size - 14;    -- subtract 14
constant c_pst_packet_udp       : integer   := c_pst_packet_ipv4 - 20;    -- subtract 20

constant c_pss_packet_size      : integer   := 3706;
constant c_pss_packet_ipv4      : integer   := c_pss_packet_size - 14;    -- subtract 14
constant c_pss_packet_udp       : integer   := c_pss_packet_ipv4 - 20;    -- subtract 20

TYPE t_CbfPsrHeader_scale_arr IS ARRAY(0 TO 4-1) OF STD_LOGIC_VECTOR(32-1 DOWNTO 0);
TYPE t_CbfPsrHeader_offset_arr IS ARRAY(0 TO 4-1) OF STD_LOGIC_VECTOR(32-1 DOWNTO 0);

type psr_packetiser_constants is record
    channels_per_packet             : STD_LOGIC_VECTOR(16-1 DOWNTO 0);
    valid_channels_per_packet       : STD_LOGIC_VECTOR(16-1 DOWNTO 0);
    number_of_time_samples          : STD_LOGIC_VECTOR(16-1 DOWNTO 0);
    oversampling_ratio_numerator    : STD_LOGIC_VECTOR(8-1 DOWNTO 0);
    oversampling_ratio_denominator  : STD_LOGIC_VECTOR(8-1 DOWNTO 0);
end record;

constant PST_metadata_constants : psr_packetiser_constants := (
                                    channels_per_packet             => x"0018",     --24
                                    valid_channels_per_packet       => x"0018",     --24
                                    number_of_time_samples          => x"0020",     --32
                                    oversampling_ratio_numerator    => x"04",
                                    oversampling_ratio_denominator  => x"03"
                                    );

constant PSS_metadata_constants : psr_packetiser_constants := (
                                    channels_per_packet             => 16D"54",
                                    valid_channels_per_packet       => 16D"54",
                                    number_of_time_samples          => 16D"16",
                                    oversampling_ratio_numerator    => 8D"1",
                                    oversampling_ratio_denominator  => 8D"1"
                                    );

constant null_scale : t_CbfPsrHeader_scale_arr := ((0) => (others=>'0'),
                                                   (1) => (others=>'0'),
                                                   (2) => (others=>'0'),
                                                   (3) => (others=>'0')
                                                   );
                                                   
constant null_offset : t_CbfPsrHeader_offset_arr :=     ((0) => (others=>'0'),
                                                         (1) => (others=>'0'),
                                                         (2) => (others=>'0'),
                                                         (3) => (others=>'0')
                                                         );  
--------------------------------------------------------------------------------------------------
-- Version 2

TYPE CbfPsrHeader_v2 IS RECORD
-- Word 0    
    packet_sequence_number      : STD_LOGIC_VECTOR(63 DOWNTO 0);
-- Word 1
    samples_since_epoch         : STD_LOGIC_VECTOR(63 DOWNTO 0);
-- Word 2
    period_numerator            : STD_LOGIC_VECTOR(15 DOWNTO 0);
    period_denominator          : STD_LOGIC_VECTOR(15 DOWNTO 0);
    channel_separation          : STD_LOGIC_VECTOR(31 DOWNTO 0);
-- Word 3
    first_channel_frequency     : STD_LOGIC_VECTOR(63 DOWNTO 0);
-- Word 4 & 5
    scale : t_CbfPsrHeader_scale_arr;
-- Word 6
    first_channel_number        : STD_LOGIC_VECTOR(31 DOWNTO 0);
    channels_per_packet         : STD_LOGIC_VECTOR(15 DOWNTO 0);
    valid_channels_per_packet   : STD_LOGIC_VECTOR(15 DOWNTO 0);
-- Word 7
    number_of_time_samples      : STD_LOGIC_VECTOR(15 DOWNTO 0);
    beam_number                 : STD_LOGIC_VECTOR(15 DOWNTO 0);
    magic_word                  : STD_LOGIC_VECTOR(31 DOWNTO 0);       --0xBEADFEED
-- Word 8    
    packet_destination                  : STD_LOGIC_VECTOR(7 DOWNTO 0);
    data_precision                      : STD_LOGIC_VECTOR(7 DOWNTO 0);
    number_of_power_samples_averaged    : STD_LOGIC_VECTOR(7 DOWNTO 0);
    number_of_time_samples_weight       : STD_LOGIC_VECTOR(7 DOWNTO 0);
    validity                            : STD_LOGIC_VECTOR(7 DOWNTO 0);
    reserved                            : STD_LOGIC_VECTOR(7 DOWNTO 0);
    beamformer_version                  : STD_LOGIC_VECTOR(15 DOWNTO 0);
-- Word 9    
    scan_id                     : STD_LOGIC_VECTOR(63 DOWNTO 0);
-- Word 10 & 11
    offset                      : t_CbfPsrHeader_offset_arr;
END RECORD;

constant default_pst_header_v2 : CbfPsrHeader_v2 := (
                                packet_sequence_number              => (others=>'0'),   -- set by logic
                                samples_since_epoch                 => (others=>'0'),   -- set by logic
                                period_numerator                    => 16D"5184",
                                period_denominator                  => 16D"25",
                                channel_separation                  => (others=>'0'),   -- set by host
                                first_channel_frequency             => (others=>'0'),   -- set by host
                                scale                               => null_scale,
                                first_channel_number                => (others=>'0'),   -- set by host
                                channels_per_packet                 => PST_metadata_constants.channels_per_packet,
                                valid_channels_per_packet           => PST_metadata_constants.valid_channels_per_packet,
                                number_of_time_samples              => PST_metadata_constants.number_of_time_samples,
                                beam_number                         => (others=>'0'),   -- set by logic
                                magic_word                          => x"BEADFEED",
                                packet_destination                  => 8D"2",
                                data_precision                      => 8D"16",
                                number_of_power_samples_averaged    => (others=>'0'),   -- Not used by LOW
                                number_of_time_samples_weight       => 8D"32",
                                validity                            => (others=>'0'),   -- set by logic
                                reserved                            => (others=>'0'),   -- set ot 0
                                beamformer_version                  => (others=>'0'),   -- set by logic
                                scan_id                             => (others=>'0'),   -- set by host
                                offset                              => null_offset
                                );

constant default_pss_header : CbfPsrHeader_v2 := (
                                packet_sequence_number              => (others=>'0'),   -- set by logic
                                samples_since_epoch                 => (others=>'0'),   -- set by logic
                                period_numerator                    => 16D"1728",
                                period_denominator                  => 16D"25",
                                channel_separation                  => (others=>'0'),   -- set by host
                                first_channel_frequency             => (others=>'0'),   -- set by host
                                scale                               => null_scale,
                                first_channel_number                => (others=>'0'),   -- set by host
                                channels_per_packet                 => PSS_metadata_constants.channels_per_packet,
                                valid_channels_per_packet           => PSS_metadata_constants.valid_channels_per_packet,
                                number_of_time_samples              => PSS_metadata_constants.number_of_time_samples,
                                beam_number                         => (others=>'0'),   -- set by logic
                                magic_word                          => x"BEADFEED",
                                packet_destination                  => 8D"0",
                                data_precision                      => 8D"8",
                                number_of_power_samples_averaged    => (others=>'0'),   -- Not used by LOW
                                number_of_time_samples_weight       => 8D"16",
                                validity                            => (others=>'0'),   -- set by logic
                                reserved                            => (others=>'0'),   -- set ot 0
                                beamformer_version                  => (others=>'0'),   -- set by logic
                                scan_id                             => (others=>'0'),   -- set by host
                                offset                              => null_offset
                                );                                
--------------------------------------------------------------------------------------------------
-- Version 1

TYPE CbfPsrHeader IS RECORD
    packet_sequence_number : STD_LOGIC_VECTOR(64-1 DOWNTO 0);
    timestamp_attoseconds : STD_LOGIC_VECTOR(64-1 DOWNTO 0);
    timestamp_seconds : STD_LOGIC_VECTOR(32-1 DOWNTO 0);
    channel_separation : STD_LOGIC_VECTOR(32-1 DOWNTO 0);
    first_channel_frequency : STD_LOGIC_VECTOR(64-1 DOWNTO 0);
    scale : t_CbfPsrHeader_scale_arr;
    first_channel_number : STD_LOGIC_VECTOR(32-1 DOWNTO 0);
    channels_per_packet : STD_LOGIC_VECTOR(16-1 DOWNTO 0);
    valid_channels_per_packet : STD_LOGIC_VECTOR(16-1 DOWNTO 0);
    number_of_time_samples : STD_LOGIC_VECTOR(16-1 DOWNTO 0);
    beam_number : STD_LOGIC_VECTOR(16-1 DOWNTO 0);
    magic_word : STD_LOGIC_VECTOR(32-1 DOWNTO 0);
    packet_destination : STD_LOGIC_VECTOR(8-1 DOWNTO 0);
    data_precision : STD_LOGIC_VECTOR(8-1 DOWNTO 0);
    number_of_power_samples_averaged : STD_LOGIC_VECTOR(8-1 DOWNTO 0);
    number_of_time_samples_weight : STD_LOGIC_VECTOR(8-1 DOWNTO 0);
    oversampling_ratio_numerator : STD_LOGIC_VECTOR(8-1 DOWNTO 0);
    oversampling_ratio_denominator : STD_LOGIC_VECTOR(8-1 DOWNTO 0);
    beamformer_version : STD_LOGIC_VECTOR(16-1 DOWNTO 0);
    scan_id : STD_LOGIC_VECTOR(64-1 DOWNTO 0);
    offset : t_CbfPsrHeader_offset_arr;
END RECORD;
                                               

constant null_CbfPsrHeader : CbfPsrHeader := (
                                packet_sequence_number              => (others=>'0'),
                                timestamp_attoseconds               => (others=>'0'),
                                timestamp_seconds                   => (others=>'0'),
                                channel_separation                  => (others=>'0'),
                                first_channel_frequency             => (others=>'0'),
                                scale                               => null_scale,
                                first_channel_number                => (others=>'0'),
                                channels_per_packet                 => (others=>'0'),
                                valid_channels_per_packet           => (others=>'0'),
                                number_of_time_samples              => (others=>'0'),
                                beam_number                         => (others=>'0'),
                                magic_word                          => (others=>'0'),
                                packet_destination                  => (others=>'0'),
                                data_precision                      => (others=>'0'),
                                number_of_power_samples_averaged    => (others=>'0'),
                                number_of_time_samples_weight       => (others=>'0'),
                                oversampling_ratio_numerator        => (others=>'0'),
                                oversampling_ratio_denominator      => (others=>'0'),
                                beamformer_version                  => (others=>'0'),
                                scan_id                             => (others=>'0'),
                                offset                              => null_offset
                                );

--------------------------------------------------------------------------------------------------
-- default values for system powerup no software config

constant default_scale : t_CbfPsrHeader_scale_arr := (  (0) => x"BADDCAFE",
                                                        (1) => x"BBAACAFF",
                                                        (2) => x"BADDCCFE",
                                                        (3) => x"BADCAFFE"
                                                      );
                                                   
constant default_offset : t_CbfPsrHeader_offset_arr :=  ((0) =>  x"11111111",
                                                         (1) =>  x"22222222",
                                                         (2) =>  x"33333333",
                                                         (3) =>  x"44444444"
                                                         );         
                                                         
constant default_PSTHeader : CbfPsrHeader := (
                                packet_sequence_number              => x"0000000000000000",
                                timestamp_attoseconds               => x"AD000000000000AD",
                                timestamp_seconds                   => x"BCBCBCBC",
                                channel_separation                  => x"50505050",
                                first_channel_frequency             => x"31415926535900FF",
                                scale                               => default_scale,
                                first_channel_number                => x"31415926",
                                channels_per_packet                 => PST_metadata_constants.channels_per_packet,
                                valid_channels_per_packet           => PST_metadata_constants.valid_channels_per_packet,
                                number_of_time_samples              => PST_metadata_constants.number_of_time_samples,
                                beam_number                         => x"0000",
                                magic_word                          => x"BEADFEED",
                                packet_destination                  => x"FF",
                                data_precision                      => x"CC",
                                number_of_power_samples_averaged    => x"34",
                                number_of_time_samples_weight       => x"56",
                                oversampling_ratio_numerator        => PST_metadata_constants.oversampling_ratio_numerator,
                                oversampling_ratio_denominator      => PST_metadata_constants.oversampling_ratio_denominator,
                                beamformer_version                  => x"0666",
                                scan_id                             => x"1D1D1D1D1D1D1D1D",
                                offset                              => default_offset
                                );

--------------------------------------------------------------------------------------------------
-- BF output bus record
type bf_output_stream is record
    beamData        : std_logic_vector(63 downto 0);
    beamPacketCount : std_logic_vector(47 downto 0);    -- count of the number of PST output packets since the SKA epoch. Each packet is 32 time samples = 6.635520 ms of data.
    beamBeam        : std_logic_vector(9 downto 0);
    beamFreqIndex   : std_logic_vector(10 downto 0);
    beamValid       : std_logic;
                                                        -- Validty field in PSR packet is 8 bits
                                                        -- 0 - beamPoly_ok(0)
                                                        -- 1 - beamPoly_ok(1)
                                                        -- 2 - beamJonesStatus(0)
                                                        -- 3 - beamJonesStatus(1)
                                                        -- 4 - beamJonesStatus(2) (PSS ONLY)
                                                        -- 5 - beamJonesStatus(3) (PSS ONLY)
                                                        -- 6 - reserved
                                                        -- 7 - reserved
    beamJonesStatus : std_logic_vector(3 downto 0);     -- Jones bits 1:0 = beam jones status (PSS) || beam specific station jones (PST)
                                                        -- Jones bits 3:2 = station jones status (PSS ONLY)
                                                        -- For both types of Jones status,  bit 0 = Used default Jones matrices
                                                        --                                  bit 1 = used valid jones matrices
    beamPoly_ok     : std_logic_vector(1 downto 0);     -- bit 0 = station polynomials were valid
                                                        -- bit 1 = beam polynomials were valid
    weights_data    : std_logic_vector(15 downto 0);    -- for PSS only, 54 cycles and comes as a burst.
    weights_data_wr : std_logic;
    weights_sop     : std_logic;
end record;

TYPE t_bf_output_stream         IS ARRAY (3 downto 0) OF bf_output_stream;

TYPE t_matrix_bf_output_stream  IS ARRAY (INTEGER RANGE <>) OF t_bf_output_stream;

-- Constant
constant null_bf_output_stream : bf_output_stream := ( 
                                                            beamData        => (others=>'0'),
                                                            beamPacketCount => (others=>'0'),
                                                            beamBeam        => (others=>'0'),
                                                            beamFreqIndex   => (others=>'0'),
                                                            beamValid       => ('0'),
                                                            beamJonesStatus => (others=>'0'),
                                                            beamPoly_ok     => (others=>'0'),
                                                            weights_data    => (others=>'0'),
                                                            weights_data_wr => ('0'),
                                                            weights_sop     => ('0')
                                                        );

constant null_t_bf_output_stream : t_bf_output_stream := (
                                                            null_bf_output_stream,
                                                            null_bf_output_stream,
                                                            null_bf_output_stream,
                                                            null_bf_output_stream
                                                        );
--------------------------------------------------------------------------------------------------
-- PSR packetiser records
type packetiser_stream_in is record
    data_clk                : std_logic;
    data_in_wr              : std_logic;
    data                    : std_logic_vector(511 downto 0);
    bytes_to_transmit       : std_logic_vector(13 downto 0);
    
    -- PST signals are passed with data to make headers on the fly. Zero out for other packet types.
    PSR_beam_freq_index     : std_logic_vector(10 downto 0);
    PSR_beam                : std_logic_vector(8 downto 0);
    PSR_time_ref            : std_logic_vector(47 downto 0);
    PSR_jones               : std_logic_vector(3 downto 0);
    PSR_delay_poly          : std_logic_vector(1 downto 0);
end record;

type packetiser_stream_out is record
    data_in_rdy             : std_logic;    -- Ready to receive data, FIFO feedback.
    
    in_rst                  : std_logic;    -- CMAC/Packetiser logic in reset
end record;

type packetiser_config_in is record
    config_data_clk         : std_logic;
    config_data             : std_logic_vector(31 downto 0);
    config_data_addr        : std_logic_vector(14 downto 0);    -- If connected to ARGs, that is byte addressing, this is 32 bit addressing.
    config_data_en          : std_logic;
    config_data_wr          : std_logic;
end record;

type packetiser_config_out is record
    config_data_valid       : std_logic;
    config_data_out         : std_logic_vector(31 downto 0);
end record;

type packetiser_stream_ctrl is record
    instruct                : std_logic_vector(7 downto 0);
end record;

type packetiser_stats is record
    valid_packets           : std_logic;
    invalid_packets         : std_logic;
    disregarded_packets     : std_logic;
    packets_sent_to_cmac    : std_logic;
    reset_counters          : std_logic;
end record;

TYPE t_packetiser_stream_in     IS ARRAY (INTEGER RANGE <>) OF packetiser_stream_in;
TYPE t_packetiser_stream_out    IS ARRAY (INTEGER RANGE <>) OF packetiser_stream_out;

TYPE t_packetiser_config_in     IS ARRAY (INTEGER RANGE <>) OF packetiser_config_in;  
TYPE t_packetiser_config_out    IS ARRAY (INTEGER RANGE <>) OF packetiser_config_out;  

TYPE t_packetiser_stream_ctrl   IS ARRAY (INTEGER RANGE <>) OF packetiser_stream_ctrl;



TYPE t_packetiser_stats         IS ARRAY (INTEGER RANGE <>) OF packetiser_stats;

---- Constants
constant null_packetiser_stream_in : packetiser_stream_in := ( 
                                                            data_clk            => '0',
                                                            data_in_wr          => '0',
                                                            data                => (others=>'0'),
                                                            bytes_to_transmit   => (others=>'0'),
                                                            PSR_beam_freq_index => (others=>'0'),
                                                            PSR_beam            => (others=>'0'),
                                                            PSR_time_ref        => (others=>'0'),
                                                            PSR_jones           => (others=>'0'),
                                                            PSR_delay_poly      => (others=>'0')
                                                            );

constant null_packetiser_stream_out : packetiser_stream_out := (
                                                            data_in_rdy => '0',
                                                            in_rst      => '0'
                                                            );
                                                            

constant null_packetiser_config_in : packetiser_config_in := (
                                                            config_data_clk     => '0',
                                                            config_data         => (others=>'0'),
                                                            config_data_addr    => (others=>'0'),
                                                            config_data_en      => '0',
                                                            config_data_wr      => '0'
                                                            );

constant null_packetiser_config_out : packetiser_config_out := (
                                                            config_data_valid   => '0',
                                                            config_data_out     => (others=>'0')
                                                            );

constant null_packetiser_stream_ctrl : packetiser_stream_ctrl := ( 
                                                            instruct            => (others=>'0')
                                                            );

end CbfPsrHeader_pkg;