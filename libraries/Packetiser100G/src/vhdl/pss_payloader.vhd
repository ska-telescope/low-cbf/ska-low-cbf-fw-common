----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: July 2024
-- Additional Comments:
-- Take in 4 output streams from the PSS Beamformer.
-- Combine the weights data and create a stream for the packet former.
--
-- Also pre-align the data before writing into FIFO, assuming single clock domain but this can be scaled to two easily.
--
--
-- It wil cache the data and play it out when the arbiter selects this interface.
-- there are 3 streams in a PST playout.
----------------------------------------------------------------------------------

library IEEE, xpm, PSR_Packetiser_lib, common_lib,signal_processing_common, ethernet_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use ethernet_lib.ethernet_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;
use xpm.vcomponents.all;
USE common_lib.common_pkg.ALL;
library technology_lib;
USE technology_lib.tech_mac_100g_pkg.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity pss_payloader is
    Generic (
        FIFO_CACHE_DEPTH                : integer := 1024;
        g_SIM                           : boolean := FALSE
    );
    Port ( 
        i_clk400                        : in std_logic;
        i_reset_400                     : in std_logic;
    
        o_invalid_packet                : out std_logic;
        
        i_wr_to_cmac                    : in std_logic;
        
        o_stats                         : out packetiser_stats;
        
        o_fifo_data_used                : out std_logic_vector(ceil_log2(FIFO_CACHE_DEPTH) downto 0);
    
        i_packetiser_data_in            : in t_bf_output_stream;
        
        o_packetiser_data_to_former     : out packetiser_stream_in;
        i_packetiser_data_to_former     : in packetiser_stream_out
        
    );
end pss_payloader;

architecture rtl of pss_payloader is

signal reset_int            : std_logic;

-- signal shift_in             : std_logic_vector(511 downto 0) := zero_512;
-- signal shift_remain         : std_logic_vector(79 downto 0);    -- 80 bits, based on where PST header finishes in LBUS.
-- signal shift_in_d1          : std_logic_vector(511 downto 0) := zero_512;
-- signal shift_cache          : std_logic_vector(63 downto 0);

signal shift_in             : t_slv_512_arr(3 downto 0);
signal shift_remain         : t_slv_80_arr(3 downto 0);
signal shift_in_d1          : t_slv_512_arr(3 downto 0);
signal shift_cache          : t_slv_64_arr(3 downto 0);

signal data_fifo_data           : t_slv_512_arr(3 downto 0);
signal data_fifo_wr             : std_logic;
signal data_fifo_wr_stream      : std_logic_vector(3 downto 0);
signal data_fifo_rd             : std_logic_vector(3 downto 0);
signal data_fifo_q              : t_slv_512_arr(3 downto 0);
signal data_fifo_empty          : std_logic_vector(3 downto 0);
signal data_fifo_rd_count       : t_slv_11_arr(3 downto 0);

constant BUFF_FIFO_WIDTH        : integer := 74;
signal meta_fifo_data           : t_slv_74_arr(3 downto 0);
signal meta_fifo_wr             : std_logic;
signal meta_fifo_wr_stream      : std_logic_vector(3 downto 0);
signal meta_fifo_rd             : std_logic_vector(3 downto 0);
signal meta_fifo_q              : t_slv_74_arr(3 downto 0);
signal meta_fifo_empty          : std_logic_vector(3 downto 0);

signal fifo_enable              : std_logic;
signal fifo_reset               : std_logic;

signal data_valid_int               : std_logic;
signal data_valid_int_d1            : std_logic_vector(15 downto 0);

signal PSR_virtual_channel_cache    : std_logic_vector(10 downto 0);
signal PSR_beam_cache               : std_logic_vector(8 downto 0);
signal PSR_time_ref_cache           : std_logic_vector(47 downto 0);
signal PSR_jones_cache              : std_logic_vector(3 downto 0);
signal PSR_delay_poly_cache         : std_logic_vector(1 downto 0);

signal reset_enable                 : std_logic_vector(3 downto 0);
signal trigger_dump                 : std_logic;

signal signal_data_wr_count         : integer range 0 to 1023 := 0;
signal signal_data_wr_count_cache   : integer range 0 to 1023 := 0;

signal assemble_64b                 : std_logic_vector(2 downto 0);

type inc_data_statemachine is (IDLE, DATA, FLUSH_FIFO, FINISH, HAND_OFF, STREAM_SEL);
signal inc_data_sm : inc_data_statemachine;
signal inc_data_sm_d : inc_data_statemachine;


type packet_statemachine is (IDLE, WEIGHT_A, WEIGHT_B, FINISH, DATA_INIT, DATA);
signal pack_data_sm : packet_statemachine;

signal process_data_count           : integer range 0 to 1023 := 0;

signal valid_packet_counter                     : std_logic := '0';
signal invalid_packet_counter                   : std_logic := '0';
signal disregarded_packets_master_enable        : std_logic := '0';
signal packets_to_ethernet_serialiser_counter   : std_logic := '0';

signal wr_to_cmac_d                 : std_logic;

signal path_to_cmac_running         : std_logic;
signal path_to_cmac_disabled        : std_logic;

signal stream_enable                : std_logic;

signal weights_value                : std_logic_vector(15 downto 0);
signal weights_value_le             : std_logic_vector(15 downto 0);

signal pss_streams_rec              : t_bf_output_stream;

signal weights_vector_a             : std_logic_vector(431 downto 0);
signal weights_vector_b             : std_logic_vector(431 downto 0);

signal weights_a                    : std_logic_vector(431 downto 0);
signal weights_b                    : std_logic_vector(431 downto 0);

signal weights_inputs               : t_slv_16_arr(53 downto 0);

signal weights_wr_pos               : integer range 0 to 53;

signal weights_update               : std_logic_vector(3 downto 0);
signal init_weights_vector          : std_logic := '1';

signal input_delay_pipe             : t_slv_64_matrix(3 downto 0,8 downto 0);

signal inc_64b_count                : unsigned(11 downto 0);

signal bf_stream_sel                : integer range 0 to 3;

signal streams_in_use               : std_logic_vector(3 downto 0);

signal weights_data                 : t_slv_16_arr(3 downto 0);    -- for PSS only, 54 cycles and comes as a burst.
signal weights_data_wr              : std_logic_vector(3 downto 0);
signal weights_sop                  : std_logic_vector(3 downto 0);

begin

----------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------
-- Reset from i_reset_400 is poweron and GT lock
reset_int   <= i_reset_400 OR path_to_cmac_disabled;

-- Reset both FIFOs
fifo_reset  <= i_reset_400 OR path_to_cmac_disabled;

reset_proc : process(i_clk400)
begin
    if rising_edge(i_clk400) then
        if pss_streams_rec(0).beamvalid = '0' AND (data_valid_int_d1 = x"0000") then
            fifo_enable <= (NOT path_to_cmac_disabled);
        end if;
    end if;
end process;

----------------------------------------------------------------------------------------------
-- register incoming bus.
inc_reg_proc : process(i_clk400)
begin
    if rising_edge(i_clk400) then
    
        pss_streams_rec <= i_packetiser_data_in;   
    
    
    end if;
end process;


----------------------------------------------------------------------------------------------
-- weights.
-- construct weights to pack
-- update the values based on input from BF.
-- initial value hardcoded until BF updated.

sim_weights_gen : IF g_SIM GENERATE
    weights_value       <= x"8888";   -- should be input data vector from BF code,
END GENERATE;

weights_live : IF (NOT g_SIM) GENERATE
    weights_value       <= pss_streams_rec(0).weights_data; --x"8000";   -- should be input data vector from BF code,
END GENERATE;

weights_value_le    <= weights_value(7 downto 0) & weights_value(15 downto 8);

weights_proc : process(i_clk400)
begin
    if rising_edge(i_clk400) then
        if reset_int = '1' then
            init_weights_vector <= '1';
            weights_wr_pos <= 0;
        else
            if weights_sop(0) = '1' then
                weights_wr_pos <= 0;
            elsif weights_data_wr(1) = '1' then 
                weights_wr_pos <= weights_wr_pos + 1;
            end if;


            if weights_data_wr(1) = '1' then
                weights_inputs(weights_wr_pos)  <= weights_data(1);
            end if;
        
            weights_update <= weights_update(2 downto 0) & weights_data_wr(3);
            
            -- weights generated at least 5us before data gets to the module, and can still be using old weights.
            -- so only update to new weights when beam 0 is starting.
            --if weights_update = "1000" then
            if (data_valid_int = '0' AND pss_streams_rec(0).beamvalid = '1') AND (pss_streams_rec(0).beamBeam(8 downto 0) = "000000000") then
                weights_vector_a <= weights_inputs(0) & weights_inputs(1) & weights_inputs(2) & weights_inputs(3) & weights_inputs(4) & weights_inputs(5) & weights_inputs(6) &
                                    weights_inputs(7) & weights_inputs(8) & weights_inputs(9) & weights_inputs(10) & weights_inputs(11) & weights_inputs(12) & weights_inputs(13) &
                                    weights_inputs(14) & weights_inputs(15) & weights_inputs(16) & weights_inputs(17) & weights_inputs(18) & weights_inputs(19) & weights_inputs(20) &
                                    weights_inputs(21) & weights_inputs(22) & weights_inputs(23) & weights_inputs(24) & weights_inputs(25) & weights_inputs(26);

                weights_vector_b <= weights_inputs(27) & weights_inputs(28) & weights_inputs(29) & weights_inputs(30) & weights_inputs(31) & weights_inputs(32) & weights_inputs(33) & weights_inputs(34) &
                                    weights_inputs(35) & weights_inputs(36) & weights_inputs(37) & weights_inputs(38) & weights_inputs(39) & weights_inputs(40) & weights_inputs(41) &
                                    weights_inputs(42) & weights_inputs(43) & weights_inputs(44) & weights_inputs(45) & weights_inputs(46) & weights_inputs(47) & weights_inputs(48) &
                                    weights_inputs(49) & weights_inputs(50) & weights_inputs(51) & weights_inputs(52) & weights_inputs(53);        
            end if;
        end if;

        weights_data        <= weights_data(2 downto 0) & weights_value_le;
        weights_data_wr     <= weights_data_wr(2 downto 0) & pss_streams_rec(0).weights_data_wr;
        weights_sop         <= weights_sop(2 downto 0) & pss_streams_rec(0).weights_sop;

        -- need to expand this to 64 byte wide for feeding into the CMAC.
        -- packet_former is expecting to merge Header_3 with this block, specifically starting with weights.
        -- vector is 64 bytes to the 100G and Header_3 is consuming 10 bytes before weights start.
        -- in those 10 bytes of padding, the scale will be encoded.
        -- 10 bytes of padding (scale 4B) | 54 bytes of weights
        -- 54 bytes of weights | 10 bytes Vis data
        -- 64 bytes of Vis data etc.
        
        -- create two weights vector as above, 54 writes will shuffle the weights in.
        weights_a       <= weights_vector_a;
        weights_b       <= weights_vector_b;
    end if;
end process;
----------------------------------------------------------------------------------------------
-- GENERATE four pipes
gen_inc_pipes : for i in 0 to 3 GENERATE

    inc_processing : process(i_clk400)
    begin
        if rising_edge(i_clk400) then
            -- packet will arrive in the 512 bit vector already shifted for the packet former.
            -- it will join on a 64 byte line where 6 bytes are already used by PSR header.
            -- 58 bytes remain
            -- data needs to be cached so packets can be constructed.

            -- added for SIM, can be taken out if timing a problem.
            if pss_streams_rec(0).beamvalid = '1' then
                input_delay_pipe(i,0)   <= pss_streams_rec(i).beamdata(63 downto 0);
            else
                input_delay_pipe(i,0)   <= (others => '0');
            end if;

            input_delay_pipe(i,1)   <= input_delay_pipe(i,0); 
            input_delay_pipe(i,2)   <= input_delay_pipe(i,1);
            input_delay_pipe(i,3)   <= input_delay_pipe(i,2);
            input_delay_pipe(i,4)   <= input_delay_pipe(i,3);
            input_delay_pipe(i,5)   <= input_delay_pipe(i,4);
            input_delay_pipe(i,6)   <= input_delay_pipe(i,5);
            input_delay_pipe(i,7)   <= input_delay_pipe(i,6);
            input_delay_pipe(i,8)   <= input_delay_pipe(i,7);
        
            shift_cache(i)                     <= input_delay_pipe(i,0);
            -- first 12 bytes of data already packed in with weights, assemble_64b keyed off 2nd 8 bytes.
            if assemble_64b(2 downto 0)  = "000" then
                shift_in(i)(511 downto 496)     <= input_delay_pipe(i,8)(15 downto 0);
                shift_in(i)(47 downto 0)        <= input_delay_pipe(i,0)(63 downto 16);
            end if; 
            if assemble_64b(2 downto 0)  = "001" then
                shift_in(i)(495 downto 432)     <= input_delay_pipe(i,8);
            end if;
            if assemble_64b(2 downto 0)  = "010" then
                shift_in(i)(431 downto 368)     <= input_delay_pipe(i,8);
            end if;
            if assemble_64b(2 downto 0)  = "011" then
                shift_in(i)(367 downto 304)     <= input_delay_pipe(i,8);
            end if;
            if assemble_64b(2 downto 0)  = "100" then
                shift_in(i)(303 downto 240)     <= input_delay_pipe(i,8);
            end if;
            if assemble_64b(2 downto 0)  = "101" then
                shift_in(i)(239 downto 176)     <= input_delay_pipe(i,8);
            end if;                                    
            if assemble_64b(2 downto 0)  = "110" then
                shift_in(i)(175 downto 112)     <= input_delay_pipe(i,8);
            end if;
            if (assemble_64b(2 downto 0) = "111") then
                shift_in(i)(111 downto 48)      <= input_delay_pipe(i,8);
            end if;
            
            if data_valid_int_d1(8) = '1' then         -- valid delay to capture.
                assemble_64b           <= std_logic_vector(unsigned(assemble_64b) + 1);
            else
                assemble_64b           <= "000";
            end if;
            
        
            if pack_data_sm = WEIGHT_A then
                data_fifo_data(i)    <= x"0000" & input_delay_pipe(i,1)(63 downto 0) & weights_a;               -- 10 bytes and 54 bytes of padding
                data_fifo_wr         <= '1' AND fifo_enable;
            elsif pack_data_sm = WEIGHT_B then
                data_fifo_data(i)    <= weights_b & x"00000000" & input_delay_pipe(i,1)(63 downto 16);  -- 54 bytes of weights + 4 bytes of zero padding + 6 bytes of vis data.
                data_fifo_wr         <= '1' AND fifo_enable;
            elsif pack_data_sm = DATA then
                if (assemble_64b = "000") AND (data_valid_int_d1(15) = '1') then
                    data_fifo_wr        <= '1' AND fifo_enable;
                    data_fifo_data(i)   <= shift_in(i);
                else
                    data_fifo_wr     <= '0';    
                end if;
            else
                data_fifo_wr         <= '0';
            end if;

            if data_fifo_wr = '1' then
                inc_64b_count   <= inc_64b_count + 64;
            elsif pack_data_sm = IDLE then
                inc_64b_count   <= x"000";
            end if;
                
            --------------------------------------------------------------------------------
            -- now offset for header in packetformer
            shift_remain(i)(79 downto 0)   <= shift_in(i)(463 downto 384);
 
        end if;
    end process;

END GENERATE;
           
inc_sm_processing : process(i_clk400)
begin
    if rising_edge(i_clk400) then
        case pack_data_sm is
            when IDLE => 
                if data_valid_int = '1' then
                    pack_data_sm        <= WEIGHT_A;
                    streams_in_use(0)   <= pss_streams_rec(0).beamvalid;
                    streams_in_use(1)   <= pss_streams_rec(1).beamvalid;
                    streams_in_use(2)   <= pss_streams_rec(2).beamvalid;
                    streams_in_use(3)   <= pss_streams_rec(3).beamvalid;
                end if;
                
            when WEIGHT_A => 
                pack_data_sm    <= WEIGHT_B;
                
            when WEIGHT_B => 
                pack_data_sm    <= DATA_INIT;
            
            when DATA_INIT => 
                pack_data_sm    <= DATA;

            when DATA => 
                if data_valid_int_d1(9 downto 8) = "10" then
                    pack_data_sm    <= FINISH;
                end if;
                
            when FINISH => 
                pack_data_sm    <= IDLE;
            
            when OTHERS => pack_data_sm <= IDLE;
        end case;


        ----------------------------------------------
        -- TBD - round robin scheme to empty pipes 2,3,4
        -- this means only the first pipe from BF is working but that should give us 128 BFs.
        -- need to see what the output gap from the BF is, to workout how to cache
        -- what streams are active and then feed that into how to control the arbiter.
        -- 1. if the gap is large enough, then all 4 streams can alternate in a 2 pipeline version
        --     by moving through the bf_stream_sel each time the arb grants access.
        -- 2. does the pipeline config ever change on the fly? will bf streams active change
        -- 
    end if;
end process;




--------------------------------------------------------
-- To data packetformer
o_packetiser_data_to_former.data_clk            <= '0';             --NOT USED
o_packetiser_data_to_former.data_in_wr          <= data_fifo_rd(bf_stream_sel);
o_packetiser_data_to_former.data(511 downto 0)  <= data_fifo_q(bf_stream_sel);
o_packetiser_data_to_former.bytes_to_transmit   <= "00" & x"000";   --NOT USED

o_packetiser_data_to_former.PSR_beam_freq_index <= PSR_virtual_channel_cache;
o_packetiser_data_to_former.PSR_beam            <= PSR_beam_cache;
o_packetiser_data_to_former.PSR_time_ref        <= PSR_time_ref_cache;

o_packetiser_data_to_former.PSR_jones           <= PSR_jones_cache;
o_packetiser_data_to_former.PSR_delay_poly      <= PSR_delay_poly_cache;

o_invalid_packet                                <= '0';

--------------------------------------------------------
-- From data packetformer
path_to_cmac_running    <= i_packetiser_data_to_former.data_in_rdy;

reset_from_packetformer_proc : process(i_clk400)
begin
    if rising_edge(i_clk400) then
        path_to_cmac_disabled   <= i_packetiser_data_to_former.in_rst;      -- packetiser disabled.
        
        stream_enable           <= NOT path_to_cmac_disabled;
    end if;
end process;
--------------------------------------------------------------------------------
-- capture packet from the signal processing.
-- PSS beamformer writes across all 4 pipelines at once.
-- if 8 beams are active the first will come in on vector 0, then 1 etc, beam 4 will appear at 0
-- if number of beams is not a multiple of 4 then only some of the pipelines will write on the last write.
-- 
-- the emptying will start based on stream 0 having a minimum amount, then keep moving through 1,2,3,0,1,2,3, etc until the fifos are all empty.
-- 


byte_check : process(i_clk400)
begin
    if rising_edge(i_clk400) then
        -- ALL 4 BEAM VECTORS SHOULD BE VALID AT THE SAME TIME, only look at one.
        data_valid_int      <= pss_streams_rec(0).beamvalid;
        data_valid_int_d1   <= data_valid_int_d1(14 downto 0) & data_valid_int;
        
        -- if invalid packet is detected then skip this streams turn
        inc_data_sm_d <= inc_data_sm;
        
        
        if i_reset_400 = '1' then
            inc_data_sm                 <= IDLE;
            reset_enable                <= x"0";
            process_data_count          <= 0;
            data_fifo_rd                <= x"0";
            
            meta_fifo_wr                <= '0';
            meta_fifo_rd                <= x"0";
            bf_stream_sel               <= 0;
        else
            case inc_data_sm is
                when IDLE => 
                    
                    -- assume once the right amount of data is in there we can stream.
                    if unsigned(data_fifo_rd_count(bf_stream_sel)) > 50 AND (path_to_cmac_running = '1') then -- 97
                        inc_data_sm <= DATA;
                    end if;
                    
                    reset_enable                <= x"0";
                    process_data_count          <= 0;
                    data_fifo_rd(bf_stream_sel) <= '0';
                    meta_fifo_rd(bf_stream_sel) <= '0';
                
                when DATA =>
                    inc_data_sm     <= FINISH;
                
                when FLUSH_FIFO => -- FLUSHING DOESNT OCCUR IN PSS.
                    inc_data_sm     <= IDLE;
                    
                when FINISH => 
                   -- logic for draining the input FIFO, from this point we assume there is no error to recover from.
                    process_data_count  <= process_data_count + 1;
                    
                    if process_data_count = 56 then  -- all done!
                        inc_data_sm     <= HAND_OFF;
                        data_fifo_rd(bf_stream_sel) <= '0';
                        meta_fifo_rd(bf_stream_sel) <= '1';
                    else
                        data_fifo_rd(bf_stream_sel) <= '1';
                    end if;

                when HAND_OFF => 
                    process_data_count  <= process_data_count + 1;
                    meta_fifo_rd(bf_stream_sel)     <= '0';
                    
                    if process_data_count = 65 then  -- wait for it to flow through arbiter, feedback from player
                        inc_data_sm     <= STREAM_SEL;
                    end if;
                
                when STREAM_SEL => 
                    -- This should continue on through the pipelines until empty, for all BF counts.
                    if bf_stream_sel = 0 then
                        if unsigned(data_fifo_rd_count(1)) > 50 then
                            bf_stream_sel <= bf_stream_sel + 1;
                        else
                            bf_stream_sel <= 0;
                        end if;
                    elsif bf_stream_sel = 1 then
                        if unsigned(data_fifo_rd_count(2)) > 50 then
                            bf_stream_sel <= bf_stream_sel + 1;
                        else
                            bf_stream_sel <= 0;
                        end if;
                    elsif bf_stream_sel = 2 then
                        if unsigned(data_fifo_rd_count(3)) > 50 then
                            bf_stream_sel <= bf_stream_sel + 1;
                        else
                            bf_stream_sel <= 0;
                        end if;
                    else
                        bf_stream_sel <= 0;
                    end if;
                    inc_data_sm     <= IDLE;
                
                when others =>
                    inc_data_sm <= IDLE;
                
            end case;
            
            ----------------------------------------------------------------------
            -- cache the beam ID, at start of frame streaming into FIFO
            if data_valid_int_d1(1 downto 0) = "01" then
                meta_fifo_wr    <= '1' AND fifo_enable;
            else
                meta_fifo_wr    <= '0';
            end if;
        
        end if;
        
        
        if data_valid_int = '1' then
            signal_data_wr_count        <= signal_data_wr_count + 1;
        elsif data_valid_int_d1(1 downto 0) = "10" then
            signal_data_wr_count        <= 0;
            signal_data_wr_count_cache  <= signal_data_wr_count;
        end if;

    end if;
end process;

gen_fifo_pipes : for i in 0 to 3 GENERATE
    
    data_fifo_wr_stream(i) <= data_fifo_wr AND streams_in_use(i);
    
    incoming_buffer_fifo : entity signal_processing_common.xpm_fifo_wrapper
        Generic map (
            FIFO_DEPTH      => FIFO_CACHE_DEPTH,
            DATA_WIDTH      => 512
        )
        Port Map ( 
            fifo_reset      => fifo_reset,
            -- RD    
            fifo_rd_clk     => i_clk400,
            fifo_rd         => data_fifo_rd(i),
            fifo_q          => data_fifo_q(i),
            fifo_q_valid    => open,
            fifo_empty      => data_fifo_empty(i),
            fifo_rd_count   => data_fifo_rd_count(i),
            -- WR        
            fifo_wr_clk     => i_clk400,
            fifo_wr         => data_fifo_wr_stream(i),
            fifo_data       => data_fifo_data(i),
            fifo_full       => open,
            fifo_wr_count   => open
        );

    meta_fifo_wr_stream(i) <= meta_fifo_wr AND streams_in_use(i);

    metadata_buffer_fifo : entity signal_processing_common.xpm_fifo_wrapper
        Generic map (
            FIFO_DEPTH      => 16,
            DATA_WIDTH      => BUFF_FIFO_WIDTH
        )
        Port Map ( 
            fifo_reset      => fifo_reset,
            -- RD    
            fifo_rd_clk     => i_clk400,
            fifo_rd         => meta_fifo_rd(i),
            fifo_q          => meta_fifo_q(i),
            fifo_q_valid    => open,
            fifo_empty      => meta_fifo_empty(i),
            fifo_rd_count   => open,
            -- WR        
            fifo_wr_clk     => i_clk400,
            fifo_wr         => meta_fifo_wr_stream(i),
            fifo_data       => meta_fifo_data(i),
            fifo_full       => open,
            fifo_wr_count   => open
        );

    meta_fifo_data(i) <=    pss_streams_rec(i).beamPoly_ok &
                            pss_streams_rec(i).beamJonesStatus & 
                            pss_streams_rec(i).beamPacketCount & 
                            pss_streams_rec(i).beamFreqIndex & 
                            pss_streams_rec(i).beamBeam(8 downto 0);
END GENERATE;

o_fifo_data_used            <= data_fifo_rd_count(bf_stream_sel);


PSR_delay_poly_cache        <= meta_fifo_q(bf_stream_sel)(73 downto 72);
PSR_jones_cache             <= meta_fifo_q(bf_stream_sel)(71 downto 68);
PSR_time_ref_cache          <= meta_fifo_q(bf_stream_sel)(67 downto 20);
PSR_virtual_channel_cache   <= meta_fifo_q(bf_stream_sel)(19 downto 9);
PSR_beam_cache              <= meta_fifo_q(bf_stream_sel)(8 downto 0);

---------------------------------------------------------------------------------------------------------------------------------------
-- module statistics

o_stats.valid_packets           <= valid_packet_counter;
o_stats.invalid_packets         <= invalid_packet_counter;
o_stats.disregarded_packets     <= disregarded_packets_master_enable;
o_stats.packets_sent_to_cmac    <= packets_to_ethernet_serialiser_counter;
o_stats.reset_counters          <= i_reset_400;

stats_proc : process(i_clk400)
begin
    if rising_edge(i_clk400) then
        if i_reset_400 = '1' then
            valid_packet_counter                    <= '0';
            invalid_packet_counter                  <= '0';
            disregarded_packets_master_enable       <= '0';
            packets_to_ethernet_serialiser_counter  <= '0';
        else
            if stream_enable = '1' then
                if ((inc_data_sm = IDLE) AND (inc_data_sm_d = HAND_OFF))  then
                    valid_packet_counter    <= '1';
                else
                    valid_packet_counter    <= '0';
                end if;

                invalid_packet_counter  <= '0';
            else
                if (data_valid_int_d1(1 downto 0) = "01") then
                    disregarded_packets_master_enable  <= '1';
                else
                    disregarded_packets_master_enable  <= '0';
                end if;
            end if;
            
            wr_to_cmac_d <= i_wr_to_cmac;
            
            if (i_wr_to_cmac = '1' AND wr_to_cmac_d = '0') then
                packets_to_ethernet_serialiser_counter  <= '1';
            else
                packets_to_ethernet_serialiser_counter  <= '0';
            end if;

        end if;
    end if;
end process;



end rtl;
