----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 31.10.2021 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- ARGS to local RAM wrapper.
--
-- 
----------------------------------------------------------------------------------

library IEEE, axi4_lib, xpm, PSR_Packetiser_lib, common_lib, signal_processing_common, ethernet_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use ethernet_lib.ethernet_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;
use xpm.vcomponents.all;
USE common_lib.common_pkg.ALL;
library technology_lib;
USE technology_lib.tech_mac_100g_pkg.ALL;
use axi4_lib.axi4_stream_pkg.ALL;
use axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.ALL;
USE PSR_Packetiser_lib.Packetiser_packetiser_reg_pkg.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity cmac_args_axi_wrapper is
    Generic (
    
        g_NUMBER_OF_STREAMS                 : integer := 3
    
    );
    Port ( 
    
        -- ARGS interface
        -- MACE clock is 300 MHz
        i_MACE_clk                          : in std_logic;
        i_MACE_rst                          : in std_logic;
        
        i_packetiser_clk                    : in std_logic;
        i_packetiser_clk_rst                : in std_logic := '0';
        
        i_PSR_packetiser_Lite_axi_mosi      : in t_axi4_lite_mosi; 
        o_PSR_packetiser_Lite_axi_miso      : out t_axi4_lite_miso;     
        
        i_axi_mosi                          : in  t_axi4_full_mosi;
        o_axi_miso                          : out t_axi4_full_miso;
        
        o_packet_stream_ctrl                : out packetiser_stream_ctrl;
        
        i_packet_stream_stats               : in packetiser_stats;
                
        o_packet_config                     : out packetiser_config_in;  
        i_packet_config_out                 : in packetiser_config_out 

    );
end cmac_args_axi_wrapper;

architecture rtl of cmac_args_axi_wrapper is
COMPONENT axi_bram_ctrl_packetiser100G IS
  PORT (
    s_axi_aclk : IN STD_LOGIC;
    s_axi_aresetn : IN STD_LOGIC;
    s_axi_awaddr : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
    s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_awlock : IN STD_LOGIC;
    s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_awvalid : IN STD_LOGIC;
    s_axi_awready : OUT STD_LOGIC;
    s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_wlast : IN STD_LOGIC;
    s_axi_wvalid : IN STD_LOGIC;
    s_axi_wready : OUT STD_LOGIC;
    s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_bvalid : OUT STD_LOGIC;
    s_axi_bready : IN STD_LOGIC;
    s_axi_araddr : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
    s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_arlock : IN STD_LOGIC;
    s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_arvalid : IN STD_LOGIC;
    s_axi_arready : OUT STD_LOGIC;
    s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_rlast : OUT STD_LOGIC;
    s_axi_rvalid : OUT STD_LOGIC;
    s_axi_rready : IN STD_LOGIC;
    bram_rst_a : OUT STD_LOGIC;
    bram_clk_a : OUT STD_LOGIC;
    bram_en_a : OUT STD_LOGIC;
    bram_we_a : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_addr_a : OUT STD_LOGIC_VECTOR(14 DOWNTO 0);
    bram_wrdata_a : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_rddata_a : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT; 

signal axi_mosi : t_axi4_full_mosi;
signal axi_miso : t_axi4_full_miso;    
signal awlock_slv : std_logic_vector(0 downto 0);
signal arlock_slv : std_logic_vector(0 downto 0);

signal axi_mosi_arlock : std_logic_vector(0 downto 0);
signal axi_mosi_awlock : std_logic_vector(0 downto 0);

constant stats                  : integer := 4;
constant stat_vector            : integer := (stats)-1;

signal bram_rst                 : STD_LOGIC;
signal bram_clk                 : STD_LOGIC;
signal bram_en                  : STD_LOGIC;
signal bram_we_byte             : STD_LOGIC_VECTOR(3 DOWNTO 0);
signal bram_addr                : STD_LOGIC_VECTOR(14 DOWNTO 0);
signal bram_wrdata              : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal bram_rddata              : STD_LOGIC_VECTOR(31 DOWNTO 0);

signal packet_registers         : t_packetiser_ctrl_rw;

signal packet_stats             : t_packetiser_ctrl_ro;

signal MACE_rst_n               : std_logic;

signal stat_vectors             : STD_LOGIC_VECTOR(3 DOWNTO 0);

signal stat_vectors_cdc         : STD_LOGIC_VECTOR(3 DOWNTO 0);

signal stat_counts              : t_slv_32_arr(stat_vector downto 0);

signal reset_counters           : std_logic;

signal ARGs_rstn_in_packet_clk  : std_logic;

begin

MACE_rst_n  <= NOT i_MACE_rst;

-----------------------------------------------------------------------------
    
sync_packet_registers_sig : entity signal_processing_common.sync
    Generic Map (
        USE_XPM     => true,
        WIDTH       => 1
    )
    Port Map ( 
        Clock_a                 => i_MACE_clk,
        Clock_b                 => i_packetiser_clk,
        
        data_in(0)              => MACE_rst_n,

        data_out(0)             => ARGs_rstn_in_packet_clk
    );

----------------------------------------------------------------------------

ARGS_AXI_BRAM : axi_bram_ctrl_packetiser100G
PORT MAP (
    s_axi_aclk      => i_MACE_clk,
    s_axi_aresetn   => MACE_rst_n,
    s_axi_awaddr    => i_axi_mosi.awaddr(14 downto 0),
    s_axi_awlen     => i_axi_mosi.awlen,
    s_axi_awsize    => i_axi_mosi.awsize,
    s_axi_awburst   => i_axi_mosi.awburst,
    s_axi_awlock    => i_axi_mosi.awlock ,
    s_axi_awcache   => i_axi_mosi.awcache,
    s_axi_awprot    => i_axi_mosi.awprot,
    s_axi_awvalid   => i_axi_mosi.awvalid,
    s_axi_awready   => o_axi_miso.awready,
    s_axi_wdata     => i_axi_mosi.wdata(31 downto 0),
    s_axi_wstrb     => i_axi_mosi.wstrb(3 downto 0),
    s_axi_wlast     => i_axi_mosi.wlast,
    s_axi_wvalid    => i_axi_mosi.wvalid,
    s_axi_wready    => o_axi_miso.wready,
    s_axi_bresp     => o_axi_miso.bresp,
    s_axi_bvalid    => o_axi_miso.bvalid,
    s_axi_bready    => i_axi_mosi.bready ,
    s_axi_araddr    => i_axi_mosi.araddr(14 downto 0),
    s_axi_arlen     => i_axi_mosi.arlen,
    s_axi_arsize    => i_axi_mosi.arsize,
    s_axi_arburst   => i_axi_mosi.arburst,
    s_axi_arlock    => i_axi_mosi.arlock ,
    s_axi_arcache   => i_axi_mosi.arcache,
    s_axi_arprot    => i_axi_mosi.arprot,
    s_axi_arvalid   => i_axi_mosi.arvalid,
    s_axi_arready   => o_axi_miso.arready,
    s_axi_rdata     => o_axi_miso.rdata(31 downto 0),
    s_axi_rresp     => o_axi_miso.rresp,
    s_axi_rlast     => o_axi_miso.rlast,
    s_axi_rvalid    => o_axi_miso.rvalid,
    s_axi_rready    => i_axi_mosi.rready,

    bram_rst_a      => bram_rst,
    bram_clk_a      => bram_clk,
    bram_en_a       => bram_en,     --: OUT STD_LOGIC;
    bram_we_a       => bram_we_byte,     --: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_addr_a     => bram_addr,   --: OUT STD_LOGIC_VECTOR(14 DOWNTO 0);
    bram_wrdata_a   => bram_wrdata, --: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_rddata_a   => bram_rddata  --: IN STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
  
ARGS_register_Packetiser : entity PSR_Packetiser_lib.Packetiser_packetiser_reg 
    
    PORT MAP (
        -- AXI Lite signals, 300 MHz Clock domain
        MM_CLK                          => i_MACE_clk,
        MM_RST                          => i_MACE_rst,
        
        SLA_IN                          => i_PSR_packetiser_lite_axi_mosi,
        SLA_OUT                         => o_PSR_packetiser_lite_axi_miso,

        PACKETISER_CTRL_FIELDS_RW       => packet_registers,
        
        PACKETISER_CTRL_FIELDS_RO       => packet_stats
        
        );
        
    
o_packet_stream_ctrl.instruct           <= packet_registers.control_vector;

o_packet_config.config_data_clk         <= i_MACE_clk;
o_packet_config.config_data             <= bram_wrdata;
o_packet_config.config_data_addr        <= bram_addr;
o_packet_config.config_data_wr          <= bram_we_byte(3) AND bram_we_byte(2) AND bram_we_byte(1) AND bram_we_byte(0);  
o_packet_config.config_data_en          <= bram_en;

bram_rddata <= i_packet_config_out.config_data_out;




packet_gen : for i in 0 to (stat_vector) GENERATE
    
    stat_sync : entity signal_processing_common.sync
        generic map (
            WIDTH => 1
        )
        Port Map ( 
            Clock_a     => i_packetiser_clk,
            data_in(0)  => stat_vectors(i),
            
            Clock_b     => i_MACE_clk,
            data_out(0) => stat_vectors_cdc(i)
        );

    count_proc : process(i_MACE_clk)
    begin
        if rising_edge(i_MACE_clk) then
            if reset_counters = '1' OR (i_MACE_rst = '1') then
                stat_counts(i)  <= (others => '0');
            elsif stat_vectors_cdc(i) = '1' then
                stat_counts(i)  <= std_logic_vector(unsigned(stat_counts(i)) + 1);
            end if;
        end if;
    end process;
        
END GENERATE;

    
stat_vectors(0) <= i_packet_stream_stats.valid_packets;
stat_vectors(1) <= i_packet_stream_stats.invalid_packets;
stat_vectors(2) <= i_packet_stream_stats.disregarded_packets;
stat_vectors(3) <= i_packet_stream_stats.packets_sent_to_cmac;

packet_stats.stats_packets_rx_sig_proc_valid        <= stat_counts(0);
packet_stats.stats_packets_rx_sig_proc_invalid      <= stat_counts(1);
packet_stats.stats_packets_rx_sm_off				<= stat_counts(2);
packet_stats.stats_packets_rx_sig_proc              <= stat_counts(3);

reset_sync : entity signal_processing_common.sync_vector
generic map (
    WIDTH => 1
)
Port Map ( 
    clock_a_rst => i_packetiser_clk_rst,
    Clock_a     => i_packetiser_clk,
    Clock_b     => i_MACE_clk,
    data_in(0)  => i_packet_stream_stats.reset_counters,
    data_out(0) => reset_counters
);


end rtl;
