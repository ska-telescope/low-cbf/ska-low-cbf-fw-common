create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_spead
set_property -dict [list CONFIG.SUPPORTS_NARROW_BURST {0} CONFIG.SINGLE_PORT_BRAM {1} CONFIG.Component_Name {axi_bram_ctrl_spead} CONFIG.READ_LATENCY {3} CONFIG.MEM_DEPTH {16384}] [get_ips axi_bram_ctrl_spead]
create_ip_run [get_ips axi_bram_ctrl_spead]

# create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp_sp_spead_hertz
# set_property -dict [list \
#   CONFIG.A_Precision_Type {Single} \
#   CONFIG.Add_Sub_Value {Add} \
#   CONFIG.C_A_Exponent_Width {8} \
#   CONFIG.C_A_Fraction_Width {24} \
#   CONFIG.C_Latency {11} \
#   CONFIG.C_Mult_Usage {Medium_Usage} \
#   CONFIG.C_Rate {1} \
#   CONFIG.C_Result_Exponent_Width {8} \
#   CONFIG.C_Result_Fraction_Width {24} \
#   CONFIG.Component_Name {fp_sp_spead_hertz} \
#   CONFIG.Flow_Control {NonBlocking} \
#   CONFIG.Has_A_TLAST {false} \
#   CONFIG.Has_RESULT_TREADY {false} \
#   CONFIG.Maximum_Latency {true} \
#   CONFIG.Operation_Type {Add_Subtract} \
#   CONFIG.RESULT_TLAST_Behv {Null} \
#   CONFIG.Result_Precision_Type {Single} \
# ] [get_ips fp_sp_spead_hertz]
# create_ip_run [get_ips fp_sp_spead_hertz]

create_ip -name mult_gen -vendor xilinx.com -library ip -version 12.0 -module_name mult_fixed_849ms_in_ns
set_property -dict [list \
  CONFIG.CcmImp {Dedicated_Multiplier} \
  CONFIG.Component_Name {mult_fixed_849ms_in_ns} \
  CONFIG.ConstValue {849346560} \
  CONFIG.MultType {Constant_Coefficient_Multiplier} \
  CONFIG.OptGoal {Speed} \
  CONFIG.PipeStages {4} \
  CONFIG.PortAType {Unsigned} \
  CONFIG.PortAWidth {32} \
] [get_ips mult_fixed_849ms_in_ns]
create_ip_run [get_ips mult_fixed_849ms_in_ns]