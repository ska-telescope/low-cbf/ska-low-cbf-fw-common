----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 20/02/2023 04:22:23 PM
-- Design Name: 
-- Module Name: correlator_data_reader - Behavioral
--  
-- Description: 
-- 
-- Take in a data stream and packetise this for transmission with SPEAD protocol.
-- 
--      
--
--  Per Instance;
--      Cache incoming data to 8k per stream.
--      4KB memory to hold SPEAD INIT or END packet. 32 bit x 1024
--      SM packing the data.
--      
-- 
--
-- Data packing onto 512 bit bus to MAC.
-- ETHERNET_HEADER_BYTES  : integer := 14;
-- IPV4_HEADER_BYTES      : integer := 20;
-- UDP_HEADER_BYTES       : integer := 8;   Total 42
--
-- 335 -> 0 = UDP + IPv4 + Ethernet ().
-- Data coming from RAM for SPEAD HEADERS needs to be padded 2 bytes. to merge into 512 bits nicely.

-- Data not cached here just packed and passed to DC FIFO in packet_player.
--
-- SPEAD DATA header = 48 bytes.
-- SPEAD DATA stream as follows;
-- 64 byte chunks
-- 0. Ethernet + IPv4 + UDP(42B) + SPEAD_DATA_HEADER (22B = 48B-> 26B)
-- 1. SPEAD_DATA_HEADER (26B) + SPEAD DATA WORD 1 (34B) + SPEAD DATA WORD 2 ( Upper 4B)
-- 2. SPEAD DATA WORD 2 (30B) + SPEAD DATA WORD 3 (34B)
-- 3. SPEAD DATA WORD 4 (34B) + SPEAD DATA WORD 5 (30B)
-- 4. SPEAD DATA WORD 5 (4B) + SPEAD DATA WORD 6 (34B) + SPEAD DATA WORD 7 (26B)
-- 5. SPEAD DATA WORD 7 (8B) + SPEAD DATA WORD 8 (34B) + SPEAD DATA WORD 9 (22B)
-- etc
-- 
--
--
-- currently setup for a 6x6 data packet
-- TODO 
--  multiple sub array config
--  IPv4 checksum on the fly.
--  INIT and END packets
----------------------------------------------------------------------------------

library IEEE, correlator_lib, common_lib, signal_processing_common, spead_lib, ethernet_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;
USE common_lib.common_pkg.ALL;
use ethernet_lib.ethernet_pkg.ALL;
use spead_lib.spead_packet_pkg.ALL;

entity spead_packet is
    Generic (
        g_DEBUG_VEC_SIZE        : INTEGER := 1;
        g_DEBUG_ILA             : BOOLEAN := FALSE
    );
    Port ( 
        -- clock used for all data input and output from this module (300 MHz)
        i_clk                   : in std_logic;
        i_clk_rst               : in std_logic;

        i_local_reset           : in std_logic;
        i_table_swap_in_progress  : in std_logic;
        i_packetiser_table_select : in std_logic;

        -- arbiter signals for round robin
        o_pending_packet        : out std_logic;
        o_spead_seq_complete    : out std_logic;
        -- S_AXI aligned.
        o_bytes_to_transmit     : OUT STD_LOGIC_VECTOR(13 downto 0);     
        o_data_to_player        : OUT STD_LOGIC_VECTOR(511 downto 0);
        o_data_to_player_wr     : OUT STD_LOGIC;
        i_data_to_player_rdy    : IN STD_LOGIC;

        -- Packed up Correlator Data.
        o_from_spead_pack       : out spead_to_hbm_bus;
        i_to_spead_pack         : in hbm_to_spead_bus;

        o_packetiser_enable     : out std_logic;

        -- debug vector
        i_debug                 : in std_logic_vector((g_DEBUG_VEC_SIZE-1) downto 0);
        
        -- ARGs interface.
        i_spead_lite_axi_mosi   : in t_axi4_lite_mosi; 
        o_spead_lite_axi_miso   : out t_axi4_lite_miso;
        i_spead_full_axi_mosi   : in  t_axi4_full_mosi;
        o_spead_full_axi_miso   : out t_axi4_full_miso

    );
end spead_packet;

architecture Behavioral of spead_packet is

COMPONENT ila_0
PORT (
    clk : IN STD_LOGIC;
    probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
END COMPONENT;

signal clk                      : std_logic;
signal reset                    : std_logic;

signal s_axi_data_in            : std_logic_vector(511 downto 0);
signal s_axi_data_out           : std_logic_vector(511 downto 0);

signal bytes_to_transmit        : STD_LOGIC_VECTOR(13 downto 0);     
signal data_to_player           : STD_LOGIC_VECTOR(511 downto 0);
signal data_to_player_wr        : STD_LOGIC;

signal bytes_to_transmit_d      : STD_LOGIC_VECTOR(13 downto 0);     
signal data_to_player_d         : STD_LOGIC_VECTOR(511 downto 0);
signal data_to_player_wr_d      : STD_LOGIC;

type spead_sm_fsm is            (
                                IDLE, CONFIGURE, GOVERN,
                                SEND_INIT_PAGE_SEL, 
                                SEND_INIT_PREP,
                                SEND_INIT_PREROLL,
                                SEND_INIT_EIU, 
                                SEND_INIT_HOLD, 
                                SEND_INIT, 
                                SEND_DATA, 
                                SEND_END_PREP,
                                SEND_END,
                                SEND_END_FINAL,
                                SEND_END_HOLD,
                                COMPLETE,
                                FRAME_HEADER_1,
                                FRAME_HEADER_2,
                                FRAME_DATA
                                );
signal spead_sm : spead_sm_fsm;

signal INIT_running_packet_fsm  : std_logic;

constant c_ethernet_frame_bytes                         : integer   := 14;
constant c_ipv4_hdr_bytes                               : integer   := 20;
constant c_udp_hdr_bytes                                : integer   := 8;
constant c_packet_start_bytes                           : integer   := c_ethernet_frame_bytes + c_ipv4_hdr_bytes + c_udp_hdr_bytes;
-- data packets
-- c_data_packet_total_header_bytes_sm
-- subtract 128 bytes, two reads on the 512 bit bus which account for the first two data structures before pure data.
-- assumes the data structures are less than 128 bytes, ie the total sum of E (14) + I (20) + U (8) + SPEAD Headers (64) + Epoch_in_heap (8) = 114
-- this is done to account for the variable sizes of visibility heaps and how they fall within 64 bytes when dealing with last beat and the hdr preload.
-- For packets that have a heap that straddles several packets, all headers are still sent, just epoch in heap is removed and
-- all byte counts are updated -8 bytes.
constant c_data_packet_total_header_bytes_sm            : integer   := c_packet_start_bytes                 + c_spead_sdp_data_hdr_w_data_in_heap_bytes - 128;
constant c_data_packet_total_header_bytes               : integer   := c_packet_start_bytes                 + c_spead_sdp_data_hdr_w_data_in_heap_bytes;
constant c_data_packet_total_ipv4_bytes                 : integer   := c_ipv4_hdr_bytes + c_udp_hdr_bytes   + c_spead_sdp_data_hdr_w_data_in_heap_bytes;
constant c_data_packet_total_udp_bytes                  : integer   := c_udp_hdr_bytes                      + c_spead_sdp_data_hdr_w_data_in_heap_bytes;
-- end packets
constant c_end_packet_total_header_bytes                : integer   := c_packet_start_bytes                 + c_spead_sdp_end_hdr;
constant c_end_packet_total_ipv4_bytes                  : integer   := c_ipv4_hdr_bytes + c_udp_hdr_bytes   + c_spead_sdp_end_hdr;
constant c_end_packet_total_udp_bytes                   : integer   := c_udp_hdr_bytes                      + c_spead_sdp_end_hdr;
-- init packets
constant c_init_packet_total_header_bytes               : integer   := c_packet_start_bytes;
constant c_init_packet_total_ipv4_bytes                 : integer   := c_ipv4_hdr_bytes + c_udp_hdr_bytes;
constant c_init_packet_total_udp_bytes                  : integer   := c_udp_hdr_bytes;

signal govern_count             : unsigned(7 downto 0)  := x"80";

signal spead_sm_fsm_debug       : std_logic_vector(3 downto 0)  := x"0";

signal current_ethernet         : ethernet_frame            := null_ethernet_frame;
signal current_ipv4             : IPv4_header               := null_ipv4_header;
signal current_udp              : UDP_header                := null_UDP_header;

signal current_ethernet_packed  : std_logic_vector(111 downto 0)    := (others => '0');
signal current_ipv4_packed      : std_logic_vector(159 downto 0)    := (others => '0');
signal current_udp_packed       : std_logic_vector(63 downto 0)     := (others => '0');

---------------------------------------------
signal bytes_coming_from_cor    : unsigned(13 downto 0);
signal data_packet_bytes_remain : unsigned(13 downto 0);
signal spead_data_rd            : std_logic;

signal cur_spead_data_packet    : spead_data_packet;

constant sdp_length             : integer   := c_spead_sdp_data_hdr_w_data_in_heap_bytes * 8;
signal packed_spead_data_packet : std_logic_vector(sdp_length-1 downto 0)       := (others => '0'); --576 bits.

signal cur_epoch_offset_heap    : std_logic_vector(63 downto 0);

signal pack_data_vector         : t_slv_512_arr(1 downto 0);

signal init_packet_data         : std_logic_vector(511 downto 0);
signal init_packet_data_d       : std_logic_vector(511 downto 0);
signal init_packet_next         : std_logic;
signal init_packet_byte_length  : std_logic_vector(13 downto 0);

----------------------------------------------------------------------
-- SPEAD END packet
signal spead_end_header_packed      : std_logic_vector(383 downto 0);

---------------------------------------------
-- to register component
signal ipv4_total_bytes_to_calc : std_logic_vector(15 downto 0);
signal packet_calc_init         : std_logic;
signal packet_calc_done         : std_logic;
signal checksum_calc_trigger    : std_logic_vector(7 downto 0);

signal udp_total_bytes          : std_logic_vector(15 downto 0);

signal enable_packetiser        : std_logic;

---------------------------------------------
-- packet signaling
signal data_packet_sending      : std_logic;
signal data_packet_sent         : std_logic;

signal end_packet_sending       : std_logic_vector(7 downto 0);
signal end_packet_ready         : std_logic;
signal end_packet_sent          : std_logic;

signal init_packet_sending      : std_logic_vector(7 downto 0);
signal init_packet_ready        : std_logic;
signal init_packet_sent         : std_logic;

signal page_flip_in_progress    : std_logic;
---------------------------------------------
signal init_govern              : std_logic_vector(7 downto 0);
signal end_govern               : std_logic_vector(7 downto 0);
signal data_govern              : std_logic_vector(7 downto 0);
signal end_init_data_current    : std_logic_vector(1 downto 0);

signal pending_packet           : std_logic;
signal spead_seq_complete       : std_logic;

signal byte_count               : std_logic_vector(13 downto 0);
signal spead_data               : std_logic_vector(511 downto 0);
signal spead_data_rdy           : std_logic;
signal freq_index               : std_logic_vector(16 downto 0);
signal time_ref                 : std_logic_vector(63 downto 0);
signal current_array            : std_logic_vector(7 downto 0);
signal vis_meta_bytes_to_send   : std_logic_vector(31 downto 0);
signal first_packet_of_data     : std_logic;

signal init_64b_cycles          : std_logic_vector(7 downto 0);
signal sub_64b                  : std_logic_vector(0 downto 0);

signal scratchpad_updated       : std_logic;

signal current_page_ct1         : std_logic;

begin
---------------------------------------------------------------------
-- PORT MAPPING
clk                     <= i_clk;
reset                   <= i_clk_rst OR i_local_reset;

o_bytes_to_transmit     <= bytes_to_transmit_d;
o_data_to_player_wr     <= data_to_player_wr_d;
o_data_to_player        <= data_to_player_d;

o_packetiser_enable     <= enable_packetiser;

o_pending_packet        <= pending_packet;
o_spead_seq_complete    <= spead_seq_complete;

o_from_spead_pack.spead_data_rd     <= spead_data_rd;
o_from_spead_pack.bytes_in_heap     <= vis_meta_bytes_to_send;
o_from_spead_pack.current_page_ct1  <= current_page_ct1;

byte_count                          <= i_to_spead_pack.byte_count;
spead_data                          <= i_to_spead_pack.spead_data;
spead_data_rdy                      <= i_to_spead_pack.spead_data_rdy;
current_array                       <= i_to_spead_pack.current_array;
freq_index                          <= i_to_spead_pack.freq_index;
time_ref                            <= i_to_spead_pack.time_ref;

--------------------------------------------------------------------
hbm_setup_proc : process(clk)
begin
    if rising_edge(clk) then
        -- HEAP size value is set by software in spead_registers, this includes the leading 8 bytes for epoch offset.
        vis_meta_bytes_to_send      <= std_logic_vector((unsigned(cur_spead_data_packet.heap_size)) - 8);

        -- from CT1 to packetiser and passed to HBM for initial value of page on power up.
        current_page_ct1            <= i_packetiser_table_select;
    end if;
end process;

dynamic_init_packet_size_proc : process(clk)
begin
    if rising_edge(clk) then
        if bytes_to_transmit(5 downto 0) /= "000000" then
            sub_64b(0) <= '1';
        else
            sub_64b(0) <= '0';
        end if;

        init_64b_cycles(7 downto 0)     <= std_logic_vector((unsigned(bytes_to_transmit(13 downto 6)))   +   (unsigned(sub_64b)));

    end if;
end process;

main_proc : process(clk)
begin
    if rising_edge(clk) then
        if reset = '1' or enable_packetiser = '0' then
            spead_sm                    <= IDLE;
            spead_sm_fsm_debug          <= x"F";
            spead_data_rd               <= '0';
            data_to_player              <= zero_512;
            data_to_player_wr           <= '0';
            bytes_to_transmit           <= (others => '0');
            bytes_coming_from_cor       <= (others => '0');
            data_packet_bytes_remain    <= (others => '0');
            ipv4_total_bytes_to_calc    <= (others => '0');
            packet_calc_init            <= '0';
            checksum_calc_trigger       <= x"00";
            data_packet_sending         <= '0';
            init_packet_sending         <= x"00";
            end_packet_sending          <= x"00";
            end_packet_sent             <= '0';
            data_packet_sent            <= '0';
            init_packet_sent            <= '0';
            init_packet_next            <= '0';
            pending_packet              <= '0';
            spead_seq_complete          <= '0';
            end_init_data_current       <= "00";
            INIT_running_packet_fsm     <= '0';
        else
            if spead_sm = CONFIGURE then
                bytes_coming_from_cor   <= unsigned(byte_count);
            elsif spead_data_rd = '1' AND (bytes_coming_from_cor >= 64) then
                bytes_coming_from_cor   <= bytes_coming_from_cor - 64;
            end if;

            pack_data_vector(0)     <= spead_data;
            pack_data_vector(1)     <= pack_data_vector(0);

            init_packet_data_d      <= init_packet_data;

            packet_calc_init        <= checksum_calc_trigger(7);
            checksum_calc_trigger   <= checksum_calc_trigger(6 downto 0) & '0';

            pending_packet          <= init_packet_ready OR end_packet_ready OR spead_data_rdy;

            init_packet_sending(7 downto 1) <= init_packet_sending(6 downto 0);
            end_packet_sending(7 downto 1)  <= end_packet_sending(6 downto 0);

            case spead_sm is
                when IDLE => 
                    spead_sm_fsm_debug          <= x"0";
                    data_to_player              <= zero_512;
                    spead_seq_complete          <= '0';
                    checksum_calc_trigger       <= x"00";

                    -- set govern to 0 as we do not need to slow the first packet.
                    govern_count                <= x"00";


                    -- if packet player has space.
                    if i_data_to_player_rdy = '1' then
                        if init_packet_ready = '1' AND (page_flip_in_progress = '0') then
                            spead_sm                    <= SEND_INIT_PAGE_SEL;
                            bytes_to_transmit           <= std_logic_vector((unsigned(init_packet_byte_length))        + c_init_packet_total_header_bytes);
                            ipv4_total_bytes_to_calc    <= std_logic_vector(("00" & unsigned(init_packet_byte_length)) + c_init_packet_total_ipv4_bytes);
                            udp_total_bytes             <= std_logic_vector(("00" & unsigned(init_packet_byte_length)) + c_init_packet_total_udp_bytes);
                            init_packet_sending(0)      <= '1';
                            end_init_data_current       <= "10";
                        elsif end_packet_ready = '1' then
                            spead_sm                    <= SEND_END_PREP;
                            bytes_to_transmit           <= std_logic_vector(to_unsigned(c_end_packet_total_header_bytes , bytes_to_transmit'length));
                            ipv4_total_bytes_to_calc    <= std_logic_vector(to_unsigned(c_end_packet_total_ipv4_bytes   , ipv4_total_bytes_to_calc'length));
                            udp_total_bytes             <= std_logic_vector(to_unsigned(c_end_packet_total_udp_bytes    , udp_total_bytes'length));
                            end_packet_sending(0)       <= '1';
                            end_init_data_current       <= "11";
                        elsif spead_data_rdy = '1' then
                            spead_sm                    <= CONFIGURE;
                            -- Ethernet + IPv4 + UDP + Data header + epoch_offset_in_data + payload
                            -- 14 + 20 + 8 + 56 + 8 + i_byte_count
                            -- First packet of data has the epoch_offset in it, 2+ does not, just Vis Data.
                            first_packet_of_data        <= i_to_spead_pack.spead_data_pending;
                            data_packet_bytes_remain    <= (unsigned(byte_count)                          + to_unsigned(c_data_packet_total_header_bytes_sm, 14));
                            bytes_to_transmit           <= std_logic_vector((unsigned(byte_count))        + c_data_packet_total_header_bytes);
                            ipv4_total_bytes_to_calc    <= std_logic_vector(("00" & unsigned(byte_count)) + c_data_packet_total_ipv4_bytes);
                            udp_total_bytes             <= std_logic_vector(("00" & unsigned(byte_count)) + c_data_packet_total_udp_bytes);
                            --checksum_calc_trigger       <= x"01";    
                            data_packet_sending         <= '1';
                            end_init_data_current       <= "01";
                        end if;
                    end if;
                    

                when GOVERN => 
                    -- this is to slow down the transmission rate
                    -- running at full rate will lead to packet loss at the reciever.
                    if govern_count = x"00" then
                        spead_sm_fsm_debug      <= x"1";

                        -- end packet ready asserted the entire time ENDs being sent.
                        if end_init_data_current = "10" then
                            spead_sm                <= SEND_INIT_PREROLL;
                        elsif end_init_data_current = "11" then
                            spead_sm                <= SEND_END;
                        elsif end_init_data_current = "01" then     -- Done at the start of the DATA packet.
                            spead_sm                <= SEND_DATA;
                            checksum_calc_trigger   <= x"01";
                        end if;
                    else
                        govern_count            <= govern_count - 1;
                    end if;

        ------------------------------------------------------------------------------------------------
        -- INIT PACKET HANDLING
                -- INIT packet needs to access the next page.
                -- This will deal with a swap in the redirect based on if
                -- the INIT sequence is running.
                -- This SM will stay in the INIT loop until all the packets are sent
                -- for visibilities based on the target subarray_beam_table entry.
                when SEND_INIT_PAGE_SEL =>
                    INIT_running_packet_fsm     <= '1';
                    spead_sm                    <= SEND_INIT_PREP;

                when SEND_INIT_PREP => 
                    spead_sm_fsm_debug          <= x"2";
                    data_to_player_wr           <= '0';
                    checksum_calc_trigger       <= x"01"; 
                    init_packet_sent            <= '0';
                    spead_sm                    <= SEND_INIT_HOLD;

                when SEND_INIT_HOLD =>
                    if init_packet_ready = '0' then
                        spead_sm                <= COMPLETE;
                        spead_seq_complete      <= '1';
                        govern_count            <= x"04";
                    elsif scratchpad_updated = '1' then
                        spead_sm                <= GOVERN;
                        govern_count            <= unsigned(init_govern); --x"80";
                    end if;        

                when SEND_INIT_PREROLL =>
                    init_packet_next            <= '1';
                    govern_count                <= x"03";
                    spead_sm                    <= SEND_INIT_EIU;

                when SEND_INIT_EIU =>
                    data_to_player              <= current_ethernet_packed & current_ipv4_packed & current_udp_packed & init_packet_data(511 downto 336);   -- first 336 bits are start of packet E/I/U... 176 left
                    
                    govern_count                <= govern_count - 1;
                    if govern_count = x"00" then
                        govern_count                <= unsigned(init_64b_cycles); --x"27";
                        spead_sm                    <= SEND_INIT;
                        data_to_player_wr           <= '1';
                    end if;

                when SEND_INIT  =>
                    data_to_player              <= init_packet_data_d(335 downto 0) & init_packet_data(511 downto 336);

                    govern_count                <= govern_count - 1;
                    if govern_count = x"02" then
                        spead_sm                    <= SEND_INIT_PREP;
                        init_packet_next            <= '0';
                        init_packet_sent            <= '1';
                    end if;

        ------------------------------------------------------------------------------------------------
        -- END PACKET HANDLING
                when SEND_END_PREP => 
                    data_to_player_wr           <= '0';
                    spead_sm_fsm_debug          <= x"A";
                    checksum_calc_trigger       <= x"01"; 
                    end_packet_sent             <= '0';
                    spead_sm                    <= SEND_END_HOLD;

                when SEND_END_HOLD =>
                    spead_sm_fsm_debug          <= x"B";
                    -- wait here for checksums to update.
                    -- if we have sent the required packets exit.
                    if end_packet_ready = '0' then
                        spead_sm                <= COMPLETE;
                        spead_seq_complete      <= '1';
                        govern_count            <= x"04";
                    elsif packet_calc_done = '1' then
                        spead_sm                <= GOVERN;
                        govern_count            <= unsigned(end_govern); --x"40";
                    end if;

                when SEND_END => 
                    data_to_player              <= current_ethernet_packed & current_ipv4_packed & current_udp_packed & spead_end_header_packed(383 downto 208);   -- first 336 bits are start of packet E/I/U... 176 left
                    data_to_player_wr           <= '1';
                    spead_sm_fsm_debug          <= x"3";
                    spead_sm                    <= SEND_END_FINAL;

                when SEND_END_FINAL => 
                    data_to_player              <= spead_end_header_packed(207 downto 0) & zero_256 & zero_32 & zero_word;   
                    
                    spead_sm_fsm_debug          <= x"C";
                    spead_sm                    <= SEND_END_PREP;
                    end_packet_sent             <= '1';

        ------------------------------------------------------------------------------------------------
        -- Data packet handling
                when CONFIGURE => 
                    spead_sm_fsm_debug          <= x"4";
                    -- if not first packet, remove the epoch offset from byte calcs.
                    if first_packet_of_data = '0' then
                        data_packet_bytes_remain    <= data_packet_bytes_remain  - 8;
                        bytes_to_transmit           <= std_logic_vector(unsigned(bytes_to_transmit)         - 8);
                        ipv4_total_bytes_to_calc    <= std_logic_vector(unsigned(ipv4_total_bytes_to_calc)  - 8);
                        udp_total_bytes             <= std_logic_vector(unsigned(udp_total_bytes)           - 8);
                    end if;
                    govern_count                <= unsigned(data_govern(7 downto 0));
                    spead_sm                    <= GOVERN;

                when SEND_DATA => 
                    spead_sm_fsm_debug          <= x"5";
                    if packet_calc_done = '1' then
                        spead_sm                <= FRAME_HEADER_1;
                        spead_data_rd           <= '1';
                    end if;               

                when FRAME_HEADER_1 =>
                    spead_sm_fsm_debug          <= x"6";
                    -- first 336 bits/42 bytes are start of packet E/I/U, then 22 bytes of DATA header, 72 bytes in header
                    data_to_player              <= current_ethernet_packed & current_ipv4_packed & current_udp_packed & packed_spead_data_packet(575 downto 400);
                    data_to_player_wr           <= '1';
                    --spead_data_rd               <= '1';
                    spead_sm                    <= FRAME_HEADER_2;
                    
                when FRAME_HEADER_2 =>
                    spead_sm_fsm_debug          <= x"7";
                    -- if first packet send epoch data.
                    if first_packet_of_data = '1' then
                        data_to_player <= packed_spead_data_packet(399 downto 0) & pack_data_vector(0)(511 downto 400);
                    -- else skip the epoch data (8 bytes).
                    else
                        data_to_player <= packed_spead_data_packet(399 downto 64) & pack_data_vector(0)(511 downto 336);
                    end if;
                    spead_sm        <= FRAME_DATA;

                when FRAME_DATA =>
                    spead_sm_fsm_debug          <= x"8";
                    -- if first packet send epoch data.
                    if first_packet_of_data = '1' then
                        data_to_player <= pack_data_vector(1)(399 downto 0) & pack_data_vector(0)(511 downto 400);
                    -- else skip the epoch data (8 bytes).
                    else
                        data_to_player <= pack_data_vector(1)(335 downto 0) & pack_data_vector(0)(511 downto 336);
                    end if;
                    
                    if bytes_coming_from_cor <= 64 then
                        spead_data_rd       <= '0';
                    end if;

                    data_packet_bytes_remain    <= data_packet_bytes_remain - 64;

                    if data_packet_bytes_remain <= 64 then
                        spead_sm            <= COMPLETE;
                        spead_seq_complete  <= '1';
                        govern_count        <= x"04";
                        data_packet_sent    <= '1';
                    end if;

        ------------------------------------------------------------------------------------------------

                when COMPLETE => 
                    spead_sm_fsm_debug      <= x"9";
                    data_to_player_wr       <= '0';

                    govern_count            <= govern_count - 1;
                    if govern_count = x"00" then
                        spead_sm            <= IDLE;
                    end if;

                    data_packet_sent        <= '0';
                    data_packet_sending     <= '0';

                    init_packet_sending(0)  <= '0';
                    end_packet_sending(0)   <= '0';

                    spead_seq_complete      <= '0';

                    INIT_running_packet_fsm <= '0';

                when OTHERS =>
                    spead_sm    <= IDLE;
            end case;
        end if;


    end if;
end process;

--------------------------------------------------------------------
-- PACK data.



--------------------------------------------------------------------
-- byte swap data vector for S_AXI transmission on the MAC.
-- delay other like signals.

s_axi_data_in       <= data_to_player;

s_axi_swap_gen : for i in 0 to 63 generate
    s_axi_byte_proc : process(clk)
    begin
        if rising_edge(clk) then
            s_axi_data_out(((8*i)+7) downto (i*8))  <= s_axi_data_in(((512-1)-(8*i)) downto ((512-8)-(8*i)));
        end if;
    end process;
end generate;

data_to_player_d    <= s_axi_data_out;

delay_proc_MAC : process(clk)
begin
    if rising_edge(clk) then
        bytes_to_transmit_d <= bytes_to_transmit;
        data_to_player_wr_d <= data_to_player_wr;
    end if;
end process;

--------------------------------------------------------------------
-- pack records for SM

current_ethernet_packed     <= current_ethernet.dst_mac & current_ethernet.src_mac & current_ethernet.eth_type;
current_ipv4_packed         <= current_ipv4.version & current_ipv4.header_length & current_ipv4.type_of_service & current_ipv4.total_length & 
                                current_ipv4.id & current_ipv4.ip_flags & current_ipv4.fragment_off & current_ipv4.TTL & 
                                current_ipv4.protocol & current_ipv4.header_chk_sum & current_ipv4.src_addr & current_ipv4.dst_addr; 
current_udp_packed          <= current_udp.src_port & current_udp.dst_port & current_udp.length & current_udp.checksum;


packed_spead_data_packet    <=  default_spead_common_fields.spead_magic & default_spead_common_fields.spead_version & default_spead_common_fields.spead_item_pointer_width & 
                                default_spead_common_fields.spead_heap_add_width & default_spead_common_fields.spead_reserved & 
                                cur_spead_data_packet.number_of_items & 
                                cur_spead_data_packet.heap_count_header &       cur_spead_data_packet.subarray_beam_freq &      cur_spead_data_packet.integration_id & 
                                cur_spead_data_packet.heap_size_header &        cur_spead_data_packet.reserved_size &           cur_spead_data_packet.heap_size & 
                                cur_spead_data_packet.heap_offset_header &      cur_spead_data_packet.reserved_offset &         cur_spead_data_packet.offset_size &
                                cur_spead_data_packet.heap_payload_header &     cur_spead_data_packet.reserved_payload &        cur_spead_data_packet.payload_size & 
                                cur_spead_data_packet.Epoch_offset_header &     cur_spead_data_packet.reserved_Epoch_offset &   cur_spead_data_packet.Epoch_offset &
                                cur_spead_data_packet.vis_flags_header &        cur_spead_data_packet.reserved_vis_flags &      cur_spead_data_packet.vis_flags &
                                cur_spead_data_packet.corr_data_header &        cur_spead_data_packet.reserved_corr_data &      cur_spead_data_packet.corr_data &
                                cur_epoch_offset_heap;

------------------------------------------------------------------------------

spead_end_header_packed     <=  default_spead_common_fields.spead_magic & default_spead_common_fields.spead_version & default_spead_common_fields.spead_item_pointer_width & 
                                default_spead_common_fields.spead_heap_add_width & default_spead_common_fields.spead_reserved & 
                                default_sdp_spead_end_packet.number_of_items & 
                                default_sdp_spead_end_packet.heap_count_header &        default_sdp_spead_end_packet.subarray_beam_freq &   default_sdp_spead_end_packet.integration_id & 
                                default_sdp_spead_end_packet.heap_size_header &         default_sdp_spead_end_packet.reserved_size &        default_sdp_spead_end_packet.heap_size & 
                                default_sdp_spead_end_packet.heap_offset_header &       default_sdp_spead_end_packet.reserved_offset &      default_sdp_spead_end_packet.offset_size & 
                                default_sdp_spead_end_packet.heap_payload_header &      default_sdp_spead_end_packet.reserved_payload &     default_sdp_spead_end_packet.payload_size & 
                                default_sdp_spead_end_packet.stream_control_header &    default_sdp_spead_end_packet.reserved_control &     default_sdp_spead_end_packet.stream_control;


------------------------------------------------------------------------------                                
host_interface : entity spead_lib.spead_registers
    generic map (
        g_DEBUG_VEC_SIZE            => g_DEBUG_VEC_SIZE,
        g_DEBUG_ILA                 => g_DEBUG_ILA
    )
    port map ( 
        -- clock used for all data input and output from this module (300 MHz)
        i_clk                       => clk,
        i_rst                       => reset,
        i_table_swap_in_progress    => i_table_swap_in_progress, --  in std_logic;
        i_packetiser_table_select   => i_packetiser_table_select, -- in std_logic;
        -- Signals
        i_ipv4_total_bytes_to_calc  => ipv4_total_bytes_to_calc,
        i_udp_total_bytes           => udp_total_bytes,
        o_spead_data_heap_size      => o_from_spead_pack.spead_data_heap_size,
        i_packet_calc               => packet_calc_init,
        o_packet_calc_done          => packet_calc_done,
        o_scratchpad_updated        => scratchpad_updated,

        -- signal from HBM reader
        i_to_spead_pack             => i_to_spead_pack,

        o_init_govern               => init_govern,
        o_end_govern                => end_govern,
        o_data_govern               => data_govern,

        -- page flip additions
        o_page_flip_in_progress     => page_flip_in_progress,
        o_end_sequence_complete     => o_from_spead_pack.end_packets_complete,
        
        -- packet signaling
        i_data_packet_sending       => data_packet_sending,
        i_data_packet_sent          => data_packet_sent,

        i_end_packet_sending        => end_packet_sending,
        o_end_packet_ready          => end_packet_ready,
        i_end_packet_sent           => end_packet_sent,

        i_init_packet_sending       => init_packet_sending,
        o_init_packet_ready         => init_packet_ready,
        i_init_packet_sent          => init_packet_sent,

        -- return structures
        o_current_ipv4              => current_ipv4,
        o_current_ethernet          => current_ethernet,
        o_current_udp               => current_udp,
        o_spead_data_packet         => cur_spead_data_packet,
        o_epoch_offset_le           => cur_epoch_offset_heap,

        o_init_packet_data          => init_packet_data,
        i_init_packet_next          => init_packet_next,
        o_init_packet_byte_length   => init_packet_byte_length,
        i_INIT_running_packet_fsm   => INIT_running_packet_fsm,

        o_enable_packetiser         => enable_packetiser,

        -- debug circuits
        i_spead_sm_fsm_debug        => spead_sm_fsm_debug,

        i_debug                     => i_debug,
        
        -- ARGs
        i_spead_lite_axi_mosi       => i_spead_lite_axi_mosi,
        o_spead_lite_axi_miso       => o_spead_lite_axi_miso,
        i_spead_full_axi_mosi       => i_spead_full_axi_mosi,
        o_spead_full_axi_miso       => o_spead_full_axi_miso

    );


------------------------------------------------------------------------------
-- ILA debug

generate_debug_ila : IF g_DEBUG_ILA GENERATE

    packetiser_debug : ila_0 PORT MAP (
        clk                     => clk,
        probe0(0)               => spead_data_rd,
        probe0(64 downto 1)     => spead_data(255 downto 192),
        probe0(72 downto 65)    => freq_index(7 downto 0),
        probe0(104 downto 73)   => pack_data_vector(0)(511 downto 480),
        probe0(108 downto 105)  => spead_sm_fsm_debug,
        probe0(116 downto 109)  => cur_spead_data_packet.integration_id(7 downto 0),
        probe0(148 downto 117)  => data_to_player(207 downto 176),
        probe0(180 downto 149)  => data_to_player(175 downto 144),
        probe0(181)             => data_to_player_wr,
        probe0(191 downto 182)  => (others => '0')
        );

END GENERATE;

end Behavioral;
