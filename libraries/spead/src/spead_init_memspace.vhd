----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 02/04/2023
-- Design Name: 
-- Module Name: spead_init_memspace
--  
-- Description: 
--      This has a 4K byte memory space, connected to ARGs, ( 4 byte / 32 bit), and is streamed out to 100G interface, (64 byte / 512 bit).
--
--      Software writes the SPEAD INIT header into the 4K memory space and this logic will deal with incrementing relevant fields and streaming this
--      to the SPEAD packetiser.
--
--      Initially this is only three parameters.
--        self.ptr_heap_counter_in_init = 15  # low byte, to 10 low byte
--        # subarray: byte 10
--        # beam: high 7 bits of byte 11
--        # freq. channel: lowest bit of byte 11 (MSB), 12, 13 (low)
--        # integration: 14 (high), 15 (low)
--
--        self.ptr_vis_chan_id_in_init = 180  # low byte, to 183 high byte
--        self.ptr_hz_field_in_init = 252  # low byte, to 255 high byte
--
--      A SM will request memory reads on the B side, update values based on the offsets provided as above.
--      Write the updated value back to memory.
--      Once complete, this module will indicate the data is ready to stream and will respond to the packetiser.
--      This will repeat for all frequency channels in the subarray-beam.
--
--
----------------------------------------------------------------------------------

library IEEE, spead_lib, xpm, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use xpm.vcomponents.all;
use common_lib.common_pkg.ALL;
library UNISIM;
use UNISIM.VComponents.all;


entity spead_init_memspace is
    Generic (
        g_NUMBER_OF_OFFSETS     : INTEGER := 3;
        g_DEBUG_ILA             : BOOLEAN := FALSE
    );
    Port ( 
        i_clk                   : in std_logic;
        i_rst                   : in std_logic;

        i_host_data             : in std_logic_vector(31 downto 0);
        i_host_addr             : in std_logic_vector(9 downto 0);
        i_host_wren             : in std_logic;
        o_host_return_data      : out std_logic_vector(31 downto 0);

        -------------------------------------------------
        i_trigger_init          : in std_logic;
        i_init_packet_sent      : in std_logic;
        i_offsets_array         : in t_slv_16_arr((g_NUMBER_OF_OFFSETS-1) downto 0);
        
        o_init_ready_to_send    : out std_logic;

        i_init_freq_interval    : in std_logic_vector(63 downto 0);
        i_init_freq_base        : in std_logic_vector(63 downto 0);

        i_hertz_is_float_int    : in std_logic;

        o_scratchpad_updated    : out std_logic;
        o_init_packet_sm_debug  : out std_logic_vector(7 downto 0);
        -------------------------------------------------

        o_data_to_packetiser    : out std_logic_vector(511 downto 0);
        i_data_inc              : in std_logic

    );
end spead_init_memspace;

architecture Behavioral of spead_init_memspace is

COMPONENT ila_0
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
END COMPONENT;

COMPONENT fp_sp_spead_hertz
    Port ( 
      aclk                  : in STD_LOGIC;
      s_axis_a_tvalid       : in STD_LOGIC;
      s_axis_a_tdata        : in STD_LOGIC_VECTOR ( 31 downto 0 );
      s_axis_b_tvalid       : in STD_LOGIC;
      s_axis_b_tdata        : in STD_LOGIC_VECTOR ( 31 downto 0 );
      m_axis_result_tvalid  : out STD_LOGIC;
      m_axis_result_tdata   : out STD_LOGIC_VECTOR ( 31 downto 0 )
    );
END COMPONENT;

constant MEMORY_INIT_FILE       : STRING := "init_mem_preload.mem";
constant g_NO_OF_ADDR_BITS      : INTEGER := 6;
constant g_D_Q_WIDTH            : INTEGER := 512;
constant g_BYTE_ENABLE_WIDTH    : INTEGER := 8;

CONSTANT ADDR_SPACE             : INTEGER := pow2(g_NO_OF_ADDR_BITS);
CONSTANT MEMORY_SIZE_GENERIC    : INTEGER := ADDR_SPACE * g_D_Q_WIDTH;

CONSTANT C_FREQ_INTERVAL        : unsigned(31 downto 0)     := x"000BEBC2";    -- 781250

signal clk_a                    : std_logic;
signal clk_b                    : std_logic;
signal clk                      : std_logic;
signal reset                    : std_logic;
signal reset_n                  : std_logic;

signal data_a                   : std_logic_vector((g_D_Q_WIDTH-1) downto 0);
signal addr_a                   : std_logic_vector((g_NO_OF_ADDR_BITS-1) downto 0);
signal data_a_wr                : std_logic; 
signal data_a_q                 : std_logic_vector((g_D_Q_WIDTH-1) downto 0);

signal data_b                   : std_logic_vector((g_D_Q_WIDTH-1) downto 0);
signal addr_b                   : std_logic_vector((g_NO_OF_ADDR_BITS-1) downto 0);
signal data_b_wr                : std_logic; 
signal data_b_q                 : std_logic_vector((g_D_Q_WIDTH-1) downto 0);

signal data_a_wr_int            : std_logic_vector(63 downto 0);
signal data_b_wr_int            : std_logic_vector(63 downto 0);

signal SM_addr                  : unsigned(7 downto 0) := x"00";

signal addr_offsets_array       : t_slv_6_arr((g_NUMBER_OF_OFFSETS-1) downto 0);
signal dword_offset_array       : t_slv_6_arr((g_NUMBER_OF_OFFSETS-1) downto 0);
signal spead_item_to_update     : std_logic_vector(31 downto 0);

signal update_scratch_value     : std_logic_vector(31 downto 0);

signal host_return_data_int     : std_logic_vector(31 downto 0);

type init_packet_sm_fsm is      (
                                IDLE, INIT_UPDATE_PULSE,
                                VAR_1_GET, VAR_1_UPDATE, VAR_1_WR,
                                VAR_2_GET, VAR_2_UPDATE, VAR_2_WR,
                                VAR_3_GET, VAR_3_UPDATE, VAR_3_WR, VAR_3_UPDATED,
                                VAR_COMPLETE,
                                SEND_PACKET, 
                                WAIT_PACKET,
                                COMPLETE
                                );
signal init_packet_sm : init_packet_sm_fsm;

signal init_packet_sm_debug     : std_logic_vector(7 downto 0);

signal init_hold_off            : std_logic_vector(7 downto 0);

signal fp_adder_a               : std_logic_vector(31 downto 0);
signal fp_adder_b               : std_logic_vector(31 downto 0);
signal fp_adder_result          : std_logic_vector(31 downto 0);

signal fp_adder_in_valid        : std_logic;
signal fp_adder_out_valid       : std_logic;

signal init_freq_interval       : unsigned(63 downto 0);
signal init_freq_base           : unsigned(63 downto 0);

signal init_freq_calc           : unsigned(63 downto 0);

signal offsets_array            : t_slv_16_arr((g_NUMBER_OF_OFFSETS-1) downto 0);

signal dword_slice              : t_slv_32_arr(63 downto 0);
-- init scratchpad update
signal byte_en_quad             : t_slv_64_arr(63 downto 0);

signal heap_counter_lkup        : integer range 0 to 63;
signal vis_chan_id_lkup         : integer range 0 to 63;
signal hz_lkup                  : integer range 0 to 63;

signal data_b_overlay           : t_slv_512_arr(63 downto 0);

signal pulse_count              : unsigned(3 downto 0);

begin

clk                     <= i_clk;
reset                   <= i_rst;


o_data_to_packetiser    <= data_b_q;

o_init_packet_sm_debug  <= init_packet_sm_debug;
-------------------------------------------------------------------------
-- Variable position calcs
-- offsets_array(0)                                <= spead_ctrl_reg.ptr_heap_counter_in_init;
-- offsets_array(1)                                <= spead_ctrl_reg.ptr_vis_chan_id_in_init;
-- offsets_array(2)                                <= spead_ctrl_reg.ptr_hz_field_in_init;
--addr_offsets_array(0)   <= i_offsets_array(0)(11 downto 6);
--addr_offsets_array(1)   <= i_offsets_array(1)(11 downto 6);
--addr_offsets_array(2)   <= i_offsets_array(2)(11 downto 6);

--dword_offset_array(0)   <= i_offsets_array(0)(5 downto 0);
--dword_offset_array(1)   <= i_offsets_array(1)(5 downto 0);
--dword_offset_array(2)   <= i_offsets_array(2)(5 downto 0);

-- Scratchpad is packed the way it is read out to merge with the packet.
-- Upper byte of address 0 (511 -> 504) goes first.
-- In software this will be byte 1.
-- software will calculate the byte location of the lowest byte of 4 to update.
-- A value of 20 will mean retrieve 17,18,19,20, and increment that.
-- There is a special case , heap counter, where only 17 bits are to be incremented.
--
-- need to subtract 1 from the value calculated by the software to be useful as row and col positioning.
--

offset_align_gen : for i in 0 to (g_NUMBER_OF_OFFSETS-1) GENERATE

    offset_align_proc : process(clk)
    begin
        if rising_edge(clk) then
            offsets_array(i)        <= std_logic_vector(unsigned(i_offsets_array(i)) - 1);

            addr_offsets_array(i)   <= offsets_array(i)(11 downto 6);
            dword_offset_array(i)   <= offsets_array(i)(5 downto 0);
        end if;
    end process;

END GENERATE;

-- need to add scratchpad update done, currently using IPv4 CRC and that is faster than INIT update loop.

dword_slice_gen : for i in 3 to 63 GENERATE
    dword_slice_proc : process(clk)
    begin
        if rising_edge(clk) then
            -- 4 bytes from 64 to update.
            dword_slice(i)              <= data_b_q(((32-1)+(504-(8*i))) downto (504-(8*i)));

            -- zero out the vector then set only required bits for the bytes
            byte_en_quad(i)             <= x"0000000000000000";
            byte_en_quad(i)(63-i)       <= '1';
            byte_en_quad(i)(63-i+1)     <= '1';
            byte_en_quad(i)(63-i+2)     <= '1';
            byte_en_quad(i)(63-i+3)     <= '1';

            -- mapping the update onto the 64 byte vector.
            data_b_overlay(i)(((32-1)+(504-(8*i))) downto (504-(8*i))) <= update_scratch_value;
        end if;
    end process;
END GENERATE;

-- access to these is an error.
dword_slice(0)  <= x"DEADBEEF";
dword_slice(1)  <= x"DEADBEEF";
dword_slice(2)  <= x"DEADBEEF";

byte_en_quad(0) <= x"0000000000000000";
byte_en_quad(1) <= x"0000000000000000";
byte_en_quad(2) <= x"0000000000000000";

lkup_proc : process(clk)
begin
    if rising_edge(clk) then
        heap_counter_lkup   <= to_integer(unsigned(dword_offset_array(0)));
        vis_chan_id_lkup    <= to_integer(unsigned(dword_offset_array(1)));
        hz_lkup             <= to_integer(unsigned(dword_offset_array(2)));
    end if;
end process;

-------------------------------------------------------------------------

addr_b  <= std_logic_vector(SM_addr(5 downto 0));
            
-------------------------------------------------------------------------
-- SM side

inti_sm_proc : process(clk)
begin
    if rising_edge(clk) then
        if reset = '1' then
            init_packet_sm_debug    <= x"0F";
            init_packet_sm          <= IDLE;
            o_init_ready_to_send    <= '0';
            SM_addr                 <= x"00";
            update_scratch_value    <= (others => '0');
            init_hold_off           <= (others => '0');
            data_b_wr_int           <= (others => '0');
            data_b                  <= (others => '0');
            fp_adder_in_valid       <= '0';
            o_scratchpad_updated    <= '0';
        else
            init_hold_off   <= init_hold_off(6 downto 0) & '1';
            data_b_wr_int   <= (others => '0');

            o_scratchpad_updated    <= '0';

            case init_packet_sm is
                WHEN IDLE =>
                    init_packet_sm_debug    <= x"00";
                    pulse_count             <= x"0";
                    SM_addr                 <= x"00";
                    fp_adder_in_valid       <= '0';
                    init_freq_calc          <= (others => '0');
                    init_freq_interval      <= unsigned(i_init_freq_interval);
                    init_freq_base          <= unsigned(i_init_freq_base);
                    
                    if i_trigger_init = '1' then
                        init_packet_sm          <= INIT_UPDATE_PULSE;
                        o_init_ready_to_send    <= '1';
                    end if;

                WHEN INIT_UPDATE_PULSE =>
                    init_packet_sm_debug    <= x"01";
                    -- replacing CRC calc with pulse, need to wait for init CRC to be done.
                    if pulse_count = 15 then
                        o_scratchpad_updated    <= '1';
                        init_packet_sm          <= SEND_PACKET;
                    else
                        pulse_count <= pulse_count + 1;
                    end if;


                WHEN SEND_PACKET =>
                    init_packet_sm_debug    <= x"02";
                    -- This is held high by the SM in spead_packet.vhd and is the playout to the packet.
                    -- when this is complete, packet sent is indicated to allow update of scratchpad can occur.
                    if i_data_inc = '1' then
                        SM_addr                 <= SM_addr + 1;
                    end if;

                    -- Packet sent
                    if i_init_packet_sent = '1' then
                        init_packet_sm          <= WAIT_PACKET;
                    end if;

                WHEN WAIT_PACKET =>
                    init_packet_sm_debug    <= x"03";
                    if i_trigger_init = '0' then
                        init_packet_sm          <= COMPLETE;
                    else
                        init_packet_sm          <= VAR_1_GET;
                        init_hold_off           <= x"00";
                    end if;

                WHEN COMPLETE =>
                    init_packet_sm_debug    <= x"04";
                    init_packet_sm          <= IDLE;
                -------------------------------------------------------
                WHEN VAR_1_GET =>
                    init_packet_sm_debug    <= x"05";
                    SM_addr                 <= "00" & unsigned(addr_offsets_array(0));
                    if init_hold_off(6) = '1' then
                        init_packet_sm          <= VAR_1_UPDATE;
                        -- only need to update 17 bits of channel ID.
                        update_scratch_value    <= dword_slice(heap_counter_lkup);
                    end if;

                
                WHEN VAR_1_UPDATE =>
                    init_packet_sm_debug    <= x"06";
                    update_scratch_value(16 downto 0)  <= std_logic_vector(unsigned(update_scratch_value(16 downto 0)) + 1);
                    
                    init_packet_sm  <= VAR_1_WR;
                    init_hold_off   <= x"00";
                
                WHEN VAR_1_WR =>
                    init_packet_sm_debug    <= x"07";
                    if init_hold_off(3) = '1' then
                        data_b              <= data_b_overlay(heap_counter_lkup);
                        -- address still set
                        data_b_wr_int       <= byte_en_quad(heap_counter_lkup);
                        init_packet_sm      <= VAR_2_GET;
                    end if;
                                
                WHEN VAR_2_GET =>
                    init_packet_sm_debug    <= x"08";
                    SM_addr                     <= "00" & unsigned(addr_offsets_array(1));
                    -- we have the channel ID from above, but needs to drop the sub array and beam info
                    -- and be formated to LE.
                    update_scratch_value(31 downto 17)  <= (others => '0');
                    init_packet_sm                      <= VAR_2_UPDATE;
                
                WHEN VAR_2_UPDATE =>
                    init_packet_sm_debug    <= x"09";
                    -- LE
                    update_scratch_value    <= update_scratch_value(7 downto 0) & update_scratch_value(15 downto 8) & update_scratch_value(23 downto 16) & update_scratch_value(31 downto 24);
                    init_packet_sm          <= VAR_2_WR;
                    init_hold_off           <= x"00";
                
                WHEN VAR_2_WR =>
                    init_packet_sm_debug    <= x"0A";
                    if init_hold_off(3) = '1' then
                        data_b              <= data_b_overlay(vis_chan_id_lkup);
                        -- address still set
                        data_b_wr_int       <= byte_en_quad(vis_chan_id_lkup);
                        
                        init_packet_sm          <= VAR_3_GET;
                        init_hold_off           <= x"00";
                    end if;
                                
                WHEN VAR_3_GET =>
                    init_packet_sm_debug    <= x"0C";
                    SM_addr                 <= "00" & unsigned(addr_offsets_array(2));
                    -- USED TO GET HZ from scratch pad, now using ARGs register.
                    init_packet_sm          <= VAR_3_UPDATE;
                    
                    init_freq_calc          <= init_freq_base + init_freq_interval;
                    
                
                WHEN VAR_3_UPDATE =>
                    init_packet_sm_debug    <= x"0D";
                    -- increment frequency in the SPEED INIT region
                    init_packet_sm          <= VAR_3_UPDATED;

                    init_freq_base          <= init_freq_calc;

                    if init_freq_calc(31) = '1' then
                        update_scratch_value    <= std_logic_vector(init_freq_calc(63 downto 32) + 1);
                    else
                        update_scratch_value    <= std_logic_vector(init_freq_calc(63 downto 32));
                    end if;
                
                WHEN VAR_3_UPDATED =>
                    init_packet_sm_debug    <= x"0E";
                    -- LE
                    update_scratch_value    <= update_scratch_value(7 downto 0) & update_scratch_value(15 downto 8) & update_scratch_value(23 downto 16) & update_scratch_value(31 downto 24);
                    init_packet_sm          <= VAR_3_WR;
                    init_hold_off           <= x"00";
                
                WHEN VAR_3_WR =>
                    init_packet_sm_debug    <= x"10";
                    if init_hold_off(3) = '1' then
                        data_b              <= data_b_overlay(hz_lkup);
                        -- address still set
                        data_b_wr_int       <= byte_en_quad(hz_lkup);

                        init_hold_off           <= x"00";
                        init_packet_sm          <= VAR_COMPLETE;
                    end if;
                     
                when VAR_COMPLETE =>
                    init_packet_sm_debug    <= x"11";
                    if init_hold_off(6) = '1' then
                        init_packet_sm          <= SEND_PACKET;
                        SM_addr                 <= (others => '0');
                        o_scratchpad_updated    <= '1';
                    end if;
                                
                when OTHERS =>
                    init_packet_sm  <= IDLE;
            end case;
        end if;
    end if;
end process;

-------------------------------------------------------------------------
-- Single Precision FP adder 
-- fp_adder_a  <= update_scratch_value;
-- fp_adder_b  <= init_freq_interval;

-- adder_instance : fp_sp_spead_hertz port map ( 
--     aclk                    => clk,
--     s_axis_a_tvalid         => fp_adder_in_valid,
--     s_axis_a_tdata          => fp_adder_a,
--     s_axis_b_tvalid         => fp_adder_in_valid,
--     s_axis_b_tdata          => fp_adder_b,
--     m_axis_result_tvalid    => fp_adder_out_valid,
--     m_axis_result_tdata     => fp_adder_result
--     );

-------------------------------------------------------------------------
-- ARGs side
data_a                  <=  i_host_data & i_host_data & i_host_data & i_host_data &
                            i_host_data & i_host_data & i_host_data & i_host_data &
                            i_host_data & i_host_data & i_host_data & i_host_data &
                            i_host_data & i_host_data & i_host_data & i_host_data;

addr_a                  <= i_host_addr(9 downto 4);

data_a_wr               <= i_host_wren;

-- Data is written in lots of 32 bits with address zero going to upper section of 512 bits.
-- This is achieved with byte write enables where the defined width is 32 bits.
-- address bits will drive the WRENs.
byte_write_gen : for i in 0 to 15 generate
    signal addr_enable_map  : std_logic_vector(3 downto 0);
begin
    addr_enable_map             <= std_logic_vector(to_unsigned(i,4));
    data_a_wr_int(63-(i*4))     <= '1' when ((i_host_addr(3 downto 0) = addr_enable_map) AND (data_a_wr = '1')) else '0';
    data_a_wr_int(63-(i*4)-1)   <= '1' when ((i_host_addr(3 downto 0) = addr_enable_map) AND (data_a_wr = '1')) else '0';
    data_a_wr_int(63-(i*4)-2)   <= '1' when ((i_host_addr(3 downto 0) = addr_enable_map) AND (data_a_wr = '1')) else '0';
    data_a_wr_int(63-(i*4)-3)   <= '1' when ((i_host_addr(3 downto 0) = addr_enable_map) AND (data_a_wr = '1')) else '0';
end generate;

o_host_return_data          <= host_return_data_int;
host_return_data_int        <=  data_a_q(511 downto 480)    when i_host_addr(3 downto 0) = x"0" else
                                data_a_q(479 downto 448)    when i_host_addr(3 downto 0) = x"1" else
                                data_a_q(447 downto 416)    when i_host_addr(3 downto 0) = x"2" else
                                data_a_q(415 downto 384)    when i_host_addr(3 downto 0) = x"3" else
                                data_a_q(383 downto 352)    when i_host_addr(3 downto 0) = x"4" else
                                data_a_q(351 downto 320)    when i_host_addr(3 downto 0) = x"5" else
                                data_a_q(319 downto 288)    when i_host_addr(3 downto 0) = x"6" else
                                data_a_q(287 downto 256)    when i_host_addr(3 downto 0) = x"7" else
                                data_a_q(255 downto 224)    when i_host_addr(3 downto 0) = x"8" else
                                data_a_q(223 downto 192)    when i_host_addr(3 downto 0) = x"9" else
                                data_a_q(191 downto 160)    when i_host_addr(3 downto 0) = x"A" else
                                data_a_q(159 downto 128)    when i_host_addr(3 downto 0) = x"B" else
                                data_a_q(127 downto 96)     when i_host_addr(3 downto 0) = x"C" else
                                data_a_q(95 downto 64)      when i_host_addr(3 downto 0) = x"D" else
                                data_a_q(63 downto 32)      when i_host_addr(3 downto 0) = x"E" else
                                data_a_q(31 downto 0)       when i_host_addr(3 downto 0) = x"F";

-------------------------------------------------------------------------
generate_debug_ila : IF g_DEBUG_ILA GENERATE                              
    init_debug : ila_0 PORT MAP (
        clk                     => clk,
        probe0(63 downto 0)     => data_a_wr_int,
        probe0(95 downto 64)    => host_return_data_int,
        probe0(127 downto 96)   => i_host_data,
        probe0(133 downto 128)  => addr_a,

        probe0(143 downto 134)  => i_host_addr,

        probe0(191 downto 144)  => (others => '0')
        );
END GENERATE;

-------------------------------------------------------------------------    
clk_a   <= i_clk;
clk_b   <= i_clk;



init_scratch : xpm_memory_tdpram
    generic map (    
        -- Common module generics
        AUTO_SLEEP_TIME         => 0,              --Do not Change
        CASCADE_HEIGHT          => 0,
        CLOCKING_MODE           => "independent_clock", --string; "common_clock", "independent_clock" 
        ECC_MODE                => "no_ecc",       --string; "no_ecc", "encode_only", "decode_only" or "both_encode_and_decode" 

        MEMORY_INIT_FILE        => "init_mem_preload.mem",         --string; "none" or "<filename>.mem" 
        MEMORY_INIT_PARAM       => "",             --string;
        MEMORY_OPTIMIZATION     => "true",          --string; "true", "false" 
        MEMORY_PRIMITIVE        => "block",         --string; "auto", "distributed", "block" or "ultra" ;
        MEMORY_SIZE             => MEMORY_SIZE_GENERIC, --262144,          -- Total memory size in bits; 512 x 512 = 262144
        MESSAGE_CONTROL         => 0,              --integer; 0,1

        USE_MEM_INIT            => 0,              --integer; 0,1
        WAKEUP_TIME             => "disable_sleep",--string; "disable_sleep" or "use_sleep_pin" 
        USE_EMBEDDED_CONSTRAINT => 0,              --integer: 0,1
       
    
        RST_MODE_A              => "SYNC",   
        RST_MODE_B              => "SYNC", 
        WRITE_MODE_A            => "no_change",    --string; "write_first", "read_first", "no_change" 
        WRITE_MODE_B            => "no_change",    --string; "write_first", "read_first", "no_change" 

        -- Port A module generics ... ARGs side
        READ_DATA_WIDTH_A       => g_D_Q_WIDTH,    
        READ_LATENCY_A          => 3,              
        READ_RESET_VALUE_A      => "0",            

        WRITE_DATA_WIDTH_A      => g_D_Q_WIDTH,
        BYTE_WRITE_WIDTH_A      => g_BYTE_ENABLE_WIDTH,
        ADDR_WIDTH_A            => g_NO_OF_ADDR_BITS,
    
        -- Port B module generics
        READ_DATA_WIDTH_B       => g_D_Q_WIDTH,
        READ_LATENCY_B          => 3,           
        READ_RESET_VALUE_B      => "0",         

        WRITE_DATA_WIDTH_B      => g_D_Q_WIDTH,
        BYTE_WRITE_WIDTH_B      => g_BYTE_ENABLE_WIDTH,             
        ADDR_WIDTH_B            => g_NO_OF_ADDR_BITS
        )
    port map (
        -- Common module ports
        sleep                   => '0',
        -- Port A side
        clka                    => clk_a,  -- clock from the 100GE core; 322 MHz
        rsta                    => '0',
        ena                     => '1',
        regcea                  => '1',

        wea                     => data_a_wr_int,
        addra                   => addr_a,
        dina                    => data_a,
        douta                   => data_a_q,

        -- Port B side
        clkb                    => clk_b,  -- This goes to a dual clock fifo to meet the external interface clock to connect to the HBM at 300 MHz.
        rstb                    => '0',
        enb                     => '1',
        regceb                  => '1',

        web                     => data_b_wr_int,
        addrb                   => addr_b,
        dinb                    => data_b,
        doutb                   => data_b_q,

        -- other features
        injectsbiterra          => '0',
        injectdbiterra          => '0',
        injectsbiterrb          => '0',
        injectdbiterrb          => '0',        
        sbiterra                => open,
        dbiterra                => open,
        sbiterrb                => open,
        dbiterrb                => open
    );      

end Behavioral;
