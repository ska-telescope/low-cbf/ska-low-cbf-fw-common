LIBRARY ieee;
USE ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

PACKAGE spead_packet_pkg IS

constant c_spead_sdp_data_hdr                           : integer   := 64;                          -- header (64)
constant c_spead_sdp_data_hdr_no_epoch                  : integer   := 48;                          -- header (48)
constant c_spead_sdp_data_hdr_w_data_in_heap_bytes      : integer   := c_spead_sdp_data_hdr + 8 ;   -- header (64) + pointers in heap, pre visibilities (8).
constant c_spead_sdp_end_hdr                            : integer   := 48;      
constant c_spead_sdp_init_hdr                           : integer   := 2521;    

-- These fields are the same across all 3 packet types and are at the start.
-- from spead.py in low-cbf-model repo
TYPE spead_common_fields is RECORD
    spead_magic                 : std_logic_vector(7 downto 0);
    spead_version               : std_logic_vector(7 downto 0);
    spead_item_pointer_width    : std_logic_vector(7 downto 0);
    spead_heap_add_width        : std_logic_vector(7 downto 0);
    spead_reserved              : std_logic_vector(15 downto 0);
END RECORD;

-- start packet now dealt with by software largely, some runtime fiedls updated by logic.
TYPE spead_start_packet IS RECORD                                               -- Bytes
    -- common spead headers between packet types with static values
    spead_common                : spead_common_fields;                          -- 6
    -- common spead headers between packet types with runtime values
    -- number of items is sum of header fields.
    number_of_items             : std_logic_vector(15 downto 0);                -- 2
    heap_count_header           : std_logic_vector(15 downto 0);                -- 2
    subarray_beam_freq          : std_logic_vector(31 downto 0);                -- 4
    integration_id              : std_logic_vector(15 downto 0);                -- 2
    heap_size_header            : std_logic_vector(15 downto 0);                -- 2
    reserved_size               : std_logic_vector(15 downto 0);                -- 2
    heap_size                   : std_logic_vector(31 downto 0);                -- 4
    heap_offset_header          : std_logic_vector(15 downto 0);                -- 2
    reserved_offset             : std_logic_vector(15 downto 0);                -- 2
    offset_size                 : std_logic_vector(31 downto 0);                -- 4
    heap_payload_header         : std_logic_vector(15 downto 0);                -- 2
    reserved_payload            : std_logic_vector(15 downto 0);                -- 2
    payload_size                : std_logic_vector(31 downto 0);                -- 4 = 40
    -- descriptors

    -- static
    stream_control_header       : std_logic_vector(15 downto 0);                -- 2
    reserved_control            : std_logic_vector(15 downto 0);                -- 2
    stream_control              : std_logic_vector(31 downto 0);                -- 4
END RECORD;

-- This is in flux as of 22 Feb 2023
TYPE spead_data_packet IS RECORD                                                -- Bytes
    -- common spead headers between packet types with static values
    spead_common                : spead_common_fields;                          -- 6
    -- common spead headers between packet types with runtime values
    -- number of items is sum of header fields.
    number_of_items             : std_logic_vector(15 downto 0);                -- 2

    heap_count_header           : std_logic_vector(15 downto 0);                -- 2
    subarray_beam_freq          : std_logic_vector(31 downto 0);                -- 4    << is updated at runtime.
    integration_id              : std_logic_vector(15 downto 0);                -- 2

    heap_size_header            : std_logic_vector(15 downto 0);                -- 2
    reserved_size               : std_logic_vector(15 downto 0);                -- 2
    heap_size                   : std_logic_vector(31 downto 0);                -- 4    << is updated at runtime.

    heap_offset_header          : std_logic_vector(15 downto 0);                -- 2
    reserved_offset             : std_logic_vector(15 downto 0);                -- 2
    offset_size                 : std_logic_vector(31 downto 0);                -- 4    << is updated at runtime.

    heap_payload_header         : std_logic_vector(15 downto 0);                -- 2
    reserved_payload            : std_logic_vector(15 downto 0);                -- 2
    payload_size                : std_logic_vector(31 downto 0);                -- 4    << is updated at runtime.
                                                                                --    = 40
    Epoch_offset_header         : std_logic_vector(15 downto 0);                -- 2
    reserved_Epoch_offset       : std_logic_vector(15 downto 0);                -- 2
    Epoch_offset                : std_logic_vector(31 downto 0);                -- 4    << This is constant, always the first 8 bytes of the HEAP, so 0
                                                                                --    = 48
    vis_flags_header            : std_logic_vector(15 downto 0);                -- 2
    reserved_vis_flags          : std_logic_vector(39 downto 0);                -- 5
    vis_flags                   : std_logic_vector(7 downto 0);                 -- 1    << is updated at runtime.
    -- Correlator Data Header                                                         = 56
    corr_data_header            : std_logic_vector(15 downto 0);                -- 2
    reserved_corr_data          : std_logic_vector(15 downto 0);                -- 2
    corr_data                   : std_logic_vector(31 downto 0);                -- 4    << This is constant, always after the first 8 bytes of the HEAP, so 8
                                                                                --    = 64
END RECORD;

TYPE spead_data_packet_no_epoch IS RECORD                                                -- Bytes
    -- common spead headers between packet types with static values
    spead_common                : spead_common_fields;                          -- 6
    -- common spead headers between packet types with runtime values
    -- number of items is sum of header fields.
    number_of_items             : std_logic_vector(15 downto 0);                -- 2

    heap_count_header           : std_logic_vector(15 downto 0);                -- 2
    subarray_beam_freq          : std_logic_vector(31 downto 0);                -- 4    << is updated at runtime.
    integration_id              : std_logic_vector(15 downto 0);                -- 2

    heap_size_header            : std_logic_vector(15 downto 0);                -- 2
    reserved_size               : std_logic_vector(15 downto 0);                -- 2
    heap_size                   : std_logic_vector(31 downto 0);                -- 4    << is updated at runtime.

    heap_offset_header          : std_logic_vector(15 downto 0);                -- 2
    reserved_offset             : std_logic_vector(15 downto 0);                -- 2
    offset_size                 : std_logic_vector(31 downto 0);                -- 4    << is updated at runtime.

    heap_payload_header         : std_logic_vector(15 downto 0);                -- 2
    reserved_payload            : std_logic_vector(15 downto 0);                -- 2
    payload_size                : std_logic_vector(31 downto 0);                -- 4    << is updated at runtime.
    -- Correlator Data Header                                                         = 40
    corr_data_header            : std_logic_vector(15 downto 0);                -- 2
    reserved_corr_data          : std_logic_vector(15 downto 0);                -- 2
    corr_data                   : std_logic_vector(31 downto 0);                -- 4    << This is constant, always after the first 8 bytes of the HEAP, so 8
                                                                                --    = 56
END RECORD;

TYPE sdp_spead_end_packet IS RECORD                                             -- Bytes
    -- common spead headers between packet types with static values
    spead_common                : spead_common_fields;                          -- 6
    -- common spead headers between packet types with runtime values
    -- number of items is sum of header fields.
    number_of_items             : std_logic_vector(15 downto 0);                -- 2
    heap_count_header           : std_logic_vector(15 downto 0);                -- 2
    subarray_beam_freq          : std_logic_vector(31 downto 0);                -- 4    << is updated at runtime.
    integration_id              : std_logic_vector(15 downto 0);                -- 2
    heap_size_header            : std_logic_vector(15 downto 0);                -- 2    << 6001
    reserved_size               : std_logic_vector(15 downto 0);                -- 2
    heap_size                   : std_logic_vector(31 downto 0);                -- 4
    heap_offset_header          : std_logic_vector(15 downto 0);                -- 2
    reserved_offset             : std_logic_vector(15 downto 0);                -- 2
    offset_size                 : std_logic_vector(31 downto 0);                -- 4
    heap_payload_header         : std_logic_vector(15 downto 0);                -- 2
    reserved_payload            : std_logic_vector(15 downto 0);                -- 2
    payload_size                : std_logic_vector(31 downto 0);                -- 4 = 40

    -- static
    stream_control_header       : std_logic_vector(15 downto 0);                -- 2
    reserved_control            : std_logic_vector(15 downto 0);                -- 2
    stream_control              : std_logic_vector(31 downto 0);                -- 4

END RECORD;

-- Headers use the MSB to indicate if the descriptor information 
-- Immediately follows the header   (0xEXXX) or (0x8XXX)
-- or
-- Is embedded down in the heap.    (0X6XXX), where the data following the header is the offset into the heap.
-- There are mandatory fields and the data in these fields are Big Endian.
constant c_sdp_heap_count_header        : std_logic_vector(15 downto 0)     := x"8001";     
constant c_sdp_heap_size_header         : std_logic_vector(15 downto 0)     := x"8002";
constant c_sdp_heap_offset_header       : std_logic_vector(15 downto 0)     := x"8003";
constant c_sdp_heap_payload_header      : std_logic_vector(15 downto 0)     := x"8004";

-- there are optional fields and the data in these descriptors are Little-Endian.
constant c_sdp_epoch_offset_header      : std_logic_vector(15 downto 0)     := x"6014";

constant c_sdp_visibility_flags         : std_logic_vector(15 downto 0)     := x"E016";

-- correlator output data is assumed BE?
constant c_sdp_correlator_data_header   : std_logic_vector(15 downto 0)     := x"600A";

constant c_sdp_stream_control_header    : std_logic_vector(15 downto 0)     := x"8006";

constant default_spead_common_fields : spead_common_fields :=   (
                                                                spead_magic                 => x"53",
                                                                spead_version               => x"04",
                                                                spead_item_pointer_width    => x"02",
                                                                spead_heap_add_width        => x"06",
                                                                spead_reserved              => x"0000"
                                                                );

constant default_sdp_spead_data_packet : spead_data_packet :=   (
                                                            spead_common            => default_spead_common_fields,
                                                            number_of_items         => x"0007",         -- 4 mandatory + 3 (epoch + flags + corr_data)
                                                            heap_count_header       => c_sdp_heap_count_header,
                                                            subarray_beam_freq      => x"00000001",
                                                            integration_id          => x"0001",
                                                            heap_size_header        => c_sdp_heap_size_header,
                                                            reserved_size           => x"0000",
                                                            heap_size               => x"00000360",
                                                            heap_offset_header      => c_sdp_heap_offset_header,
                                                            reserved_offset         => x"0000",
                                                            offset_size             => x"00000000",
                                                            heap_payload_header     => c_sdp_heap_payload_header,
                                                            reserved_payload        => x"0000",
                                                            payload_size            => x"00000360",
                                                                                                
                                                            epoch_offset_header     => c_sdp_epoch_offset_header,
                                                            reserved_epoch_offset   => x"0000",
                                                            epoch_offset            => x"00000000",

                                                            vis_flags_header        => c_sdp_visibility_flags,
                                                            reserved_vis_flags      => (others => '0'),
                                                            vis_flags               => (others => '0'),

                                                            corr_data_header        => c_sdp_correlator_data_header,
                                                            reserved_corr_data      => x"0000",
                                                            corr_data               => x"00000008"
                                                            );

-- this construct not used currently
constant default_sdp_spead_data_packet_no_epoch : spead_data_packet_no_epoch :=   (
                                                            spead_common            => default_spead_common_fields,
                                                            number_of_items         => x"0006",         -- 4 mandatory + 2 (epoch + corr_data)
                                                            heap_count_header       => c_sdp_heap_count_header,
                                                            subarray_beam_freq      => x"00000001",
                                                            integration_id          => x"0001",
                                                            heap_size_header        => c_sdp_heap_size_header,
                                                            reserved_size           => x"0000",
                                                            heap_size               => x"00000360",
                                                            heap_offset_header      => c_sdp_heap_offset_header,
                                                            reserved_offset         => x"0000",
                                                            offset_size             => x"00000000",
                                                            heap_payload_header     => c_sdp_heap_payload_header,
                                                            reserved_payload        => x"0000",
                                                            payload_size            => x"00000360",

                                                            corr_data_header        => c_sdp_correlator_data_header,
                                                            reserved_corr_data      => x"0000",
                                                            corr_data               => x"00000000"
                                                            );                                                            

constant default_sdp_spead_end_packet : sdp_spead_end_packet := (
                                                            spead_common            => default_spead_common_fields,
                                                            number_of_items         => x"0005",         -- 4 mandatory + 1
                                                            heap_count_header       => c_sdp_heap_count_header,
                                                            subarray_beam_freq      => x"00000000",
                                                            integration_id          => x"0001",
                                                            heap_size_header        => c_sdp_heap_size_header,
                                                            reserved_size           => x"0000",
                                                            heap_size               => x"00000000",
                                                            heap_offset_header      => c_sdp_heap_offset_header,
                                                            reserved_offset         => x"0000",
                                                            offset_size             => x"00000000",
                                                            heap_payload_header     => c_sdp_heap_payload_header,
                                                            reserved_payload        => x"0000",
                                                            payload_size            => x"00000000",
                                                                                                
                                                            stream_control_header   => c_sdp_stream_control_header,
                                                            reserved_control        => x"0000",
                                                            stream_control          => x"00000002"
                                                            );


TYPE spead_to_hbm_bus is RECORD
    spead_data_rd           : std_logic;                        -- FWFT FIFO
    enabled_array           : std_logic_vector(7 downto 0);     -- not used in current control scheme.
    bytes_in_heap           : std_logic_vector(31 downto 0);    -- (4096*4097*34/2) = 268500992 (29 bits required)
    spead_data_heap_size    : std_logic_vector(15 downto 0);    -- size of data chunks in a data packet.
    end_packets_complete    : std_logic;                        -- pulse
    current_page_ct1        : std_logic;                        -- from CT1 to packetiser and passed to HBM for initial value of page on power up.
END RECORD;

TYPE hbm_to_spead_bus is RECORD
    spead_data              : std_logic_vector(511 downto 0);
    current_array           : std_logic_vector(7 downto 0);     -- max of 16 zooms x 8 sub arrays = 128, zero-based.
    spead_data_rdy          : std_logic;
    spead_data_pending      : std_logic;                        -- indicate that more data is pending on the current instruction.
    hbm_readout_complete    : std_logic;                        -- indicate the last of potentially several data packets sent.
    byte_count              : std_logic_vector(13 downto 0);
    freq_index              : std_logic_vector(16 downto 0);
    time_ref                : std_logic_vector(63 downto 0);
    valid_del_poly          : std_logic;
    statically_flagged      : std_logic;
    dynamically_flagged     : std_logic;
    table_select            : std_logic;                        -- page flip selection bit for config in SPEAD packetiser that reflects the current config from earlier in the correlator.
    trigger_end_packets     : std_logic;                        -- a change has been detected on the table select, send END packets then pass through the updated signal.
END RECORD;

type t_hbm_to_spead_bus_array is array (INTEGER RANGE <>) of hbm_to_spead_bus;
type t_spead_to_hbm_bus_array is array (INTEGER RANGE <>) of spead_to_hbm_bus;

end spead_packet_pkg;