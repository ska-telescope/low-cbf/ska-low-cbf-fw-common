----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 20/02/2023 04:22:23 PM
-- Design Name: 
-- Module Name: spead_registers
--  
-- Description: 
--      Map ARGs interface to signals and records.
--      Initial design intent for 48 subarrays.
--
-- IPv4 checksum circuit with 4 cycle turn around to accomodate the dynamic nature of SPEAD.
----------------------------------------------------------------------------------

library IEEE, correlator_lib, common_lib, signal_processing_common, spead_lib, ethernet_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;
USE common_lib.common_pkg.ALL;
use ethernet_lib.ethernet_pkg.ALL;
use spead_lib.spead_spead_sdp_reg_pkg.ALL;
use spead_lib.spead_packet_pkg.ALL;


entity spead_registers is
    Generic (
        g_DEBUG_VEC_SIZE        : INTEGER := 1;
        g_DEBUG_ILA             : BOOLEAN := FALSE
    );
    Port ( 
        -- clock used for all data input and output from this module (300 MHz)
        i_clk                           : in std_logic;
        i_rst                           : in std_logic;
        i_table_swap_in_progress        : in std_logic;
        i_packetiser_table_select       : in std_logic;
        -- Signals
        i_ipv4_total_bytes_to_calc      : in std_logic_vector(15 downto 0);
        i_udp_total_bytes               : in std_logic_vector(15 downto 0);
        o_spead_data_heap_size          : out std_logic_vector(15 downto 0);
        i_packet_calc                   : in std_logic;
        o_packet_calc_done              : out std_logic;
        o_scratchpad_updated            : out std_logic;

        i_to_spead_pack                 : in hbm_to_spead_bus;

        o_epoch_offset_done             : out std_logic;

        o_init_govern                   : out std_logic_vector(7 downto 0);
        o_end_govern                    : out std_logic_vector(7 downto 0);
        o_data_govern                   : out std_logic_vector(7 downto 0);

        o_page_flip_in_progress         : out std_logic;
        o_end_sequence_complete         : out std_logic;

        -- packet signaling
        i_data_packet_sending           : in std_logic;
        i_data_packet_sent              : in std_logic;

        i_end_packet_sending            : in std_logic_vector(7 downto 0);
        o_end_packet_ready              : out std_logic;
        i_end_packet_sent               : in std_logic;

        i_init_packet_sending           : in std_logic_vector(7 downto 0);
        o_init_packet_ready             : out std_logic;
        i_init_packet_sent              : in std_logic;

        -- return packet structure
        o_current_ipv4                  : out IPv4_header;
        o_current_ethernet              : out ethernet_frame;
        o_current_udp                   : out UDP_header;    
        o_spead_data_packet             : out spead_data_packet;
        o_epoch_offset_le               : out std_logic_vector(63 downto 0);

        o_init_packet_data              : out std_logic_vector(511 downto 0);
        i_init_packet_next              : in std_logic;
        o_init_packet_byte_length       : out std_logic_vector(13 downto 0);
        i_INIT_running_packet_fsm       : in std_logic;

        o_enable_packetiser             : out std_logic;

        -- debug circuits
        i_spead_sm_fsm_debug            : in std_logic_vector(3 downto 0);

        -- debug vector
        i_debug                         : in std_logic_vector((g_DEBUG_VEC_SIZE-1) downto 0);

        -- ARGs
        -- Interface for correlator instance #1
        i_spead_lite_axi_mosi           : in t_axi4_lite_mosi; 
        o_spead_lite_axi_miso           : out t_axi4_lite_miso;
        i_spead_full_axi_mosi           : in  t_axi4_full_mosi;
        o_spead_full_axi_miso           : out t_axi4_full_miso

    );
end spead_registers;

architecture Behavioral of spead_registers is

COMPONENT ila_0
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
END COMPONENT;

COMPONENT mult_fixed_849ms_in_ns is
    Port ( 
      CLK   : in STD_LOGIC;
      A     : in STD_LOGIC_VECTOR ( 31 downto 0 );
      P     : out STD_LOGIC_VECTOR ( 61 downto 0 )
    );
END COMPONENT;

constant c_last_sample_int1_283ms   : unsigned(31 downto 0) := 32D"280902600";  -- (847133640) - 64*4096*1080*2
constant c_step_for_283ms           : unsigned(31 downto 0) := 32D"283115520";  -- 64*4096*1080*1
constant c_last_sample_849ms        : unsigned(31 downto 0) := 32D"847133640";

constant dest_udp_one           : STRING := "dest_udp_preload_one.mem";
constant dest_udp_two           : STRING := "dest_udp_preload_two.mem";

constant no_of_freq_chan_one    : STRING := "no_of_freq_chan_preload_one.mem";
constant no_of_freq_chan_two    : STRING := "no_of_freq_chan_preload_two.mem";

TYPE t_mem_init_arr IS ARRAY (INTEGER RANGE <>) OF STRING;

constant dest_udp_init          : t_mem_init_arr(1 downto 0) := (dest_udp_one, dest_udp_two);
constant no_of_freq_chan_init   : t_mem_init_arr(1 downto 0) := (no_of_freq_chan_one, no_of_freq_chan_two);

signal clk                      : std_logic;
signal reset                    : std_logic;
signal reset_n                  : std_logic;

signal bytes_to_transmit        : STD_LOGIC_VECTOR(13 downto 0);     
signal data_to_player           : STD_LOGIC_VECTOR(511 downto 0);
signal data_to_player_wr        : STD_LOGIC;
signal data_to_player_rdy       : STD_LOGIC;


signal spead_ctrl_reg           : t_spead_ctrl_rw;
signal spead_ctrl_status        : t_spead_ctrl_ro;

signal i_list_subarrays_ending  : t_spead_ctrl_list_subarrays_ending_ram_in;
signal o_list_subarrays_ending  : t_spead_ctrl_list_subarrays_ending_ram_out;

-----------------------------------
signal enable_packetiser        : std_logic     := '1';


signal current_ethernet         : ethernet_frame            := null_ethernet_frame;
signal current_ipv4             : IPv4_header               := null_ipv4_header;
signal current_udp              : UDP_header                := null_UDP_header;

signal spead_data_header        : spead_data_packet         := default_sdp_spead_data_packet;

signal ipv4_chk_sum_calc        : std_logic_vector(31 downto 0);
signal ipv4_chk_sum_base        : std_logic_vector(31 downto 0);

signal ipv4_dst_addr_chk        : unsigned(16 downto 0)     := (others => '0');
signal ipv4_src_addr_chk        : unsigned(16 downto 0)     := (others => '0');

type ipv4_chksum_sm_fsm is      (
                                IDLE, 
                                CALC_1, 
                                WAIT_PACKET, 
                                CALC_2,
                                CALC_3,
                                READY,
                                HOLDING
                                );
signal ipv4_chksum_sm : ipv4_chksum_sm_fsm;

signal ipv4_chksum_sm_cnt       : unsigned(3 downto 0);

signal ipv4_chksum_sm_fsm_debug : std_logic_vector(3 downto 0)  := x"0";

signal reset_packetiser_value   : std_logic;

signal packet_calc_done         : std_logic;
----------------------------------------------------------------------
-- memory maps.
signal sub_array_data   : std_logic_vector(31 downto 0);
signal sub_array_addr   : std_logic_vector(7 downto 0);
signal sub_array_wr     : std_logic;
signal sub_array_dout   : std_logic_vector(31 downto 0);

----------------------------------------------------------------------
-- static config ports, will be replaced with dynamic allocation.
signal udp_destination_port         : unsigned(15 downto 0);

signal udp_dest_port_init_end       : unsigned(15 downto 0);
signal udp_dest_port_data           : unsigned(15 downto 0);

signal current_freq_sub_beam        : unsigned(16 downto 0);

signal integration_id               : unsigned(15 downto 0);

signal heap_counter_chanel_id       : std_logic_vector(16 downto 0);
signal heap_counter_data_calc       : unsigned(16 downto 0);
signal heap_counter_ie_calc         : unsigned(16 downto 0);
----------------------------------------------------------------------
-- stats
signal reset_stats                  : std_logic;
signal no_of_spead_start_packets    : unsigned(31 downto 0)         := (others => '0');
signal no_of_spead_data_packets     : unsigned(31 downto 0)         := (others => '0');
signal no_of_spead_end_packets      : unsigned(31 downto 0)         := (others => '0');
signal no_of_packets_sent           : unsigned(31 downto 0)         := (others => '0');

signal epoch_offset                 : std_logic_vector(63 downto 0);
signal epoch_offset_le              : std_logic_vector(63 downto 0);

----------------------------------------------------------------------
-- ARGs mappings.
signal bram_rst                 : STD_LOGIC;
signal bram_clk                 : STD_LOGIC;
signal bram_en                  : STD_LOGIC;
signal bram_we                  : STD_LOGIC_VECTOR(3 DOWNTO 0);
signal bram_addr                : STD_LOGIC_VECTOR(15 DOWNTO 0);
signal bram_wrdata              : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal bram_rddata              : STD_LOGIC_VECTOR(31 DOWNTO 0);

signal args_addr                : STD_LOGIC_VECTOR(13 DOWNTO 0);

signal bram_addr_d1             : std_logic_vector(6 downto 0);
signal bram_addr_d2             : std_logic_vector(6 downto 0);

signal destination_ip_data      : std_logic_vector(31 downto 0);
signal destination_ip_addr      : std_logic_vector(10 downto 0);

signal destination_udp_data     : std_logic_vector(31 downto 0);
signal destination_udp_addr     : std_logic_vector(10 downto 0);

signal heap_counter_data        : std_logic_vector(31 downto 0);
signal heap_counter_addr        : std_logic_vector(10 downto 0);

signal heap_size_data           : std_logic_vector(31 downto 0);
signal heap_size_addr           : std_logic_vector(6 downto 0);

signal block_size_data          : std_logic_vector(15 downto 0);
signal block_size_addr          : std_logic_vector(6 downto 0);

signal no_of_freq_chan_data     : std_logic_vector(31 downto 0);
signal no_of_freq_chan_addr     : std_logic_vector(6 downto 0);

------------------------------
-- instance for page flipping
CONSTANT CONFIG_MEM_PAGES       : integer := 2;

signal destination_ip_data_p    : t_slv_32_arr((CONFIG_MEM_PAGES-1) downto 0);
signal destination_ip_return_p  : t_slv_32_arr((CONFIG_MEM_PAGES-1) downto 0);
signal destination_ip_wren_p    : std_logic_vector(1 downto 0);

signal destination_udp_data_p   : t_slv_32_arr((CONFIG_MEM_PAGES-1) downto 0);
signal destination_udp_return_p : t_slv_32_arr((CONFIG_MEM_PAGES-1) downto 0);
signal destination_udp_wren_p   : std_logic_vector(1 downto 0);

signal heap_counter_data_p      : t_slv_32_arr((CONFIG_MEM_PAGES-1) downto 0);
signal heap_counter_return_p    : t_slv_32_arr((CONFIG_MEM_PAGES-1) downto 0);
signal heap_counter_wren_p      : std_logic_vector(1 downto 0);

signal heap_size_data_p         : t_slv_32_arr((CONFIG_MEM_PAGES-1) downto 0);
signal heap_size_return_p       : t_slv_32_arr((CONFIG_MEM_PAGES-1) downto 0);
signal heap_size_wren_p         : std_logic_vector(1 downto 0);

signal block_size_data_p        : t_slv_16_arr((CONFIG_MEM_PAGES-1) downto 0);
signal block_size_return_p      : t_slv_16_arr((CONFIG_MEM_PAGES-1) downto 0);
signal block_size_wren_p        : std_logic_vector(1 downto 0);

signal no_of_freq_chan_data_p   : t_slv_32_arr((CONFIG_MEM_PAGES-1) downto 0);
signal no_of_freq_chan_return_p : t_slv_32_arr((CONFIG_MEM_PAGES-1) downto 0);
signal no_of_freq_chan_wren_p   : std_logic_vector(1 downto 0);
----------------------------

signal init_packet_return       : std_logic_vector(31 downto 0);
signal init_packet_wren         : std_logic;

signal init_packet_data         : std_logic_vector(511 downto 0);
signal init_packet_next         : std_logic;

constant C_NUMBER_OF_OFFSETS    : INTEGER := 3;
signal offsets_array            : t_slv_16_arr((C_NUMBER_OF_OFFSETS-1) downto 0);

signal trigger_end              : std_logic := '0';
signal trigger_init             : std_logic := '0';
signal init_sending             : std_logic;
signal init_ready_to_send       : std_logic;

signal subarraybeam_addr_lkup   : std_logic_vector(6 downto 0);
signal block_step_addr_lkup     : std_logic_vector(3 downto 0);

signal data_packet_sending      : std_logic_vector(1 downto 0);
signal spead_trigger            : std_logic_vector(7 downto 0);
signal end_packet_trig          : std_logic;
signal end_packet_trig_d        : std_logic;
signal end_packet_ready_int     : std_logic;
signal end_packet_ready_int_d   : std_logic := '0';

signal init_packet_trig         : std_logic;
signal init_packet_trig_d       : std_logic;
signal init_packet_ready_int    : std_logic;
signal init_packet_ready_int_d  : std_logic := '0';

signal init_sm_trigger          : std_logic;

signal end_init_track_count     : unsigned(16 downto 0);

signal init_complete            : std_logic;
signal end_complete             : std_logic;
signal end_manual_complete      : std_logic;

signal init_freq_interval       : std_logic_vector(63 downto 0);
signal init_freq_base           : std_logic_vector(63 downto 0);

signal hertz_is_float_int       : std_logic;

signal multi_a                  : std_logic_vector(31 downto 0);
signal multi_q                  : std_logic_vector(61 downto 0);
signal epoch_final              : std_logic_vector(63 downto 0);
signal epoch_count              : unsigned(3 downto 0);
signal epoch_offset_done        : std_logic;

type epoch_sm_fsm is      (
                            IDLE, 
                            WAIT_MULTI, 
                            ADD_283_OFFSET,
                            ADD_LAST_OFFSET
                            );
signal epoch_sm : epoch_sm_fsm;

signal current_subarray         : std_logic_vector(7 downto 0);
signal byte_count_from_cor      : std_logic_vector(13 downto 0);
signal freq_index               : std_logic_vector(16 downto 0);
signal time_ref                 : std_logic_vector(63 downto 0);
signal spead_data_pending       : std_logic;
signal hbm_readout_complete     : std_logic;

signal offset_size_calc         : unsigned(31 downto 0);
signal offset_size_tracker      : unsigned(31 downto 0);

signal spead_data_heap_size     : std_logic_vector(15 downto 0);
signal spead_data_heap_size_int : std_logic_vector(15 downto 0);

signal current_table            : std_logic;
signal current_table_ram        : std_logic;
signal trigger_end_proc         : std_logic_vector(3 downto 0);
signal page_flip_in_progress    : std_logic;
signal trigger_end_packets      : std_logic_vector(1 downto 0);

signal end_packet_arr_adr       : unsigned(6 downto 0);
signal end_packet_arr_q         : std_logic_vector(7 downto 0);
signal end_packet_arr_q_val     : std_logic;

signal end_sequence_complete    : std_logic;

signal end_seq_counter          : unsigned(3 downto 0) := (others => '0');
signal end_subarray_lkup        : std_logic_vector(6 downto 0)  := (others => '0');

signal table_swap_in_progress_del   : std_logic_vector(1 downto 0); 
signal page_flip_active_from_ct1    : std_logic := '0';
signal packetiser_table_select      : std_logic;

type end_packet_sm_fsm is   (
                            IDLE, 
                            SETUP, 
                            -- RD_ARRAY,
                            -- CHK_VALUE,
                            START_END,
                            SUBARRAY_ENDED,
                            PROCESS_COMPLETE
                            );
signal end_packet_sm : end_packet_sm_fsm;

signal init_packet_sm_debug     : std_logic_vector(7 downto 0);

signal last_track_count_zero    : std_logic;

begin

------------------------------------------------------------------------------
-- PORT MAPPINGS
clk                         <= i_clk;
reset                       <= i_rst;
reset_n                     <= NOT i_rst;

current_subarray            <= i_to_spead_pack.current_array;
byte_count_from_cor         <= i_to_spead_pack.byte_count;
freq_index                  <= i_to_spead_pack.freq_index;
time_ref                    <= i_to_spead_pack.time_ref;
spead_data_pending          <= i_to_spead_pack.spead_data_pending;
hbm_readout_complete        <= i_to_spead_pack.hbm_readout_complete;

current_freq_sub_beam       <= unsigned(freq_index);

o_current_ipv4              <= current_ipv4;
o_current_ethernet          <= current_ethernet;
o_current_udp               <= current_udp;
o_spead_data_packet         <= spead_data_header;

o_enable_packetiser         <= enable_packetiser;
o_packet_calc_done          <= packet_calc_done;

o_epoch_offset_le           <= epoch_offset_le;

o_end_packet_ready          <= end_packet_ready_int;
o_init_packet_ready         <= init_packet_ready_int;

o_init_packet_data          <= init_packet_data;
init_packet_next            <= i_init_packet_next;

o_init_packet_byte_length   <= spead_ctrl_reg.spead_init_byte_count(13 downto 0);

o_epoch_offset_done         <= epoch_offset_done;
o_spead_data_heap_size      <= spead_data_heap_size;

o_end_sequence_complete     <= end_sequence_complete;

------------------------------------------------------------------------------
-- Register mappings.
reg_args_mapping_proc : process(clk)
begin
    if rising_edge(clk) then
        reset_stats                                     <= spead_ctrl_reg.reset_packetiser_stats OR i_debug(0);
        reset_packetiser_value                          <= spead_ctrl_reg.reset_packetiser_value OR i_debug(1);
        spead_trigger(0)                                <= spead_ctrl_reg.trigger_packet(0)      OR i_debug(2);
        spead_trigger(1)                                <= spead_ctrl_reg.trigger_packet(1)      OR i_debug(3);
        spead_trigger(2)                                <= spead_ctrl_reg.trigger_packet(2)      OR i_debug(4);
        spead_trigger(3)                                <= spead_ctrl_reg.trigger_packet(3)      OR i_debug(5);
        spead_trigger(4)                                <= spead_ctrl_reg.trigger_packet(4)      OR i_debug(6);
        spead_trigger(5)                                <= spead_ctrl_reg.trigger_packet(5)      OR i_debug(7);
        spead_trigger(6)                                <= spead_ctrl_reg.trigger_packet(6)      OR i_debug(8);
        spead_trigger(7)                                <= spead_ctrl_reg.trigger_packet(7)      OR i_debug(9);
        enable_packetiser                               <= spead_ctrl_reg.enable_packetiser      OR i_debug(10);
        
        offsets_array(0)                                <= spead_ctrl_reg.ptr_heap_counter_in_init;
        offsets_array(1)                                <= spead_ctrl_reg.ptr_vis_chan_id_in_init;
        offsets_array(2)                                <= spead_ctrl_reg.ptr_hz_field_in_init;
        
        o_init_govern                                   <= spead_ctrl_reg.spead_init_pad_cycles;
        o_end_govern                                    <= spead_ctrl_reg.spead_end_pad_cycles;
        o_data_govern                                   <= spead_ctrl_reg.spead_data_pad_cycles;
    end if;
end process;

init_freq_interval                              <= spead_ctrl_reg.spead_init_hz_incr_int & spead_ctrl_reg.spead_init_hz_incr_frac;
init_freq_base                                  <= spead_ctrl_reg.spead_init_hz_base_int & spead_ctrl_reg.spead_init_hz_base_frac;
hertz_is_float_int                              <= spead_ctrl_reg.hertz_int_float;

spead_data_heap_size                            <= spead_ctrl_reg.spead_data_heap_size(15 downto 0);

spead_ctrl_status.no_of_spead_start_packets     <= std_logic_vector(no_of_spead_start_packets);
spead_ctrl_status.no_of_spead_data_packets      <= std_logic_vector(no_of_spead_data_packets);
spead_ctrl_status.no_of_spead_end_packets       <= std_logic_vector(no_of_spead_end_packets);
spead_ctrl_status.no_of_packets_sent            <= std_logic_vector(no_of_packets_sent);

spead_ctrl_status.debug_ipv4_chksum_sm_fsm_debug    <= ipv4_chksum_sm_fsm_debug;
spead_ctrl_status.debug_spead_sm_fsm                <= i_spead_sm_fsm_debug;

spead_ctrl_status.spead_init_end_status(0)      <= init_complete AND spead_trigger(0);
spead_ctrl_status.spead_init_end_status(1)      <= end_manual_complete AND spead_trigger(1);
------------------------------------------------------------------------------
-- IPv4 Checksum
-- precalculate the static fields and be ready for dynamic updates to
-- total_length and dst_addr.

-- 0x4500   = base_ipv4_header.version & base_ipv4_header.header_length & base_ipv4_header.type_of_service;
-- 0xACDC   = base_ipv4_header.id;
-- 0x4000   = base_ipv4_header.ip_flags & base_ipv4_header.fragment_off;
-- 0x4011   = base_ipv4_header.TTL & base_ipv4_header.protocol;
-- summed   = 0x0001_71ED


ipv4_checksum_proc : process(clk)
begin
    if rising_edge(clk) then
        if reset = '1' or enable_packetiser = '0' then
            current_udp             <= base_UDP_header;
            current_ethernet        <= base_ethernet_frame;
            current_ipv4            <= base_ipv4_header;
            current_ipv4.src_addr   <= spead_ctrl_reg.ipv4_src_addr;
            ipv4_chk_sum_calc       <= (others => '0');
            ipv4_chk_sum_base       <= x"000171ED";
            ipv4_chksum_sm          <= IDLE;
            ipv4_chksum_sm_fsm_debug <= x"F";
            packet_calc_done        <= '0';
            ipv4_src_addr_chk       <= (others => '0');
            ipv4_dst_addr_chk       <= (others => '0');
            ipv4_chksum_sm_cnt      <= x"0";
        else
            ipv4_src_addr_chk       <= ((unsigned('0' & current_ipv4.src_addr(31 downto 16))) + (unsigned('0' & current_ipv4.src_addr(15 downto 0))));
            ipv4_dst_addr_chk       <= ((unsigned('0' & current_ipv4.dst_addr(31 downto 16))) + (unsigned('0' & current_ipv4.dst_addr(15 downto 0))));

            
            current_ipv4.dst_addr       <= destination_ip_data; --spead_ctrl_reg.ipv4_dst_addr;
            current_ethernet.dst_mac    <= spead_ctrl_reg.ethernet_dst_mac_u & spead_ctrl_reg.ethernet_dst_mac_l;
            current_ethernet.src_mac    <= spead_ctrl_reg.ethernet_src_mac_u & spead_ctrl_reg.ethernet_src_mac_l;
            current_udp.src_port        <= spead_ctrl_reg.udp_src_port;
            current_udp.dst_port        <= std_logic_vector(udp_destination_port);    --spead_ctrl_reg.udp_dst_port;
            current_udp.length          <= i_udp_total_bytes;

            case ipv4_chksum_sm is
                when IDLE =>
                    ipv4_chksum_sm_fsm_debug    <= x"0";
                    if enable_packetiser = '1' then
                        ipv4_chksum_sm          <= CALC_1;
                    end if;

                when CALC_1 => 
                    ipv4_chksum_sm_fsm_debug    <= x"1";
                    ipv4_chk_sum_base           <= std_logic_vector(unsigned(ipv4_chk_sum_base) + (ipv4_src_addr_chk));
                    ipv4_chksum_sm              <= WAIT_PACKET;

                when WAIT_PACKET =>
                    ipv4_chksum_sm_fsm_debug    <= x"2";
                    packet_calc_done            <= '0';
                    ipv4_chksum_sm_cnt          <= x"0";
                    --  Wait for packet parameters to settle then generate checksum.
                    current_ipv4.total_length   <= i_ipv4_total_bytes_to_calc;
                    if i_packet_calc = '1' then
                        ipv4_chksum_sm          <= CALC_2;
                        ipv4_chk_sum_calc       <= std_logic_vector(unsigned(ipv4_chk_sum_base) + (ipv4_dst_addr_chk));
                    end if;

                when CALC_2 => 
                    ipv4_chksum_sm_fsm_debug    <= x"3";
                    ipv4_chk_sum_calc           <= std_logic_vector(unsigned(ipv4_chk_sum_calc) + unsigned(current_ipv4.total_length));
                    ipv4_chksum_sm              <= CALC_3;

                when CALC_3 => 
                    ipv4_chksum_sm_fsm_debug    <= x"4";
                    ipv4_chk_sum_calc           <= NOT (zero_word & std_logic_vector(unsigned(ipv4_chk_sum_calc(15 downto 0)) + unsigned(ipv4_chk_sum_calc(31 downto 16))));
                    ipv4_chksum_sm              <= READY;                    

                when READY => 
                    ipv4_chksum_sm_fsm_debug    <= x"5";

                    current_ipv4.header_chk_sum <= ipv4_chk_sum_calc(15 downto 0);
                    ipv4_chksum_sm              <= HOLDING;

                when HOLDING =>
                    if ipv4_chksum_sm_cnt < 2 then
                        ipv4_chksum_sm_cnt      <= ipv4_chksum_sm_cnt + 1;
                    else
                        ipv4_chksum_sm          <= WAIT_PACKET;
                        packet_calc_done        <= '1';
                    end if;

                when others =>
                    ipv4_chksum_sm      <= IDLE;
            end case;
        end if;
    end if;
end process;

------------------------------------------------------------------------------
-- Stats
stats_proc : process(clk)
begin
    if rising_edge(clk) then
        if reset = '1' or reset_stats = '1' then
            no_of_spead_start_packets   <= (others => '0');
            no_of_spead_data_packets    <= (others => '0');
            no_of_spead_end_packets     <= (others => '0');
            no_of_packets_sent          <= (others => '0');
        else
            if (i_init_packet_sent = '1') then
                no_of_spead_start_packets   <= no_of_spead_start_packets + 1;
            end if;
            
            if (i_end_packet_sent = '1') then
                no_of_spead_end_packets     <= no_of_spead_end_packets + 1;
            end if;

            if (i_data_packet_sent = '1') then
                no_of_spead_data_packets    <= no_of_spead_data_packets + 1;
            end if;

            if (i_data_packet_sent = '1') OR (i_end_packet_sent = '1') OR (i_init_packet_sent = '1') then
                no_of_packets_sent          <= no_of_packets_sent + 1;
            end if;
        end if;
    end if;
end process;

------------------------------------------------------------------------------
-- static config logic.
static_config_proc : process(clk)
begin
    if rising_edge(clk) then
        if (reset_packetiser_value = '1') OR (reset = '1') then
            udp_destination_port    <= unsigned(spead_ctrl_reg.udp_dst_port);
            udp_dest_port_init_end  <= (others => '0');
            udp_dest_port_data      <= (others => '0');
            spead_data_header       <= default_sdp_spead_data_packet;
            integration_id          <= 16D"0";
            end_packet_ready_int    <= '0';
            init_packet_ready_int   <= '0';
            end_init_track_count    <= (others => '0');
            init_complete           <= '0';
            end_complete            <= '0';
            heap_counter_data_calc  <= (others => '0');
            heap_counter_ie_calc    <= (others => '0');
            heap_counter_chanel_id  <= (others => '0');
        else
            -----------------------------------------------------------------------------------------------
            -- DATA fields

            -- Heap Offset.
            -- 0 for the first packet in a heap, and then needs to scale based on data_bytes_per_packet register.
            -- spead_data_pending = 1 indicates first burst of a heap.
            -- evaluate at start of SM DATA packet handling.
            -- first packet will be     0
            -- second packet will be    data_per_bytes + 8 bytes for the EPOCH OFFSET
            -- third++ will be          (data_per_bytes * (packet number - 1)) + (data_per_bytes + 8 bytes for the EPOCH OFFSET)

            spead_data_header.offset_size                       <= std_logic_vector(offset_size_calc);

            if (i_data_packet_sending = '1' AND i_packet_calc = '1') then
                if spead_data_pending = '1' then
                    offset_size_calc                            <= x"00000000";
                    offset_size_tracker                         <= (x"0000" & unsigned(spead_data_heap_size_int)) + 8;
                else
                    offset_size_calc                            <= offset_size_calc + offset_size_tracker;
                    offset_size_tracker                         <= (x"0000" & unsigned(spead_data_heap_size_int));
                end if;
            end if;

            spead_data_heap_size_int                            <= spead_data_heap_size;

            -- Visibility flags
            spead_data_header.vis_flags(0)                      <= i_to_spead_pack.valid_del_poly;
            spead_data_header.vis_flags(1)                      <= i_to_spead_pack.statically_flagged;
            spead_data_header.vis_flags(2)                      <= i_to_spead_pack.dynamically_flagged;
            spead_data_header.vis_flags(7 downto 3)             <= (others => '0');

            ----------------------------------------

            -- Integration ID (16 bits)
            spead_data_header.integration_id                    <= std_logic_vector(integration_id);
            --Subarray ID (5-bits) + Beam ID (9-bits) + Channel ID (17-bits)
            --spead_data_header.subarray_beam_freq    <= '0' & current_array & x"00" & std_logic_vector(current_freq_sub_beam);

            spead_data_header.subarray_beam_freq(31 downto 17)  <= heap_counter_data(31 downto 17);
            spead_data_header.subarray_beam_freq(16 downto 0)   <= heap_counter_chanel_id;

            -- this will be an array of 64 values in later versions... static_spead_heap_size
            spead_data_header.heap_size                         <= heap_size_data;

            -- Payload = 8 bytes of Epoch offset + visibilities/meta.   (This will be dynamic when the data straddles multiple packets, ie a heap bigger than 9000 bytes.)
            -- if the first data packet of a heap, spead_data_pending, then need to add the 8 bytes for the epoch.
            if (i_packet_calc = '1') AND (spead_data_pending = '1') then
                spead_data_header.payload_size(31 downto 16)    <= x"0000";
                spead_data_header.payload_size(15 downto 0)     <= std_logic_vector(("00" & unsigned(byte_count_from_cor)) + 8);
            elsif (i_packet_calc = '1') AND (spead_data_pending = '0') then
                spead_data_header.payload_size(31 downto 16)    <= x"0000";
                spead_data_header.payload_size(15 downto 0)     <= std_logic_vector(("00" & unsigned(byte_count_from_cor)));
            end if;

            -- 0.849s per integration, resolution is nanoseconds
            -- reset to 1 second, otherwise increment on each read out.
            if (reset_packetiser_value = '1') then
                integration_id  <= 16D"0";
            -- if we are dealing with the first frequency channel in sub array 0, and it is the pre the first packet of a potential split.
            elsif (i_data_packet_sending = '1') and (i_packet_calc = '1' and current_freq_sub_beam = 0 AND (unsigned(current_subarray) = 0) AND spead_data_pending = '1') then
                integration_id  <= integration_id + 1;
            end if;

            -----------------------------------------------------------------------------------------------
            -- INIT / DATA / END packets
            -- HEAP COUNTER CHANNEL ID Increments
            if (init_packet_ready_int = '1') OR (end_packet_ready_int = '1') then
                heap_counter_chanel_id  <= std_logic_vector(heap_counter_ie_calc);
            else
                heap_counter_chanel_id  <= std_logic_vector(heap_counter_data_calc);
            end if;

            -- When Freq_index is zero, new sub-array-beam so load value from RAM.
            if (i_packet_calc = '1' and current_freq_sub_beam = 0) OR (reset_packetiser_value = '1') then
                heap_counter_data_calc  <= unsigned(heap_counter_data(16 downto 0));
            -- when freq has been finally read out from HBM, including packet splits...
            elsif hbm_readout_complete = '1' then
                heap_counter_data_calc  <= heap_counter_data_calc + 1;
            end if;


            if (i_init_packet_sending(6 downto 5) = "01") OR (i_end_packet_sending(6 downto 5) = "01") then
                heap_counter_ie_calc    <= unsigned(heap_counter_data(16 downto 0));
            elsif (i_end_packet_sent = '1') OR (i_init_packet_sent = '1') then
                heap_counter_ie_calc    <= heap_counter_ie_calc + 1;
            end if;

            -----------------------------------------------------------------------------------------------
            -- DATA / END fields

            if i_data_packet_sending = '1' then
                udp_destination_port    <= udp_dest_port_data;
            elsif i_init_packet_sending(0) = '1' OR i_end_packet_sending(0) = '1' then
                udp_destination_port    <= udp_dest_port_init_end;
            end if;

            -- asssume each packet write requires increment on UDP, wrap at freq/channel ID 0
            if (i_packet_calc = '1' and current_freq_sub_beam = 0) OR (reset_packetiser_value = '1') then
                udp_dest_port_data    <= unsigned(destination_udp_data(15 downto 0)); --unsigned(spead_ctrl_reg.udp_dst_port);
            elsif (hbm_readout_complete = '1') then
                udp_dest_port_data    <= udp_dest_port_data + 1;
            end if;

            -- END & INIT single pulse trigger, setup the SM ready signals and clear as appropriate.
            if (trigger_end = '1') OR (trigger_init = '1') then
                end_packet_ready_int    <= trigger_end;
                init_packet_ready_int   <= trigger_init;
            -- when the packet SM starts a INIT or END sequence, load value.
            elsif (i_init_packet_sending(6 downto 5) = "01") OR (i_end_packet_sending(6 downto 5) = "01") then
                udp_dest_port_init_end  <= unsigned(destination_udp_data(15 downto 0));
                end_init_track_count    <= unsigned(no_of_freq_chan_data(16 downto 0));
                if unsigned(no_of_freq_chan_data) = 0 then
                    last_track_count_zero   <= '1';
                else
                    last_track_count_zero   <= '0';
                end if;
            elsif (i_end_packet_sent = '1') OR (i_init_packet_sent = '1') then
                udp_dest_port_init_end  <= udp_dest_port_init_end + 1;
                if end_init_track_count = 1 then
                    if (i_end_packet_sent = '1') then
                        end_packet_ready_int    <= '0';
                    end if;
                    if (i_init_packet_sent = '1') then
                        init_packet_ready_int   <= '0';
                    end if;
                else
                    end_init_track_count    <= end_init_track_count - 1;
                end if;
            end if;

            end_packet_ready_int_d  <= end_packet_ready_int;
            init_packet_ready_int_d <= init_packet_ready_int;

            if end_packet_ready_int_d = '1' and end_packet_ready_int = '0' then
                end_complete <= '1';
            else
                end_complete <= '0';
            end if;

            if init_packet_ready_int_d = '1' and init_packet_ready_int = '0' then
                init_complete <= '1';
            elsif init_packet_trig = '0' then
                init_complete <= '0';
            end if;
            
        end if;
    end if;
end process;

------------------------------------------------------------------------------
-- Epoch Offset calculation.

epoch_offset_proc : process(clk)
begin
    if rising_edge(clk) then
        if (reset_packetiser_value = '1') OR (reset = '1') then
            epoch_offset        <= 64D"1000000000";
            epoch_offset_done   <= '0';
        else
            -- 0.849s or 3 lots of 283 ms integration, resolution is nanoseconds
            -- time_ref - 34 bits
            -- 33-> 32 = bits to indicate which 283 ms interval (if subarray integrating at this)
            -- 31->0 = number of 849ms intervals since epoch.
            -- 34 -> integration interval = 1 bit = 0 - 849ms, 1 - 283 ms.
            -- Calculate for every visibility data packet.

            -- 31 -> 0 supplied from correlator is the timestamp of the first sample of the integration.
            -- ICD calls for the time of the last sample.
            -- additional state in SM added to calculate this.
            -- 1080ns per sample, and 786432 samples in a 849,346,560 ns integration window
            -- ADD_LAST_OFFSET will add, for 849ms, 849,345,480 ns
            -- for 283ms, 283,114,440 ns
            -- constant c_last_sample_283ms   : unsigned(31 downto 0) := 32D"283114440";
            -- constant c_last_sample_849ms   : unsigned(31 downto 0) := 32D"849345480";
            -- Note : 6*4096 SPS samples come from before and after the “first” SPS sample used to calculate a 226Hz sample, 
            -- so the 226 Hz sample is also centered at the time of the “first” SPS sample 
            -- If we want start or end time of the 226Hz sample we have to add/subtract a half-sample period, 
            -- ie 2048 SPS samples time
            -- update time contants from above to 
            -- (first sample = current firmware timestamp) + (frame length = 384*2048*1080ns) - (1 sample = 1080ns) - (2048 samples = 2048*1080ns)
            -- (X*849346560 + (849345480 - (2048*1080))) ns = (X*849346560 + 847133640) ns
            -- constant c_last_sample_849ms   : unsigned(31 downto 0) := 32D"847133640";
            -- for 283 integrations
            -- (X*849346560 + 847133640) - 64*4096*1080*2
            -- (X*849346560 + 847133640) - 64*4096*1080*1 = (X*849346560 + 847133640) - (283115520)
            -- (X*849346560 + 847133640) - 0
            -- constant c_last_sample_int1_283ms   : unsigned(31 downto 0) := 32D"280902600";  -- (847133640) - 64*4096*1080*2
            -- constant c_step_for_283ms           : unsigned(31 downto 0) := 32D"283115520";  -- 64*4096*1080*1

            case epoch_sm is
                when IDLE =>
                    epoch_offset_done   <= '0';
                    epoch_count         <= x"0";

                    if (i_data_packet_sending = '1') and (i_packet_calc = '1') then
                        epoch_sm <= WAIT_MULTI;
                    end if;

                when WAIT_MULTI =>
                    if (time_ref(34) = '0') OR ((time_ref(34) = '1') AND (time_ref(33 downto 32) = "10")) then
                        epoch_sm        <= ADD_LAST_OFFSET;
                    else
                        epoch_sm        <= ADD_283_OFFSET;
                        epoch_count     <= x"0";
                    end if;

                    epoch_final     <= "00" & multi_q;

                when ADD_283_OFFSET =>
                    -- Add first window of 283
                    if ((time_ref(33 downto 32) = "00")) then
                        epoch_final     <= std_logic_vector(unsigned(epoch_final) + (c_last_sample_int1_283ms));
                    end if;

                    epoch_sm        <= ADD_LAST_OFFSET;

                when ADD_LAST_OFFSET =>
                    -- 3rd window of 283 and 849 are the same.
                    if (time_ref(34) = '0') OR ((time_ref(34) = '1') AND (time_ref(33 downto 32) = "10")) then
                        epoch_final     <= std_logic_vector(unsigned(epoch_final) + (c_last_sample_849ms));

                    -- add second window of 283
                    elsif (time_ref(33 downto 32) = "01") then 
                        epoch_final     <= std_logic_vector(unsigned(epoch_final) + (c_step_for_283ms));
                    end if;

                    epoch_sm    <= IDLE;

                when OTHERS =>
                    epoch_sm    <= IDLE;
            end case;

        end if;

        epoch_offset        <= epoch_final;
        multi_a             <= time_ref(31 downto 0);
    end if;
end process;

uint_32_multi : mult_fixed_849ms_in_ns
    Port Map (
        CLK => clk,
        A   => multi_a,
        P   => multi_q
    );

    epoch_offset_le     <= epoch_offset(7 downto 0) & epoch_offset(15 downto 8) & epoch_offset(23 downto 16) & epoch_offset(31 downto 24) & 
    epoch_offset(39 downto 32) & epoch_offset(47 downto 40) & epoch_offset(55 downto 48) & epoch_offset(63 downto 56);

------------------------------------------------------------------------------
-- which subarray reference to use, coming from correlator or set from host for INIT and END.

lookup_proc : process(clk)
begin
    if rising_edge(clk) then
    
        current_table   <= i_to_spead_pack.table_select;

        if i_INIT_running_packet_fsm = '1' then
            current_table_ram   <= NOT i_to_spead_pack.table_select;
        else
            current_table_ram   <= i_to_spead_pack.table_select;
        end if;

        -- This doesn't apply to the heap_size ram, that is always mapped as it is only used for data.
        if i_data_packet_sending = '1' then                  -- PACKET SM is in a DATA PACKET Sequence.
            subarraybeam_addr_lkup      <= current_subarray(6 downto 0);
        elsif i_end_packet_sending(0) = '1' then                -- running END sequence, DATA will not be running if AUTO END mechanism, Manual shold have waited in software land.
            subarraybeam_addr_lkup      <= end_subarray_lkup;
        elsif i_init_packet_sending(0) = '1' then
            subarraybeam_addr_lkup      <= spead_ctrl_reg.current_subarray_beam_target(6 downto 0);
        end if;

        -- tie to zero atm
        block_step_addr_lkup    <= "0000";

        --------------------------------------------------------------------------------------
        -- END Trigger, this tells the SM one level up, that INIT is needed to be sent.
        -- replaced by page flip trigger.
        end_packet_trig         <= spead_trigger(1);

        -- trigger comes from HBM readout circuit based on page flip occuring.
        -- if there is nothing to flip to and the correlator is finishing, END packets can be triggered by software.
        -- this is the start condition for the END_SM
        trigger_end_packets     <= trigger_end_packets(0) & (i_to_spead_pack.trigger_end_packets OR end_packet_trig);

        --------------------------------------------------------------------------------------
        -- INIT Trigger, this tells the SM one level up, that INIT is needed to be sent.
        data_packet_sending     <= data_packet_sending(0) & i_data_packet_sending;

        if page_flip_in_progress = '0' AND data_packet_sending = "00" then
            init_packet_trig    <= spead_trigger(0);
        end if;
        init_packet_trig_d      <= init_packet_trig;

        if init_packet_trig_d = '0' and init_packet_trig = '1' then
            trigger_init <= '1';
        else
            trigger_init <= '0';
        end if;

    end if;
end process;

------------------------------------------------------------------------------
-- END packet SM triggered by page swap
End_packet_SM_proc : process(clk)
begin
    if rising_edge(clk) then
   
        -- The packetiser should hold its switch active bit high from when it sees a rising edge on 
        -- i_table_swap_in_progress through to when it gets notification of a packet to be sent using o_packetiser_table_select 
        table_swap_in_progress_del  <= table_swap_in_progress_del(0) & i_table_swap_in_progress;
        
        packetiser_table_select     <= i_packetiser_table_select;

        if enable_packetiser = '0' then
            page_flip_active_from_ct1 <= '0';
        else
            if table_swap_in_progress_del = "01" then
                page_flip_active_from_ct1 <= '1';
            elsif current_table = packetiser_table_select then
                page_flip_active_from_ct1 <= '0';
            end if;
        end if;

        spead_ctrl_status.table_sel_status <=   x"0" &
                                                std_logic_vector(end_seq_counter) &
                                                '0' &
                                                last_track_count_zero &
                                                i_packetiser_table_select &
                                                i_table_swap_in_progress & 
                                                page_flip_in_progress &
                                                page_flip_active_from_ct1 &
                                                (page_flip_in_progress or page_flip_active_from_ct1) &
                                                current_table;
        
        o_page_flip_in_progress     <= page_flip_in_progress;

        trigger_end                 <= '0';
        end_sequence_complete       <= '0';

        if reset = '1' or enable_packetiser = '0' then
            end_packet_sm           <= IDLE;
            page_flip_in_progress   <= '0';
            end_manual_complete     <= '0';
        else
            case end_packet_sm is
                when IDLE =>
                    end_packet_arr_adr      <= 7D"0";
                    -- clear complete of manual with de-assertion of the manual trigger.
                    if end_packet_trig = '0' then
                        end_manual_complete     <= '0';
                    end if;
                    if trigger_end_packets = "01" then
                        page_flip_in_progress   <= '1';
                        end_packet_sm           <= SETUP;
                    end if;
    
                when SETUP =>
                    -- if entry bit 7 is high, there is a sub array to end, setup entry.
                    -- otherwise complete and page flip can occur.
                    if end_packet_arr_q(7) = '1' then
                        end_packet_sm       <= START_END;
                        end_subarray_lkup   <= end_packet_arr_q(6 downto 0);
                    else
                        end_packet_sm       <= PROCESS_COMPLETE;
                    end if;
                    end_packet_arr_adr <= end_packet_arr_adr + 1;
    
                -- when RD_ARRAY =>
    
                -- when CHK_VALUE =>
    
                when START_END =>
                    trigger_end         <= '1';
                    end_packet_sm       <= SUBARRAY_ENDED;
    
                when SUBARRAY_ENDED =>
                    if end_complete = '1' then
                        end_packet_sm       <= SETUP;
                    end if;
    
                when PROCESS_COMPLETE =>
                    page_flip_in_progress   <= '0';
                    end_packet_sm           <= IDLE;
                    end_sequence_complete   <= '1';
                    end_seq_counter         <= end_seq_counter + 1;

                    end_manual_complete     <= end_packet_trig;
    
                when others => end_packet_sm <= IDLE;
            end case;
        end if;

        -- added for sim
        if spead_trigger(2) = '1' then
            if end_packet_arr_adr = 7D"0" then
                end_packet_arr_q   <= x"80";
            else
                end_packet_arr_q   <= x"00";
            end if;
        else
            end_packet_arr_q   <= o_list_subarrays_ending.rd_dat(7 downto 0);
        end if;
    end if;
end process;

-- NOT current_table below to match the software approach at the front end of not 
-- writing to pages in use.
-- and we will be using the non-live page when deleting a subarray.
-- will not know when this occurs and can not be pre-scheduled.

i_list_subarrays_ending.adr     <= (NOT current_table) & std_logic_vector(end_packet_arr_adr);
i_list_subarrays_ending.wr_dat  <= (others => '0');
i_list_subarrays_ending.wr_en   <= '0';
i_list_subarrays_ending.rd_en   <= '1';
i_list_subarrays_ending.clk     <= clk;
i_list_subarrays_ending.rst     <= '0';



------------------------------------------------------------------------------
-- provision assuming 64 subarrays.
-- 16 entries per sub array, subarray is counted from 0.

i_sub_array_remapper : entity spead_lib.memory_tdp_spead 
    GENERIC MAP (
        MEMORY_INIT_FILE    => "none",
        g_NO_OF_ADDR_BITS   => 8,
        g_D_Q_WIDTH         => 32
    )
    Port Map ( 
        clk_a           => clk,
        clk_b           => clk,
    
        data_in_a       => (others => '0'),
        addr_in_a       => (others => '0'),
        data_in_wr_a    => '0',
        data_out_a      => open,

        data_in_b       => sub_array_data,
        addr_in_b       => sub_array_addr,
        data_in_wr_b    => sub_array_wr,
        data_out_b      => sub_array_dout
    );

------------------------------------------------------------------------------
-- ARGs
ARGS_register_Packetiser : entity spead_lib.spead_spead_sdp_reg 
    PORT MAP (
        -- AXI Lite signals, 300 MHz Clock domain
        MM_CLK                  => i_clk,
        MM_RST                  => i_rst,
        
        SLA_IN                  => i_spead_lite_axi_mosi,
        SLA_OUT                 => o_spead_lite_axi_miso,

        SPEAD_CTRL_FIELDS_RW    => spead_ctrl_reg,
        SPEAD_CTRL_FIELDS_RO    => spead_ctrl_status,

        SPEAD_CTRL_LIST_SUBARRAYS_ENDING_IN     => i_list_subarrays_ending,
        SPEAD_CTRL_LIST_SUBARRAYS_ENDING_OUT    => o_list_subarrays_ending
        );

generate_debug_ila : IF g_DEBUG_ILA GENERATE
    memspace_debug : ila_0 PORT MAP (
        clk                     => clk,
        probe0(3 downto 0)      => bram_we,
        probe0(19 downto 4)     => bram_addr(15 downto 0),
        probe0(51 downto 20)    => bram_wrdata,
        probe0(83 downto 52)    => bram_rddata,
        probe0(85 downto 84)    => destination_ip_wren_p,
        probe0(87 downto 86)    => destination_udp_wren_p,
        probe0(89 downto 88)    => heap_counter_wren_p,
        probe0(91 downto 90)    => heap_size_wren_p,
        probe0(93 downto 92)    => block_size_wren_p,
        probe0(95 downto 94)    => no_of_freq_chan_wren_p,
        probe0(96)              => init_packet_wren,
        probe0(97)              => bram_en,
        probe0(99 downto 98)    => (others => '0'),
        probe0(131 downto 100)  => no_of_freq_chan_return_p(0),
        probe0(163 downto 132)  => no_of_freq_chan_return_p(1),
        probe0(179 downto 164)  => init_packet_return(15 downto 0),
        probe0(186 downto 180)  => bram_addr_d2,
        probe0(191 downto 187)  => (others => '0')
        );

    init_debug : ila_0 PORT MAP (
        clk                     => clk,
        probe0(3 downto 0)      => i_spead_sm_fsm_debug,
        probe0(7 downto 4)      => ipv4_chksum_sm_fsm_debug,
        probe0(15 downto 8)     => init_packet_sm_debug,
        probe0(16)              => trigger_init,
        probe0(17)              => i_init_packet_sent,
        probe0(18)              => init_packet_ready_int,
        probe0(19)              => current_table_ram,
        probe0(36 downto 20)    => std_logic_vector(end_init_track_count),
        probe0(37)              => page_flip_in_progress,
        probe0(38)              => i_INIT_running_packet_fsm,
        probe0(39)              => init_packet_next,
        probe0(71 downto 40)    => destination_udp_data_p(0),
        probe0(103 downto 72)   => destination_udp_data_p(1),
        probe0(119 downto 104)  => destination_udp_data(15 downto 0),
        probe0(126 downto 120)  => subarraybeam_addr_lkup,

        probe0(191 downto 127)  => (others => '0')
        );
end generate;

Spead_sdp_memspace : entity spead_lib.spead_axi_bram_wrapper 
    PORT MAP (
        i_clk                   => i_clk,
        i_rst                   => i_rst,
        
        i_spead_full_axi_mosi   => i_spead_full_axi_mosi,
        o_spead_full_axi_miso   => o_spead_full_axi_miso,
    
        bram_rst                => bram_rst,
        bram_clk                => bram_clk,
        bram_en                 => bram_en,     --: OUT STD_LOGIC;
        bram_we_byte            => bram_we,     --: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        bram_addr               => bram_addr,   --: OUT STD_LOGIC_VECTOR(14 DOWNTO 0);
        bram_wrdata             => bram_wrdata, --: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        bram_rddata             => bram_rddata  --: IN STD_LOGIC_VECTOR(31 DOWNTO 0)
    );        

-- ARGs mappings. AXI is byte addressing, ARGs is 4 byte addressing.
args_addr                <= bram_addr(15 downto 2);


-- 0    - 2047 ... 2048 (128 (7 bits) subarray_beams with 16 blocks (4bit) = 11 bits) x 4B - Destination IP
-- 2048 - 4095 ... 2048 (128 (7 bits) subarray_beams with 16 blocks (4bit) = 11 bits) x 4B - Destination UDP (lower bytes)
-- 4096 - 6143 ... 2048 (128 (7 bits) subarray_beams with 16 blocks (4bit) = 11 bits) x 4B - Heap Counter (Subarray, Beam and channel ID section)

-- 6144 - 6271 ... 128 x 4B - heap-size (baseline x 34 bytes + 8 bytes from Epoch Offset)
-- 6272 - 6399 ... 128 x 2B - Block size step per sub array.
-- 6400 - 6527 ... 128 x 4B - number of frequency channels per subarray.

-- 7168 - 8191 ... 4kB memory space for INIT template.

-- 8192  - 10239 ... 2048 (128 (7 bits) subarray_beams with 16 blocks (4bit) = 11 bits) x 4B - Destination IP
-- 10240 - 12287 ... 2048 (128 (7 bits) subarray_beams with 16 blocks (4bit) = 11 bits) x 4B - Destination UDP (lower bytes)
-- 12288 - 14335 ... 2048 (128 (7 bits) subarray_beams with 16 blocks (4bit) = 11 bits) x 4B - Heap Counter (Subarray, Beam and channel ID section)

-- 14336 - 14463 ... 128 x 4B - heap-size (baseline x 34 bytes + 8 bytes from Epoch Offset)
-- 14464 - 14591 ... 128 x 2B - Block size step per sub array.
-- 14592 - 14719 ... 128 x 4B - number of frequency channels per subarray.

-- 14720 - 16384 ... unused.

destination_ip_wren_p(0)    <=  '1' when (args_addr(13 downto 11) = "000") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';
destination_udp_wren_p(0)   <=  '1' when (args_addr(13 downto 11) = "001") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';
heap_counter_wren_p(0)      <=  '1' when (args_addr(13 downto 11) = "010") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';
heap_size_wren_p(0)         <=  '1' when (args_addr(13 downto 7)  = "0110000") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';
block_size_wren_p(0)        <=  '1' when (args_addr(13 downto 7)  = "0110001") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';
no_of_freq_chan_wren_p(0)   <=  '1' when (args_addr(13 downto 7)  = "0110010") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';
init_packet_wren            <=  '1' when (args_addr(13 downto 10) = "0111") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';

destination_ip_wren_p(1)    <=  '1' when (args_addr(13 downto 11) = "100") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';
destination_udp_wren_p(1)   <=  '1' when (args_addr(13 downto 11) = "101") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';
heap_counter_wren_p(1)      <=  '1' when (args_addr(13 downto 11) = "110") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';
heap_size_wren_p(1)         <=  '1' when (args_addr(13 downto 7)  = "1110000") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';
block_size_wren_p(1)        <=  '1' when (args_addr(13 downto 7)  = "1110001") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';
no_of_freq_chan_wren_p(1)   <=  '1' when (args_addr(13 downto 7)  = "1110010") AND bram_we(0) = '1' AND bram_en = '1' else
                                '0';


bram_return_data_proc : process(bram_clk)
begin
    if rising_edge(bram_clk) then
        bram_addr_d1    <= args_addr(13 downto 7);
        bram_addr_d2    <= bram_addr_d1;
    end if;
end process;

bram_rddata     <=  destination_ip_return_p(0)          when bram_addr_d2(6 downto 4) = "000"        else
                    destination_udp_return_p(0)         when bram_addr_d2(6 downto 4) = "001"        else
                    heap_counter_return_p(0)            when bram_addr_d2(6 downto 4) = "010"        else
                    heap_size_return_p(0)               when bram_addr_d2(6 downto 0) = "0110000"    else
                    x"0000" & block_size_return_p(0)    when bram_addr_d2(6 downto 0) = "0110001"    else
                    no_of_freq_chan_return_p(0)         when bram_addr_d2(6 downto 0) = "0110010"    else
                    init_packet_return                  when bram_addr_d2(6 downto 3) = "0111"       else
                    destination_ip_return_p(1)          when bram_addr_d2(6 downto 4) = "100"        else
                    destination_udp_return_p(1)         when bram_addr_d2(6 downto 4) = "101"        else
                    heap_counter_return_p(1)            when bram_addr_d2(6 downto 4) = "110"        else
                    heap_size_return_p(1)               when bram_addr_d2(6 downto 0) = "1110000"    else
                    x"0000" & block_size_return_p(1)    when bram_addr_d2(6 downto 0) = "1110001"    else
                    no_of_freq_chan_return_p(1)         when bram_addr_d2(6 downto 0) = "1110010"    else
                    x"DEADBEEF";


destination_ip_addr     <= subarraybeam_addr_lkup & block_step_addr_lkup;
destination_udp_addr    <= subarraybeam_addr_lkup & block_step_addr_lkup;
heap_counter_addr       <= subarraybeam_addr_lkup & block_step_addr_lkup;

heap_size_addr          <= current_subarray(6 downto 0); --subarraybeam_addr_lkup;
block_size_addr         <= subarraybeam_addr_lkup;
no_of_freq_chan_addr    <= subarraybeam_addr_lkup;

-- page select
destination_ip_data     <= destination_ip_data_p(0)     when current_table_ram = '0' else destination_ip_data_p(1);
destination_udp_data    <= destination_udp_data_p(0)    when current_table_ram = '0' else destination_udp_data_p(1);
heap_counter_data       <= heap_counter_data_p(0)       when current_table_ram = '0' else heap_counter_data_p(1);
heap_size_data          <= heap_size_data_p(0)          when current_table_ram = '0' else heap_size_data_p(1);
block_size_data         <= block_size_data_p(0)         when current_table_ram = '0' else block_size_data_p(1);
no_of_freq_chan_data    <= no_of_freq_chan_data_p(0)    when current_table_ram = '0' else no_of_freq_chan_data_p(1);


gen_paged_config_ram : for i in 0 to 1 GENERATE

    dest_ip_mem : entity signal_processing_common.memory_tdp_wrapper 
        generic map (
            MEMORY_INIT_FILE    => "dest_ip_preload.mem",
            g_NO_OF_ADDR_BITS   => 11,
            g_D_Q_WIDTH         => 32 )
        port map ( 
            clk_a           => i_clk,
            clk_b           => i_clk,
        
            data_a          => bram_wrdata,
            addr_a          => args_addr(10 downto 0),
            data_a_wr       => destination_ip_wren_p(i),
            data_a_q        => destination_ip_return_p(i),
            
            data_b          => (others => '0'),
            addr_b          => destination_ip_addr,
            data_b_wr       => '0',
            data_b_q        => destination_ip_data_p(i)
        );                                
    
    dest_udp_mem : entity signal_processing_common.memory_tdp_wrapper 
        generic map (
            MEMORY_INIT_FILE    => dest_udp_init(i),
            g_NO_OF_ADDR_BITS   => 11,
            g_D_Q_WIDTH         => 32 )
        port map ( 
            clk_a           => i_clk,
            clk_b           => i_clk,
        
            data_a          => bram_wrdata,
            addr_a          => args_addr(10 downto 0),
            data_a_wr       => destination_udp_wren_p(i),
            data_a_q        => destination_udp_return_p(i),
            
            data_b          => (others => '0'),
            addr_b          => destination_udp_addr,
            data_b_wr       => '0',
            data_b_q        => destination_udp_data_p(i)
        
        );   
    
    heap_counter_mem : entity signal_processing_common.memory_tdp_wrapper 
        generic map (
            MEMORY_INIT_FILE    => "heap_counter_preload.mem",
            g_NO_OF_ADDR_BITS   => 11,
            g_D_Q_WIDTH         => 32 )
        port map ( 
            clk_a           => i_clk,
            clk_b           => i_clk,
        
            data_a          => bram_wrdata,
            addr_a          => args_addr(10 downto 0),
            data_a_wr       => heap_counter_wren_p(i),
            data_a_q        => heap_counter_return_p(i),
            
            data_b          => (others => '0'),
            addr_b          => heap_counter_addr,
            data_b_wr       => '0',
            data_b_q        => heap_counter_data_p(i)
        );   
    
    heap_size_mem : entity signal_processing_common.memory_tdp_wrapper 
        generic map (
            MEMORY_INIT_FILE    => "heap_size_preload.mem",
            g_NO_OF_ADDR_BITS   => 7,
            g_D_Q_WIDTH         => 32 )
        port map ( 
            clk_a           => i_clk,
            clk_b           => i_clk,
        
            data_a          => bram_wrdata,
            addr_a          => args_addr(6 downto 0),
            data_a_wr       => heap_size_wren_p(i),
            data_a_q        => heap_size_return_p(i),
            
            data_b          => (others => '0'),
            addr_b          => heap_size_addr,
            data_b_wr       => '0',
            data_b_q        => heap_size_data_p(i)
        );   
    
    block_size_mem : entity signal_processing_common.memory_tdp_wrapper 
        generic map (
            MEMORY_INIT_FILE    => "none",
            g_NO_OF_ADDR_BITS   => 7,
            g_D_Q_WIDTH         => 16 )
        port map ( 
            clk_a           => i_clk,
            clk_b           => i_clk,
        
            data_a          => bram_wrdata(15 downto 0),
            addr_a          => args_addr(6 downto 0),
            data_a_wr       => block_size_wren_p(i),
            data_a_q        => block_size_return_p(i),
            
            data_b          => (others => '0'),
            addr_b          => block_size_addr,
            data_b_wr       => '0',
            data_b_q        => block_size_data_p(i)
            
        
        );   
    
    no_of_freq_chan_mem : entity signal_processing_common.memory_tdp_wrapper 
        generic map (
            MEMORY_INIT_FILE    => no_of_freq_chan_init(i),
            g_NO_OF_ADDR_BITS   => 7,
            g_D_Q_WIDTH         => 32)
        port map ( 
            clk_a           => i_clk,
            clk_b           => i_clk,
        
            data_a          => bram_wrdata,
            addr_a          => args_addr(6 downto 0),
            data_a_wr       => no_of_freq_chan_wren_p(i),
            data_a_q        => no_of_freq_chan_return_p(i),
            
            data_b          => (others => '0'),
            addr_b          => no_of_freq_chan_addr,
            data_b_wr       => '0',
            data_b_q        => no_of_freq_chan_data_p(i)    
        );
END GENERATE;

init_sm_trigger     <= init_packet_ready_int AND i_INIT_running_packet_fsm;

scratch_pad : entity spead_lib.spead_init_memspace 
    Generic map (
        g_NUMBER_OF_OFFSETS     => C_NUMBER_OF_OFFSETS,
        g_DEBUG_ILA             => g_DEBUG_ILA
    )
    Port map ( 
        i_clk                   => i_clk,
        i_rst                   => i_rst,

        i_host_data             => bram_wrdata,
        i_host_addr             => args_addr(9 downto 0),
        i_host_wren             => init_packet_wren,
        o_host_return_data      => init_packet_return,

        -------------------------------------------------
        i_trigger_init          => init_sm_trigger,
        i_init_packet_sent      => i_init_packet_sent, 
        i_offsets_array         => offsets_array,
        
        o_init_ready_to_send    => init_ready_to_send,

        i_init_freq_interval    => init_freq_interval,
        i_init_freq_base        => init_freq_base,
        
        i_hertz_is_float_int    => hertz_is_float_int,
        o_scratchpad_updated    => o_scratchpad_updated,
        o_init_packet_sm_debug  => init_packet_sm_debug,
        -------------------------------------------------

        o_data_to_packetiser    => init_packet_data,
        i_data_inc              => init_packet_next

    );    



end Behavioral;
