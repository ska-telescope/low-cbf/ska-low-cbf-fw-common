----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 20/02/2023 04:22:23 PM
-- Design Name: 
-- Module Name: correlator_data_reader - Behavioral
--  
-- Description: 
-- 
-- Take in a data stream and packetise this for transmission with SPEAD protocol.
-- 
-- Flow of the file (instancing)
--      
--      SM handling the overall creation of a SPEAD packet, start, data, end.
--      
--
--  Per Instance;
--      Cache incoming data to 8k per stream.
--      4KB memory to hold SPEAD INIT or END packet. 32 bit x 1024
--      
--      SM generating the packet
--      
-- 
-- 
-- 
-- 
-- 
----------------------------------------------------------------------------------

library IEEE, correlator_lib, common_lib, signal_processing_common, spead_lib, technology_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;
USE common_lib.common_pkg.ALL;
use technology_lib.tech_mac_100g_pkg.ALL;
use spead_lib.spead_packet_pkg.ALL;

entity spead_top is
    Generic ( 
        g_CORRELATORS           : INTEGER := 1;
        g_DEBUG_VEC_SIZE        : INTEGER := 11;
        g_DEBUG_ILA             : BOOLEAN := FALSE
    );
    Port ( 
        -- clock used for all data input and output from this module (300 MHz)
        i_axi_clk               : in std_logic;
        i_axi_rst               : in std_logic;
        i_local_reset           : in std_logic;
        --
        i_table_swap_in_progress  : in std_logic;
        i_packetiser_table_select : in std_logic;
        -- streaming AXI to CMAC
        i_cmac_clk              : IN STD_LOGIC;
        i_cmac_clk_rst          : IN STD_LOGIC;

        o_tx_axis_tdata         : OUT STD_LOGIC_VECTOR(511 downto 0);
        o_tx_axis_tkeep         : OUT STD_LOGIC_VECTOR(63 downto 0);
        o_tx_axis_tvalid        : OUT STD_LOGIC;
        o_tx_axis_tlast         : OUT STD_LOGIC;
        o_tx_axis_tuser         : OUT STD_LOGIC;
        i_tx_axis_tready        : in STD_LOGIC;

        -- Packed up Correlator Data.
        o_from_spead_pack       : out t_spead_to_hbm_bus_array(1 downto 0);
        i_to_spead_pack         : in t_hbm_to_spead_bus_array(1 downto 0);

        o_packetiser_enable     : out std_logic_vector(1 downto 0); 
        
        -- debug vector
        i_debug                 : in std_logic_vector(((g_DEBUG_VEC_SIZE)-1) downto 0);

        -- ARGs interfaces
        i_spead_lite_axi_mosi   : in t_axi4_lite_mosi_arr(1 downto 0); 
        o_spead_lite_axi_miso   : out t_axi4_lite_miso_arr(1 downto 0);
        i_spead_full_axi_mosi   : in  t_axi4_full_mosi_arr(1 downto 0);
        o_spead_full_axi_miso   : out t_axi4_full_miso_arr(1 downto 0)

    );
end spead_top;

architecture Behavioral of spead_top is

signal clk                      : STD_LOGIC;
signal reset                    : STD_LOGIC;

constant C_SOURCES              : integer := (g_CORRELATORS-1);
signal arb_sel_count            : integer range 0 to 1;
signal arb_sel                  : std_logic;
signal arb_enable               : std_logic;

signal bytes_to_transmit        : t_slv_14_arr(1 downto 0);
signal data_to_player           : t_slv_512_arr(1 downto 0);
signal data_to_player_wr        : STD_LOGIC_VECTOR(1 downto 0);
signal data_to_player_rdy       : STD_LOGIC_VECTOR(1 downto 0);

signal pending_packet           : STD_LOGIC_VECTOR(1 downto 0);
signal spead_seq_complete       : STD_LOGIC_VECTOR(1 downto 0);

signal bytes_to_transmit_sel    : STD_LOGIC_VECTOR(13 downto 0);
signal data_to_player_sel       : STD_LOGIC_VECTOR(511 downto 0);
signal data_to_player_wr_sel    : STD_LOGIC;
signal data_to_player_rdy_sel   : STD_LOGIC;

type arb_sm_fsm is      (
                        IDLE, 
                        SESSION, 
                        CLEANUP
                        );
signal arb_sm : arb_sm_fsm;



begin

------------------------------------------------------------------------------
-- PORT MAPPINGS
clk         <= i_axi_clk;
reset       <= i_axi_rst;


------------------------------------------------------------------------------

cor_speader : entity spead_lib.spead_packet 
    generic map (
        g_DEBUG_VEC_SIZE        => g_DEBUG_VEC_SIZE,
        g_DEBUG_ILA             => g_DEBUG_ILA
    )
    port map ( 
        -- clock used for all data input and output from this module (300 MHz)
        i_clk                   => clk,
        i_clk_rst               => reset,

        i_local_reset           => '0',
        i_table_swap_in_progress  => i_table_swap_in_progress,   --  in std_logic;
        i_packetiser_table_select => i_packetiser_table_select, --  in std_logic;
        -- arbiter signals for round robin
        o_pending_packet        => pending_packet(0),
        o_spead_seq_complete    => spead_seq_complete(0),
        -- data to packet_player
        -- S_AXI aligned.
        o_bytes_to_transmit     => bytes_to_transmit(0),
        o_data_to_player        => data_to_player(0),
        o_data_to_player_wr     => data_to_player_wr(0),
        i_data_to_player_rdy    => data_to_player_rdy(0),

        -- Packed up Correlator Data.
        o_from_spead_pack       => o_from_spead_pack(0),
        i_to_spead_pack         => i_to_spead_pack(0),

        o_packetiser_enable     => o_packetiser_enable(0),

        i_debug                 => i_debug,
        
        -- ARGs interface.
        i_spead_lite_axi_mosi   => i_spead_lite_axi_mosi(0),
        o_spead_lite_axi_miso   => o_spead_lite_axi_miso(0),
        i_spead_full_axi_mosi   => i_spead_full_axi_mosi(0),
        o_spead_full_axi_miso   => o_spead_full_axi_miso(0)

    );


no_other_packetiser : if (g_CORRELATORS < 2) GENERATE
    ARGs_tie_off : entity signal_processing_common.args_axi_terminus
    port map ( 
        -- ARGS interface
        -- MACE clock is 300 MHz
        i_MACE_clk                          => clk,
        i_MACE_rst                          => reset,
                
        i_args_axi_terminus_full_axi_mosi   => i_spead_full_axi_mosi(1),
        o_args_axi_terminus_full_axi_miso   => o_spead_full_axi_miso(1)

    );

    pending_packet(1)       <= '0';
    spead_seq_complete(1)   <= '0';

    bytes_to_transmit(1)    <= ( others => '0');
    data_to_player(1)       <= ( others => '0');
    data_to_player_wr(1)    <= '0';

END GENERATE;

additional_packetiser_gen : if (g_CORRELATORS > 1) GENERATE
    cor_speader : entity spead_lib.spead_packet 
        generic map (
            g_DEBUG_VEC_SIZE        => g_DEBUG_VEC_SIZE,
            g_DEBUG_ILA             => g_DEBUG_ILA
        )
        port map ( 
            -- clock used for all data input and output from this module (300 MHz)
            i_clk                   => clk,
            i_clk_rst               => reset,

            i_local_reset           => '0',
            i_table_swap_in_progress  => i_table_swap_in_progress,   --  in std_logic;
            i_packetiser_table_select => i_packetiser_table_select, --  in std_logic;
            -- arbiter signals for round robin
            o_pending_packet        => pending_packet(1),
            o_spead_seq_complete    => spead_seq_complete(1),
            -- data to packet_player
            -- S_AXI aligned.
            o_bytes_to_transmit     => bytes_to_transmit(1),
            o_data_to_player        => data_to_player(1),
            o_data_to_player_wr     => data_to_player_wr(1),
            i_data_to_player_rdy    => data_to_player_rdy(1),

            -- Packed up Correlator Data.
            o_from_spead_pack       => o_from_spead_pack(1),
            i_to_spead_pack         => i_to_spead_pack(1),

            o_packetiser_enable     => o_packetiser_enable(1),

            i_debug                 => i_debug,
            
            -- ARGs interface.
            i_spead_lite_axi_mosi   => i_spead_lite_axi_mosi(1),
            o_spead_lite_axi_miso   => o_spead_lite_axi_miso(1),
            i_spead_full_axi_mosi   => i_spead_full_axi_mosi(1),
            o_spead_full_axi_miso   => o_spead_full_axi_miso(1)

        );
END GENERATE;
---------------------------------------------------------------------------------------------------------------------------------------
-- Simple round robin access.

packetiser_arb_proc : process (clk)
begin
    if rising_edge(clk) then
        if reset = '1' then
            arb_sel         <= '0';
            arb_enable      <= '0';
            arb_sm          <= IDLE;
        else

            CASE arb_sm IS
                WHEN IDLE => 
                    if pending_packet = "11" then
                        arb_sel         <= NOT arb_sel;
                        arb_sm          <= SESSION;
                    elsif pending_packet = "01" then
                        arb_sel         <= '0';
                        arb_sm          <= SESSION;
                    elsif pending_packet = "10" then
                        arb_sel         <= '1';
                        arb_sm          <= SESSION;
                    end if;

                WHEN SESSION =>
                    arb_enable  <= '1';

                    if arb_sel = '0' and spead_seq_complete(0) = '1' then
                        arb_sm          <= CLEANUP;
                    elsif  arb_sel = '1' and spead_seq_complete(1) = '1' then
                        arb_sm          <= CLEANUP;
                    end if;

                WHEN CLEANUP =>
                    arb_enable  <= '0';
                    arb_sm      <= IDLE;

                WHEN OTHERS =>
                    arb_sm <= IDLE;

            END CASE;            
        end if;
    end if;
end process;

arb_sel_count           <= 1 when arb_sel = '1' else 0;

bytes_to_transmit_sel   <= bytes_to_transmit(arb_sel_count);
data_to_player_sel      <= data_to_player(arb_sel_count);
data_to_player_wr_sel   <= data_to_player_wr(arb_sel_count);

data_to_player_rdy(0)   <= data_to_player_rdy_sel when ((arb_sel = '0') AND (arb_enable = '1')) else '0';
data_to_player_rdy(1)   <= data_to_player_rdy_sel when ((arb_sel = '1') AND (arb_enable = '1')) else '0';

--------------------------------------------------------------------------------------------------
-- S_AXI packet player config    
cmac_cdc : entity spead_lib.packet_player generic map (
            g_DEBUG_ILA             => g_DEBUG_ILA,
            LBUS_TO_CMAC_INUSE      => FALSE,
            PLAYER_CDC_FIFO_DEPTH   => 512   
        )
        port map ( 
            i_clk                   => clk,
            i_clk_reset             => reset,
        
            i_cmac_clk              => i_cmac_clk,
            i_cmac_clk_rst          => i_cmac_clk_rst,
            
            i_bytes_to_transmit     => bytes_to_transmit_sel,
            i_data_to_player        => data_to_player_sel,
            i_data_to_player_wr     => data_to_player_wr_sel,
            o_data_to_player_rdy    => data_to_player_rdy_sel,
            
            -- streaming AXI to CMAC
            o_tx_axis_tdata         => o_tx_axis_tdata,
            o_tx_axis_tkeep         => o_tx_axis_tkeep,
            o_tx_axis_tvalid        => o_tx_axis_tvalid,
            o_tx_axis_tlast         => o_tx_axis_tlast,
            o_tx_axis_tuser         => o_tx_axis_tuser,
            i_tx_axis_tready        => i_tx_axis_tready,
            
            -- LBUS to CMAC
            -- o_data_to_transmit      : out t_lbus_sosi;
            i_data_to_transmit_ctl  => c_lbus_siso_rst
        );

end Behavioral;
