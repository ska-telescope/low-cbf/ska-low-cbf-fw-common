----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 07/14/2022 
-- Design Name: 
-- Module Name: memory_dp_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE, common_lib, xpm;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use common_lib.common_pkg.ALL;
use xpm.vcomponents.all;
library UNISIM;
use UNISIM.VComponents.all;


entity memory_tdp_spead is
    GENERIC (
        MEMORY_INIT_FILE    : STRING := "none";
        g_NO_OF_ADDR_BITS   : INTEGER := 9;
        g_D_Q_WIDTH         : INTEGER := 512
    );
    Port ( 
        clk_a           : in std_logic;
        clk_b           : in std_logic;
    
        data_in_a       : in std_logic_vector((g_D_Q_WIDTH-1) downto 0);
        addr_in_a       : in std_logic_vector((g_NO_OF_ADDR_BITS-1) downto 0);
        data_in_wr_a    : in std_logic; 
        data_out_a      : out std_logic_vector((g_D_Q_WIDTH-1) downto 0);

        data_in_b       : in std_logic_vector((g_D_Q_WIDTH-1) downto 0);
        addr_in_b       : in std_logic_vector((g_NO_OF_ADDR_BITS-1) downto 0);
        data_in_wr_b    : in std_logic; 
        data_out_b      : out std_logic_vector((g_D_Q_WIDTH-1) downto 0)
    
    );
end memory_tdp_spead;

architecture Behavioral of memory_tdp_spead is


CONSTANT ADDR_SPACE             : INTEGER := pow2(g_NO_OF_ADDR_BITS);
CONSTANT MEMORY_SIZE_GENERIC    : INTEGER := ADDR_SPACE * g_D_Q_WIDTH;

signal ram_wr_a     : std_logic_vector(0 downto 0);
signal ram_wr_b     : std_logic_vector(0 downto 0);

begin

ram_wr_a(0)   <= data_in_wr_a;
ram_wr_b(0)   <= data_in_wr_b;


xpm_memory_sdpram_inst : xpm_memory_tdpram
    generic map (    
        -- Common module generics
        AUTO_SLEEP_TIME         => 0,              --Do not Change
        CASCADE_HEIGHT          => 0,
        CLOCKING_MODE           => "independent_clock", --string; "common_clock", "independent_clock" 
        ECC_MODE                => "no_ecc",       --string; "no_ecc", "encode_only", "decode_only" or "both_encode_and_decode" 

        MEMORY_INIT_FILE        => MEMORY_INIT_FILE,         --string; "none" or "<filename>.mem" 
        MEMORY_INIT_PARAM       => "",             --string;
        MEMORY_OPTIMIZATION     => "true",          --string; "true", "false" 
        MEMORY_PRIMITIVE        => "block",         --string; "auto", "distributed", "block" or "ultra" ;
        MEMORY_SIZE             => MEMORY_SIZE_GENERIC, --262144,          -- Total memory size in bits; 512 x 512 = 262144
        MESSAGE_CONTROL         => 0,              --integer; 0,1

        USE_MEM_INIT            => 0,              --integer; 0,1
        WAKEUP_TIME             => "disable_sleep",--string; "disable_sleep" or "use_sleep_pin" 
        USE_EMBEDDED_CONSTRAINT => 0,              --integer: 0,1
       
    
        RST_MODE_A              => "SYNC",   
        RST_MODE_B              => "SYNC", 
        WRITE_MODE_A            => "no_change",    --string; "write_first", "read_first", "no_change" 
        WRITE_MODE_B            => "no_change",    --string; "write_first", "read_first", "no_change" 

        -- Port A module generics
        READ_DATA_WIDTH_A       => g_D_Q_WIDTH,    
        READ_LATENCY_A          => 3,              
        READ_RESET_VALUE_A      => "0",            

        WRITE_DATA_WIDTH_A      => g_D_Q_WIDTH,
        BYTE_WRITE_WIDTH_A      => g_D_Q_WIDTH,
        ADDR_WIDTH_A            => g_NO_OF_ADDR_BITS,
    
        -- Port B module generics
        READ_DATA_WIDTH_B       => g_D_Q_WIDTH,
        READ_LATENCY_B          => 3,           
        READ_RESET_VALUE_B      => "0",         

        WRITE_DATA_WIDTH_B      => g_D_Q_WIDTH,
        BYTE_WRITE_WIDTH_B      => g_D_Q_WIDTH,             
        ADDR_WIDTH_B            => g_NO_OF_ADDR_BITS
        )
    port map (
        -- Common module ports
        sleep                   => '0',
        injectsbiterra          => '0',
        injectdbiterra          => '0',
        injectsbiterrb          => '0',
        injectdbiterrb          => '0',
        sbiterrb                => open,
        dbiterrb                => open,

        -- Port A (Write side)
        rsta                    => '0',
        clka                    => clk_a,  
        ena                     => '1',
        regcea                  => '1',

        wea                     => ram_wr_a,
        addra                   => addr_in_a,
        dina                    => data_in_a,
        douta                   => data_out_a,

        -- Port B (read side)
        rstb                    => '0',
        clkb                    => clk_b,
        enb                     => '1',
        regceb                  => '1',

        web                     => ram_wr_b,
        addrb                   => addr_in_b,
        dinb                    => data_in_b,
        doutb                   => data_out_b

    );


end Behavioral;
