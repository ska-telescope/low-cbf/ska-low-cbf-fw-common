----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: Sept 2023 
-- Design Name: 
-- Module Name: s_axi_to_txt
--  
-- Description: 
--      For simulation purposes to record to txt file a streaming axi bus for 100G CMAC
--      
--      to match the PCAP / Wireshark flow
--      64 bytes of data width.
--
--      TXT file format
--      Col 1 - 4 zeroes
--      Col 2 - tvalid
--      Col 3 - tlast
--      Col 4 - tkeep (64 bits to indicate validity)
--      Col 5 - tdata (1st byte in the packet is in bits 511:504)
--
--      data is swapped such that the upper byte goes to 7->0, to match the s_axi implementation
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;
use IEEE.std_logic_textio.all;

entity s_axi_to_txt is
    generic (
        g_output_folder_location    : string := "../../../../../../../";
        
        g_output_file_name          : string := "s_axi_output.txt"
    );
    Port (
        streaming_axi_clock     : in std_logic;                      
        streaming_axi_reset     : in std_logic;                      

        streaming_axi_tdata     : in std_logic_vector(511 downto 0); 
        streaming_axi_tkeep     : in std_logic_vector(63 downto 0);  
        streaming_axi_tlast     : in std_logic;                      
        streaming_axi_tuser     : in std_logic;  
        streaming_axi_tvalid    : in std_logic;

        tx_pkt_num              : out integer
    );
end s_axi_to_txt;

architecture Behavioral of s_axi_to_txt is

constant FOUR0 : std_logic_vector(15 downto 0) := x"0000";
constant FOUR1 : std_logic_vector(3 downto 0) := "0001";

-- padding signals
signal tvalid_ext   : std_logic_vector(3 downto 0);
signal tlast_ext    : std_logic_vector(3 downto 0);
signal tuser_ext    : std_logic_vector(3 downto 0);
signal tdata_ext    : std_logic_vector(511 downto 0);
signal tkeep_ext    : std_logic_vector(63 downto 0);

signal eth100_tx_pkt_num : integer := 0;

begin

    -- PAD single bit signals to 4 bits for HEX.
    tvalid_ext  <= "000" & streaming_axi_tvalid;
    tlast_ext   <= "000" & streaming_axi_tlast;
    tuser_ext   <= "000" & streaming_axi_tuser;

    -- byte swap (LE) S_AXI to readable.
    gen_byte_swaps: for i in 0 to 63 GENERATE
        tdata_ext(i*8+7 downto i*8)     <= streaming_axi_tdata(503 - i*8 + 8 downto (504 - i*8)) ;  -- 512 bits
        tkeep_ext(i)                    <= streaming_axi_tkeep(63 - i);
    end GENERATE;


    process
		file logfile: TEXT;
		variable line_out : Line;
    begin
	    FILE_OPEN(logfile, g_output_folder_location & g_output_file_name, WRITE_MODE);
		
		loop
            -- wait until we need to read another command
            -- need to when : rising clock edge, and last_cmd_cycle high
            -- read the next entry from the file and put it out into the command queue.
            wait until rising_edge(streaming_axi_clock);
            if streaming_axi_tvalid = '1' then
                
                -- write data to the file
                hwrite(line_out,FOUR0,RIGHT,4);  -- repeats of this line, tied to 0
                
                hwrite(line_out,tvalid_ext,RIGHT,2);    -- tvalid
                hwrite(line_out,tlast_ext,RIGHT,2);     -- tlast
                hwrite(line_out,tkeep_ext,RIGHT,18);    -- tkeep
                hwrite(line_out,tdata_ext,RIGHT,130);   -- tdata
                hwrite(line_out,tuser_ext,RIGHT,2);     -- tuser
                
                writeline(logfile,line_out);

                if streaming_axi_tlast = '1' then
                    eth100_tx_pkt_num   <= eth100_tx_pkt_num + 1;
                end if;
            end if;
         
        end loop;
        file_close(logfile);	
        wait;
    end process;

    tx_pkt_num      <= eth100_tx_pkt_num;
end;