----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: Sept 2023 
-- Design Name: 
-- Module Name: txt_to_s_axi
--  
-- Description: 
--      For simulation purposes to record to txt file a streaming axi bus for 100G CMAC
--      
--      to match the PCAP / Wireshark flow
--      64 bytes of data, 1st byte in the packet is in bits 511:504.
--
--      TXT file format
--      Col 1 - 4 zeroes
--      Col 2 - tvalid
--      Col 3 - tlast
--      Col 4 - tkeep (64 bits to indicate validity)
--      Col 5 - tdata (1st byte in the packet is in bits 511:504)
--
--      data is swapped such that the upper byte goes to 7->0, to match the s_axi implementation
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;
use IEEE.std_logic_textio.all;

entity txt_to_s_axi is
    generic (
        g_input_folder_location     : string := "../../../../../../../";
        
        g_input_file_name           : string := "s_axi_input.txt"
    );
    Port (
        streaming_axi_clock     : in std_logic;                      
        streaming_axi_reset     : in std_logic;                      

        streaming_axi_tdata     : out std_logic_vector(511 downto 0); 
        streaming_axi_tkeep     : out std_logic_vector(63 downto 0);  
        streaming_axi_tlast     : out std_logic;                      
        streaming_axi_tuser     : out std_logic_vector(79 downto 0);
        streaming_axi_tvalid    : out std_logic;

        start_reading           : in std_logic;
        end_of_file             : out std_logic
    );
end txt_to_s_axi;

architecture Behavioral of txt_to_s_axi is

begin

    process
        file cmdfile        : TEXT;
        variable line_in    : Line;
        variable good       : boolean;

        variable sps_axi_repeats    : std_logic_vector(15 downto 0);
        variable sps_axi_tvalid     : std_logic_vector(3 downto 0);
        variable sps_axi_tlast      : std_logic_vector(3 downto 0);
        variable sps_axi_tkeep      : std_logic_vector(63 downto 0);
        variable sps_axi_tdata      : std_logic_vector(511 downto 0);
        variable sps_axi_tuser      : std_logic_vector(79 downto 0);
        
    begin
        
        streaming_axi_tdata     <= (others => '0'); -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
        streaming_axi_tkeep     <= (others => '0'); -- 64 bits,  one bit per byte in i_axi_tdata
        streaming_axi_tlast     <= '0';             -- in std_logic;
        streaming_axi_tuser     <= (others => '0'); -- 80 bit timestamp for the packet.
        streaming_axi_tvalid    <= '0';   -- in std_logic;        

        end_of_file             <= '0';
        
        FILE_OPEN(cmdfile, g_input_folder_location & g_input_file_name, READ_MODE);
        wait until start_reading = '1';
        
        wait until rising_edge(streaming_axi_clock);
        
        while (not endfile(cmdfile)) loop 
            readline(cmdfile, line_in);
            hread(line_in, sps_axi_repeats, good);
            hread(line_in, sps_axi_tvalid, good);
            hread(line_in, sps_axi_tlast, good);
            hread(line_in, sps_axi_tkeep, good);
            hread(line_in, sps_axi_tdata, good);
            hread(line_in, sps_axi_tuser, good);
            
            for i in 0 to 63 loop
                streaming_axi_tdata(i*8+7 downto i*8) <= sps_axi_tdata(503 - i*8 + 8 downto (504 - i*8)) ;  -- 512 bits
                streaming_axi_tkeep(i) <= sps_axi_tkeep(63 - i);
            end loop;

            streaming_axi_tlast     <= sps_axi_tlast(0);
            streaming_axi_tuser     <= sps_axi_tuser;
            streaming_axi_tvalid    <= sps_axi_tvalid(0);
            
            wait until rising_edge(streaming_axi_clock);
            while sps_axi_repeats /= "0000000000000000" loop
                sps_axi_repeats := std_logic_vector(unsigned(sps_axi_repeats) - 1);
                wait until rising_edge(streaming_axi_clock);
            end loop;
        end loop;
        
        end_of_file     <= '1';

        report "rx packets all received";
        wait;
    end process;

end;