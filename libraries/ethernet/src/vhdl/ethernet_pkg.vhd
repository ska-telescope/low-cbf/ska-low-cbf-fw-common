----------------------------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
----------------------------------------------------------------------------------------------------
--
-- Ethernet frames, headers etc
--
----------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

PACKAGE ethernet_pkg IS

constant c_ones_nibble  : std_logic_vector(3 downto 0) 	 := "1111";
constant c_ones_byte 	: std_logic_vector(7 downto 0) 	 := c_ones_nibble & c_ones_nibble;
constant c_ones_word 	: std_logic_vector(15 downto 0)  := c_ones_byte & c_ones_byte;
constant c_ones_dword 	: std_logic_vector(31 downto 0)  := c_ones_word & c_ones_word;
constant c_ones_qword 	: std_logic_vector(63 downto 0)  := c_ones_dword & c_ones_dword;
constant c_ones_64 	    : std_logic_vector(63 downto 0)  := c_ones_dword & c_ones_dword;
constant c_ones_128 	: std_logic_vector(127 downto 0) := c_ones_qword & c_ones_qword;
constant c_ones_256 	: std_logic_vector(255 downto 0) := c_ones_128 & c_ones_128;
constant c_ones_512 	: std_logic_vector(511 downto 0) := c_ones_256 & c_ones_256;

constant c_hex_a_32     : std_logic_vector(31 downto 0)  := (x"AAAAAAAA");
constant c_hex_a_64     : std_logic_vector(63 downto 0)  := c_hex_a_32 & c_hex_a_32;
constant c_hex_a_128    : std_logic_vector(127 downto 0) := c_hex_a_64 & c_hex_a_64;
constant c_hex_a_256    : std_logic_vector(255 downto 0) := c_hex_a_128 & c_hex_a_128;
constant c_hex_a_512    : std_logic_vector(511 downto 0) := c_hex_a_256 & c_hex_a_256;

constant c_hex_b_32     : std_logic_vector(31 downto 0)  := (x"bbbbbbbb");
constant c_hex_b_64     : std_logic_vector(63 downto 0)  := c_hex_b_32 & c_hex_b_32;
constant c_hex_b_128    : std_logic_vector(127 downto 0) := c_hex_b_64 & c_hex_b_64;
constant c_hex_b_256    : std_logic_vector(255 downto 0) := c_hex_b_128 & c_hex_b_128;
constant c_hex_b_512    : std_logic_vector(511 downto 0) := c_hex_b_256 & c_hex_b_256;

constant zero_nibble    : std_logic_vector(3 downto 0) 	:= "0000";
constant zero_byte 	    : std_logic_vector(7 downto 0) 	:= zero_nibble & zero_nibble;
constant zero_word 	    : std_logic_vector(15 downto 0) := zero_byte & zero_byte;
constant zero_dword 	: std_logic_vector(31 downto 0) := zero_word & zero_word;
constant zero_qword 	: std_logic_vector(63 downto 0) := zero_dword & zero_dword;
constant zero_32 	    : std_logic_vector(31 downto 0) := zero_word & zero_word;
constant zero_64 	    : std_logic_vector(63 downto 0) := zero_dword & zero_dword;
constant zero_128 	    : std_logic_vector(127 downto 0):= zero_qword & zero_qword;
constant zero_256 	    : std_logic_vector(255 downto 0):= zero_128 & zero_128;
constant zero_512       : std_logic_vector(511 downto 0):= zero_256 & zero_256;

constant one_nibble     : std_logic_vector(3 downto 0) 	:= "0001";
constant one_byte 	    : std_logic_vector(7 downto 0) 	:= zero_nibble & one_nibble;
constant one_word 	    : std_logic_vector(15 downto 0) := zero_byte & one_byte;
constant one_dword 	    : std_logic_vector(31 downto 0) := zero_word & one_word;

constant c_ethernet_frame_bytes                         : integer   := 14;
constant c_ipv4_hdr_bytes                               : integer   := 20;
constant c_udp_hdr_bytes                                : integer   := 8;
constant c_packet_start_bytes                           : integer   := c_ethernet_frame_bytes + c_ipv4_hdr_bytes + c_udp_hdr_bytes;

type ethernet_frame is record
    dst_mac     : std_logic_vector(47 downto 0);
    src_mac     : std_logic_vector(47 downto 0);
    eth_type    : std_logic_vector(15 downto 0);
end record;

type IPv4_header is record
    version         : std_logic_vector(3 downto 0);
    header_length   : std_logic_vector(3 downto 0);
    type_of_service : std_logic_vector(7 downto 0);
    total_length    : std_logic_vector(15 downto 0);
    id              : std_logic_vector(15 downto 0);
    ip_flags        : std_logic_vector(2 downto 0);
    fragment_off    : std_logic_vector(12 downto 0);
    TTL             : std_logic_vector(7 downto 0);
    protocol        : std_logic_vector(7 downto 0);
    header_chk_sum  : std_logic_vector(15 downto 0);
    src_addr        : std_logic_vector(31 downto 0);
    dst_addr        : std_logic_vector(31 downto 0);
end record;

type UDP_header is record
    src_port    : std_logic_vector(15 downto 0);
    dst_port    : std_logic_vector(15 downto 0);
    length      : std_logic_vector(15 downto 0);
    checksum    : std_logic_vector(15 downto 0);
end record;

TYPE t_ethernet_frame           IS ARRAY (INTEGER RANGE <>) OF ethernet_frame;
TYPE t_IPv4_header              IS ARRAY (INTEGER RANGE <>) OF IPv4_header;
TYPE t_UDP_header               IS ARRAY (INTEGER RANGE <>) OF UDP_header;

constant null_ethernet_frame : ethernet_frame := (
                                dst_mac         => (others=>'0'),
                                src_mac         => (others=>'0'),
                                eth_type        => (others=>'0')
                                );
                                
constant null_ipv4_header : IPv4_header := (
                                version         => (others=>'0'),
                                header_length   => (others=>'0'),
                                type_of_service => (others=>'0'),
                                total_length    => (others=>'0'),
                                id              => (others=>'0'),
                                ip_flags        => (others=>'0'),
                                fragment_off    => (others=>'0'),
                                TTL             => (others=>'0'),
                                protocol        => (others=>'0'),
                                header_chk_sum  => (others=>'0'),
                                src_addr        => (others=>'0'),
                                dst_addr        => (others=>'0')
                                );

constant null_UDP_header : UDP_header := (
                                src_port        => (others=>'0'),
                                dst_port        => (others=>'0'),
                                length          => (others=>'0'),
                                checksum        => (others=>'0')
                                );

constant base_ethernet_frame : ethernet_frame := (
                                dst_mac         => x"000000000000", -- SET by software
                                src_mac         => x"BEEF00C0FFEE", -- SET by software
                                eth_type        => x"0800"          -- STATIC
                                );                                

constant default_ethernet_frame : ethernet_frame := (
                                dst_mac         => x"DEAD0FEE0BEE",
                                src_mac         => x"BEEF00C0FFEE",
                                eth_type        => x"0800"
                                );

constant base_ipv4_header : IPv4_header := (
                                version         => x"4",            -- STATIC
                                header_length   => x"5",            -- STATIC
                                type_of_service => x"00",           -- STATIC
                                total_length    => x"0000",         -- CALCULATED LIVE
                                id              => x"ACDC",         -- STATIC
                                ip_flags        => "010",           -- STATIC
                                fragment_off    => (others=>'0'),   -- STATIC
                                TTL             => x"40",           -- STATIC
                                protocol        => x"11",           -- STATIC, UDP
                                header_chk_sum  => x"0000",         -- CALCULATED LIVE
                                src_addr        => x"00000000",     -- SET by software
                                dst_addr        => x"00000000"      -- SET by software
                                );

constant default_ipv4_header : IPv4_header := (
                                version         => x"4",
                                header_length   => x"5",
                                type_of_service => x"00",
                                total_length    => x"18AC",
                                id              => x"ACDC",
                                ip_flags        => "010",
                                fragment_off    => (others=>'0'),
                                TTL             => x"40",
                                protocol        => x"11",
                                header_chk_sum  => x"2F90",
                                src_addr        => x"C0A80065",     -- 192.168.0.101
                                dst_addr        => x"C0A80066"      -- 192.168.0.102
                                );

constant default_ipv4_header_2 : IPv4_header := (
                                version         => x"4",
                                header_length   => x"5",
                                type_of_service => x"00",
                                total_length    => x"18AC",
                                id              => x"ABBA",
                                ip_flags        => "010",
                                fragment_off    => (others=>'0'),
                                TTL             => x"40",
                                protocol        => x"11",
                                header_chk_sum  => x"2F90",
                                src_addr        => x"C0A80065",     -- 192.168.0.101
                                dst_addr        => x"C0A80067"      -- 192.168.0.103
                                );
                                
constant default_ipv4_header_3 : IPv4_header := (
                                version         => x"4",
                                header_length   => x"5",
                                type_of_service => x"00",
                                total_length    => x"18AC",
                                id              => x"ACDC",
                                ip_flags        => "010",
                                fragment_off    => (others=>'0'),
                                TTL             => x"40",
                                protocol        => x"11",
                                header_chk_sum  => x"2F90",
                                src_addr        => x"C0A80065",     -- 192.168.0.101
                                dst_addr        => x"C0A80068"      -- 192.168.0.104
                                );                                

constant base_UDP_header : UDP_header := (
                                src_port        => x"223D",         -- 8765, Set by Software
                                dst_port        => x"0000",         -- 0000, Set by Software
                                length          => x"0000",         -- 0000, Calculated LIVE
                                checksum        => (others=>'0')
                                );

constant default_UDP_header : UDP_header := (
                                src_port        => x"270F",         -- 9999
                                dst_port        => x"2526",         -- 9510
                                length          => x"1898",         -- 6296
                                checksum        => (others=>'0')
                                );

constant t_default_IPv4_header : t_IPv4_header(2 downto 0) := ( default_ipv4_header,
                                                                default_ipv4_header_2,
                                                                default_ipv4_header_3
                                                                );                                                            

Function byte_en_to_vector_count(byte_en: std_logic_vector(15 downto 0)) return std_logic_vector;

Function byte_en_to_integer_count(byte_en: std_logic_vector(15 downto 0)) return integer;

Function byte_en_to_unsigned_count(byte_en: std_logic_vector(15 downto 0)) return unsigned;

end ethernet_pkg;

PACKAGE BODY ethernet_pkg IS

FUNCTION byte_en_to_vector_count(byte_en: std_logic_vector(15 downto 0)) RETURN STD_LOGIC_VECTOR IS
    VARIABLE count : STD_LOGIC_VECTOR(4 DOWNTO 0);
    BEGIN
        CASE byte_en IS
            WHEN x"0000" => count :=  ('0' & x"0");
            WHEN x"0001" => count :=  ('0' & x"1");
            WHEN x"0003" => count :=  ('0' & x"2");
            WHEN x"0007" => count :=  ('0' & x"3");
            WHEN x"000F" => count :=  ('0' & x"4");
            WHEN x"001F" => count :=  ('0' & x"5");
            WHEN x"003F" => count :=  ('0' & x"6");
            WHEN x"007F" => count :=  ('0' & x"7");
            WHEN x"00FF" => count :=  ('0' & x"8");
            WHEN x"01FF" => count :=  ('0' & x"9");
            WHEN x"03FF" => count :=  ('0' & x"A");
            WHEN x"07FF" => count :=  ('0' & x"B");
            WHEN x"0FFF" => count :=  ('0' & x"C");
            WHEN x"1FFF" => count :=  ('0' & x"D");
            WHEN x"3FFF" => count :=  ('0' & x"E");
            WHEN x"7FFF" => count :=  ('0' & x"F");
            WHEN x"FFFF" => count :=  ('1' & x"0");
            
            WHEN OTHERS  => count := "00000";
        END CASE;
        
   RETURN count;
END byte_en_to_vector_count;

FUNCTION byte_en_to_integer_count(byte_en: std_logic_vector(15 downto 0)) RETURN integer IS
    VARIABLE value : integer;
    BEGIN
        CASE byte_en IS
            WHEN x"0000" => value :=  0 ;
            WHEN x"0001" => value :=  1 ;
            WHEN x"0003" => value :=  2 ;
            WHEN x"0007" => value :=  3 ;
            WHEN x"000F" => value :=  4 ;
            WHEN x"001F" => value :=  5 ;
            WHEN x"003F" => value :=  6 ;
            WHEN x"007F" => value :=  7 ;
            WHEN x"00FF" => value :=  8 ;
            WHEN x"01FF" => value :=  9 ;
            WHEN x"03FF" => value :=  10 ;
            WHEN x"07FF" => value :=  11 ;
            WHEN x"0FFF" => value :=  12 ;
            WHEN x"1FFF" => value :=  13 ;
            WHEN x"3FFF" => value :=  14 ;
            WHEN x"7FFF" => value :=  15 ;
            WHEN x"FFFF" => value :=  16 ;
            
            WHEN OTHERS  => value := 0;
        END CASE;
        
   RETURN value;
END byte_en_to_integer_count;

FUNCTION byte_en_to_unsigned_count(byte_en: std_logic_vector(15 downto 0)) RETURN unsigned IS
    VARIABLE value : unsigned(4 downto 0);
    BEGIN
        CASE byte_en IS
            WHEN x"0000" => value :=  ('0' & x"0");
            WHEN x"0001" => value :=  ('0' & x"1");
            WHEN x"0003" => value :=  ('0' & x"2");
            WHEN x"0007" => value :=  ('0' & x"3");
            WHEN x"000F" => value :=  ('0' & x"4");
            WHEN x"001F" => value :=  ('0' & x"5");
            WHEN x"003F" => value :=  ('0' & x"6");
            WHEN x"007F" => value :=  ('0' & x"7");
            WHEN x"00FF" => value :=  ('0' & x"8");
            WHEN x"01FF" => value :=  ('0' & x"9");
            WHEN x"03FF" => value :=  ('0' & x"A");
            WHEN x"07FF" => value :=  ('0' & x"B");
            WHEN x"0FFF" => value :=  ('0' & x"C");
            WHEN x"1FFF" => value :=  ('0' & x"D");
            WHEN x"3FFF" => value :=  ('0' & x"E");
            WHEN x"7FFF" => value :=  ('0' & x"F");
            WHEN x"FFFF" => value :=  ('1' & x"0");
            
            WHEN OTHERS  => value := "00000";
        END CASE;
        
   RETURN value;
END byte_en_to_unsigned_count;


end ethernet_pkg;