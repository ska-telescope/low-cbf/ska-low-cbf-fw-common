----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: June 2024
--
-- IPv4 checksum module
--
----------------------------------------------------------------------------------


library IEEE, xpm, common_lib, signal_processing_common, ethernet_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use xpm.vcomponents.all;
use common_lib.common_pkg.ALL;
use ethernet_lib.ethernet_pkg.all;


library UNISIM;
use UNISIM.VComponents.all;

entity ipv4_chksum is
    Port ( 
        i_clk                   : IN STD_LOGIC;
        i_clk_reset             : IN STD_LOGIC;
    
        i_chksum_start          : IN STD_LOGIC;
        o_chksum_complete       : OUT STD_LOGIC;

        i_ipv4_total_bytes      : IN STD_LOGIC_VECTOR(15 downto 0);
        
        i_current_ipv4          : IPv4_header

    );
end ipv4_chksum;

architecture rtl of ipv4_chksum is

type ipv4_chksum_sm_fsm is      (
        IDLE, 
        CALC_1, 
        WAIT_PACKET, 
        CALC_2,
        CALC_3,
        READY,
        HOLDING
        );
signal ipv4_chksum_sm : ipv4_chksum_sm_fsm;

signal clk      : std_logic;
signal reset    : std_logic;

signal packet_calc_done     : std_logic;
signal packet_calc_start    : std_logic;

signal current_ipv4         : IPv4_header := null_ipv4_header;

signal ipv4_chk_sum_calc        : std_logic_vector(31 downto 0);
signal ipv4_chk_sum_base        : std_logic_vector(31 downto 0);

signal ipv4_dst_addr_chk        : unsigned(16 downto 0)     := (others => '0');
signal ipv4_src_addr_chk        : unsigned(16 downto 0)     := (others => '0');

signal ipv4_chksum_sm_cnt       : unsigned(3 downto 0);

signal ipv4_chksum_sm_fsm_debug : std_logic_vector(3 downto 0)  := x"0";

begin

------------------------------------------------------------------------------

o_chksum_complete       <= packet_calc_done;

packet_calc_start       <= i_chksum_start;
clk                     <= i_clk;
reset                   <= i_clk_reset;

------------------------------------------------------------------------------
-- IPv4 Checksum
-- precalculate the static fields and be ready for dynamic updates to
-- total_length and dst_addr.

-- 0x4500   = base_ipv4_header.version & base_ipv4_header.header_length & base_ipv4_header.type_of_service;
-- 0xACDC   = base_ipv4_header.id;
-- 0x4000   = base_ipv4_header.ip_flags & base_ipv4_header.fragment_off;
-- 0x4011   = base_ipv4_header.TTL & base_ipv4_header.protocol;
-- summed   = 0x0001_71ED


ipv4_checksum_proc : process(clk)
begin
    if rising_edge(clk) then
        if reset = '1' then
            current_ipv4                <= base_ipv4_header;    -- init the preload fields.
            ipv4_chk_sum_calc           <= (others => '0');
            ipv4_chk_sum_base           <= x"000171ED";
            ipv4_chksum_sm              <= IDLE;
            ipv4_chksum_sm_fsm_debug    <= x"F";
            packet_calc_done            <= '0';
            ipv4_src_addr_chk           <= (others => '0');
            ipv4_dst_addr_chk           <= (others => '0');
            ipv4_chksum_sm_cnt          <= x"0";
        else
            current_ipv4.src_addr       <= i_current_ipv4.src_addr;
            current_ipv4.dst_addr       <= i_current_ipv4.dst_addr;
            current_ipv4.total_length   <= i_current_ipv4.total_length;

            ipv4_src_addr_chk           <= ((unsigned('0' & current_ipv4.src_addr(31 downto 16))) + (unsigned('0' & current_ipv4.src_addr(15 downto 0))));
            ipv4_dst_addr_chk           <= ((unsigned('0' & current_ipv4.dst_addr(31 downto 16))) + (unsigned('0' & current_ipv4.dst_addr(15 downto 0))));

            case ipv4_chksum_sm is
                when IDLE =>
                    ipv4_chksum_sm_fsm_debug    <= x"0";
                    if i_chksum_start = '1' then
                        ipv4_chksum_sm          <= CALC_1;
                    end if;

                when CALC_1 => 
                    ipv4_chksum_sm_fsm_debug    <= x"1";
                    ipv4_chk_sum_base           <= std_logic_vector(unsigned(ipv4_chk_sum_base) + (ipv4_src_addr_chk));
                    ipv4_chksum_sm              <= WAIT_PACKET;

                when WAIT_PACKET =>
                    ipv4_chksum_sm_fsm_debug    <= x"2";
                    packet_calc_done            <= '0';
                    ipv4_chksum_sm_cnt          <= x"0";
                    --  Wait for packet parameters to settle then generate checksum.
                    current_ipv4.total_length   <= i_ipv4_total_bytes;
                    if i_chksum_start = '1' then
                        ipv4_chksum_sm          <= CALC_2;
                        ipv4_chk_sum_calc       <= std_logic_vector(unsigned(ipv4_chk_sum_base) + (ipv4_dst_addr_chk));
                    end if;

                when CALC_2 => 
                    ipv4_chksum_sm_fsm_debug    <= x"3";
                    ipv4_chk_sum_calc           <= std_logic_vector(unsigned(ipv4_chk_sum_calc) + unsigned(current_ipv4.total_length));
                    ipv4_chksum_sm              <= CALC_3;

                when CALC_3 => 
                    ipv4_chksum_sm_fsm_debug    <= x"4";
                    ipv4_chk_sum_calc           <= NOT (zero_word & std_logic_vector(unsigned(ipv4_chk_sum_calc(15 downto 0)) + unsigned(ipv4_chk_sum_calc(31 downto 16))));
                    ipv4_chksum_sm              <= READY;                    

                when READY => 
                    ipv4_chksum_sm_fsm_debug    <= x"5";

                    current_ipv4.header_chk_sum <= ipv4_chk_sum_calc(15 downto 0);
                    ipv4_chksum_sm              <= HOLDING;

                when HOLDING =>
                    if ipv4_chksum_sm_cnt < 2 then
                        ipv4_chksum_sm_cnt      <= ipv4_chksum_sm_cnt + 1;
                    else
                        ipv4_chksum_sm          <= WAIT_PACKET;
                        packet_calc_done        <= '1';
                    end if;

                when others =>
                    ipv4_chksum_sm      <= IDLE;
            end case;
        end if;
    end if;
end process;

end rtl;