----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: June 2024
--
-- calculate incoming data rate for 100G CMAC interface
-- provide windows of summary.
--
--
-- Clock rate for 100G is 322,265,625 Hz
-- This allows for data much faster than 100G
-- interface is 512 bit (64 byte)
-- ~165 Gbit on the interface possible.
--
-- 
----------------------------------------------------------------------------------

library IEEE, xpm, common_lib, signal_processing_common;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use xpm.vcomponents.all;
use common_lib.common_pkg.ALL;


library UNISIM;
use UNISIM.VComponents.all;

entity cmac_100g_bw_mon is
    Port ( 
        i_clk               : IN STD_LOGIC;
        i_clk_reset         : IN STD_LOGIC;
    
        -- Data in from the 100GE MAC
        i_100GE_clk         : in std_logic;     -- 322 MHz from the 100GE MAC; note 512 bits x 322 MHz = 165 Mbit/sec, so even full rate traffic will have .valid low 1/3rd of the time.
        i_100GE_rst         : in std_logic;

        i_axis_tdata        : in std_logic_vector(511 downto 0); -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
        i_axis_tkeep        : in std_logic_vector(63 downto 0);  -- one bit per byte in i_axi_tdata
        i_axis_tlast        : in std_logic;                      
        i_axis_tuser        : in std_logic_vector(79 downto 0);  -- Timestamp for the packet.
        i_axis_tvalid       : in std_logic;

        o_bw_sec            : out t_slv_8_arr(3 downto 0);
        o_bw_min            : out t_slv_8_arr(3 downto 0)

    );
end cmac_100g_bw_mon;

architecture rtl of cmac_100g_bw_mon is

type ipv4_chksum_sm_fsm is      (
        IDLE, 
        CALC_1, 
        WAIT_PACKET, 
        CALC_2,
        CALC_3,
        READY,
        HOLDING
        );
signal ipv4_chksum_sm : ipv4_chksum_sm_fsm;

signal clk      : std_logic;
signal reset    : std_logic;

signal bw_sec               : t_slv_8_arr(3 downto 0);
signal bw_min               : t_slv_8_arr(3 downto 0);

signal axis_tdata           : std_logic_vector(511 downto 0); -- 64 bytes of data, 1st byte in the packet is in bits 7:0.
signal axis_tkeep           : std_logic_vector(63 downto 0);  -- one bit per byte in i_axi_tdata
signal axis_tlast           : std_logic;                      
signal axis_tuser           : std_logic_vector(79 downto 0);  -- Timestamp for the packet.
signal axis_tvalid          : std_logic;

-- cycles for 5us for a 100G CMAC clock rate of ~322.265 MHz.
constant five_ms        : unsigned(23 downto 0) := 24D"16113281";
signal timer_count      : unsigned(23 downto 0) := (others => '0');
signal quarter_sec      : unsigned(3 downto 0)  := x"0";
signal quarter_min      : unsigned(7 downto 0)  := x"00";

signal quarter_sec_p    : std_logic_vector(3 downto 0);
signal quarter_min_p    : std_logic_vector(3 downto 0);

signal write_count      : unsigned(35 downto 0);

signal current_count    : unsigned(35 downto 0);

signal packet_tracker   : unsigned(15 downto 0) := x"0000";

begin

------------------------------------------------------------------------------

inc_reg_proc : process (i_100GE_clk)
begin
    if rising_edge(i_100GE_clk) then
        axis_tdata  <= i_axis_tdata;
        axis_tkeep  <= i_axis_tkeep;
        axis_tlast  <= i_axis_tlast;
        axis_tuser  <= i_axis_tuser;
        axis_tvalid <= i_axis_tvalid;

        o_bw_sec    <= bw_sec;
        o_bw_min    <= bw_min;

    end if;
end process;

------------------------------------------------------------------------------

timing_proc : process (i_100GE_clk)
begin
    if rising_edge(i_100GE_clk) then
        if i_100GE_rst = '1' then
            timer_count     <= 24D"16100000";
            quarter_sec     <= x"0";
            quarter_min     <= x"00";
            quarter_sec_p   <= x"0";
            quarter_min_p   <= x"0";
        else
            if five_ms = timer_count then
                timer_count <= (others => '0');
                
                if quarter_sec = 4 then
                    quarter_sec         <= x"0";
                    quarter_sec_p(0)    <= '1';

                    if quarter_min = 59 then
                        quarter_min         <= x"00";
                        quarter_min_p(0)    <= '1';
                    else
                        quarter_min <= quarter_min + 1;
                    end if;

                else
                    quarter_sec <= quarter_sec + 1;
                end if;
            else
                timer_count         <= timer_count + 1;
                quarter_sec_p(0)    <= '0';
                quarter_min_p(0)    <= '0';
            end if;

            quarter_sec_p   <= quarter_sec_p(2 downto 0) & '0';
            quarter_min_p   <= quarter_min_p(2 downto 0) & '0';
        end if;
    end if;
end process;

bw_proc : process (i_100GE_clk)
begin
    if rising_edge(i_100GE_clk) then
        if i_100GE_rst = '1' then
            write_count     <= (others => '0');
            current_count   <= (others => '0');
            packet_tracker  <= (others => '0');
        else
            if quarter_sec_p(1) = '1' then
                write_count     <= (others => '0');
                current_count   <= write_count;
            elsif axis_tvalid = '1' then
                write_count     <= write_count + 1;
            end if;

            -- Rolling counter that indicates how often the following occurs.
            -- The peak data rate for 100 consecutive packets on a link shall
            -- be no more than 20% above the average data rate of 69.3 Gbps.
            -- 83.16 Gbps -> 83,160,000,000 -> 162,421,875 writes on the bus.
            -- assume each write is 512 bits
            -- 162,421,875 writes.
            -- SPEAD packets are 8276 bytes -> 66208 bits, 130 writes on the bus.
            -- 100 packets = 13000 writes.
            -- Total packets per second = writes on bus (83.16) / 130 = 1,249,399
            -- Time window for packets = 100 / 1,249,399 = 
            -- 0.000,080,038 -> 80.038 us.
            -- 322 MHz -> 3.1ns
            -- number of cycles in the 80us window -> 25820 cycles

            -- timestamp for 




            -- setup for Gbit
            -- count records 512 bits, want a number 1-99 Gbit
            -- dealing in quarter second intervals so number should be 25ish as a max.
            if quarter_sec_p(2) = '1' then
                bw_sec(0)  <= 


            end if;
        end if;
    end if;
end process;

--------------------------------------
-- store packet capture information.
-- Timeslave IP provides time on the first write of a packet.
-- Rest of the time the tuser is zero.

end rtl;