----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 01/05/2023 
-- Design Name: 
-- Module Name: sps_spead_packet.vhd
--  
-- Description: 
-- 
-- Generate SPS spead packets, just one packet type required.
-- Initial case is zero payload.
--      
--

----------------------------------------------------------------------------------

library IEEE, correlator_lib, common_lib, signal_processing_common, spead_sps_lib, ethernet_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;
USE common_lib.common_pkg.ALL;
use ethernet_lib.ethernet_pkg.ALL;
use spead_sps_lib.spead_sps_pkg.ALL;

entity sps_spead_packet is
    Generic ( 
        g_DEBUG_VEC_SIZE        : INTEGER := 1;
        g_DEBUG_ILA             : BOOLEAN := FALSE
    );
    Port ( 
        -- clock used for all data input and output from this module (300 MHz)
        i_clk                   : in std_logic;
        i_rst                   : in std_logic;

        i_local_reset           : in std_logic;

        -- signals to pass into packetiser
        i_spead_config_list     : in spead_config_items;

        -- data for SPEAD section signals
        i_data_from_hbm         : in std_logic_vector(511 downto 0);
        o_data_from_hbm_rd      : out std_logic;
        i_data_from_hbm_pkt_rdy : in std_logic;

        -- S_AXI aligned.
        o_bytes_to_transmit     : OUT STD_LOGIC_VECTOR(13 downto 0);     
        o_data_to_player        : OUT STD_LOGIC_VECTOR(511 downto 0);
        o_data_to_player_wr     : OUT STD_LOGIC;
        i_data_to_player_rdy    : IN STD_LOGIC;

        o_packetiser_enable     : out std_logic;

        -- debug vector
        i_debug                 : in std_logic_vector((g_DEBUG_VEC_SIZE-1) downto 0);
        
        -- ARGs interface.
        i_spead_lite_axi_mosi   : in t_axi4_lite_mosi; 
        o_spead_lite_axi_miso   : out t_axi4_lite_miso;
        i_spead_full_axi_mosi   : in  t_axi4_full_mosi;
        o_spead_full_axi_miso   : out t_axi4_full_miso

    );
end sps_spead_packet;

architecture Behavioral of sps_spead_packet is

COMPONENT ila_0
PORT (
    clk : IN STD_LOGIC;
    probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
END COMPONENT;

signal clk                      : std_logic;
signal reset                    : std_logic;

signal s_axi_data_in            : std_logic_vector(511 downto 0);
signal s_axi_data_out           : std_logic_vector(511 downto 0);

signal data_to_player           : STD_LOGIC_VECTOR(511 downto 0);
signal data_to_player_wr        : STD_LOGIC;

signal bytes_to_transmit_d      : STD_LOGIC_VECTOR(13 downto 0);     
signal data_to_player_d         : STD_LOGIC_VECTOR(511 downto 0);
signal data_to_player_wr_d      : STD_LOGIC;

type spead_sm_fsm is            (
                                IDLE, 
                                CONFIGURE, 
                                SEND_DATA, 
                                FRAME_HEADER_1,
                                FRAME_HEADER_2,
                                FRAME_DATA,
                                COMPLETE                                
                                );
signal spead_sm : spead_sm_fsm;

-------------------------------------
-- Byte calcs for packet

signal bytes_to_transmit                                : std_logic_vector(13 downto 0);
signal ipv4_total_bytes_to_calc                         : std_logic_vector(15 downto 0);
signal udp_total_bytes                                  : std_logic_vector(15 downto 0);

-------------------------------------

signal govern_count             : unsigned(31 downto 0)  := x"00000000";

signal spead_sm_fsm_debug       : std_logic_vector(3 downto 0)  := x"0";

signal current_ethernet         : ethernet_frame            := null_ethernet_frame;
signal current_ipv4             : IPv4_header               := null_ipv4_header;
signal current_udp              : UDP_header                := null_UDP_header;

signal current_ethernet_packed  : std_logic_vector(111 downto 0)    := (others => '0');
signal current_ipv4_packed      : std_logic_vector(159 downto 0)    := (others => '0');
signal current_udp_packed       : std_logic_vector(63 downto 0)     := (others => '0');

---------------------------------------------
signal spead_data_v1_packet     : spead_sps_v1_packet   := default_sps_spead_v1_packet;
signal spead_data_v2_packet     : spead_sps_v2_packet   := default_sps_spead_v2_packet;
signal spead_data_v3_packet     : spead_sps_v3_packet   := default_sps_spead_v3_packet;

constant sps_length             : integer   := c_spead_sps_data_header_byte_count * 8;
signal packed_sel_spead_header  : std_logic_vector(sps_length-1 downto 0)       := (others => '0');
signal packed_v1_spead_header   : std_logic_vector(sps_length-1 downto 0)       := (others => '0');
signal packed_v2_spead_header   : std_logic_vector(sps_length-1 downto 0)       := (others => '0');
signal packed_v3_spead_header   : std_logic_vector(sps_length-1 downto 0)       := (others => '0');

signal version_header_select    : std_logic_vector(1 downto 0);

---------------------------------------------
signal spead_data_d1            : std_logic_vector(511 downto 0);
signal spead_data_d2            : std_logic_vector(511 downto 0);

---------------------------------------------
-- to register component
signal packet_calc_init         : std_logic;
signal packet_calc_done         : std_logic;
signal checksum_calc_trigger    : std_logic_vector(7 downto 0);

signal data_packet_sent         : std_logic;

signal enable_packetiser        : std_logic;


begin

---------------------------------------------------------------------
-- PORT MAPPING
clk                     <= i_clk;
reset                   <= i_rst OR i_local_reset;

o_bytes_to_transmit     <= bytes_to_transmit_d;
o_data_to_player_wr     <= data_to_player_wr_d;
o_data_to_player        <= data_to_player_d;

o_packetiser_enable     <= enable_packetiser;

--------------------------------------------------------------------
-- byte counts for packet
bytes_to_transmit           <=  std_logic_vector(to_unsigned(c_data_packet_total_header_bytes       , bytes_to_transmit'length))         when version_header_select(1) = '0' else
                                std_logic_vector(to_unsigned(c_data_packet_v3_total_header_bytes    , bytes_to_transmit'length));

ipv4_total_bytes_to_calc    <=  std_logic_vector(to_unsigned(c_data_packet_total_ipv4_bytes         , ipv4_total_bytes_to_calc'length))  when version_header_select(1) = '0' else
                                std_logic_vector(to_unsigned(c_data_packet_v3_total_ipv4_bytes      , ipv4_total_bytes_to_calc'length));

udp_total_bytes             <=  std_logic_vector(to_unsigned(c_data_packet_total_udp_bytes          , udp_total_bytes'length))           when version_header_select(1) = '0' else
                                std_logic_vector(to_unsigned(c_data_packet_v3_total_udp_bytes       , udp_total_bytes'length));

--------------------------------------------------------------------
main_proc : process(clk)
begin
    if rising_edge(clk) then
        if reset = '1' or enable_packetiser = '0' then
            spead_sm                    <= IDLE;
            spead_sm_fsm_debug          <= x"F";
            data_to_player              <= zero_512;
            data_to_player_wr           <= '0';
            data_packet_sent            <= '0';
            packet_calc_init            <= '0';
            checksum_calc_trigger       <= x"00";
            govern_count                <= x"00000000";
            o_data_from_hbm_rd          <= '0';
        else

            packet_calc_init        <= checksum_calc_trigger(7);
            checksum_calc_trigger   <= checksum_calc_trigger(6 downto 0) & '0';

            spead_data_d1           <= i_data_from_hbm;
            spead_data_d2           <= spead_data_d1;

            case spead_sm is
                when IDLE => 
                    spead_sm_fsm_debug          <= x"0";

                    data_packet_sent            <= '0';
                    data_to_player_wr           <= '0';
                    data_to_player              <= zero_512;
                    checksum_calc_trigger       <= x"00";
                    o_data_from_hbm_rd          <= '0';

                    -- if packet player has space and enable is on.
                    if i_data_to_player_rdy = '1' then
                        if i_data_from_hbm_pkt_rdy = '1' and govern_count = x"00000000" then
                            spead_sm                    <= CONFIGURE;
                            checksum_calc_trigger       <= x"01";    
                        end if;
                        
                    end if;

                    -- govern is a hold off, this should count even if the HBM hasn't filled the packet data FIFO.
                    if govern_count /= x"00000000" then
                        govern_count <= govern_count - 1;
                    end if;
                    

        ------------------------------------------------------------------------------------------------
        -- Data packet handling
                when CONFIGURE => 
                    spead_sm_fsm_debug          <= x"4";
                    spead_sm                    <= SEND_DATA;

                when SEND_DATA => 
                    spead_sm_fsm_debug          <= x"5";
                    if packet_calc_done = '1' then
                        spead_sm                <= FRAME_HEADER_1;
                    end if;               


                -- Ethernet headers = 42 bytes
                -- SPEAD V1 and V2 = 72 Bytes
                -- Data = 8192 Bytes
                -- Total bytes = 8306
                -- Writes to CMAC in lots of 64 bytes, so 130 writes.
                -- 130th write is not all 64 bytes = 8306 - (129*64) = 50 bytes.
                -- V3 is 16 bytes less so no changes to the number of writes to CMAC. (ie govern count)
                when FRAME_HEADER_1 =>
                    spead_sm_fsm_debug          <= x"6";
                    data_to_player              <= current_ethernet_packed & current_ipv4_packed & current_udp_packed & packed_sel_spead_header(575 downto 400);   -- first 336 bits are start of packet E/I/U.
                    data_to_player_wr           <= '1';
                    o_data_from_hbm_rd          <= '1';

                    spead_sm                    <= FRAME_HEADER_2;
                    
                when FRAME_HEADER_2 =>
                    spead_sm_fsm_debug          <= x"7";
                    if version_header_select(1) = '0' then
                        data_to_player              <= packed_sel_spead_header(399 downto 0) & i_data_from_hbm(511 downto 400); --112D"0";
                    else
                        data_to_player              <= packed_sel_spead_header(399 downto 128) & i_data_from_hbm(511 downto 272); --112D"0";
                    end if;
                    spead_sm                    <= FRAME_DATA;

                when FRAME_DATA =>
                    spead_sm_fsm_debug          <= x"8";
                    if version_header_select(1) = '0' then
                        data_to_player              <= spead_data_d1(399 downto 0) & i_data_from_hbm(511 downto 400);
                    else
                        data_to_player              <= spead_data_d1(271 downto 0) & i_data_from_hbm(511 downto 272);
                    end if;
                    govern_count                <= x"0000007E";   -- count 126 of zero data for 8306 bytes.

                    spead_sm                    <= COMPLETE;

        ------------------------------------------------------------------------------------------------

                when COMPLETE => 
                    spead_sm_fsm_debug          <= x"9";
                    if version_header_select(1) = '0' then
                        data_to_player              <= spead_data_d1(399 downto 0) & i_data_from_hbm(511 downto 400);
                    else
                        data_to_player              <= spead_data_d1(271 downto 0) & i_data_from_hbm(511 downto 272);
                    end if;

                    govern_count                <= govern_count - 1;

                    if govern_count = x"00000001" then
                        o_data_from_hbm_rd  <= '0';
                    end if;

                    if govern_count = x"00000000" then
                        spead_sm            <= IDLE;
                        data_packet_sent    <= '1';
                        govern_count        <= unsigned(i_spead_config_list.time_between_packets);
                    end if;

                when OTHERS =>
                    spead_sm    <= IDLE;
            end case;
        end if;


    end if;
end process;

--------------------------------------------------------------------
-- PACK data.



--------------------------------------------------------------------
-- byte swap data vector for S_AXI transmission on the MAC.
-- delay other like signals.

s_axi_data_in       <= data_to_player;

s_axi_swap_gen : for i in 0 to 63 generate
    s_axi_byte_proc : process(clk)
    begin
        if rising_edge(clk) then
            s_axi_data_out(((8*i)+7) downto (i*8))  <= s_axi_data_in(((512-1)-(8*i)) downto ((512-8)-(8*i)));
        end if;
    end process;
end generate;

data_to_player_d    <= s_axi_data_out;

delay_proc_MAC : process(clk)
begin
    if rising_edge(clk) then
        bytes_to_transmit_d <= bytes_to_transmit;
        data_to_player_wr_d <= data_to_player_wr;
    end if;
end process;

--------------------------------------------------------------------
-- pack records for SM

current_ethernet_packed     <=  current_ethernet.dst_mac & current_ethernet.src_mac & current_ethernet.eth_type;

current_ipv4_packed         <=  current_ipv4.version    & current_ipv4.header_length    & current_ipv4.type_of_service  & current_ipv4.total_length & 
                                current_ipv4.id         & current_ipv4.ip_flags         & current_ipv4.fragment_off     & current_ipv4.TTL & 
                                current_ipv4.protocol   & current_ipv4.header_chk_sum   & current_ipv4.src_addr         & current_ipv4.dst_addr; 

current_udp_packed          <=  current_udp.src_port    & current_udp.dst_port          & current_udp.length            & current_udp.checksum;

version_header_select       <= i_spead_config_list.version_select;

packed_header : process(clk)
begin
    if rising_edge(clk) then
        if version_header_select = "00" then
            packed_sel_spead_header     <= packed_v1_spead_header;
        elsif version_header_select = "01" then
            packed_sel_spead_header     <= packed_v2_spead_header;
        else
            packed_sel_spead_header     <= packed_v3_spead_header;
        end if;
    end if;
end process;

packed_v1_spead_header      <=  default_spead_common_fields.spead_magic             & default_spead_common_fields.spead_version         & default_spead_common_fields.spead_item_pointer_width & 
                                default_spead_common_fields.spead_heap_add_width    & default_spead_common_fields.spead_reserved        & 
                                spead_data_v1_packet.number_of_items & 
                                -- 1
                                spead_data_v1_packet.heap_count_header              & spead_data_v1_packet.logical_channel_id           & i_spead_config_list.packet_counter(31 downto 0) & 
                                -- 2
                                spead_data_v1_packet.pkt_length_header              & i_spead_config_list.packet_payload_length         & 
                                -- 3
                                spead_data_v1_packet.sync_time_header               & i_spead_config_list.unix_epoch_time               & 
                                -- 4
                                spead_data_v1_packet.timestamp_header               & i_spead_config_list.timestamp                     & 
                                -- 5
                                spead_data_v1_packet.center_freq_header             & i_spead_config_list.frequency_hz                  &
                                -- 6
                                spead_data_v1_packet.csp_chan_info_header           & spead_data_v1_packet.csp_chan_info_reserved       & i_spead_config_list.beam_id & i_spead_config_list.freq_id &
                                -- 7
                                spead_data_v1_packet.csp_ant_info_header            & i_spead_config_list.substation_id & i_spead_config_list.subarray_id & i_spead_config_list.station_id & spead_data_v1_packet.nof_contrib_antenna &
                                -- 8
                                spead_data_v1_packet.sample_offset_header           & spead_data_v1_packet.payload_offset;


packed_v2_spead_header      <=  default_spead_common_fields.spead_magic             & default_spead_common_fields.spead_version         & default_spead_common_fields.spead_item_pointer_width & 
                                default_spead_common_fields.spead_heap_add_width    & default_spead_common_fields.spead_reserved        & 
                                spead_data_v2_packet.number_of_items & 
                                -- 1
                                spead_data_v2_packet.heap_count_header              & spead_data_v2_packet.station_channel_id           & i_spead_config_list.packet_counter(31 downto 0) & 
                                -- 2
                                spead_data_v2_packet.pkt_length_header              & i_spead_config_list.packet_payload_length         & 
                                -- 3
                                spead_data_v2_packet.sync_time_header               & spead_data_v2_packet.sync_time_reserved           & i_spead_config_list.unix_epoch_time(31 downto 0) & 
                                -- 4
                                spead_data_v2_packet.timestamp_header               & i_spead_config_list.timestamp                     & 
                                -- 5
                                spead_data_v2_packet.scan_id_header                 & spead_data_v2_packet.scan_id_reserved             & i_spead_config_list.scan_id(31 downto 0) &
                                -- 6
                                spead_data_v2_packet.chan_info_header               & spead_data_v2_packet.chan_info_reserved           & i_spead_config_list.beam_id & i_spead_config_list.freq_id &
                                -- 7
                                spead_data_v2_packet.ant_info_header                & i_spead_config_list.substation_id & i_spead_config_list.subarray_id & i_spead_config_list.station_id & spead_data_v2_packet.ant_info_reserved &
                                -- 8
                                spead_data_v2_packet.sample_offset_header           & spead_data_v2_packet.payload_offset;


packed_v3_spead_header      <=  default_spead_common_fields.spead_magic             & default_spead_common_fields.spead_version         & default_spead_common_fields.spead_item_pointer_width & 
                                default_spead_common_fields.spead_heap_add_width    & default_spead_common_fields.spead_reserved        & 
                                spead_data_v3_packet.number_of_items & 
                                -- 1
                                spead_data_v3_packet.heap_count_header              & i_spead_config_list.packet_counter                & 
                                -- 2
                                spead_data_v3_packet.pkt_length_header              & i_spead_config_list.packet_payload_length         & 
                                -- 3
                                spead_data_v3_packet.scan_id_header                 & i_spead_config_list.scan_id                       &
                                -- 4
                                spead_data_v3_packet.chan_info_header               & spead_data_v3_packet.chan_info_reserved           & i_spead_config_list.beam_id & i_spead_config_list.freq_id &
                                -- 5
                                spead_data_v3_packet.ant_info_header                & i_spead_config_list.substation_id & i_spead_config_list.subarray_id & i_spead_config_list.station_id & spead_data_v3_packet.ant_info_reserved &
                                -- 6
                                spead_data_v3_packet.sample_offset_header           & spead_data_v3_packet.payload_offset &
                                -- this has dropped two headers, pack 128 bits of zero on the end.
                                128D"0";

------------------------------------------------------------------------------                                
host_interface : entity spead_sps_lib.spead_sps_registers
    generic map (
        g_DEBUG_VEC_SIZE            => g_DEBUG_VEC_SIZE,
        g_DEBUG_ILA                 => g_DEBUG_ILA
    )
    port map ( 
        -- clock used for all data input and output from this module (300 MHz)
        i_clk                       => clk,
        i_rst                       => reset,

        -- Signals
        i_ipv4_total_bytes_to_calc  => ipv4_total_bytes_to_calc,
        i_udp_total_bytes           => udp_total_bytes,

        i_packet_calc               => packet_calc_init,
        o_packet_calc_done          => packet_calc_done,

        i_packet_sent               => data_packet_sent,

        -- return structures
        o_current_ipv4              => current_ipv4,
        o_current_ethernet          => current_ethernet,
        o_current_udp               => current_udp,

        o_enable_packetiser         => enable_packetiser,

        -- debug circuits
        i_spead_sm_fsm_debug        => spead_sm_fsm_debug,

        i_debug                     => i_debug,
        
        -- ARGs
        i_spead_lite_axi_mosi       => i_spead_lite_axi_mosi,
        o_spead_lite_axi_miso       => o_spead_lite_axi_miso,
        i_spead_full_axi_mosi       => i_spead_full_axi_mosi,
        o_spead_full_axi_miso       => o_spead_full_axi_miso

    );


------------------------------------------------------------------------------
-- ILA debug

generate_debug_ila : IF g_DEBUG_ILA GENERATE

    -- packetiser_debug : ila_0 PORT MAP (
    --     clk                     => clk,
    --     probe0(0)               => '0',
    --     probe0(64 downto 1)     => (others => '0'),
    --     probe0(72 downto 65)    => (others => '0'),
    --     probe0(104 downto 73)   => (others => '0'),
    --     probe0(108 downto 105)  => spead_sm_fsm_debug,
    --     probe0(116 downto 109)  => (others => '0'),
    --     probe0(148 downto 117)  => data_to_player(207 downto 176),
    --     probe0(180 downto 149)  => data_to_player(175 downto 144),
    --     probe0(181)             => data_to_player_wr,
    --     probe0(191 downto 182)  => (others => '0')
    --     );

END GENERATE;

end Behavioral;
