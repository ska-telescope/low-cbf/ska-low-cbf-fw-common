LIBRARY ieee, ethernet_lib;
USE ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use ethernet_lib.ethernet_pkg.ALL;

PACKAGE spead_sps_pkg IS


-- NO INIT, END packets, just Data.
CONSTANT c_spead_sps_v1_no_of_headers                   : integer := 8;
CONSTANT c_spead_sps_v3_no_of_headers                   : integer := 6;
CONSTANT c_spead_sps_common_field_byte_count            : integer := 6;
CONSTANT c_spead_sps_number_of_items_byte_count         : integer := 2;
CONSTANT c_spead_sps_data_header_byte_count             : integer := c_spead_sps_common_field_byte_count + c_spead_sps_number_of_items_byte_count + (c_spead_sps_v1_no_of_headers * 8);
CONSTANT c_spead_sps_v3_data_header_byte_count          : integer := c_spead_sps_common_field_byte_count + c_spead_sps_number_of_items_byte_count + (c_spead_sps_v3_no_of_headers * 8);

CONSTANT c_spead_sps_data_payload                       : integer := 8192;

-- sps spead data packets
-- v1 and v2
CONSTANT c_data_packet_total_header_bytes               : integer := c_packet_start_bytes                 + c_spead_sps_data_header_byte_count      + c_spead_sps_data_payload;
CONSTANT c_data_packet_total_ipv4_bytes                 : integer := c_ipv4_hdr_bytes + c_udp_hdr_bytes   + c_spead_sps_data_header_byte_count      + c_spead_sps_data_payload;
CONSTANT c_data_packet_total_udp_bytes                  : integer := c_udp_hdr_bytes                      + c_spead_sps_data_header_byte_count      + c_spead_sps_data_payload;

-- v3
CONSTANT c_data_packet_v3_total_header_bytes            : integer := c_packet_start_bytes                 + c_spead_sps_v3_data_header_byte_count   + c_spead_sps_data_payload;
CONSTANT c_data_packet_v3_total_ipv4_bytes              : integer := c_ipv4_hdr_bytes + c_udp_hdr_bytes   + c_spead_sps_v3_data_header_byte_count   + c_spead_sps_data_payload;
CONSTANT c_data_packet_v3_total_udp_bytes               : integer := c_udp_hdr_bytes                      + c_spead_sps_v3_data_header_byte_count   + c_spead_sps_data_payload;


TYPE spead_common_fields is RECORD
    spead_magic                 : std_logic_vector(7 downto 0);
    spead_version               : std_logic_vector(7 downto 0);
    spead_item_pointer_width    : std_logic_vector(7 downto 0);
    spead_heap_add_width        : std_logic_vector(7 downto 0);
    spead_reserved              : std_logic_vector(15 downto 0);
END RECORD;


TYPE spead_sps_v1_packet IS RECORD                                              -- Bytes
    -- common spead headers between packet types with static values
    spead_common                : spead_common_fields;                          -- 6
    -- common spead headers between packet types with runtime values
    -- number of items is sum of header fields.
    number_of_items             : std_logic_vector(15 downto 0);                -- 2
-- 1
    heap_count_header           : std_logic_vector(15 downto 0);                -- 2
    logical_channel_id          : std_logic_vector(15 downto 0);                -- 2
    packet_counter              : std_logic_vector(31 downto 0);                -- 4
-- 2
    pkt_length_header           : std_logic_vector(15 downto 0);                -- 2
    packet_payload_length       : std_logic_vector(47 downto 0);                -- 6
-- 3
    sync_time_header            : std_logic_vector(15 downto 0);                -- 2
    unix_epoch_time             : std_logic_vector(47 downto 0);                -- 6
-- 4
    timestamp_header            : std_logic_vector(15 downto 0);                -- 2
    timestamp                   : std_logic_vector(47 downto 0);                -- 6
-- 5
    center_freq_header          : std_logic_vector(15 downto 0);                -- 2
    frequency                   : std_logic_vector(47 downto 0);                -- 6
-- 6
    csp_chan_info_header        : std_logic_vector(15 downto 0);                -- 2
    csp_chan_info_reserved      : std_logic_vector(15 downto 0);                -- 2
    beam_id                     : std_logic_vector(15 downto 0);                -- 2
    frequency_id                : std_logic_vector(15 downto 0);                -- 2
-- 7
    csp_ant_info_header         : std_logic_vector(15 downto 0);                -- 2
    substation_id               : std_logic_vector(7 downto 0);                 -- 1
    subarray_id                 : std_logic_vector(7 downto 0);                 -- 1
    station_id                  : std_logic_vector(15 downto 0);                -- 2
    nof_contrib_antenna         : std_logic_vector(15 downto 0);                -- 2
-- 8
    sample_offset_header        : std_logic_vector(15 downto 0);                -- 2
    payload_offset              : std_logic_vector(47 downto 0);                -- 6
END RECORD;


TYPE spead_sps_v2_packet IS RECORD                                              -- Bytes
    -- common spead headers between packet types with static values
    spead_common                : spead_common_fields;                          -- 6
    -- common spead headers between packet types with runtime values
    -- number of items is sum of header fields.
    number_of_items             : std_logic_vector(15 downto 0);                -- 2
-- 1
    heap_count_header           : std_logic_vector(15 downto 0);                -- 2
    station_channel_id          : std_logic_vector(15 downto 0);                -- 2
    packet_counter              : std_logic_vector(31 downto 0);                -- 4
-- 2
    pkt_length_header           : std_logic_vector(15 downto 0);                -- 2
    packet_payload_length       : std_logic_vector(47 downto 0);                -- 6
-- 3
    sync_time_header            : std_logic_vector(15 downto 0);                -- 2
    sync_time_reserved          : std_logic_vector(15 downto 0);                -- 2
    unix_epoch_time             : std_logic_vector(31 downto 0);                -- 4
-- 4
    timestamp_header            : std_logic_vector(15 downto 0);                -- 2
    timestamp                   : std_logic_vector(47 downto 0);                -- 6
-- 5
    scan_id_header              : std_logic_vector(15 downto 0);                -- 2
    scan_id_reserved            : std_logic_vector(15 downto 0);                -- 2
    scan_id                     : std_logic_vector(31 downto 0);                -- 4
-- 6
    chan_info_header            : std_logic_vector(15 downto 0);                -- 2
    chan_info_reserved          : std_logic_vector(15 downto 0);                -- 2
    beam_id                     : std_logic_vector(15 downto 0);                -- 2
    frequency_id                : std_logic_vector(15 downto 0);                -- 2
-- 7
    ant_info_header             : std_logic_vector(15 downto 0);                -- 2
    substation_id               : std_logic_vector(7 downto 0);                 -- 1
    subarray_id                 : std_logic_vector(7 downto 0);                 -- 1
    station_id                  : std_logic_vector(15 downto 0);                -- 2
    ant_info_reserved           : std_logic_vector(15 downto 0);                -- 2
-- 8
    sample_offset_header        : std_logic_vector(15 downto 0);                -- 2
    payload_offset              : std_logic_vector(47 downto 0);                -- 6
END RECORD;

TYPE spead_sps_v3_packet IS RECORD                                              -- Bytes
    -- common spead headers between packet types with static values
    spead_common                : spead_common_fields;                          -- 6
    -- common spead headers between packet types with runtime values
    -- number of items is sum of header fields.
    number_of_items             : std_logic_vector(15 downto 0);                -- 2
-- 1
    heap_count_header           : std_logic_vector(15 downto 0);                -- 2
    packet_counter              : std_logic_vector(47 downto 0);                -- 6
-- 2
    pkt_length_header           : std_logic_vector(15 downto 0);                -- 2
    packet_payload_length       : std_logic_vector(47 downto 0);                -- 6
-- 3
    scan_id_header              : std_logic_vector(15 downto 0);                -- 2
    scan_id                     : std_logic_vector(47 downto 0);                -- 6
-- 4
    chan_info_header            : std_logic_vector(15 downto 0);                -- 2
    chan_info_reserved          : std_logic_vector(15 downto 0);                -- 2
    beam_id                     : std_logic_vector(15 downto 0);                -- 2
    frequency_id                : std_logic_vector(15 downto 0);                -- 2
-- 5
    ant_info_header             : std_logic_vector(15 downto 0);                -- 2
    substation_id               : std_logic_vector(7 downto 0);                 -- 1
    subarray_id                 : std_logic_vector(7 downto 0);                 -- 1
    station_id                  : std_logic_vector(15 downto 0);                -- 2
    ant_info_reserved           : std_logic_vector(15 downto 0);                -- 2
-- 6
    sample_offset_header        : std_logic_vector(15 downto 0);                -- 2
    payload_offset              : std_logic_vector(47 downto 0);                -- 6
END RECORD;

TYPE spead_config_items IS RECORD
    scan_id                         : std_logic_vector(47 downto 0);
    frequency_hz                    : std_logic_vector(47 downto 0);
    beam_id                         : std_logic_vector(15 downto 0);
    freq_id                         : std_logic_vector(15 downto 0);
    substation_id                   : std_logic_vector(7 downto 0);
    subarray_id                     : std_logic_vector(7 downto 0);
    station_id                      : std_logic_vector(15 downto 0);
    unix_epoch_time                 : std_logic_vector(47 downto 0);
    timestamp                       : std_logic_vector(47 downto 0);
    channel_id						: std_logic_vector(15 downto 0);
    packet_counter                  : std_logic_vector(47 downto 0);
    packet_payload_length           : std_logic_vector(47 downto 0);
    time_between_packets            : std_logic_vector(31 downto 0);
    version_select                  : std_logic_vector(1 downto 0);
END RECORD;

constant c_sps_heaader_items            : std_logic_vector(15 downto 0)     := x"0008";
--v1 headers
constant c_sps_heap_count_header        : std_logic_vector(15 downto 0)     := x"8001";     
constant c_sps_pkt_length_header        : std_logic_vector(15 downto 0)     := x"8004";
constant c_sps_sync_time_header         : std_logic_vector(15 downto 0)     := x"9027";
constant c_sps_timestamp_header         : std_logic_vector(15 downto 0)     := x"9600";
constant c_sps_center_freq_header       : std_logic_vector(15 downto 0)     := x"9011";     
constant c_sps_csp_chan_info_header     : std_logic_vector(15 downto 0)     := x"B000";
constant c_sps_csp_ant_info_header      : std_logic_vector(15 downto 0)     := x"B001";
constant c_sps_sample_offset_header     : std_logic_vector(15 downto 0)     := x"3300";
--v2 headers
constant c_sps_v2_timestamp_header      : std_logic_vector(15 downto 0)     := x"9600";     -- ICD has this set to 9800, TPM firmware did not update from 9600
constant c_sps_v2_scan_id_header        : std_logic_vector(15 downto 0)     := x"B010";
--v3 headers
constant c_sps_v3_heaader_items         : std_logic_vector(15 downto 0)     := x"0006";

constant default_spead_common_fields : spead_common_fields :=   (
                                                                spead_magic                 => x"53",
                                                                spead_version               => x"04",
                                                                spead_item_pointer_width    => x"02",
                                                                spead_heap_add_width        => x"06",
                                                                spead_reserved              => x"0000"
                                                                );

constant default_sps_spead_v1_packet : spead_sps_v1_packet :=   (
                                                            spead_common                => default_spead_common_fields,
                                                            number_of_items             => c_sps_heaader_items,
                                                        -- 1
                                                            heap_count_header           => c_sps_heap_count_header,
                                                            logical_channel_id          => x"0001",
                                                            packet_counter              => x"00000001",
                                                        -- 2
                                                            pkt_length_header           => c_sps_pkt_length_header,
                                                            packet_payload_length       => x"000000000001",
                                                        -- 3
                                                            sync_time_header            => c_sps_sync_time_header,
                                                            unix_epoch_time             => x"000000000001",
                                                        -- 4
                                                            timestamp_header            => c_sps_timestamp_header,
                                                            timestamp                   => x"000000000001",
                                                        -- 5
                                                            center_freq_header          => c_sps_center_freq_header,
                                                            frequency                   => x"000000000001",
                                                        -- 6
                                                            csp_chan_info_header        => c_sps_csp_chan_info_header,
                                                            csp_chan_info_reserved      => ( others => '0'),
                                                            beam_id                     => x"0001",
                                                            frequency_id                => x"0001",
                                                        -- 7
                                                            csp_ant_info_header         => c_sps_csp_ant_info_header,
                                                            substation_id               => x"01",
                                                            subarray_id                 => x"01",
                                                            station_id                  => x"0001",
                                                            nof_contrib_antenna         => x"0001",
                                                        -- 8
                                                            sample_offset_header        => c_sps_sample_offset_header,
                                                            payload_offset              => x"000000000000"
                                                            );

constant default_sps_spead_v2_packet : spead_sps_v2_packet := (
                                                            spead_common                => default_spead_common_fields,
                                                            number_of_items             => c_sps_heaader_items,
                                                        -- 1
                                                            heap_count_header           => c_sps_heap_count_header,
                                                            station_channel_id          => x"0001",
                                                            packet_counter              => x"00000001",
                                                        -- 2
                                                            pkt_length_header           => c_sps_pkt_length_header,
                                                            packet_payload_length       => x"000000000001",
                                                        -- 3
                                                            sync_time_header            => c_sps_sync_time_header,
                                                            sync_time_reserved          => x"0000",
                                                            unix_epoch_time             => x"00000001",
                                                        -- 4
                                                            timestamp_header            => c_sps_v2_timestamp_header,
                                                            timestamp                   => x"000000000001",
                                                        -- 5
                                                            scan_id_header              => c_sps_v2_scan_id_header,
                                                            scan_id_reserved            => x"0001",
                                                            scan_id                     => x"00000001",
                                                        -- 6
                                                            chan_info_header            => c_sps_csp_chan_info_header,
                                                            chan_info_reserved          => ( others => '0'),
                                                            beam_id                     => x"0001",
                                                            frequency_id                => x"0001",
                                                        -- 7
                                                            ant_info_header             => c_sps_csp_ant_info_header,
                                                            substation_id               => x"01",
                                                            subarray_id                 => x"01",
                                                            station_id                  => x"0001",
                                                            ant_info_reserved           => x"0000",
                                                        -- 8
                                                            sample_offset_header        => c_sps_sample_offset_header,
                                                            payload_offset              => x"000000000000"
                                                            );

constant default_sps_spead_v3_packet : spead_sps_v3_packet := (
                                                            spead_common                => default_spead_common_fields,
                                                            number_of_items             => c_sps_v3_heaader_items,
                                                        -- 1
                                                            heap_count_header           => c_sps_heap_count_header,
                                                            packet_counter              => x"000000000001",
                                                        -- 2
                                                            pkt_length_header           => c_sps_pkt_length_header,
                                                            packet_payload_length       => x"000000000001",
                                                        -- 3
                                                            scan_id_header              => c_sps_v2_scan_id_header,
                                                            scan_id                     => x"000000000001",
                                                        -- 4
                                                            chan_info_header            => c_sps_csp_chan_info_header,
                                                            chan_info_reserved          => x"0000",
                                                            beam_id                     => x"0001",
                                                            frequency_id                => x"0001",
                                                        -- 5
                                                            ant_info_header             => c_sps_csp_ant_info_header,
                                                            substation_id               => x"01",
                                                            subarray_id                 => x"01",
                                                            station_id                  => x"0001",
                                                            ant_info_reserved           => x"0000",
                                                        -- 6
                                                            sample_offset_header        => c_sps_sample_offset_header,
                                                            payload_offset              => x"000000000000"
                                                            );

------------------------------------------------------
-- SPEAD SPS MATCHING ARRAY for LFAA Decode.
    constant fieldsToMatch : natural := 30;
    
    type t_field_match is record
        wordCount  : std_logic_vector(0 downto 0); -- which 512-bit word the field should match to, either 0 or 1.
        byteOffset : natural;     -- where in the 512-bit word the relevant bits should sit
        bytes      : natural;     -- How many bits we are checking
        expected   : std_logic_vector(47 downto 0); -- Value we expect for a valid SPEAD packet
        check      : std_logic;   -- Whether we should check against the expected value or not
    end record;
    
    type t_field_values is array(0 to (fieldsToMatch-1)) of std_logic_vector(47 downto 0);
    type t_field_match_loc_array is array(0 to (fieldsToMatch-1)) of t_field_match;
    
    -- S_AXI 100G interface
    -- Total header is 114 bytes, so fits in the first two 512 bit words.
    -- Bit 31 of the SPEAD ID is set, so 0x0001 becomes 0x8001
    -- true for all except 0x3300
    constant c_sps_v1_fieldmatch_loc : t_field_match_loc_array := 
        ((wordCount => "0", byteOffset => 0,  bytes => 6, expected => x"000000000000", check => '0'),  -- 0. Destination MAC address, first 6 bytes of the frame.
         (wordCount => "0", byteOffset => 12, bytes => 2, expected => x"000000000800", check => '1'),  -- 1. Ethertype field at byte 12 should be 0x0800 for IPv4 packets
         (wordCount => "0", byteOffset => 14, bytes => 1, expected => x"000000000045", check => '1'),  -- 2. Version and header length fields of the IPv4 header, at byte 14. should be x45.
         (wordCount => "0", byteOffset => 16, bytes => 2, expected => x"000000002064", check => '1'),  -- 3. Total Length from the IPv4 header. Should be 20 (IPv4) + 8 (UDP) + 72 (SPEAD) + 8192 (Data) = 8292 = x2064
         (wordCount => "0", byteOffset => 23, bytes => 1, expected => x"000000000011", check => '1'),  -- 4. Protocol field from the IPv4 header, Should be 0x11 (UDP).
         (wordCount => "0", byteOffset => 36, bytes => 2, expected => x"000000000000", check => '0'),  -- 5. Destination UDP port - expected value to be configured via MACE
         (wordCount => "0", byteOffset => 38, bytes => 2, expected => x"000000002050", check => '1'),  -- 6. UDP length. Should be 8 (UDP) + 72 (SPEAD) + 8192 (Data) = x2050
         (wordCount => "0", byteOffset => 42, bytes => 2, expected => x"000000005304", check => '1'),  -- 7. First 2 bytes of the SPEAD header, should be 0x53 ("MAGIC"), 0x04 ("Version")
         (wordCount => "0", byteOffset => 44, bytes => 2, expected => x"000000000206", check => '1'),  -- 8. Bytes 3 and 4 of the SPEAD header, should be 0x02 ("ItemPointerWidth"), 0x06 ("HeapAddrWidht")
         (wordCount => "0", byteOffset => 48, bytes => 2, expected => x"000000000008", check => '1'),  -- 9. Bytes 7 and 8 of the SPEAD header, should be 0x00 and 0x08 ("Number of Items")
         (wordCount => "0", byteOffset => 50, bytes => 2, expected => x"000000008001", check => '1'),  -- 10. SPEAD ID 0x0001 = "heap_counter" field, should be 0x8001.
         (wordCount => "0", byteOffset => 52, bytes => 2, expected => x"000000000000", check => '0'),  -- 11. Logical Channel ID
         (wordCount => "0", byteOffset => 54, bytes => 4, expected => x"000000000000", check => '0'),  -- 12. Packet Counter 
         (wordCount => "0", byteOffset => 58, bytes => 2, expected => x"000000008004", check => '1'),  -- 13. SPEAD ID 0x0004 = "pkt_len" (data for this SPEAD ID is ignored)
         (wordCount => "1", byteOffset => 2,  bytes => 2, expected => x"000000009027", check => '1'),  -- 14. SPEAD ID 0x1027 = "sync_time"
         (wordCount => "1", byteOffset => 4,  bytes => 6, expected => x"000000000000", check => '0'),  -- 15. sync time in seconds from UNIX epoch
         (wordCount => "1", byteOffset => 10, bytes => 2, expected => x"000000009600", check => '1'),  -- 16. SPEAD ID 0x1600 = timestamp, time in nanoseconds after "sync_time"
         (wordCount => "1", byteOffset => 12, bytes => 4, expected => x"000000000000", check => '0'),  -- 17. first 4 bytes of timestamp
         (wordCount => "1", byteoffset => 16, bytes => 2, expected => x"000000000000", check => '0'),  -- 18. Last 2 bytes of the timestamp
         (wordCount => "1", byteoffset => 18, bytes => 2, expected => x"000000009011", check => '1'),  -- 19. SPEAD ID 0x1011 = center_freq
         (wordCount => "1", byteoffset => 20, bytes => 6, expected => x"000000000000", check => '0'),  -- 20. center_frequency in Hz
         (wordCount => "1", byteoffset => 26, bytes => 2, expected => x"00000000b000", check => '1'),  -- 21. SPEAD ID 0x3000 = csp_channel_info
         (wordCount => "1", byteoffset => 30, bytes => 2, expected => x"000000000000", check => '0'),  -- 22. beam_id
         (wordCount => "1", byteoffset => 32, bytes => 2, expected => x"000000000000", check => '0'),  -- 23. frequency_id
         (wordCount => "1", byteoffset => 34, bytes => 2, expected => x"00000000b001", check => '1'),  -- 24. SPEAD ID 0x3001 = csp_antenna_info
         (wordCount => "1", byteoffset => 36, bytes => 1, expected => x"000000000000", check => '0'),  -- 25. substation_id
         (wordCount => "1", byteoffset => 37, bytes => 1, expected => x"000000000000", check => '0'),  -- 26. subarray_id
         (wordCount => "1", byteoffset => 38, bytes => 2, expected => x"000000000000", check => '0'),  -- 27. station_id
         (wordCount => "1", byteoffset => 40, bytes => 2, expected => x"000000000000", check => '0'),  -- 28. nof_contributing_antennas
         (wordCount => "1", byteoffset => 42, bytes => 2, expected => x"000000003300", check => '0')   -- 29. SPEAD ID 0x3300 = sample_offset. top bit is not set since this is "indirect" even though it is a null pointer.
    );  

    -- V2 updates
    -- Bit 31 of the SPEAD ID is set, so 0x0001 becomes 0x8001
    -- true for all except 0x3300
    -- Changes to SPEAD IDs
    -- 1600 -> 1800, 1011 -> 3010
    constant c_sps_v2_fieldmatch_loc : t_field_match_loc_array := 
        ((wordCount => "0", byteOffset => 0,  bytes => 6, expected => x"000000000000", check => '0'),  -- 0. Destination MAC address, first 6 bytes of the frame.
         (wordCount => "0", byteOffset => 12, bytes => 2, expected => x"000000000800", check => '1'),  -- 1. Ethertype field at byte 12 should be 0x0800 for IPv4 packets
         (wordCount => "0", byteOffset => 14, bytes => 1, expected => x"000000000045", check => '1'),  -- 2. Version and header length fields of the IPv4 header, at byte 14. should be x45.
         (wordCount => "0", byteOffset => 16, bytes => 2, expected => x"000000002064", check => '1'),  -- 3. Total Length from the IPv4 header. Should be 20 (IPv4) + 8 (UDP) + 72 (SPEAD) + 8192 (Data) = 8292 = x2064
         (wordCount => "0", byteOffset => 23, bytes => 1, expected => x"000000000011", check => '1'),  -- 4. Protocol field from the IPv4 header, Should be 0x11 (UDP).
         (wordCount => "0", byteOffset => 36, bytes => 2, expected => x"000000000000", check => '0'),  -- 5. Destination UDP port - expected value to be configured via MACE
         (wordCount => "0", byteOffset => 38, bytes => 2, expected => x"000000002050", check => '1'),  -- 6. UDP length. Should be 8 (UDP) + 72 (SPEAD) + 8192 (Data) = x2050
         (wordCount => "0", byteOffset => 42, bytes => 2, expected => x"000000005304", check => '1'),  -- 7. First 2 bytes of the SPEAD header, should be 0x53 ("MAGIC"), 0x04 ("Version")
         (wordCount => "0", byteOffset => 44, bytes => 2, expected => x"000000000206", check => '1'),  -- 8. Bytes 3 and 4 of the SPEAD header, should be 0x02 ("ItemPointerWidth"), 0x06 ("HeapAddrWidht")
         (wordCount => "0", byteOffset => 48, bytes => 2, expected => x"000000000008", check => '1'),  -- 9. Bytes 7 and 8 of the SPEAD header, should be 0x00 and 0x08 ("Number of Items")
         (wordCount => "0", byteOffset => 50, bytes => 2, expected => x"000000008001", check => '1'),  -- 10. SPEAD ID 0x0001 = "heap_counter" field, should be 0x8001.
         (wordCount => "0", byteOffset => 52, bytes => 2, expected => x"000000000000", check => '0'),  -- 11. Logical Channel ID
         (wordCount => "0", byteOffset => 54, bytes => 4, expected => x"000000000000", check => '0'),  -- 12. Packet Counter 
         (wordCount => "0", byteOffset => 58, bytes => 2, expected => x"000000008004", check => '1'),  -- 13. SPEAD ID 0x0004 = "pkt_len" (data for this SPEAD ID is ignored)
         (wordCount => "1", byteOffset => 2,  bytes => 2, expected => x"000000009027", check => '1'),  -- 14. SPEAD ID 0x1027 = "sync_time"
         (wordCount => "1", byteOffset => 4,  bytes => 6, expected => x"000000000000", check => '0'),  -- 15. sync time in seconds from UNIX epoch
         (wordCount => "1", byteOffset => 10, bytes => 2, expected => x"000000009600", check => '1'),  -- 16. SPEAD ID 0x1800 = timestamp, ICD has this set to 9800, TPM firmware did not update from 9600
         (wordCount => "1", byteOffset => 12, bytes => 4, expected => x"000000000000", check => '0'),  -- 17. first 4 bytes of timestamp
         (wordCount => "1", byteoffset => 16, bytes => 2, expected => x"000000000000", check => '0'),  -- 18. Last 2 bytes of the timestamp
         (wordCount => "1", byteoffset => 18, bytes => 2, expected => x"00000000b010", check => '1'),  -- 19. SPEAD ID 0x3010 = scan ID
         (wordCount => "1", byteoffset => 20, bytes => 6, expected => x"000000000000", check => '0'),  -- 20. scan_id
         (wordCount => "1", byteoffset => 26, bytes => 2, expected => x"00000000b000", check => '1'),  -- 21. SPEAD ID 0x3000 = csp_channel_info
         (wordCount => "1", byteoffset => 30, bytes => 2, expected => x"000000000000", check => '0'),  -- 22. beam_id
         (wordCount => "1", byteoffset => 32, bytes => 2, expected => x"000000000000", check => '0'),  -- 23. frequency_id
         (wordCount => "1", byteoffset => 34, bytes => 2, expected => x"00000000b001", check => '1'),  -- 24. SPEAD ID 0x3001 = csp_antenna_info
         (wordCount => "1", byteoffset => 36, bytes => 1, expected => x"000000000000", check => '0'),  -- 25. substation_id
         (wordCount => "1", byteoffset => 37, bytes => 1, expected => x"000000000000", check => '0'),  -- 26. subarray_id
         (wordCount => "1", byteoffset => 38, bytes => 2, expected => x"000000000000", check => '0'),  -- 27. station_id
         (wordCount => "1", byteoffset => 40, bytes => 2, expected => x"000000000000", check => '0'),  -- 28. nof_contributing_antennas
         (wordCount => "1", byteoffset => 42, bytes => 2, expected => x"000000003300", check => '0')   -- 29. SPEAD ID 0x3300 = sample_offset. top bit is not set since this is "indirect" even though it is a null pointer.
    );

    -- V3 updates
    -- Bit 31 of the SPEAD ID is set, so 0x0001 becomes 0x8001
    -- true for all except 0x3300
    -- Changes to SPEAD IDs, delete two headers and data
    -- SCAN ID value changed?
    constant c_sps_v3_fieldmatch_loc : t_field_match_loc_array := 
        ((wordCount => "0", byteOffset => 0,  bytes => 6, expected => x"000000000000", check => '0'),  -- 0. Destination MAC address, first 6 bytes of the frame.
         (wordCount => "0", byteOffset => 12, bytes => 2, expected => x"000000000800", check => '1'),  -- 1. Ethertype field at byte 12 should be 0x0800 for IPv4 packets
         (wordCount => "0", byteOffset => 14, bytes => 1, expected => x"000000000045", check => '1'),  -- 2. Version and header length fields of the IPv4 header, at byte 14. should be x45.
         (wordCount => "0", byteOffset => 16, bytes => 2, expected => x"000000002054", check => '1'),  -- 3. Total Length from the IPv4 header. Should be 20 (IPv4) + 8 (UDP) + 56 (SPEAD) + 8192 (Data) = 8292 = x2054
         (wordCount => "0", byteOffset => 23, bytes => 1, expected => x"000000000011", check => '1'),  -- 4. Protocol field from the IPv4 header, Should be 0x11 (UDP).
         (wordCount => "0", byteOffset => 36, bytes => 2, expected => x"000000000000", check => '0'),  -- 5. Destination UDP port - expected value to be configured via MACE
         (wordCount => "0", byteOffset => 38, bytes => 2, expected => x"000000002040", check => '1'),  -- 6. UDP length. Should be 8 (UDP) + 56 (SPEAD) + 8192 (Data) = x2040
         (wordCount => "0", byteOffset => 42, bytes => 2, expected => x"000000005304", check => '1'),  -- 7. First 2 bytes of the SPEAD header, should be 0x53 ("MAGIC"), 0x04 ("Version")
         (wordCount => "0", byteOffset => 44, bytes => 2, expected => x"000000000206", check => '1'),  -- 8. Bytes 3 and 4 of the SPEAD header, should be 0x02 ("ItemPointerWidth"), 0x06 ("HeapAddrWidht")
         (wordCount => "0", byteOffset => 48, bytes => 2, expected => x"000000000006", check => '1'),  -- 9. Bytes 7 and 8 of the SPEAD header, should be 0x00 and 0x06 ("Number of Items") is 6 in v3.
         (wordCount => "0", byteOffset => 50, bytes => 2, expected => x"000000008001", check => '1'),  -- 10. SPEAD ID 0x0001 = "heap_counter" field, should be 0x8001.
         (wordCount => "0", byteOffset => 52, bytes => 1, expected => x"000000000000", check => '0'),  -- 11. Reserved
         (wordCount => "0", byteOffset => 53, bytes => 5, expected => x"000000000000", check => '0'),  -- 12. Packet Counter since TAI 2000
         (wordCount => "0", byteOffset => 58, bytes => 2, expected => x"000000008004", check => '1'),  -- 13. SPEAD ID 0x0004 = "pkt_len" (data for this SPEAD ID is ignored)
         (wordCount => "1", byteoffset => 2,  bytes => 2, expected => x"00000000b010", check => '1'),  -- 14. SPEAD ID 0x3010 = scan ID
         (wordCount => "1", byteoffset => 4,  bytes => 6, expected => x"000000000000", check => '0'),  -- 15. scan_id
         (wordCount => "1", byteoffset => 10, bytes => 2, expected => x"00000000b000", check => '1'),  -- 16. SPEAD ID 0x3000 = csp_channel_info
         (wordCount => "1", byteoffset => 14, bytes => 2, expected => x"000000000000", check => '0'),  -- 17. beam_id
         (wordCount => "1", byteoffset => 16, bytes => 2, expected => x"000000000000", check => '0'),  -- 18. frequency_id
         (wordCount => "1", byteoffset => 18, bytes => 2, expected => x"00000000b001", check => '1'),  -- 19. SPEAD ID 0x3001 = csp_antenna_info
         (wordCount => "1", byteoffset => 20, bytes => 1, expected => x"000000000000", check => '0'),  -- 20. substation_id
         (wordCount => "1", byteoffset => 21, bytes => 1, expected => x"000000000000", check => '0'),  -- 21. subarray_id
         (wordCount => "1", byteoffset => 22, bytes => 2, expected => x"000000000000", check => '0'),  -- 22. station_id
         (wordCount => "1", byteoffset => 24, bytes => 2, expected => x"000000000000", check => '0'),  -- 23. nof_contributing_antennas
         (wordCount => "1", byteoffset => 26, bytes => 2, expected => x"000000003300", check => '0'),  -- 24. SPEAD ID 0x3300 = sample_offset. top bit is not set since this is "indirect" even though it is a null pointer.
         (wordCount => "1", byteoffset => 27, bytes => 1, expected => x"000000000000", check => '0'),  -- (25) !! Remaining entries are unused    !!  
         (wordCount => "1", byteoffset => 28, bytes => 1, expected => x"000000000000", check => '0'),  -- (26) !! since the SPEAD v3 header is    !!
         (wordCount => "1", byteoffset => 29, bytes => 1, expected => x"000000000000", check => '0'),  -- (27) !! 16 bytes shorter than V1 and V2 !!
         (wordCount => "1", byteoffset => 30, bytes => 1, expected => x"000000000000", check => '0'),  -- (28)
         (wordCount => "1", byteoffset => 31, bytes => 1, expected => x"000000000000", check => '0')   -- (29)
    );  
end spead_sps_pkg;