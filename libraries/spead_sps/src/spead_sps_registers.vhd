----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 20/02/2023 04:22:23 PM
-- Design Name: 
-- Module Name: spead_sps_registers
--  
-- Description: 
--      Map ARGs interface to signals and records.
--      Initial design intent for 48 subarrays.
--
-- IPv4 checksum circuit with 4 cycle turn around to accomodate the dynamic nature of SPEAD.
----------------------------------------------------------------------------------

library IEEE, correlator_lib, common_lib, signal_processing_common, spead_sps_lib, ethernet_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;
USE common_lib.common_pkg.ALL;
use ethernet_lib.ethernet_pkg.ALL;
use spead_sps_lib.spead_sps_reg_pkg.ALL;
use spead_sps_lib.spead_sps_pkg.ALL;


entity spead_sps_registers is
    Generic ( 
        g_DEBUG_VEC_SIZE                : INTEGER := 1;
        g_DEBUG_ILA                     : BOOLEAN := FALSE
    );
    Port ( 
        -- clock used for all data input and output from this module (300 MHz)
        i_clk                           : in std_logic;
        i_rst                           : in std_logic;

        -- Signals
        i_ipv4_total_bytes_to_calc      : in std_logic_vector(15 downto 0);
        i_udp_total_bytes               : in std_logic_vector(15 downto 0);

        i_packet_calc                   : in std_logic;
        o_packet_calc_done              : out std_logic;

        i_packet_sent                   : in std_logic;

        -- return packet structure
        o_current_ipv4                  : out IPv4_header;
        o_current_ethernet              : out ethernet_frame;
        o_current_udp                   : out UDP_header;    

        o_enable_packetiser             : out std_logic;
        
        -- debug circuits
        i_spead_sm_fsm_debug            : in std_logic_vector(3 downto 0);

        -- debug vector
        i_debug                         : in std_logic_vector((g_DEBUG_VEC_SIZE-1) downto 0);

        -- ARGs
        -- Interface for correlator instance #1
        i_spead_lite_axi_mosi           : in t_axi4_lite_mosi; 
        o_spead_lite_axi_miso           : out t_axi4_lite_miso;
        i_spead_full_axi_mosi           : in  t_axi4_full_mosi;
        o_spead_full_axi_miso           : out t_axi4_full_miso

    );
end spead_sps_registers;

architecture Behavioral of spead_sps_registers is

COMPONENT ila_0
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
END COMPONENT;

signal clk                      : std_logic;
signal reset                    : std_logic;
signal reset_n                  : std_logic;

signal bytes_to_transmit        : STD_LOGIC_VECTOR(13 downto 0);     
signal data_to_player           : STD_LOGIC_VECTOR(511 downto 0);
signal data_to_player_wr        : STD_LOGIC;
signal data_to_player_rdy       : STD_LOGIC;


signal spead_ctrl_reg           : t_spead_ctrl_rw;
signal spead_ctrl_status        : t_spead_ctrl_ro;

-----------------------------------
signal enable_packetiser        : std_logic     := '1';


signal current_ethernet         : ethernet_frame            := null_ethernet_frame;
signal current_ipv4             : IPv4_header               := null_ipv4_header;
signal current_udp              : UDP_header                := null_UDP_header;

signal ipv4_chk_sum_calc        : std_logic_vector(31 downto 0);
signal ipv4_chk_sum_base        : std_logic_vector(31 downto 0);

signal ipv4_dst_addr_chk        : unsigned(16 downto 0)     := (others => '0');
signal ipv4_src_addr_chk        : unsigned(16 downto 0)     := (others => '0');

type ipv4_chksum_sm_fsm is      (
                                IDLE, 
                                CALC_1, 
                                WAIT_PACKET, 
                                CALC_2,
                                CALC_3,
                                READY
                                );
signal ipv4_chksum_sm : ipv4_chksum_sm_fsm;

signal ipv4_chksum_sm_fsm_debug : std_logic_vector(3 downto 0)  := x"0";

signal reset_packetiser_value   : std_logic;

signal packet_calc_done         : std_logic;

----------------------------------------------------------------------
-- stats
signal reset_stats              : std_logic;
signal no_of_packets_sent       : unsigned(31 downto 0)         := (others => '0');

----------------------------------------------------------------------
-- ARGs mappings.
signal bram_rst                 : STD_LOGIC;
signal bram_clk                 : STD_LOGIC;
signal bram_en                  : STD_LOGIC;
signal bram_we                  : STD_LOGIC_VECTOR(3 DOWNTO 0);
signal bram_addr                : STD_LOGIC_VECTOR(14 DOWNTO 0);
signal bram_wrdata              : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal bram_rddata              : STD_LOGIC_VECTOR(31 DOWNTO 0);

signal spead_trigger            : std_logic_vector(7 downto 0);

begin

------------------------------------------------------------------------------
-- PORT MAPPINGS
clk                         <= i_clk;
reset                       <= i_rst;
reset_n                     <= NOT i_rst;

o_current_ipv4          <= current_ipv4;
o_current_ethernet      <= current_ethernet;
o_current_udp           <= current_udp;

o_enable_packetiser     <= enable_packetiser;
o_packet_calc_done      <= packet_calc_done;


------------------------------------------------------------------------------
-- Register mappings.
reset_stats                                     <= spead_ctrl_reg.reset_packetiser_stats OR i_debug(0);
reset_packetiser_value                          <= spead_ctrl_reg.reset_packetiser_value OR i_debug(1);

enable_reg_proc : process(clk)
begin
    if rising_edge(clk) then
        enable_packetiser                       <= spead_ctrl_reg.enable_packetiser      OR i_debug(2);
    end if;
end process;

spead_ctrl_status.no_of_packets_sent            <= std_logic_vector(no_of_packets_sent);

spead_ctrl_status.debug_ipv4_chksum_sm_fsm_debug    <= ipv4_chksum_sm_fsm_debug;
spead_ctrl_status.debug_spead_sm_fsm                <= i_spead_sm_fsm_debug;

------------------------------------------------------------------------------
-- IPv4 Checksum
-- precalculate the static fields and be ready for dynamic updates to
-- total_length and dst_addr.

-- 0x4500   = base_ipv4_header.version & base_ipv4_header.header_length & base_ipv4_header.type_of_service;
-- 0xACDC   = base_ipv4_header.id;
-- 0x4000   = base_ipv4_header.ip_flags & base_ipv4_header.fragment_off;
-- 0x4011   = base_ipv4_header.TTL & base_ipv4_header.protocol;
-- summed   = 0x0001_71ED


ipv4_checksum_proc : process(clk)
begin
    if rising_edge(clk) then
        current_ipv4.dst_addr       <= spead_ctrl_reg.ipv4_dst_addr;
        current_ipv4.src_addr       <= spead_ctrl_reg.ipv4_src_addr;

        current_ethernet.dst_mac    <= spead_ctrl_reg.ethernet_dst_mac_u & spead_ctrl_reg.ethernet_dst_mac_l;
        current_ethernet.src_mac    <= spead_ctrl_reg.ethernet_src_mac_u & spead_ctrl_reg.ethernet_src_mac_l;
        current_udp.src_port        <= spead_ctrl_reg.udp_src_port;
        current_udp.dst_port        <= spead_ctrl_reg.udp_dst_port;
        current_udp.length          <= i_udp_total_bytes;

        ipv4_src_addr_chk           <= ((unsigned('0' & current_ipv4.src_addr(31 downto 16))) + (unsigned('0' & current_ipv4.src_addr(15 downto 0))));
        ipv4_dst_addr_chk           <= ((unsigned('0' & current_ipv4.dst_addr(31 downto 16))) + (unsigned('0' & current_ipv4.dst_addr(15 downto 0))));

        if reset = '1' or enable_packetiser = '0' then
            current_udp             <= base_UDP_header;
            current_ethernet        <= base_ethernet_frame;
            current_ipv4            <= base_ipv4_header;
            ipv4_chk_sum_calc       <= (others => '0');
            ipv4_chk_sum_base       <= x"000171ED";
            ipv4_chksum_sm          <= IDLE;
            ipv4_chksum_sm_fsm_debug <= x"F";
            packet_calc_done        <= '0';
        else

            case ipv4_chksum_sm is
                when IDLE =>
                    ipv4_chksum_sm_fsm_debug    <= x"0";
                    if enable_packetiser = '1' then
                        ipv4_chksum_sm          <= CALC_1;
                    end if;

                when CALC_1 => 
                    ipv4_chksum_sm_fsm_debug    <= x"1";
                    ipv4_chk_sum_base           <= std_logic_vector(unsigned(ipv4_chk_sum_base) + (ipv4_src_addr_chk));
                    ipv4_chksum_sm              <= WAIT_PACKET;

                when WAIT_PACKET =>
                    ipv4_chksum_sm_fsm_debug    <= x"2";
                    packet_calc_done            <= '0';
                    --  Wait for packet parameters to settle then generate checksum.
                    current_ipv4.total_length   <= i_ipv4_total_bytes_to_calc;
                    if i_packet_calc = '1' then
                        ipv4_chksum_sm          <= CALC_2;
                        ipv4_chk_sum_calc       <= std_logic_vector(unsigned(ipv4_chk_sum_base) + (ipv4_dst_addr_chk));
                    end if;

                when CALC_2 => 
                    ipv4_chksum_sm_fsm_debug    <= x"3";
                    ipv4_chk_sum_calc           <= std_logic_vector(unsigned(ipv4_chk_sum_calc) + unsigned(current_ipv4.total_length));
                    ipv4_chksum_sm              <= CALC_3;

                when CALC_3 => 
                    ipv4_chksum_sm_fsm_debug    <= x"4";
                    ipv4_chk_sum_calc           <= NOT (zero_word & std_logic_vector(unsigned(ipv4_chk_sum_calc(15 downto 0)) + unsigned(ipv4_chk_sum_calc(31 downto 16))));
                    ipv4_chksum_sm              <= READY;                    

                when READY => 
                    ipv4_chksum_sm_fsm_debug    <= x"5";
                    packet_calc_done            <= '1';
                    current_ipv4.header_chk_sum <= ipv4_chk_sum_calc(15 downto 0);
                    ipv4_chksum_sm              <= WAIT_PACKET;

                when others =>
                    ipv4_chksum_sm      <= IDLE;
            end case;
        end if;
    end if;
end process;

------------------------------------------------------------------------------
-- Stats
stats_proc : process(clk)
begin
    if rising_edge(clk) then
        if reset = '1' or reset_stats = '1' then
            no_of_packets_sent          <= (others => '0');
        else
            if (i_packet_sent = '1') then
                no_of_packets_sent          <= no_of_packets_sent + 1;
            end if;
        end if;
    end if;
end process;


------------------------------------------------------------------------------
-- ARGs
ARGS_register_Packetiser : entity spead_sps_lib.spead_sps_reg 
    PORT MAP (
        -- AXI Lite signals, 300 MHz Clock domain
        MM_CLK                  => i_clk,
        MM_RST                  => i_rst,
        
        SLA_IN                  => i_spead_lite_axi_mosi,
        SLA_OUT                 => o_spead_lite_axi_miso,

        SPEAD_CTRL_FIELDS_RW    => spead_ctrl_reg,
        SPEAD_CTRL_FIELDS_RO    => spead_ctrl_status
        
        );

------------------------------------------------------------------------------

generate_debug_ila : IF g_DEBUG_ILA GENERATE
    -- memspace_debug : ila_0 PORT MAP (
    --         clk                     => clk,
    --         probe0(3 downto 0)      => bram_we,
    --         probe0(18 downto 4)     => bram_addr,
    --         probe0(19)              => bram_en,
    --         probe0(51 downto 20)    => bram_wrdata,
    --         probe0(83 downto 52)    => bram_rddata,
    --         probe0(191 downto 84)   => (others => '0')
    --         );
end generate;

spead_sdp_memspace : entity spead_sps_lib.spead_sps_axi_bram_wrapper 
    PORT MAP (
        i_clk                   => i_clk,
        i_rst                   => i_rst,
        
        i_spead_full_axi_mosi   => i_spead_full_axi_mosi,
        o_spead_full_axi_miso   => o_spead_full_axi_miso,
    
        bram_rst                => bram_rst,
        bram_clk                => bram_clk,
        bram_en                 => bram_en,     --: OUT STD_LOGIC;
        bram_we_byte            => bram_we,     --: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        bram_addr               => bram_addr,   --: OUT STD_LOGIC_VECTOR(14 DOWNTO 0);
        bram_wrdata             => bram_wrdata, --: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        bram_rddata             => bram_rddata  --: IN STD_LOGIC_VECTOR(31 DOWNTO 0)
    );        


end Behavioral;
