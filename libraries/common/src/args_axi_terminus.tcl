# AXI BRAM control for axi_terminus
create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_1k
set_property -dict [list CONFIG.SUPPORTS_NARROW_BURST {0} CONFIG.SINGLE_PORT_BRAM {1} CONFIG.Component_Name {axi_bram_ctrl_1k} CONFIG.MEM_DEPTH {1024}] [get_ips axi_bram_ctrl_1k]
create_ip_run [get_ips axi_bram_ctrl_1k]
