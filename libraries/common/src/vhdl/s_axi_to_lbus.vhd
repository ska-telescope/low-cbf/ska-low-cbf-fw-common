----------------------------------------------------------------------------------
-- Company:     CSIRO
-- Create Date: Oct 2022
-- Engineer:    Giles Babich
--
-- Simple S_AXI to LBUS remapper to glue PTP to existing design work.
-- Needs to be simmed!!!
----------------------------------------------------------------------------------


library IEEE, XPM, technology_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use xpm.vcomponents.all;
use technology_lib.tech_mac_100g_pkg.all;

library UNISIM;
use UNISIM.VComponents.all;


entity s_axi_to_lbus is
    Port ( 
        i_cmac_clk              : in std_logic;
        i_cmac_rst              : in std_logic;
        
        -- S_AXI
        i_rx_axis_tdata         : in STD_LOGIC_VECTOR ( 511 downto 0 );
        i_rx_axis_tkeep         : in STD_LOGIC_VECTOR ( 63 downto 0 );
        i_rx_axis_tlast         : in STD_LOGIC;
        o_rx_axis_tready        : out STD_LOGIC;
        i_rx_axis_tuser         : in STD_LOGIC_VECTOR ( 79 downto 0 );
        i_rx_axis_tvalid        : in STD_LOGIC;
        
        -- PTP timestamp, aligned to first 64 bytes and only available then, otherwise 0.
        o_ptp_time              : out STD_LOGIC_VECTOR ( 79 downto 0 );

        -- LBUS
        o_eth100_rx_sosi        : out t_lbus_sosi      
        
    );
end s_axi_to_lbus;

architecture rtl of s_axi_to_lbus is

--TYPE t_lbus_sosi IS RECORD  -- Source Out and Sink In
--    data       : STD_LOGIC_VECTOR(511 DOWNTO 0);                -- Data bus
--    valid      : STD_LOGIC_VECTOR(3 DOWNTO 0);    -- Data segment enable
--    eop        : STD_LOGIC_VECTOR(3 DOWNTO 0);    -- End of packet
--    sop        : STD_LOGIC_VECTOR(3 DOWNTO 0);    -- Start of packet
--    error      : STD_LOGIC_VECTOR(3 DOWNTO 0);    -- Error flag, indicates data has an error
--    empty      : t_empty_arr(3 DOWNTO 0);         -- Array of Array 3 -> 0
--END RECORD;

signal next_wr_is_sop   : std_logic := '1';

begin

-- always ready
o_rx_axis_tready    <= '1';    

remap_proc : process(i_cmac_clk)
begin
    if rising_edge(i_cmac_clk) then
        if (i_cmac_rst = '1') OR (i_rx_axis_tlast = '1') then
            next_wr_is_sop <= '1';
        elsif i_rx_axis_tvalid = '1' then
            next_wr_is_sop <= '0';
        end if;            
        o_ptp_time              <= i_rx_axis_tuser;

        -- always starting at lower quad.
        o_eth100_rx_sosi.sop(0) <= i_rx_axis_tvalid AND next_wr_is_sop;
        o_eth100_rx_sosi.sop(1) <= '0';
        o_eth100_rx_sosi.sop(2) <= '0';
        o_eth100_rx_sosi.sop(3) <= '0';

    end if;
end process;


remap_byte_enables : process(i_cmac_clk)
begin
    if rising_edge(i_cmac_clk) then
        o_eth100_rx_sosi.valid(0)   <= i_rx_axis_tvalid AND (OR i_rx_axis_tkeep(15 downto 0));
        o_eth100_rx_sosi.valid(1)   <= i_rx_axis_tvalid AND (OR i_rx_axis_tkeep(31 downto 16));
        o_eth100_rx_sosi.valid(2)   <= i_rx_axis_tvalid AND (OR i_rx_axis_tkeep(47 downto 32));
        o_eth100_rx_sosi.valid(3)   <= i_rx_axis_tvalid AND (OR i_rx_axis_tkeep(63 downto 48));

        -- empty indicates how many bytes are invalid in lbus
        -- s_axi indicates how many valid bytes.
        -- thus all valid bytes on s_axi = 0 on LBUS.
        if (i_rx_axis_tlast) then
            if (OR i_rx_axis_tkeep(63 downto 48)) then
                o_eth100_rx_sosi.eop(3)     <= i_rx_axis_tlast;
                if      i_rx_axis_tkeep(63 downto 48) = x"FFFF" then
                    o_eth100_rx_sosi.empty(3)   <= x"0";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"7FFF" then
                    o_eth100_rx_sosi.empty(3)   <= x"1";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"3FFF" then
                    o_eth100_rx_sosi.empty(3)   <= x"2";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"1FFF" then
                    o_eth100_rx_sosi.empty(3)   <= x"3";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"0FFF" then
                    o_eth100_rx_sosi.empty(3)   <= x"4";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"07FF" then
                    o_eth100_rx_sosi.empty(3)   <= x"5";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"03FF" then
                    o_eth100_rx_sosi.empty(3)   <= x"6";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"01FF" then
                    o_eth100_rx_sosi.empty(3)   <= x"7";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"00FF" then
                    o_eth100_rx_sosi.empty(3)   <= x"8";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"007F" then
                    o_eth100_rx_sosi.empty(3)   <= x"9";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"003F" then
                    o_eth100_rx_sosi.empty(3)   <= x"A";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"001F" then
                    o_eth100_rx_sosi.empty(3)   <= x"B";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"000F" then
                    o_eth100_rx_sosi.empty(3)   <= x"C";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"0007" then
                    o_eth100_rx_sosi.empty(3)   <= x"D";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"0003" then
                    o_eth100_rx_sosi.empty(3)   <= x"E";
                elsif   i_rx_axis_tkeep(63 downto 48) = x"0001" then
                    o_eth100_rx_sosi.empty(3)   <= x"F";
                end if;
            elsif (OR i_rx_axis_tkeep(47 downto 32)) then
                o_eth100_rx_sosi.eop(2) <= i_rx_axis_tlast;
                if      i_rx_axis_tkeep(47 downto 32) = x"FFFF" then
                    o_eth100_rx_sosi.empty(2)   <= x"0";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"7FFF" then
                    o_eth100_rx_sosi.empty(2)   <= x"1";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"3FFF" then
                    o_eth100_rx_sosi.empty(2)   <= x"2";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"1FFF" then
                    o_eth100_rx_sosi.empty(2)   <= x"3";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"0FFF" then
                    o_eth100_rx_sosi.empty(2)   <= x"4";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"07FF" then
                    o_eth100_rx_sosi.empty(2)   <= x"5";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"03FF" then
                    o_eth100_rx_sosi.empty(2)   <= x"6";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"01FF" then
                    o_eth100_rx_sosi.empty(2)   <= x"7";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"00FF" then
                    o_eth100_rx_sosi.empty(2)   <= x"8";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"007F" then
                    o_eth100_rx_sosi.empty(2)   <= x"9";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"003F" then
                    o_eth100_rx_sosi.empty(2)   <= x"A";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"001F" then
                    o_eth100_rx_sosi.empty(2)   <= x"B";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"000F" then
                    o_eth100_rx_sosi.empty(2)   <= x"C";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"0007" then
                    o_eth100_rx_sosi.empty(2)   <= x"D";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"0003" then
                    o_eth100_rx_sosi.empty(2)   <= x"E";
                elsif   i_rx_axis_tkeep(47 downto 32) = x"0001" then
                    o_eth100_rx_sosi.empty(2)   <= x"F";
                end if;
            elsif (OR i_rx_axis_tkeep(31 downto 16)) then
                o_eth100_rx_sosi.eop(1) <= i_rx_axis_tlast;
                if      i_rx_axis_tkeep(31 downto 16) = x"FFFF" then
                    o_eth100_rx_sosi.empty(1)   <= x"0";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"7FFF" then
                    o_eth100_rx_sosi.empty(1)   <= x"1";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"3FFF" then
                    o_eth100_rx_sosi.empty(1)   <= x"2";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"1FFF" then
                    o_eth100_rx_sosi.empty(1)   <= x"3";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"0FFF" then
                    o_eth100_rx_sosi.empty(1)   <= x"4";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"07FF" then
                    o_eth100_rx_sosi.empty(1)   <= x"5";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"03FF" then
                    o_eth100_rx_sosi.empty(1)   <= x"6";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"01FF" then
                    o_eth100_rx_sosi.empty(1)   <= x"7";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"00FF" then
                    o_eth100_rx_sosi.empty(1)   <= x"8";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"007F" then
                    o_eth100_rx_sosi.empty(1)   <= x"9";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"003F" then
                    o_eth100_rx_sosi.empty(1)   <= x"A";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"001F" then
                    o_eth100_rx_sosi.empty(1)   <= x"B";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"000F" then
                    o_eth100_rx_sosi.empty(1)   <= x"C";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"0007" then
                    o_eth100_rx_sosi.empty(1)   <= x"D";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"0003" then
                    o_eth100_rx_sosi.empty(1)   <= x"E";
                elsif   i_rx_axis_tkeep(31 downto 16) = x"0001" then
                    o_eth100_rx_sosi.empty(1)   <= x"F";
                end if;
            elsif (OR i_rx_axis_tkeep(15 downto 0)) then
                o_eth100_rx_sosi.eop(0) <= i_rx_axis_tlast;
                if      i_rx_axis_tkeep(15 downto 0) = x"FFFF" then
                    o_eth100_rx_sosi.empty(0)   <= x"0";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"7FFF" then
                    o_eth100_rx_sosi.empty(0)   <= x"1";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"3FFF" then
                    o_eth100_rx_sosi.empty(0)   <= x"2";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"1FFF" then
                    o_eth100_rx_sosi.empty(0)   <= x"3";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"0FFF" then
                    o_eth100_rx_sosi.empty(0)   <= x"4";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"07FF" then
                    o_eth100_rx_sosi.empty(0)   <= x"5";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"03FF" then
                    o_eth100_rx_sosi.empty(0)   <= x"6";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"01FF" then
                    o_eth100_rx_sosi.empty(0)   <= x"7";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"00FF" then
                    o_eth100_rx_sosi.empty(0)   <= x"8";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"007F" then
                    o_eth100_rx_sosi.empty(0)   <= x"9";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"003F" then
                    o_eth100_rx_sosi.empty(0)   <= x"A";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"001F" then
                    o_eth100_rx_sosi.empty(0)   <= x"B";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"000F" then
                    o_eth100_rx_sosi.empty(0)   <= x"C";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"0007" then
                    o_eth100_rx_sosi.empty(0)   <= x"D";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"0003" then
                    o_eth100_rx_sosi.empty(0)   <= x"E";
                elsif   i_rx_axis_tkeep(15 downto 0) = x"0001" then
                    o_eth100_rx_sosi.empty(0)   <= x"F";
                end if;
            else
                o_eth100_rx_sosi.eop(0)     <= '0';
                o_eth100_rx_sosi.eop(1)     <= '0';
                o_eth100_rx_sosi.eop(2)     <= '0';
                o_eth100_rx_sosi.eop(3)     <= '0';

                o_eth100_rx_sosi.empty(0)   <= x"0";
                o_eth100_rx_sosi.empty(1)   <= x"0";
                o_eth100_rx_sosi.empty(2)   <= x"0";
                o_eth100_rx_sosi.empty(3)   <= x"0";
            end if;
        end if;

    end if;

end process;


remap_data_proc : for n in 0 to 3 generate
begin
    quad: for i in 0 to 15 generate

        s_to_l_proc : process(i_cmac_clk)
        begin
            if rising_edge(i_cmac_clk) then
                                    -- n modifier + i modifier...etc
                o_eth100_rx_sosi.data(((128*n) + (127 - i*8)) downto ((128*n) + (120 - (i*8)))) <= i_rx_axis_tdata(((128*n) + ((i*8)+7)) downto ((128*n)+(i*8)));
            end if;
        end process;

    end generate quad;
end generate remap_data_proc;
    


end rtl;
