----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 07/14/2022 
-- Design Name: 
-- Module Name: memory_dp_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE, common_lib, xpm;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use common_lib.common_pkg.ALL;
use xpm.vcomponents.all;
library UNISIM;
use UNISIM.VComponents.all;


entity memory_tdp_wrapper is
    GENERIC (
        MEMORY_INIT_FILE    : STRING := "none";
        MEMORY_PRIMITIVE    : STRING := "block"; -- "auto", "distributed", "block" or "ultra" ;
        CLOCKING_MODE       : STRING := "independent_clock"; -- "common_clock", "independent_clock" 
        g_NO_OF_ADDR_BITS   : INTEGER := 9;
        g_D_Q_WIDTH         : INTEGER := 512;
        g_READ_LATENCY_B    : INTEGER := 3

    );
    Port ( 
        clk_a           : in std_logic;
        clk_b           : in std_logic;
    
        data_a          : in std_logic_vector((g_D_Q_WIDTH-1) downto 0);
        addr_a          : in std_logic_vector((g_NO_OF_ADDR_BITS-1) downto 0);
        data_a_wr       : in std_logic; 
        data_a_q        : out std_logic_vector((g_D_Q_WIDTH-1) downto 0);

        data_b          : in std_logic_vector((g_D_Q_WIDTH-1) downto 0);
        addr_b          : in std_logic_vector((g_NO_OF_ADDR_BITS-1) downto 0);
        data_b_wr       : in std_logic; 
        data_b_q        : out std_logic_vector((g_D_Q_WIDTH-1) downto 0)
    
    );
end memory_tdp_wrapper;

architecture Behavioral of memory_tdp_wrapper is


CONSTANT ADDR_SPACE             : INTEGER := pow2(g_NO_OF_ADDR_BITS);
CONSTANT MEMORY_SIZE_GENERIC    : INTEGER := ADDR_SPACE * g_D_Q_WIDTH;

signal data_a_wr_int            : std_logic_vector(0 downto 0);
signal data_b_wr_int            : std_logic_vector(0 downto 0);

begin

data_a_wr_int(0)    <= data_a_wr;
data_b_wr_int(0)    <= data_b_wr;


xpm_memory_tdpram_inst : xpm_memory_tdpram
    generic map (    
        -- Common module generics
        AUTO_SLEEP_TIME         => 0,              --Do not Change
        CASCADE_HEIGHT          => 0,
        CLOCKING_MODE           => CLOCKING_MODE, --"independent_clock", --string; "common_clock", "independent_clock" 
        ECC_MODE                => "no_ecc",       --string; "no_ecc", "encode_only", "decode_only" or "both_encode_and_decode" 

        MEMORY_INIT_FILE        => MEMORY_INIT_FILE,         --string; "none" or "<filename>.mem" 
        MEMORY_INIT_PARAM       => "",             --string;
        MEMORY_OPTIMIZATION     => "true",                  --string; "true", "false" 
        MEMORY_PRIMITIVE        => MEMORY_PRIMITIVE,        --string; "auto", "distributed", "block" or "ultra" ;
        MEMORY_SIZE             => MEMORY_SIZE_GENERIC,     --262144,          -- Total memory size in bits; 512 x 512 = 262144
        MESSAGE_CONTROL         => 0,              --integer; 0,1

        USE_MEM_INIT            => 0,              --integer; 0,1
        WAKEUP_TIME             => "disable_sleep",--string; "disable_sleep" or "use_sleep_pin" 
        USE_EMBEDDED_CONSTRAINT => 0,              --integer: 0,1
       
    
        RST_MODE_A              => "SYNC",   
        RST_MODE_B              => "SYNC", 
        WRITE_MODE_A            => "no_change",    --string; "write_first", "read_first", "no_change" 
        WRITE_MODE_B            => "no_change",    --string; "write_first", "read_first", "no_change" 

        -- Port A module generics
        READ_DATA_WIDTH_A       => g_D_Q_WIDTH,    
        READ_LATENCY_A          => 3,              
        READ_RESET_VALUE_A      => "0",            

        WRITE_DATA_WIDTH_A      => g_D_Q_WIDTH,
        BYTE_WRITE_WIDTH_A      => g_D_Q_WIDTH,
        ADDR_WIDTH_A            => g_NO_OF_ADDR_BITS,
    
        -- Port B module generics
        READ_DATA_WIDTH_B       => g_D_Q_WIDTH,
        READ_LATENCY_B          => g_READ_LATENCY_B,           
        READ_RESET_VALUE_B      => "0",         

        WRITE_DATA_WIDTH_B      => g_D_Q_WIDTH,
        BYTE_WRITE_WIDTH_B      => g_D_Q_WIDTH,             
        ADDR_WIDTH_B            => g_NO_OF_ADDR_BITS
        )
    port map (
        -- Common module ports
        sleep                   => '0',
        -- Port A side
        clka                    => clk_a,  -- clock from the 100GE core; 322 MHz
        rsta                    => '0',
        ena                     => '1',
        regcea                  => '1',

        wea                     => data_a_wr_int,
        addra                   => addr_a,
        dina                    => data_a,
        douta                   => data_a_q,

        -- Port B side
        clkb                    => clk_b,  -- This goes to a dual clock fifo to meet the external interface clock to connect to the HBM at 300 MHz.
        rstb                    => '0',
        enb                     => '1',
        regceb                  => '1',

        web                     => data_b_wr_int,
        addrb                   => addr_b,
        dinb                    => data_b,
        doutb                   => data_b_q,

        -- other features
        injectsbiterra          => '0',
        injectdbiterra          => '0',
        injectsbiterrb          => '0',
        injectdbiterrb          => '0',        
        sbiterra                => open,
        dbiterra                => open,
        sbiterrb                => open,
        dbiterrb                => open
    );


end Behavioral;
