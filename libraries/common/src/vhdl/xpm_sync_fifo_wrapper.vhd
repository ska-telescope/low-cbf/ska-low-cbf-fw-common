----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: Nov 2022
-- Design Name: 
-- Module Name: xpm_sync_fifo_wrapper
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Additional Comments:
-- Wrapper to make this resource appear in heirarchy.
--
-- FUNCTION ceil_log2(n : NATURAL) RETURN NATURAL;
----------------------------------------------------------------------------------


library IEEE, common_lib, xpm;
use IEEE.STD_LOGIC_1164.ALL;
USE common_lib.common_pkg.ALL;
use IEEE.NUMERIC_STD.ALL;
use xpm.vcomponents.all;


entity xpm_sync_fifo_wrapper is
    Generic (
        FIFO_MEMORY_TYPE    : STRING := "auto";
        READ_MODE           : STRING := "fwft";
        FIFO_DEPTH          : INTEGER := 16;
        DATA_WIDTH          : INTEGER := 16
    );
    Port ( 
        fifo_reset          : IN STD_LOGIC;    
        fifo_clk            : IN STD_LOGIC;   
        
        fifo_in_reset       : OUT STD_LOGIC;

        -- RD    
        fifo_rd             : IN STD_LOGIC;
        fifo_q              : OUT STD_LOGIC_VECTOR((DATA_WIDTH-1) downto 0);
        fifo_q_valid        : OUT STD_LOGIC;
        fifo_empty          : OUT STD_LOGIC;
        fifo_rd_count       : OUT STD_LOGIC_VECTOR(ceil_log2(FIFO_DEPTH) downto 0);
        -- WR        
        fifo_wr             : IN STD_LOGIC;
        fifo_data           : IN STD_LOGIC_VECTOR((DATA_WIDTH-1) downto 0);
        fifo_full           : OUT STD_LOGIC;
        fifo_wr_count       : OUT STD_LOGIC_VECTOR(ceil_log2(FIFO_DEPTH) downto 0) 
    );
end xpm_sync_fifo_wrapper;

architecture rtl of xpm_sync_fifo_wrapper is

    signal fifo_in_reset_int : std_logic;

    signal rd_rst_busy : std_logic;
    signal wr_rst_busy : std_logic;
begin

    fifo_in_reset   <= fifo_in_reset_int;
---------------------------------------------------
fifo_in_reset_int   <= wr_rst_busy OR rd_rst_busy;

CDC_fifo : xpm_fifo_sync
    generic map (
        DOUT_RESET_VALUE    => "0",    
        ECC_MODE            => "no_ecc",
        FIFO_MEMORY_TYPE    => FIFO_MEMORY_TYPE, 
        FIFO_READ_LATENCY   => 0,
        FIFO_WRITE_DEPTH    => FIFO_DEPTH,     
        FULL_RESET_VALUE    => 0,      
        PROG_EMPTY_THRESH   => 0,    
        PROG_FULL_THRESH    => 0,     
        RD_DATA_COUNT_WIDTH => ((ceil_log2(FIFO_DEPTH))+1),  
        READ_DATA_WIDTH     => DATA_WIDTH,      
        READ_MODE           => READ_MODE,  
        SIM_ASSERT_CHK      => 0,      
        USE_ADV_FEATURES    => "1404", 
        WAKEUP_TIME         => 0,      
        WRITE_DATA_WIDTH    => DATA_WIDTH,     
        WR_DATA_COUNT_WIDTH => ((ceil_log2(FIFO_DEPTH))+1)   
    )
    port map (
        rst           => fifo_reset, 
        wr_clk        => fifo_clk, 
        rd_rst_busy   => rd_rst_busy,
        wr_rst_busy   => wr_rst_busy,

        --------------------------------
        rd_data_count => fifo_rd_count, 
        rd_en         => fifo_rd,  
        dout          => fifo_q,
        empty         => fifo_empty,

        data_valid    => fifo_q_valid,   
        --------------------------------        
        wr_data_count => fifo_wr_count,
        wr_en         => fifo_wr,
        din           => fifo_data,
        full          => fifo_full,

        almost_empty  => open,  
        almost_full   => open,
        dbiterr       => open, 
        overflow      => open,
        prog_empty    => open, 
        prog_full     => open,

        sbiterr       => open,
        underflow     => open,
        wr_ack        => open,
        sleep         => '0',         
        injectdbiterr => '0',     
        injectsbiterr => '0'        
    );

end rtl;
