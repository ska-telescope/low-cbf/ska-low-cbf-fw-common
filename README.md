# Low CBF Firmware Common
This project provides features that are common to the various Low CBF firmware
projects.

## Documentation
[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-cbf-fw-common/badge/?version=latest)](https://developer.skao.int/projects/ska-low-cbf-fw-common/en/latest/?badge=latest)

The documentation for this project can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-low-cbf-fw-common documentation](https://developer.skatelescope.org/projects/ska-low-cbf-fw-common/en/latest/index.html "SKA Developer Portal: ska-low-cbf-fw-common documentation")

## Project Avatar (Repository Icon)
[Solution icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/solution "solution icons")

# Quick Start

This is the minimum to get you up and running quickly. Refer to other
documentation for further details.

## Prerequisites

The [yq](https://github.com/kislyuk/yq) YAML processor is used in scripts.

* You should be able to install it with `pip3 install yq`. (Do not use the
'snap' version of yq, it's not compatible!)
  * yq uses jq under the hood, install it with `sudo apt install jq`.

For version labelling, the SKA standard `.release` file should be used.
(A text file `version` containing only a version string is supported for
non-SKA projects)

## Integration into other projects

### Git Submodule

For access to the scripts, use a git submodule, in a directory named
`common` in the base directory of your repository.

e.g.
```
git submodule add "../ska-low-cbf-fw-common" common
git add .gitmodules common
git commit
```

### GitLab CI Configuration

Include `gitlab/build.yml` in your CI configuration file `.gitlab-ci.yml`, and
tell GitLab that it needs to clone the submodule when it's running pipelines:

```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive

include:
  # Alveo FPGA build common CI configuration
  # Note: GitLab includes always grab the latest.
  # This may not match the copy in our 'common' submodule!
  - project: 'ska-telescope/low-cbf/ska-low-cbf-fw-common'
    file: 'gitlab/build.yml'
```

You may need to modify other variables depending on your project, for example
a different platform file:

```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  XPFM: "/opt/xilinx/platforms/xilinx_u50lv_gen3x4_xdma_2_202010_1/xilinx_u50lv_gen3x4_xdma_2_202010_1.xpfm"
```
