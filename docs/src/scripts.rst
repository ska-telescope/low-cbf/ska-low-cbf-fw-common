.. _scripts:

#######################
Using The Build Scripts
#######################

The same shell scripts are used both in CI and for local builds.

Running a Local Build
~~~~~~~~~~~~~~~~~~~~~

Run :code:`scripts/build.sh`. This will execute the desired job stages in
sequence. See `build.sh`_ for further detail & command-line options.


Variables
~~~~~~~~~

To enforce consistency between CI & local builds, variables are read from the
GitLab YAML files. First from :code:`gitlab/build.yml` in this project, then
:code:`.gitlab-ci.yml` in the downstream project.

Note that this file lookup relies on the common submodule being at the root
of the other project. (If you followed the quick start, you're good to go).

BASEDIR Note
============

The :code:`BASEDIR` variable is used to provide the absolute path to the base
directory where the build is starting from. As this is not known at CI pipeline
instantiation time, we have to use some tricks to evaluate it later, when the
jobs are running.

So you will see :code:`$${BASEDIR}` in the YAML, escaping the $ so GitLab does
not interpret it. The variable :code:`BASEDIR` is set to the repositry root in
util.sh. Then, at the time of use, :code:`envsubst` is used to substitue the 
current value in.

(Perhaps all this complexity could go away if the TCL scripts / tools were able
to use relative paths instead??)

Optional Variables
==================

By default, any variable defined in build.yml is considered mandatory.
Optional variables are indicated by having the word :code:`EXTRA` in their
name.

That is, if a variable :code:`MY_VAR` is defined in build.yml
it must have a non-empty value, either from our build.yml, or from
.gitlab-ci.yml in the downstream project using this submodule.
A variable such as :code:`MY_EXTRA_VAR` is allowed to remain empty.


Quality Control
~~~~~~~~~~~~~~~

We use shellcheck to enforce good shell scripting practises. You may see some
shellcheck directives in comments to control its behaviour.


##############
Script Details
##############

Generally, each CI job stage runs one script.

Local Build
~~~~~~~~~~~

build.sh
========

Only used for local builds, this loads all variables from the YAML files (both
the common one speicified in this repository and the downstream one if present)
and then runs the requested jobs in sequence. An interactive menu is presented
to allow the choice of running partial builds (e.g. only ARGS, or only up until
the synthesis check), with or without simulation & verification.

Optional arguments specify a build string, build mode, and a 'clean mode' (what
to do if the build directory exists):

:code:`build.sh [BUILD_STRING BUILD_MODE CLEAN_MODE]`

If specifying arguments by command-line, you must specify all three.

Alternately, any or all options can be specified as environment variables (named 
as above). An interactive menu will be presented for any unspecified options.

:code:`BUILD_MODE` choices (use the number):

* 1 - **ARGS** only
* 2 - ARGS + **Create project**
* 3 - ARGS + Create project + **Synthesis (lint check)**
* 4 - ARGS + Create project + Synthesis + **Build (no sim)**
* 5 - ARGS + Create project + Synthesis + **Simulate & Verify (no build)**
* 6 - **Everything!** (Full build, Sim & Verfiy)

:code:`CLEAN_MODE` choices:

* clean - delete the directory
* rename - rename the directory, appending a timestamp
* use - re-use it in place, adding new build products

e.g. :code:`build.sh "my first build" 4 rename`


Utility Scripts
~~~~~~~~~~~~~~~

util.sh
=======

Sets the :code:`BASEDIR` variable, and provides these common utility functions,
used by most (all?) job stages:

* :code:`export_variables` loads the variables section of a YAML file.
* :code:`export_default_variables` loads the common variable definitions from
  this project.
* :code:`substitute_variables` mimics GitLab's behaviour of substituting variable
  names with their contents at the start of pipeline execution.
* :code:`check_all_variables` is used to check that all mandatory variables
  defined in build.yml are set at the start of each script.
* :code:`get_version` gets the version number from either the ".release" 
  or "version" file.
* :code:`read_version_file` parses the "version" file.
* :code:`check_build_dir` is used to either re-use, delete, or rename an existing
  build directory.
* :code:`prepare_package_files` to collate the compilation output for packaging


self-test.sh
============

Verifies the script prerequisites:

* Version number is available
* Required variables are not empty
* yq command installed
* jq command installed

vhdl_set_version.sh
===================

Injects a given semantic version number into VHDL code. Used to ensure the
version reported by the FPGA matches the label of the file uploaded to the
Central Artefact Repository (CAR).

vitis_settings.sh
=================

Loads the Xilinx Vitis/Vivado settings from the configured :code:`XILINX_PATH`
and :code:`VITIS_VERSION`.

args_settings.sh
================

Loads the ARGS setup_radiohdl.sh settings.

Job Steps
~~~~~~~~~

args.sh
=======

Run the Automatic Register Generation System (ARGS) job. This builds a register
map for use in later stages.

create_project.sh
=================

Create the Vivado project by calling a downstream TCL script (defined in
:code:`CREATE_PRJ_SCRIPT_PATH`).

lint_step.sh & synth_check.tcl
==============================

Run synthesis to verify that the project is viable to build.

package_kernel.sh
=================

Your guess is as good as mine.

generate_xo.sh
==============

Your guess is as good as mine.

vpp.sh
======

Run v++, the Vitis compiler. This takes a very long time.

xsim.sh
=======

Run a vivado simulation, using the downstream-provided xsim.tcl. Requires
variables :code:`EXTRA_XSIM_DIR` and :code:`EXTRA_MATLAB_MODEL_DIR`. 

There are long and short simulations available, long simulation set number of 
packets per frame to be 27 and process 486 packets; short simulation set
number of packets per frame to be 6 and process 108 packets only. To run short 
simulation, simply add two arguments to the script, i.e xsim.sh 2 false. To
run long simulation, no arguments are needed. By default, vivado simulation is 
long simulation for gitlab, unless there is merge from a branch to main or 
key word 's_sim' is defined in the commit message, then a short simulation is
triggered. Long simulation takes about 10 hours to finish, short simulation
takes about 3 hours to finish

riviera_sim.sh
==============

Run a vivado simulation, using the downstream-provided riviera.tcl. Requires
variables :code:`EXTRA_XSIM_DIR` and :code:`EXTRA_MATLAB_MODEL_DIR`.

This script is basically a copy of `xsim.sh`_, modified to use the `Riviera-PRO
<https://www.aldec.com/en/products/functional_verification/riviera-pro>`_
simulator.

matlab.sh
=========

Verify the simulation result. Requires :code:`EXTRA_MATLAB_MODEL_DIR`. Same as 
vivado simulation, there are verify long and short simulations available as well. 
To run verify short simulation, simply add one argument to the scrip, i.e
matlab.sh false. To run verify long simulation, then no argument is needed  

package_firmware.sh
===================

Bundles up the output products into an archive for distribution. If running in
a CI pipeline, the archive is uploaded to the GitLab package registry.
