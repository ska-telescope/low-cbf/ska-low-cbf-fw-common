.. vim: syntax=rst

This repository contains components that are common across multiple Low CBF
Firmware personalities, including CI configuration and build scripts.

Low CBF Overview
================

This block diagram represents the Low CBF subsystem. The firmware personalities
are represented by the four rows inside the large red box at top right.

.. image:: images/low-cbf-diagram.png


Hardware Platform
-----------------

Low CBF firmware is intended to run on
`Xilinx Alveo U55C FPGA cards <https://www.xilinx.com/products/boards-and-kits/alveo/u55c.html>`_.

The Alveo cards feature:

- PCIe interface to a host PC
- 100GbE (100 Gigabit Ethernet) interface for signal data flow
- High Bandwidth Memory (HBM).
  The HBM allows station data to be re-ordered for correlation. Station data is
  received as a time-based data stream and stored in the HBM. When a complete
  integration period is held in the HBM it is read out by frequency channel and
  a cross-correlation is performed for each frequency channel.
  Because the HBM is part of the FPGA rather
  than an external DIMM there is no need for power-hungry memory bus interfaces.

Each firmware personality is implemented as an accelerator Kernel using the
Vitis software suite from Xilinx.

Vitis FPGA Accelerator Kernels
------------------------------

Designs compiled within the Vitis framework consist of two main elements:

- An FPGA kernel with standardised interfaces which runs on an Alveo board.
- A software application running on a host processor that controls the FPGA
  kernel over PCI Express.

FPGA Kernels compiled for Vitis have the following interfaces:

- 100GbE. This uses the 100GbE hard core in the FPGA, which is connected on the
  Alveo card to a QSFP.
- AXI-lite slave interface. This is a compulsory AXI-lite slave with a
  standardised set of registers to control the kernel.
- AXI-full interfaces. These interfaces access shared memory. The memory is
  shared with the host processor, in the sense that the host processor can
  access it. The shared memory can be either block ram in the FPGA or HBM.

The requirements for Vitis Accelerator kernels are described in detail in the
Xilinx
`Vitis Unified Software Platform Documentation
<https://www.xilinx.com/support/documentation/sw_manuals/xilinx2020_1/ug1393-vitis-application-acceleration.pdf>`_.

The software application interacts with the FPGA kernel by read and writing
shared memory, and by writing to the AXI-lite slave interface.

Monitor and Control Interface
-----------------------------

The Perentie FPGA kernel takes data in from LFAA via the 100GE interface, and
outputs data to PSS, PST or SDP via the same 100GE interface. The processing
is monitored and controlled via a large number of registers which are defined
within the FPGA kernel. The software application running on the host processor
is responsible for setting and reading these registers.

To allow access to the FPGA kernel's internal register space, the kernel
contains an interface to a shared memory block which it can copy register
contents to/from.

To access the registers in the kernel:

- The host software writes the address of the kernel registers it want to
  read/write
- The host software writes the address of the shared memory to read/write from
- The host software triggers the kernel to execute, i.e. copy data from internal
  registers to shared memory (or from shared memory to internal registers)
- The kernel reports done once the copy is complete, and the host can access any
  register contents in the shared memory pool.

This is illustrated in the diagram below. The Interconnect and signal processing
registers within the kernel are generated using the Automatic Register
Generation System (ARGS), which has been developed jointly by Astron and CSIRO.

.. image:: images/kernel-interface-diagram.png

The `Xilinx XRT driver <https://www.xilinx.com/products/design-tools/vitis/xrt.html>`_
is required to communicate to the Alveo cards. See also
`XRT's documentation <https://xilinx.github.io/XRT/2021.2/html/index.html>`_.

The software library that Low CBF uses to monitor & control the FPGAs is
`ska-low-cbf-fgpa <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fpga>`_.

Firmware Roadmap
================

See the
`Perentie Program Increment Roadmap <https://confluence.skatelescope.org/display/SE/Perentie+Program+Increment+Roadmap>`_
on Confluence.

Building Firmware
=================

The build process is automated, and :ref:`gitlab` is provided.
Refer to :ref:`scripts` for instructions on how to perform a local build.

Behind the scenes, a software tool RadioHDL
(to be moved to the common repository later)
is used to automate building
registers in the firmware design and creating a Project File so that
the Xilinx tools can compile the firmware. The process to
`use RadioHDL to build code <https://confluence.skatelescope.org/display/SE/howto%3A+First-time+build+on+giant.atnf.csiro.au>`_
is described on SKA Confluence.

Vivado and Vitis are licensed software. Some of the IP included in the design is
also licensed software.

