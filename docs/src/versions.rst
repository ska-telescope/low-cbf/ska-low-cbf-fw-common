Version Numbering
=================

After configuring your downstream Makefile as show below, incrementing of 
version numbers can be done via the SKA Makefile commands, e.g. 
:code:`make bump-patch-release`.
Use :code:`make help release` to see the full set of available commands.

Makefile configuration:

.. code-block:: Makefile 

    # Hook into SKA release logic to sync .release label with our VHDL code
    post-set-release:
        common/scripts/vhdl_set_version.sh "$(VERSION)" "boards/alveo/designs/vitisAccelCore/src/vhdl/version_pkg.vhd"

    # Include the required modules from the SKA makefile submodule
    include .make/release.mk
    include .make/raw.mk
    include .make/make.mk
    include .make/help.mk
    include .make/docs.mk


Workflow:

1. Make your changes
2. Commit the changes
3. Run the appropriate 'bump' command, depending on the scope of the changes:
   
   * :code:`make bump-patch-release`
   * :code:`make bump-minor-release`
   * :code:`make bump-major-release`

   You'll see something like:

.. code-block:: shell-session

    $ make bump-patch-release
    set-release: 0.0.20
    common/scripts/vhdl_set_version.sh "0.0.20" "boards/alveo/designs/vitisAccelCore/src/vhdl/version_pkg.vhd"
    Got version 0.0.20
    Modifying file boards/alveo/designs/vitisAccelCore/src/vhdl/version_pkg.vhd
    === Modified VHDL ===
    LIBRARY ieee;
    USE ieee.std_logic_1164.all;

    PACKAGE version_pkg IS
        constant C_FIRMWARE_MAJOR_VERSION        : std_logic_vector(15 downto 0) := x"0000";
        constant C_FIRMWARE_MINOR_VERSION        : std_logic_vector(15 downto 0) := x"0000";
        constant C_FIRMWARE_PATCH_VERSION        : std_logic_vector(15 downto 0) := x"0014";
    end version_pkg;
    === End VHDL ===

4. Commit these changes (you can probably use a command like 
   :code:`git commit -am "JIRA-1234 Increment version"`)
5. Push

Making a Release
----------------

See `Release steps on the SKA Developer Portal <https://developer.skao.int/en/latest/tools/software-package-release-procedure.html#release-steps>`_

