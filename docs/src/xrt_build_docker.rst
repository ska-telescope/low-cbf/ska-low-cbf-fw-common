.. vim: syntax=rst

XRT Build
=========

Generally we use Xilinx's official releases of
`XRT (Xilinx Runtime) <https://github.com/Xilinx/XRT/>`_,
however from time to time it may be useful to build XRT from source. For
example, if there is a bug in the published package that has since been
resolved in the XRT source repository.

As there are a lot of dependencies required for the build that are not
useful in production, we have a containerised build process.

Warnings
--------

- The build script will clone the latest version of the XRT repository
- There's no straightforward way to inject a patch (you could edit xrt_build.sh
  to clone a different repo/branch?)
- Really the dependency installation should go into the Dockerfile to save time
  if you're modifying code and doing multiple builds
- This whole thing is a bit of a hack that hopefully isn't needed again
- There may be an extra step required to pack some firmware from an official
  XRT release into the build result - copy the contents of
  :code:`/opt/xilinx/xrt/share/fw` from an XRT install into the build tree

Prerequisites
-------------
- Docker installed
- You are in the docker user group
- If you need to use a web proxy, set http_proxy and/or https_proxy 
  environment variables

.. code-block:: bash

    cd tools/xrt_build_docker
    mkdir build

Build Container Image
---------------------
If direct web access available:

.. code-block:: bash

    docker build . -t xrt_build

If HTTP proxy is required:

.. code-block:: bash

    docker build . --build-arg https_proxy --build-arg http_proxy -t xrt_build
  
Compile XRT
-----------

If direct web access available:

.. code-block:: bash

    docker run -it --rm --mount type=bind,source="$(pwd)"/build,target=/build xrt_build bash -c ./xrt_build.sh

If HTTP proxy is required:

.. code-block:: bash

    docker run -it --rm --mount type=bind,source="$(pwd)"/build,target=/build -e https_proxy -e http_proxy xrt_build bash -c ./xrt_build.sh
