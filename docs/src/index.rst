.. toctree::
    :maxdepth: 2
    :caption: Contents:

.. toctree::
   :maxdepth: 2
   :caption: Readme

   README.md

.. toctree::
   :maxdepth: 3
   :caption: Low CBF Firmware - Common

   low_cbf_firmware.rst

.. toctree::
   :maxdepth: 2
   :caption: GitLab CI

   gitlab.rst

.. toctree::
   :maxdepth: 2
   :caption: Version Numbering

   versions.rst

.. toctree::
   :maxdepth: 2
   :caption: Scripts

   scripts.rst

.. toctree::
    :maxdepth: 2
    :caption: How to build XRT from source

    xrt_build_docker




More information available on
`SKA Confluence - Perentie Firmware Development <https://confluence.skatelescope.org/display/SE/Perentie+-+Firmware+Development+Home>`_

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
