#!/bin/bash

# Run vivado simulation via downstream TCL script
#
# Arguments:
# 1: passed to TCL script
# 2: passed to TCL script

MY_DIR=$(dirname "${BASH_SOURCE[0]}")
#shellcheck source=util.sh
source "$MY_DIR/util.sh"
# Prepare the environment
# shellcheck source=vitis_settings.sh
source "$MY_DIR/vitis_settings.sh"

export PATH="$PATH:/tools/Aldec/$RIVIERA_TARGET/bin"
# Riviera license is installed in the .profile for gitlab-runner.

#check_all_variables

if [ -z "$EXTRA_XSIM_DIR" ]; then
    echo "Error: EXTRA_XSIM_DIR variable is required for Riviera job"
    exit 2
fi

if [[ -z "$EXTRA_MATLAB_MODEL_DIR" ]]; then
    2>&1 echo "Error: EXTRA_MATLAB_MODEL_DIR variable is required for Riviera job"
    exit 3
fi

echo "Running Simulation in Riviera"

XSIM_DIR="$EXTRA_XSIM_DIR"
PROJ_DIR=$(find_proj_build_dir)
echo "Vivado project directory: $PROJ_DIR"
cd "$PROJ_DIR" || exit 2

# STACK_ARG deliberately left unquoted
# shellcheck disable=SC2086
vivado "./$PERSONALITY.xpr" $STACK_ARG -mode batch \
  -source "$(envsubst <<< "$XSIM_DIR")/riviera.tcl" -tclargs "$1" "$2"

# prep files for packaging at a later step.
cd $CI_PROJECT_DIR
mkdir build/sim_output
cp build/tb_result.txt build/sim_output