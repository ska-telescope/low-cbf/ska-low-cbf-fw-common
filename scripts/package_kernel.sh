#!/bin/bash

# Package Kernel

MY_DIR=$(dirname "${BASH_SOURCE[0]}")
# shellcheck source=util.sh
source "$MY_DIR/util.sh"
# Prepare the environment
# shellcheck source=vitis_settings.sh
source "$MY_DIR/vitis_settings.sh"

check_all_variables

echo "Vivado build base directory $VIVADO_BUILD_BASE_DIR"
PROJ_DIR=$(find_proj_build_dir)
echo "Found project directory: $PROJ_DIR"
cd "$PROJ_DIR" || exit 2

# Note the unquoted STACK_ARG variable is because we want it to expand.
# Using an aray would be better, but I'm not sure how that will go with
# GitLab YAML.
# shellcheck disable=SC2086
vivado "./$PERSONALITY.xpr" $STACK_ARG -mode batch \
  -source "$(envsubst <<< "$PACKAGE_KRNL_SCRIPT_PATH")"
