#!/bin/bash
# Package simulation output files into an archive
# 
# Other scripts will deal with simulation but this one assumes that the output
# is copied to /build/sim_output/ at some previous CI step and has been artifacted.

BUILD_STRING="${1:-$BUILD_STRING}"

MY_DIR=$(dirname "${BASH_SOURCE[0]}")
# shellcheck source=util.sh
source "$MY_DIR/util.sh"

VERSION=$(get_version "$MY_DIR/../..")
echo "Using version: $VERSION"

SIM_DIR="build/sim_output"

if [[ -z "$PERSONALITY" ]]; then
    2>&1 echo "Error: PERSONALITY variable is not set"
    exit 4
fi

# Note: MODE logic must be the same as build_details.sh

MODE="manual"  # Are we doing manual or CI (dev/release) build?
# (Note: manual build & upload is only used in CPAF)

if [[ "$CI" == "true" ]]; then
    if [[ "$CI_JOB_NAME" =~ "pretest" ]]; then
        MODE="pretest"  # CPAF only - for differentiating untested packages
    elif [[ -n "$CI_COMMIT_TAG" ]]; then
        # CI pipeline running for a tag
        echo "Tag detected: ${CI_COMMIT_TAG}"
        MODE="release"
    elif [[ "$CI_JOB_NAME" =~ "release" ]]; then
        echo "DEPRECATION WARNING - the keyword 'release' to control behaviour appears to be unused and will go away soon." >&2
        MODE="release"
    elif [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" && "$MAIN_BUILD_TAG" == "TRUE" ]]; then
        MODE="main"     # Main branch builds.
    else
        MODE="dev"
    fi
fi

echo "Packaging sim output"

# get platform is simulation not an ALVEO in this instance
PLATFORM="sim"
echo "Platform information : $PLATFORM"

if [[ "$MODE" == "release" ]]; then
    PACKAGE_VERSION="${VERSION}"
    PACKAGE_FILENAME="${PERSONALITY}_${PLATFORM}_${PACKAGE_VERSION}.tar.xz"
elif [[ "$MODE" == "main" ]]; then
    PACKAGE_VERSION="${VERSION}-main.${CI_COMMIT_SHORT_SHA}"
    PACKAGE_FILENAME="${PERSONALITY}_${PLATFORM}_${PACKAGE_VERSION}+vitis.${VITIS_VERSION}.tar.xz"    
elif [[ "$MODE" == "dev" ]]; then
    PACKAGE_VERSION="${VERSION}-dev.${CI_COMMIT_SHORT_SHA}"
    PACKAGE_FILENAME="${PERSONALITY}_${PLATFORM}_${PACKAGE_VERSION}+vitis.${VITIS_VERSION}.tar.xz"
elif [[ "$MODE" == "pretest" ]]; then
    PACKAGE_VERSION="${VERSION}-dev.pretest.${CI_COMMIT_SHORT_SHA}"
    PACKAGE_FILENAME="${PERSONALITY}_${PLATFORM}_${PACKAGE_VERSION}+vitis.${VITIS_VERSION}.tar.xz"
else # manual
    echo "DEPRECATION WARNING - manual upload option is likely to disappear soon" >&2
    PACKAGE_VERSION="${VERSION}-dev.manual.$(date +%Y%m%d_%H%M)"
    PACKAGE_FILENAME="${PERSONALITY}_${PLATFORM}_${PACKAGE_VERSION}+vitis.${VITIS_VERSION}.tar.xz"
fi

echo "Creating archive: $PACKAGE_FILENAME"

tar -cJvf "build/$PACKAGE_FILENAME" -C $SIM_DIR .

# Upload if running in CI
if [[ "$CI" == "true" ]]; then
    UPLOAD_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PERSONALITY}/${PACKAGE_VERSION}/${PACKAGE_FILENAME}"
    curl --header "JOB-TOKEN: $CI_JOB_TOKEN" \
      --upload-file "build/${PACKAGE_FILENAME}" \
      "$UPLOAD_URL"
else
    UPLOAD_URL="${GITLAB_API_V4_URL}/projects/${GITLAB_PROJECT_ID}/packages/generic/${PERSONALITY}/${PACKAGE_VERSION}/${PACKAGE_FILENAME}"
    if [[ -z "$TOKEN" ]]; then
        echo "TOKEN environment variable is unset, you can upload the result by running:"
        cat << EOF
        curl --header "PRIVATE-TOKEN: \${TOKEN}" \\
            --upload-file "build/${PACKAGE_FILENAME}" \\
            "$UPLOAD_URL"
EOF
    else
        # echo command for debugging / in case manual retry is req'd
        set -x
        curl --header "PRIVATE-TOKEN: ${TOKEN}" \
            --upload-file "build/${PACKAGE_FILENAME}" \
            "$UPLOAD_URL"
        set +x
    fi
fi

