#!/bin/bash
#
# Modify version settings inside VHDL source file
#
# Arguments:
# 1: Version string ( <number>.<number>.<number>[-ignored][+extra_strings] )
# 2: Path to file

# Insert numbers (as hex) into lines such as this:
# constant C_FIRMWARE_MAJOR_VERSION        : std_logic_vector(15 downto 0) := x"12ab";

# exit on error
set -e

if [[ -z "$1" || -z "$2" ]]; then
    2>&1 echo "Error: Missing argument(s). Use: $0 <version string> <VHDL file path>"
    exit 1
fi

echo "Inserting version $1 into $2"

# semantic version looks like <digits>.<digits>.<digits>[-anything][+anything]
# (we don't care about the extra 'anythings' after plus or minus so we just grab the digits
regex='^([[:digit:]]+)\.([[:digit:]]+)\.([[:digit:]]+).*?$'
[[ $1 =~ $regex ]]
VERSION_MAJOR=${BASH_REMATCH[1]}
VERSION_MINOR=${BASH_REMATCH[2]}
VERSION_PATCH=${BASH_REMATCH[3]}

VERSION_MAJOR_HEX=$(printf "x\"%04x\"" "$VERSION_MAJOR")
VERSION_MINOR_HEX=$(printf "x\"%04x\"" "$VERSION_MINOR")
VERSION_PATCH_HEX=$(printf "x\"%04x\"" "$VERSION_PATCH")

sed -i -E "s/(.*C_FIRMWARE_MAJOR_VERSION.*)(x\"[[:xdigit:]]*\")(;)/\1$VERSION_MAJOR_HEX\3/" "$2"
sed -i -E "s/(.*C_FIRMWARE_MINOR_VERSION.*)(x\"[[:xdigit:]]*\")(;)/\1$VERSION_MINOR_HEX\3/" "$2"
sed -i -E "s/(.*C_FIRMWARE_PATCH_VERSION.*)(x\"[[:xdigit:]]*\")(;)/\1$VERSION_PATCH_HEX\3/" "$2"

echo "=== Modified VHDL ==="
cat "$2"
echo "=== End VHDL ==="
