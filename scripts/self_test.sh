#!/bin/bash

# Check some pre-requisites before we run

# exit on failure
set -e

MY_DIR=$(dirname "${BASH_SOURCE[0]}")
# shellcheck source=util.sh
source "$MY_DIR"/util.sh


# directory logic assumes this file is in
# <parent project>/common/scripts
# and version file is in <project root>
if get_version "$MY_DIR"/../..; then
    VERSION=$(get_version "$MY_DIR"/../..)
    echo "Found version $VERSION"
else
    echo "No version file"
    exit 1
fi

if ! check_all_variables; then
    echo "Exiting due to problem with variables"
    exit 128
fi

ERRORS=0

# check that the correct yq command is available
if yq -r -h &> /dev/null; then
   echo yq command OK
else
   echo "Missing/wrong yq. Install with 'pip3 install yq'."
   ERRORS=$((ERRORS+1))
fi

# and the jq command that yq uses under the hood
if jq -h &> /dev/null; then
   echo jq command OK
else
   echo "Missing/wrong jq. Install with 'sudo apt install jq'."
   ERRORS=$((ERRORS+1))
fi

echo "self test errors $ERRORS"
exit $ERRORS
