#!/bin/bash
# call this with 'source' so the variables are exported

MY_DIR=$(dirname "${BASH_SOURCE[0]}")
# shellcheck source=util.sh
source "$MY_DIR/util.sh"

# assume MY_DIR is <project root>/common/scripts
# SVN needs to be the path of project root for use in setup_radiohdl script
SVN=$(readlink -f "$MY_DIR/../..")
export SVN
# shellcheck source=/dev/null
source "$SVN/tools/bin/setup_radiohdl.sh"
