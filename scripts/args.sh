#!/bin/bash

# Run ARGS to generate register map

MY_DIR=$(dirname "${BASH_SOURCE[0]}")
# shellcheck source=util.sh
source "$MY_DIR/util.sh"
# Prepare the environment
# shellcheck source=args_settings.sh
source "$MY_DIR/args_settings.sh"

# If people want to run just ARGS, this check could be relaxed,
# ARGS only needs $PERSONALITY...
check_all_variables

ARGS_DIR="$SVN/build/ARGS"
if [ -d "$ARGS_DIR" ]; then
    NEW_ARGS_DIR="${ARGS_DIR}_old_$(date +%Y%m%d_%H%M%S)"
    echo "ARGS directory exists, moving it to ${NEW_ARGS_DIR}"
    mv "${ARGS_DIR}" "${NEW_ARGS_DIR}"
fi

python3 "$SVN/tools/radiohdl/base/vivado_config.py" -l "$PERSONALITY" -a

MAP_COUNT=$(find "$ARGS_DIR/py/${PERSONALITY}" -name "fpgamap*.py" | wc -l)
# Check that exactly one map was generated
if [ "$MAP_COUNT" -ne 1 ]; then
    echo "Error: Wrong number of FPGA map files generated, should be one.";
    ls "$ARGS_DIR/py/${PERSONALITY}" ;
    exit 1 ;
fi
