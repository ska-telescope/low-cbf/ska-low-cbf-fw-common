#!/bin/bash

# exit on error
set -e

# we assume the repo base directory is two levels up from our location
# e.g. <repo base>/common/scripts/util.sh
BASEDIR=$(readlink -f "$(dirname "${BASH_SOURCE[0]}")/../..")
export BASEDIR

MY_BUILD_DIR=build

# Note: GitLab uses $$ to escape $ , and we use this to evaluate
# BASEDIR at runtime with envsubst, rather than letting GitLab substitute
# nothing at CI pipeline launch.
#
# So we need to mirror this behaviour.
# A multi stage process is used:
# export_variables:
#  - load the yaml, read the "variables" section, export 'key=value' pairs
#  - sed to swap $$ with ^^^ (meaningless to bash and unlikely to appear)
#  - set environment variables with initial values
# substitute_variables:
#  - perform first-pass substitution
#  - swap ^^^ back to $ for evaulation in later scripts
#
# is this the best way? I dunno.
# Maybe using a bash file as the source of truth would be easier?

export_variables () {
    # Load variables specified in the gitlab CI YAML file, if they are not
    # already set in the environment.
    # Arguments:
    # 1. Path to YAML file

    local YQ_CMD
    local all_yaml_variables
    local yaml_variables
    # query string to load variable names & values from yaml
    # will output as "name=value", one per line
    YQ_CMD='.variables | to_entries | .[] | .key + "=" + (.value | @sh)'
    # run query, convert $$ -> ^^^
    all_yaml_variables="$(yq "$YQ_CMD" "$1" -r | sed 's/\$\$/^^^/g')"

    # check if each variable is set in user environment
    readarray -t yaml_variables <<< $(echo -e "$all_yaml_variables")
    for name_value in "${yaml_variables[@]}"; do
        name=${name_value%=*}  # extract part before "="
        value=${name_value##*=}  # extract part after "="
        if [[ -v $name ]]; then
            # variable set in env, don't replace it
            echo "$(realpath -Ls --relative-to=$(pwd) $1)" overriden by setting: $name=${!name} 1>&2
        else
            # variable not in env, export value found in YAML
            eval "export $name=$value"
        fi
    done
}

substitute_variables () {
    # get list of variables from common/gitlab/build.yaml
    local YAML_PATH
    YAML_PATH="$(dirname "${BASH_SOURCE[0]}")/../gitlab/build.yml"
    local variable_list
    variable_list="$(yq '.variables | keys | .[]' "$YAML_PATH" -r)"
    for var in $variable_list; do
        # perform first stage variable substitution,
        # then convert ^^^ -> $
        export "$var"="$(envsubst <<< "${!var}" | sed 's/\^\^\^/\$/g')"
    done
}

export_default_variables () {
    # export the varaiables defined in common GitLab configuration
    export_variables "$(dirname "${BASH_SOURCE[0]}")/../gitlab/build.yml"
}

check_all_variables () {
    # Load names of variables from GitLab CI YAML
    # Make sure they are set to something
    # (allow variables with EXTRA in name to be unset)

    local YAML_PATH
    YAML_PATH="$(dirname "${BASH_SOURCE[0]}")/../gitlab/build.yml"
    local REQUIRED_VARIABLES
    REQUIRED_VARIABLES="$(yq '.variables | keys | .[]' "$YAML_PATH" -r)"
    local ERRORS=0
    for VAR in $REQUIRED_VARIABLES; do
        # if "EXTRA" is in name we don't care if it's empty
        # i.e. we only check variables without "EXTRA" in the name
        if [[ ! $VAR =~ "EXTRA" ]]; then
            if [[ -z "${!VAR}" ]]; then
                # variable is unset or empty string
                2>&1 echo "Required variable ${VAR} is empty"
                ERRORS=$((ERRORS+1))
            fi
        fi
    done

    return $ERRORS
}

get_version () {
    # Prints version using Makefile if .release file exists (SKA projects)
    # or version file otherwise.
    #
    # Arguments:
    # 1. (optional) directory to look in (assumes ".")

    local CHECK_DIR
    CHECK_DIR=${1:-.}

    if [[ -f "${CHECK_DIR}/.release" ]]; then
        cd "${CHECK_DIR}" || return 2
        VERSION=$(make show-version)
    elif [[ -f "${CHECK_DIR}/version" ]]; then
        VERSION=$(read_version_file "${CHECK_DIR}")
    else
        2>&1 echo "Error: Can't find version!"
        return 1
    fi

    echo "$VERSION"
    return 0
}

read_version_file () {
    # version file must contain only a version string
    # e.g. "1.2.3"
    #
    # Arguments:
    # 1. (optional) directory to look in (assumes ".")
    local CHECK_DIR
    CHECK_DIR=${1:-.}

    local VERSION_FILE
    VERSION_FILE="${CHECK_DIR}/version"
    # ensure file exists
    if [[ ! -f ${VERSION_FILE} ]]; then
        2>&1 echo "Warning: Version file ${VERSION_FILE} does not exist, creating it with '0.0.0'"
        echo "0.0.0" > "${VERSION_FILE}"
    fi
    VERSION=$(cat "${VERSION_FILE}")
    # check format looks like <digits>.<digits>.<digits>[-anything]
    regex='^([0-9]+\.){2}([0-9]+)(-.*)?$'
    if [[ $VERSION =~ $regex ]]; then
        echo "$VERSION"
        return 0
    else
        2>&1 echo "Version \"$VERSION\" no good. Should be 3 dot-delimited numbers, e.g. '12.34.567'"
        return 1
    fi
}

check_build_dir () {
    # Arguments:
    # 1. (Optional) what to do if build dir exists
    #    "clean" - delete
    #    "rename" (default) - rename, append timestamp
    #    "use" - reuse it
    CLEAN_MODE=${1:-rename}
    ERRORS=0
    if [ -d $MY_BUILD_DIR ]; then
        echo "Build directory exists"
        if [[ "$CLEAN_MODE" == "rename" ]]; then
            NEW_BUILD_DIR="${MY_BUILD_DIR}_old_$(date +%Y%m%d_%H%M%S)"
            echo "Renaming $MY_BUILD_DIR to $NEW_BUILD_DIR"
            mv "$MY_BUILD_DIR" "$NEW_BUILD_DIR" || ERRORS=$((ERRORS+1))
        elif [[ "$CLEAN_MODE" == "clean" ]]; then
            echo "Deleting $MY_BUILD_DIR"
            rm -rf "$MY_BUILD_DIR" || ERRORS=$((ERRORS+1))
        elif [[ "$CLEAN_MODE" == "use" ]]; then
            echo "Re-using $MY_BUILD_DIR, let's hope it works..."
        else
            echo "check_build_dir error: unknown clean mode $CLEAN_MODE"
            ERRORS=$((ERRORS+1))
        fi
    fi
    return $ERRORS
}

find_newest_dir () {
    # Arguments:
    # 1. Directory to look in
    # 2. Pattern to match
    if [[ -z "$1" ]]; then
        2>&1 echo "find_newest_dir needs a directory to search in"
        return 1
    fi
    if [[ -z "$2" ]]; then
        2>&1 echo "find_newest_dir needs a search pattern"
        return 2
    fi

    # find, under search path, directories matching the pattern,
    #  then print their modification timestamp and path
    # sort (by timestamp) in reverse order (largest timestamp = newest = first)
    # head: take the first 
    # cut: remove the timestamp (space delimited, take fields 2 onward)
    find "$1" -type d -name "$2" -printf "%T@ %p\n" | sort -k1 -r | head -n 1 | cut -d ' ' -f 2-
}

find_proj_build_dir () {
    find_newest_dir "${VIVADO_BUILD_BASE_DIR}/${PERSONALITY}" "${PERSONALITY}_${TARGET_ALVEO}_build*"
}

prepare_package_files () {
    # collate files that we want to package up
    # Arguments:
    # 1. Destination directory
    # 2. Build string

    echo "$2" >> "$1"/build_string.txt
    cp "build/${PERSONALITY}".* "$1"
    cp "build/ARGS/${PERSONALITY}/${PERSONALITY}.ccfg" "$1"
    cp "build/ARGS/py/${PERSONALITY}"/fpgamap*.py "$1"
    mkdir "$1"/reports/
    cp build/reports/*.* "$1"/reports/
    # Check that there's only one map file
    # (should be impossible as we check in the ARGS job)
    if [ "$(find "$1" -name "fpgamap*.py" | wc -l)" -ne 1 ]; then
        echo "Error: Wrong number of FPGA map files, should be exactly one.";
        ls "$1"/fpgamap*.py ;
        return 1 ;
    fi
}

trim_xpfm () {
    # Shorten an XPFM path into a platform version used to label packages,
    # containing only the portions that are important for compatibility.
    # e.g. /opt/xilinx/platforms/xilinx_u55c_gen3x16_xdma_2_202110_1/xilinx_u55c_gen3x16_xdma_2_202110_1.xpfm
    # -> u55c_gen3x16_xdma_2
    #
    # Arguments:
    # 1. full XPFM filename (can include full path)
    local PLATFORM
    PLATFORM=$(basename --suffix=.xpfm "$1" | sed 's/xilinx[_]*//' | sed 's/\(.*dma_[[:digit:]]*\).*/\1/')
    echo "${PLATFORM}"
    return 0
}
