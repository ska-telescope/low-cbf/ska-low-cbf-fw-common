#!/bin/bash

# Create Project

MY_DIR=$(dirname "${BASH_SOURCE[0]}")
# shellcheck source=util.sh
source "$MY_DIR/util.sh"
# Prepare the environment
# shellcheck source=args_settings.sh
source "$MY_DIR/args_settings.sh"
# shellcheck source=vitis_settings.sh
source "$MY_DIR/vitis_settings.sh"

check_all_variables

#########################################
# sim files generate, assumes a sim dir with bash script to generate stim files from MATLAB

if [[ -z "$EXTRA_MATLAB_MODEL_DIR" ]]; then
    2>&1 echo "No MATLAB dir specified, not going to SIM"
else
    ./sim/gen_sims_matlab.sh
fi

#########################################

BUILD_DIR="./build/$PERSONALITY"
mkdir -p "$BUILD_DIR"
cd "$BUILD_DIR" || exit 2
if [ "$TARGET_ALVEO" == "u50" ]; then
    EXTRA_VIVADO_ARGS="-tclargs $PERSONALITY"
else
    EXTRA_VIVADO_ARGS=""
fi


#########################################

# Generate build constants file for project
# this is based on CI flow and Commit Hash
cd ..
# now at /build, need to go up one then down to access the generation script
../"$MY_DIR"/build_details.sh

#########################################

# Note the unquoted variables are because we want them to expand.
# Using an aray would be better, but I'm not sure how that will go with
# GitLab YAML.
# shellcheck disable=SC2086
vivado $STACK_ARG -mode batch \
    -source "$(envsubst <<< "${CREATE_PRJ_SCRIPT_PATH}")" \
    $EXTRA_VIVADO_ARGS
