#!/bin/bash

# Run v++

MY_DIR=$(dirname "${BASH_SOURCE[0]}")
# shellcheck source=util.sh
source "$MY_DIR/util.sh"
# Prepare the environment
# shellcheck source=vitis_settings.sh
source "$MY_DIR/vitis_settings.sh"
# shellcheck source=/dev/null  # part of XRT, not available in this repo
source /opt/xilinx/xrt/setup.sh

check_all_variables

# directories are specified relative to project root, but we are now changing
# into ./build, so we need to do some modification.
# This regex grabs everything AFTER "build/"
[[ "$(find_proj_build_dir)/$PERSONALITY.xo" =~ .*build/(.*) ]]
XO_PATH=${BASH_REMATCH[1]}

echo "XO path (relative to build dir): $XO_PATH"
cd build

# Note the unquoted EXTRA_VPP_ARGS envsubst is because we want it to expand.
# Using an aray would be better, but I'm not sure how that will go with
# GitLab YAML.
# shellcheck disable=SC2086
# shellcheck disable=SC2046
v++ --optimize 0 --report_level 2 --save-temps \
    --config "../${VPP_SCRIPT_DIR}/connectivity.ini" \
    -l -t hw -o "./$PERSONALITY.xclbin" \
    -f "$XPFM" $(envsubst <<< "$EXTRA_VPP_ARGS") \
    "$XO_PATH"

# u50 base 3    - clk_kernel_in = 300
# u50 base 5    - aclk_kernel_00 = 300
# u50lv         - clk_kernel = 300
# u55 base 3    - aclk_kernel_00 = 300
# u280          - aclk_kernel_00 = 300

# only execute this check on SKA builds

if [[ $PERSONALITY = "cnic" ]] || [[ $PERSONALITY = "correlator" ]] || [[ $PERSONALITY = "pst" ]] || [[ $PERSONALITY = "pss" ]]; then
    File=v++_$PERSONALITY.log

    if [ $TARGET_ALVEO = "u280" ]; then
        if ! grep -q "aclk_kernel_00 = 300" "$File"; then
            echo "BANG BOOM"
            exit 1
        else
            echo "U280 kernel met timing"
        fi
    fi

    if [ $TARGET_ALVEO = "u55c" ]; then
        if ! grep -q "aclk_kernel_00 = 300" "$File"; then
            echo "BANG BOOM"
            exit 1
        else
            echo "U55C kernel met timing"
        fi
    fi

    # Test U50 by XPFM since TARGET_ALVEO is reused
    if grep -q "u50_gen3x16_xdma_5" "$XPFM"; then
        if ! grep -q "aclk_kernel_00 = 300" "$File"; then
            echo "BANG BOOM"
            exit 1
        else
            echo "U50 base 5 Kernel met timing"
        fi

    fi

fi