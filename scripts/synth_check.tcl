## Synth check for commit
## source /tools/Xilinx/Vitis/2021.2/settings64.sh
## vivado -mode batch -source <your_Tcl_script>


open_project $env(PERSONALITY).xpr

update_compile_order -fileset sources_1

launch_runs synth_1 -jobs 16
wait_on_run synth_1