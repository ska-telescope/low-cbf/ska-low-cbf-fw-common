#!/bin/bash

# call this with 'source' so the variables are exported

MY_DIR=$(dirname "${BASH_SOURCE[0]}")
# shellcheck source=util.sh
source "$MY_DIR/util.sh"

if check_all_variables; then
    # shellcheck source=/dev/null  # part of vitis, not available in this repo
    source "${XILINX_PATH}/Vitis/${VITIS_VERSION}/settings64.sh"
else
    echo 2>&1 "Error: problem with environment variables"

    exit 1
fi

