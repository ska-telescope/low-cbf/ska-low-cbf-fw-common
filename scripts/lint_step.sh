#!/bin/bash

# Validate project by synthesizing

MY_DIR=$(dirname "${BASH_SOURCE[0]}")
# shellcheck source=util.sh
source "$MY_DIR/util.sh"

# shellcheck source=vitis_settings.sh
source "$MY_DIR/vitis_settings.sh"

check_all_variables

PATH_TO_SYNTH_CHECK=$(readlink -f "$MY_DIR/synth_check.tcl")

PROJ_DIR=$(find_proj_build_dir)
echo "Vivado project directory: $PROJ_DIR"
cd "$PROJ_DIR" || exit 2

# run synth step
vivado -mode batch -source "$PATH_TO_SYNTH_CHECK"

# check the output log for error
echo Check log
if grep "ERROR:" "vivado.log"; then
    echo Synthesis Failed
    exit 1
fi
