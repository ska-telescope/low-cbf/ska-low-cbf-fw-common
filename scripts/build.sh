#!/bin/bash
# Perform a manual build, with steps selected by user
#
# Arguments:
# see read_build_settings

read_build_settings () {
    # Read the user's settings from command-line, env, or interactive menu
    # Arguments:
    # * Note: supply all arguments or none.
    #   (set environment variables if you want to specify only some)
    # 1. build string (free text that goes to a text file in the package)
    # 2. build mode 1-6, see select_build_mode for values
    # 3. clean mode, see select_clean_mode for options
    if [[ $# -eq 3 ]]; then
        BUILD_STRING="$1"
        BUILD_MODE="$2"
        CLEAN_MODE="$3"
    elif [[ $# -eq 0 ]]; then
        if [[ -z "$BUILD_MODE" ]]; then
            BUILD_MODE=$(select_build_mode)
        fi
        # build string only used at the packaging stage (after build)
        if [[ ("$BUILD_MODE" -eq 4 || "$BUILD_MODE" -eq 6) && -z "$BUILD_STRING" ]]; then
            BUILD_STRING=$(select_build_string)
        fi
        if [[ -z "$CLEAN_MODE" && -d "$MY_BUILD_DIR" ]]; then
            CLEAN_MODE=$(select_clean_mode)
        fi
    else
        2>&1 echo "Error: specify all options as command-line args or none"
        2>&1 echo "Usage: $0 [BUILD_STRING BUILD_MODE CLEAN_MODE]"
        2>&1 echo "(or you can use env vars)"
        2>&1 echo "see interactive menu for BUILD_MODE values (use the number)"
        exit 1
    fi

    echo "Build string: ${BUILD_STRING}"
    echo "Build mode: ${BUILD_MODE}"
    echo "Clean mode: ${CLEAN_MODE}"
}

select_build_mode () {
    SELECTION=$(whiptail --title "ska-low-cbf-fw-common FPGA Build" \
        --menu "Select build mode" 16 40 6 \
        --default-item 4 \
        "1" "ARGS" \
        "2" "Create project" \
        "3" "Synthesis (lint check)" \
        "4" "Build (no sim)" \
        "5" "Simulate & Verify (no build)" \
        "6" "Everything! (build & sim)" \
        3>&1 1>&2 2>&3
    )
    if [[ -z "$SELECTION" ]]; then
        exit 255
    fi
    echo "$SELECTION"
}

select_build_string () {
    TIME_STR=$(date "+%Y-%m-%d %H:%M:%S")
    SELECTION=$(whiptail --title "ska-low-cbf-fw-common FPGA Build" \
        --inputbox text 16 40 "manual build $TIME_STR" \
        3>&1 1>&2 2>&3
    )
    if [[ -z "$SELECTION" ]]; then
        exit 255
    fi
    echo "$SELECTION"
}

select_clean_mode () {
    SELECTION=$(whiptail --title "ska-low-cbf-fw-common FPGA Build" \
        --menu "Build dir exists:" 16 40 6 \
        --default-item use \
        "use" "Use it" \
        "rename" "Rename with a timestamp" \
        "clean" "Delete it" \
        3>&1 1>&2 2>&3
    )
    if [[ -z "$SELECTION" ]]; then
        exit 255
    fi
    echo "$SELECTION"
}

stage_echo () {
    # echo the stage name in some prominent way
    # Arguments:
    # 1. string to display
    echo -e "\n\e[0;94m*** $1 ***\e[0m\n"
}

# exit on failure
set -e

MY_DIR=$(dirname "${BASH_SOURCE[0]}")
# shellcheck source=util.sh
source "$MY_DIR/util.sh"

# Load project-specific settings
# assume we are in a submodle
# i.e. <FPGA project root>/common/scripts
PROJECT_CI_CFG="$MY_DIR/../../.gitlab-ci.yml"
if [ -f "$PROJECT_CI_CFG" ]; then
    echo "Loading variables from project GitLab CI config: $(realpath -Ls --relative-to=$(pwd) $PROJECT_CI_CFG)"
    export_variables "$PROJECT_CI_CFG"
fi

# load default (common) values (note: skips variables already set)
export_default_variables

# perform variable substitution as GitLab would at beginning of CI job
substitute_variables

# self-test (check pre-requisites)
"$MY_DIR"/self_test.sh

# Get user settings
read_build_settings "$@"

# deal with any old build directory as requested
check_build_dir "$CLEAN_MODE"

# Run the appropriate job steps
stage_echo "ARGS"
"$MY_DIR"/args.sh
if [[ $BUILD_MODE -lt 2 ]]; then
    exit 0
fi

stage_echo "Create Project"
"$MY_DIR"/create_project.sh
if [[ $BUILD_MODE -lt 3 ]]; then
    exit 0
fi

stage_echo "Hardware Lint"
"$MY_DIR"/lint_step.sh
if [[ $BUILD_MODE -lt 4 ]]; then
    exit 0
fi

if [[ $BUILD_MODE -ge 5 ]]; then
    stage_echo "Run Simulation"
    "$MY_DIR"/xsim.sh
    stage_echo "Run Verification"
    "$MY_DIR"/matlab.sh
fi
if [[ $BUILD_MODE -eq 5 ]]; then
    exit 0
fi

stage_echo "Package Kernel"
"$MY_DIR"/package_kernel.sh
stage_echo "Generate XO"
"$MY_DIR"/generate_xo.sh
stage_echo "Run V++"
"$MY_DIR"/vpp.sh
stage_echo "Package Firmware"
"$MY_DIR"/package_firmware.sh

