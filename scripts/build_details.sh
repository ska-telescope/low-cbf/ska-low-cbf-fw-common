#!/bin/bash
#  Distributed under the terms of the CSIRO Open Source Software Licence Agreement
#  See the file LICENSE for more info.

## This script creates build_details_pkg.vhd
## The constants are to be connected to registers for software to read
## to provide the FPGA with the build source code information.
## These will match GITLAB commits and package types.

CURR_GIT_SHA_SHORT=${CI_COMMIT_SHORT_SHA}
if [[ -z "$CURR_GIT_SHA_SHORT" ]]; then
    CURR_GIT_SHA_SHORT=$(git rev-parse --short=8 HEAD)
fi
BUILD_TYPE="44444444" # init value if run on dev box - 'DDDD'

# Note: this branch logic must be the same as package_firmware.sh
if [[ "$CI" == "true" ]]; then
    if [[ "$CI_JOB_NAME" =~ "pretest" ]]; then
        BUILD_TYPE="20505245"       # ' PRE'
    elif [[ -n "$CI_COMMIT_TAG" ]]; then
        # CI pipeline running for a tag
        echo "Tag detected: ${CI_COMMIT_TAG}"
        BUILD_TYPE="2052454C"       # ' REL'
    elif [[ "$CI_JOB_NAME" =~ "release" ]]; then
        echo "DEPRECATION WARNING - the keyword 'release' to control behaviour appears to be unused and will go away soon." >&2
        BUILD_TYPE="2052454C"       # ' REL'
    elif [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" && "$MAIN_BUILD_TAG" == "TRUE" ]]; then
        BUILD_TYPE="4D41494E"       # 'MAIN'
    else
        BUILD_TYPE="20444556"       # ' DEV'
    fi
fi

echo "LIBRARY ieee;
USE ieee.std_logic_1164.all;

PACKAGE build_details_pkg IS
    constant C_SHA_SHORT        : std_logic_vector(31 downto 0) := x\"$CURR_GIT_SHA_SHORT\";
    constant C_BUILD_TYPE       : std_logic_vector(31 downto 0) := x\"$BUILD_TYPE\";
end build_details_pkg;" > build_details_pkg.vhd
